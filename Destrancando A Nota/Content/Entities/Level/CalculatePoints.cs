﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonRun.MinhasClasses
{
    class CalculatePoints
    {

        /*
        public static int points { get; set; }
        private static int qtdtotalSala = 7;
        private static int minLife = 5;
        public static int levelRoomToCompletedPoints = 50;
        public static int completedInTimePoints = 25;

        public static int minLivePoints = 25;
        private static int modalLivePoints = 5; // pontos para cada vida extra 

        private static double maxTimeSecs = 300;
        private static double modalTime = 10;

        public static int extraPointsMax = 25;
       // private static int extrapoints;

        private static CalculatePoints instance;

        private CalculatePoints()
        {
            
            
        }

        public static CalculatePoints Instance()
        {
            if (instance == null)
            {
                instance = new CalculatePoints();
            }
            return instance;
        }

        public static int calculateTotalPoints(int completedNivel, int qtdLife , double sec, int monsterKilled)
        {
            int points = 0;
            points += toLevelCompleted(completedNivel);
            points += completedWithMinLive(qtdLife);
            points += completedInTime(sec, completedNivel);
            points += extraPoints(qtdLife, monsterKilled);

            return points;
        }

        public static int toLevelCompleted(int completedNivel)
        {
            int x;
            x = (levelRoomToCompletedPoints * completedNivel) / qtdtotalSala;
            return x;

        }
        public static int completedWithMinLive(int qtdLife)
        {
            int x = 0;
            
            x = (minLivePoints * qtdLife/ minLife);     
            if(x > minLivePoints)
            {
                x = minLivePoints;
            }       
            
            return x ;

        }
        

        public static int completedInTime(double sec, int completedNivel)
        {
            int x;
            int lostPoints = 0;
            double faseCompletadasMod = Convert.ToDouble(completedNivel)/ qtdtotalSala;
            //Console.WriteLine("Completo:" + completedNivel + "/" + qtdtotalSala);
            if (sec > maxTimeSecs)
            {
                double extrapolatedTime = maxTimeSecs - sec;
                lostPoints = Convert.ToInt32(Math.Round(extrapolatedTime / modalTime));
                if (lostPoints > completedInTimePoints)
                {
                    lostPoints = completedInTimePoints;
                }

            }

            //Console.WriteLine("fassemod:"+ (completedInTimePoints - lostPoints) * faseCompletadasMod);
            x = Convert.ToInt32((completedInTimePoints - lostPoints) * faseCompletadasMod);
            return x;

        }

        public static int extraPoints(int qtdLife, int monsterKilled)
        {
            int extraPoints = 0;

            ///EXTRA LIFE POINTES
            if (qtdLife > minLife) { 
            
            int extraLife = qtdLife - minLife;

            if (extraLife > minLife)
            {
                    extraPoints = modalLivePoints * extraLife;
                }
            }

            //EXTRA MONSTER KILL POINTES
            extraPoints += monsterKilled;

            if(extraPoints > extraPointsMax)
            {
                extraPoints = extraPointsMax;
            }
            
            return extraPoints;

        }

        */
    }
}
