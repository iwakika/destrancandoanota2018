using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;

namespace DungeonRun.Entities
{
	public partial class CalculatePoints
	{

        private static CalculatePoints instance;        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{


		}

		private void CustomActivity()
		{


		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public static CalculatePoints Instance()
        {
            if (instance == null)
            {
                instance = new CalculatePoints();
            }
            return instance;
        }

        public int calculateTotalPoints(int completedNivel, int qtdLife, double sec, int monsterKilled)
        {
            int points = 0;
            points += toLevelCompleted(completedNivel);
            points += completedWithMinLive(qtdLife);
            points += completedInTime(sec, completedNivel);
            points += extraPoints(qtdLife, monsterKilled);

            return points;
        }

        public int toLevelCompleted(int completedNivel)
        {
            int x;
            x = (levelRoomToCompletedPoints * completedNivel) / qtdtotalSala;
            return x;

        }
        public int completedWithMinLive(int qtdLife)
        {
            int x = 0;

            x = (minLivePoints * qtdLife / minLife);
            if (x > minLivePoints)
            {
                x = minLivePoints;
            }

            return x;

        }


        public  int completedInTime(double sec, int completedNivel)
        {
            int x;
            int lostPoints = 0;
            double faseCompletadasMod = Convert.ToDouble(completedNivel) / qtdtotalSala;
            //Console.WriteLine("Completo:" + completedNivel + "/" + qtdtotalSala);
            if (sec > maxTimeSecs)
            {
                double extrapolatedTime = maxTimeSecs - sec;
                lostPoints = Convert.ToInt32(Math.Round(extrapolatedTime / modalTime));
                if (lostPoints > completedInTimePoints)
                {
                    lostPoints = completedInTimePoints;
                }

            }

            //Console.WriteLine("fassemod:"+ (completedInTimePoints - lostPoints) * faseCompletadasMod);
            x = Convert.ToInt32((completedInTimePoints - lostPoints) * faseCompletadasMod);
            return x;

        }

        public int extraPoints(int qtdLife, int monsterKilled)
        {
            int extraPoints = 0;

            ///EXTRA LIFE POINTES
            if (qtdLife > minLife)
            {

                int extraLife = qtdLife - minLife;

                if (extraLife > minLife)
                {
                    extraPoints = modalLivePoints * extraLife;
                }
            }

            //EXTRA MONSTER KILL POINTES
            extraPoints += monsterKilled;

            if (extraPoints > extraPointsMax)
            {
                extraPoints = extraPointsMax;
            }

            return extraPoints;

        }
    }
}
