#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using DungeonRun.Performance;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
namespace DungeonRun.Entities
{
    public partial class DashBar : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        public enum VariableState
        {
            Uninitialized = 0, //This exists so that the first set call actually does something
            Unknown = 1, //This exists so that if the entity is actually a child entity and has set a child state, you will get this
            Empty = 2, 
            Full = 3
        }
        protected int mCurrentState = 0;
        public Entities.DashBar.VariableState CurrentState
        {
            get
            {
                if (mCurrentState >= 0 && mCurrentState <= 3)
                {
                    return (VariableState)mCurrentState;
                }
                else
                {
                    return VariableState.Unknown;
                }
            }
            set
            {
                mCurrentState = (int)value;
                switch(CurrentState)
                {
                    case  VariableState.Uninitialized:
                        break;
                    case  VariableState.Unknown:
                        break;
                    case  VariableState.Empty:
                        if (this.Parent == null)
                        {
                            X = -16f;
                        }
                        else
                        {
                            RelativeX = -16f;
                        }
                        BarSpriteWidth = 0f;
                        if (BarSprite.Parent == null)
                        {
                            BarSpriteX = -16f;
                        }
                        else
                        {
                            BarSprite.RelativeX = -16f;
                        }
                        break;
                    case  VariableState.Full:
                        if (this.Parent == null)
                        {
                            X = 0f;
                        }
                        else
                        {
                            RelativeX = 0f;
                        }
                        BarSpriteWidth = 30f;
                        break;
                }
            }
        }
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        
        private FlatRedBall.Sprite FrameSprite;
        private FlatRedBall.Sprite BarSprite;
        public float FrameSpriteRed
        {
            get
            {
                return FrameSprite.Red;
            }
            set
            {
                FrameSprite.Red = value;
            }
        }
        public float FrameSpriteGreen
        {
            get
            {
                return FrameSprite.Green;
            }
            set
            {
                FrameSprite.Green = value;
            }
        }
        public float FrameSpriteBlue
        {
            get
            {
                return FrameSprite.Blue;
            }
            set
            {
                FrameSprite.Blue = value;
            }
        }
        public float BarSpriteRed
        {
            get
            {
                return BarSprite.Red;
            }
            set
            {
                BarSprite.Red = value;
            }
        }
        public float BarSpriteGreen
        {
            get
            {
                return BarSprite.Green;
            }
            set
            {
                BarSprite.Green = value;
            }
        }
        public float BarSpriteBlue
        {
            get
            {
                return BarSprite.Blue;
            }
            set
            {
                BarSprite.Blue = value;
            }
        }
        public float BarSpriteWidth
        {
            get
            {
                return BarSprite.Width;
            }
            set
            {
                BarSprite.Width = value;
            }
        }
        public float FrameSpriteX
        {
            get
            {
                if (FrameSprite.Parent == null)
                {
                    return FrameSprite.X;
                }
                else
                {
                    return FrameSprite.RelativeX;
                }
            }
            set
            {
                if (FrameSprite.Parent == null)
                {
                    FrameSprite.X = value;
                }
                else
                {
                    FrameSprite.RelativeX = value;
                }
            }
        }
        public float BarSpriteX
        {
            get
            {
                if (BarSprite.Parent == null)
                {
                    return BarSprite.X;
                }
                else
                {
                    return BarSprite.RelativeX;
                }
            }
            set
            {
                if (BarSprite.Parent == null)
                {
                    BarSprite.X = value;
                }
                else
                {
                    BarSprite.RelativeX = value;
                }
            }
        }
        public int Index { get; set; }
        public bool Used { get; set; }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public DashBar () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public DashBar (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public DashBar (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            FrameSprite = new FlatRedBall.Sprite();
            FrameSprite.Name = "FrameSprite";
            BarSprite = new FlatRedBall.Sprite();
            BarSprite.Name = "BarSprite";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(FrameSprite, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(BarSprite, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(FrameSprite, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(BarSprite, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.DashBarFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (FrameSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(FrameSprite);
            }
            if (BarSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(BarSprite);
            }
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (FrameSprite.Parent == null)
            {
                FrameSprite.CopyAbsoluteToRelative();
                FrameSprite.AttachTo(this, false);
            }
            if (FrameSprite.Parent == null)
            {
                FrameSprite.Z = 4f;
            }
            else
            {
                FrameSprite.RelativeZ = 4f;
            }
            FrameSprite.TextureScale = 1f;
            FrameSprite.Width = 34f;
            FrameSprite.Height = 8f;
            if (BarSprite.Parent == null)
            {
                BarSprite.CopyAbsoluteToRelative();
                BarSprite.AttachTo(this, false);
            }
            if (BarSprite.Parent == null)
            {
                BarSprite.X = 0f;
            }
            else
            {
                BarSprite.RelativeX = 0f;
            }
            if (BarSprite.Parent == null)
            {
                BarSprite.Y = 0f;
            }
            else
            {
                BarSprite.RelativeY = 0f;
            }
            if (BarSprite.Parent == null)
            {
                BarSprite.Z = 5f;
            }
            else
            {
                BarSprite.RelativeZ = 5f;
            }
            BarSprite.FlipHorizontal = true;
            BarSprite.TextureScale = 1f;
            BarSprite.Width = 32f;
            BarSprite.Height = 6f;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (FrameSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(FrameSprite);
            }
            if (BarSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(BarSprite);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            if (FrameSprite.Parent == null)
            {
                FrameSprite.Z = 4f;
            }
            else
            {
                FrameSprite.RelativeZ = 4f;
            }
            FrameSprite.TextureScale = 1f;
            FrameSprite.Width = 34f;
            FrameSprite.Height = 8f;
            if (BarSprite.Parent == null)
            {
                BarSprite.X = 0f;
            }
            else
            {
                BarSprite.RelativeX = 0f;
            }
            if (BarSprite.Parent == null)
            {
                BarSprite.Y = 0f;
            }
            else
            {
                BarSprite.RelativeY = 0f;
            }
            if (BarSprite.Parent == null)
            {
                BarSprite.Z = 5f;
            }
            else
            {
                BarSprite.RelativeZ = 5f;
            }
            BarSprite.FlipHorizontal = true;
            BarSprite.TextureScale = 1f;
            BarSprite.Width = 32f;
            BarSprite.Height = 6f;
            if (Parent == null)
            {
                Z = 5f;
            }
            else if (Parent is FlatRedBall.Camera)
            {
                RelativeZ = 5f - 40.0f;
            }
            else
            {
                RelativeZ = 5f;
            }
            FrameSpriteRed = 0f;
            FrameSpriteGreen = 0f;
            FrameSpriteBlue = 0f;
            BarSpriteRed = 0f;
            BarSpriteGreen = 0f;
            BarSpriteBlue = 255f;
            BarSpriteWidth = 32f;
            FrameSpriteX = 0f;
            BarSpriteX = 0f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(FrameSprite);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(BarSprite);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("DashBarStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("DashBarStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
            }
        }
        static VariableState mLoadingState = VariableState.Uninitialized;
        public static VariableState LoadingState
        {
            get
            {
                return mLoadingState;
            }
            set
            {
                mLoadingState = value;
            }
        }
        public FlatRedBall.Instructions.Instruction InterpolateToState (VariableState stateToInterpolateTo, double secondsToTake) 
        {
            switch(stateToInterpolateTo)
            {
                case  VariableState.Empty:
                    if (this.Parent != null)
                    {
                        RelativeXVelocity = (-16f - RelativeX) / (float)secondsToTake;
                    }
                    else
                    {
                        XVelocity = (-16f - X) / (float)secondsToTake;
                    }
                    if (BarSprite.Parent != null)
                    {
                        BarSprite.RelativeXVelocity = (-16f - BarSprite.RelativeX) / (float)secondsToTake;
                    }
                    else
                    {
                        BarSprite.XVelocity = (-16f - BarSprite.X) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.Full:
                    if (this.Parent != null)
                    {
                        RelativeXVelocity = (0f - RelativeX) / (float)secondsToTake;
                    }
                    else
                    {
                        XVelocity = (0f - X) / (float)secondsToTake;
                    }
                    break;
            }
            var instruction = new FlatRedBall.Instructions.DelegateInstruction<VariableState>(StopStateInterpolation, stateToInterpolateTo);
            instruction.TimeToExecute = FlatRedBall.TimeManager.CurrentTime + secondsToTake;
            this.Instructions.Add(instruction);
            return instruction;
        }
        public void StopStateInterpolation (VariableState stateToStop) 
        {
            switch(stateToStop)
            {
                case  VariableState.Empty:
                    if (this.Parent != null)
                    {
                        RelativeXVelocity =  0;
                    }
                    else
                    {
                        XVelocity =  0;
                    }
                    if (BarSprite.Parent != null)
                    {
                        BarSprite.RelativeXVelocity =  0;
                    }
                    else
                    {
                        BarSprite.XVelocity =  0;
                    }
                    break;
                case  VariableState.Full:
                    if (this.Parent != null)
                    {
                        RelativeXVelocity =  0;
                    }
                    else
                    {
                        XVelocity =  0;
                    }
                    break;
            }
            CurrentState = stateToStop;
        }
        public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
        {
            #if DEBUG
            if (float.IsNaN(interpolationValue))
            {
                throw new System.Exception("interpolationValue cannot be NaN");
            }
            #endif
            bool setX = true;
            float XFirstValue= 0;
            float XSecondValue= 0;
            bool setBarSpriteWidth = true;
            float BarSpriteWidthFirstValue= 0;
            float BarSpriteWidthSecondValue= 0;
            bool setBarSpriteX = true;
            float BarSpriteXFirstValue= 0;
            float BarSpriteXSecondValue= 0;
            switch(firstState)
            {
                case  VariableState.Empty:
                    XFirstValue = -16f;
                    BarSpriteWidthFirstValue = 0f;
                    BarSpriteXFirstValue = -16f;
                    break;
                case  VariableState.Full:
                    XFirstValue = 0f;
                    BarSpriteWidthFirstValue = 30f;
                    break;
            }
            switch(secondState)
            {
                case  VariableState.Empty:
                    XSecondValue = -16f;
                    BarSpriteWidthSecondValue = 0f;
                    BarSpriteXSecondValue = -16f;
                    break;
                case  VariableState.Full:
                    XSecondValue = 0f;
                    BarSpriteWidthSecondValue = 30f;
                    break;
            }
            if (setX)
            {
                if (this.Parent != null)
                {
                    RelativeX = XFirstValue * (1 - interpolationValue) + XSecondValue * interpolationValue;
                }
                else
                {
                    X = XFirstValue * (1 - interpolationValue) + XSecondValue * interpolationValue;
                }
            }
            if (setBarSpriteWidth)
            {
                BarSpriteWidth = BarSpriteWidthFirstValue * (1 - interpolationValue) + BarSpriteWidthSecondValue * interpolationValue;
            }
            if (setBarSpriteX)
            {
                if (BarSprite.Parent != null)
                {
                    BarSprite.RelativeX = BarSpriteXFirstValue * (1 - interpolationValue) + BarSpriteXSecondValue * interpolationValue;
                }
                else
                {
                    BarSpriteX = BarSpriteXFirstValue * (1 - interpolationValue) + BarSpriteXSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentState = (int)firstState;
                }
                else
                {
                    mCurrentState = (int)secondState;
                }
            }
        }
        public static void PreloadStateContent (VariableState state, string contentManagerName) 
        {
            ContentManagerName = contentManagerName;
            switch(state)
            {
                case  VariableState.Empty:
                    break;
                case  VariableState.Full:
                    break;
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(FrameSprite);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(BarSprite);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(FrameSprite);
            }
            FlatRedBall.SpriteManager.AddToLayer(FrameSprite, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(BarSprite);
            }
            FlatRedBall.SpriteManager.AddToLayer(BarSprite, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
