using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;

namespace DungeonRun.Entities
{
	public partial class DashBar
	{
        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
        /// 
        public Player player
        {
            get;
            set;
        }

        float mRatioFull;
        public float RatioFull
        {
            get { return mRatioFull; }
            set
            {
                mRatioFull = value;
                InterpolateBetween(VariableState.Empty, VariableState.Full, mRatioFull);
            }
        }

        private void CustomInitialize()
		{


		}

		private void CustomActivity()
		{



            if (player != null)
            {


                double secondSince = TimeManager.SecondsSince(player.startDashTime);
                if (secondSince <= player.DashFrequency) { 
                    RatioFull = (float)(secondSince / player.DashFrequency);
                    

                }
                ChangeBarcolor(secondSince);
                SetDashBarPosicion(player.CollisionHitBox, player.CollisionHitBox.Height / 2);
               // Console.WriteLine(secondSince + "/" + player.DashFrequency);

                

                // RatioFull = player.Health / (float)enemy.StartingHealth;                
                // SetHealthBarPosicion(enemy.CollisionHitBox, (enemy.CollisionHitBox.Height / 2));
                // if (enemy.Health == 0)
                //  {
                // this.Destroy();
                // }
            }


        }

        private void ChangeBarcolor(double secondSince)
        {

            // this.BarSpriteRed += ratioFull;
            this.BarSpriteGreen =255 - (float)((255 /player.DashFrequency) * secondSince);
            //Console.WriteLine((float)((255 / player.DashFrequency) * secondSince));
                //this.BarSpriteBlue = ;


        }

        private void SetDashBarPosicion(PositionedObject obj, float altura)
        {
           // Console.WriteLine("Posi��oz:" +Z);
            this.X = obj.X;
            this.Y = obj.Y + altura + 25;
            this.Z = 7;

           // Console.WriteLine("Posi��oz:" + Z);
        }

        private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
	}
}
