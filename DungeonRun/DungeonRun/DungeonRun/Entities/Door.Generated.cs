#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using DungeonRun.Performance;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
namespace DungeonRun.Entities
{
    public partial class Door : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable, FlatRedBall.Math.Geometry.ICollidable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        public enum VariableState
        {
            Uninitialized = 0, //This exists so that the first set call actually does something
            Unknown = 1, //This exists so that if the entity is actually a child entity and has set a child state, you will get this
            DoorTop = 2, 
            DoorNoTop = 3, 
            DoorTopOpen = 4, 
            DoorNoTopOpen = 5, 
            DoorBotton = 6, 
            DoorBottonOpen = 7, 
            WallTop = 8, 
            WallNotop = 9, 
            WallBottom = 10
        }
        protected int mCurrentState = 0;
        public Entities.Door.VariableState CurrentState
        {
            get
            {
                if (mCurrentState >= 0 && mCurrentState <= 10)
                {
                    return (VariableState)mCurrentState;
                }
                else
                {
                    return VariableState.Unknown;
                }
            }
            set
            {
                mCurrentState = (int)value;
                switch(CurrentState)
                {
                    case  VariableState.Uninitialized:
                        break;
                    case  VariableState.Unknown:
                        break;
                    case  VariableState.DoorTop:
                        SpriteInstanceTexture = DoorSprite;
                        CollisionInstanceWidth = 32f;
                        CollisionInstanceHeight = 20f;
                        if (SpriteInstance.Parent == null)
                        {
                            SpriteInstanceY = 16f;
                        }
                        else
                        {
                            SpriteInstance.RelativeY = 16f;
                        }
                        if (CollisionInstance.Parent == null)
                        {
                            CollisionInstanceY = 16f;
                        }
                        else
                        {
                            CollisionInstance.RelativeY = 16f;
                        }
                        break;
                    case  VariableState.DoorNoTop:
                        SpriteInstanceTexture = DoorShort;
                        CollisionInstanceWidth = 10f;
                        CollisionInstanceHeight = 32f;
                        if (SpriteInstance.Parent == null)
                        {
                            SpriteInstanceY = 0f;
                        }
                        else
                        {
                            SpriteInstance.RelativeY = 0f;
                        }
                        if (CollisionInstance.Parent == null)
                        {
                            CollisionInstanceY = 0f;
                        }
                        else
                        {
                            CollisionInstance.RelativeY = 0f;
                        }
                        break;
                    case  VariableState.DoorTopOpen:
                        SpriteInstanceTexture = DoorOpenSprite;
                        break;
                    case  VariableState.DoorNoTopOpen:
                        SpriteInstanceTexture = none;
                        CollisionInstanceWidth = 10f;
                        CollisionInstanceHeight = 32f;
                        break;
                    case  VariableState.DoorBotton:
                        SpriteInstanceTexture = DoorShort;
                        CollisionInstanceWidth = 32f;
                        CollisionInstanceHeight = 10f;
                        if (this.Parent == null)
                        {
                            RotationZ = 1.55f;
                        }
                        else
                        {
                            RelativeRotationZ = 1.55f;
                        }
                        if (SpriteInstance.Parent == null)
                        {
                            SpriteInstanceY = 0f;
                        }
                        else
                        {
                            SpriteInstance.RelativeY = 0f;
                        }
                        if (CollisionInstance.Parent == null)
                        {
                            CollisionInstanceY = 0f;
                        }
                        else
                        {
                            CollisionInstance.RelativeY = 0f;
                        }
                        break;
                    case  VariableState.DoorBottonOpen:
                        SpriteInstanceTexture = none;
                        CollisionInstanceWidth = 32f;
                        CollisionInstanceHeight = 10f;
                        if (this.Parent == null)
                        {
                            RotationZ = 1.55f;
                        }
                        else
                        {
                            RelativeRotationZ = 1.55f;
                        }
                        break;
                    case  VariableState.WallTop:
                        SpriteInstanceTexture = Wall;
                        CollisionInstanceWidth = 32f;
                        CollisionInstanceHeight = 64f;
                        break;
                    case  VariableState.WallNotop:
                        SpriteInstanceTexture = Wallshort;
                        CollisionInstanceWidth = 32f;
                        CollisionInstanceHeight = 32f;
                        break;
                    case  VariableState.WallBottom:
                        SpriteInstanceTexture = WallshortBottom;
                        CollisionInstanceWidth = 32f;
                        CollisionInstanceHeight = 32f;
                        break;
                }
            }
        }
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static Microsoft.Xna.Framework.Graphics.Texture2D build_atlas;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Wall;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D DoorShort;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Wallshort;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D DoorSprite;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D none;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D WallshortBottom;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D DoorOpenSprite;
        static Microsoft.Xna.Framework.Audio.SoundEffectInstance mdoor_lock;
        static string mLastContentManagerFordoor_lock;
        public static Microsoft.Xna.Framework.Audio.SoundEffectInstance door_lock
        {
            get
            {
                if (mdoor_lock == null || mLastContentManagerFordoor_lock != ContentManagerName)
                {
                    mLastContentManagerFordoor_lock = ContentManagerName;
                    mdoor_lock = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/door/door_lock", ContentManagerName).CreateInstance();
                    lock (mLockObject)
                    {
                        if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                        {
                            FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("DoorStaticUnload", UnloadStaticContent);
                            mRegisteredUnloads.Add(ContentManagerName);
                        }
                    }
                }
                return mdoor_lock;
            }
        }
        
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mCollisionInstance;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle CollisionInstance
        {
            get
            {
                return mCollisionInstance;
            }
            private set
            {
                mCollisionInstance = value;
            }
        }
        static float CollisionInstanceXReset;
        static float CollisionInstanceYReset;
        static float CollisionInstanceZReset;
        static float CollisionInstanceXVelocityReset;
        static float CollisionInstanceYVelocityReset;
        static float CollisionInstanceZVelocityReset;
        static float CollisionInstanceRotationXReset;
        static float CollisionInstanceRotationYReset;
        static float CollisionInstanceRotationZReset;
        static float CollisionInstanceRotationXVelocityReset;
        static float CollisionInstanceRotationYVelocityReset;
        static float CollisionInstanceRotationZVelocityReset;
        private FlatRedBall.Sprite mSpriteInstance;
        public FlatRedBall.Sprite SpriteInstance
        {
            get
            {
                return mSpriteInstance;
            }
            private set
            {
                mSpriteInstance = value;
            }
        }
        static float SpriteInstanceXReset;
        static float SpriteInstanceYReset;
        static float SpriteInstanceZReset;
        static float SpriteInstanceXVelocityReset;
        static float SpriteInstanceYVelocityReset;
        static float SpriteInstanceZVelocityReset;
        static float SpriteInstanceRotationXReset;
        static float SpriteInstanceRotationYReset;
        static float SpriteInstanceRotationZReset;
        static float SpriteInstanceRotationXVelocityReset;
        static float SpriteInstanceRotationYVelocityReset;
        static float SpriteInstanceRotationZVelocityReset;
        static float SpriteInstanceAlphaReset;
        static float SpriteInstanceAlphaRateReset;
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundDoorOpen;
        public string LevelToGo = "";
        public Microsoft.Xna.Framework.Graphics.Texture2D SpriteInstanceTexture
        {
            get
            {
                return SpriteInstance.Texture;
            }
            set
            {
                SpriteInstance.Texture = value;
            }
        }
        public float CollisionInstanceWidth
        {
            get
            {
                return CollisionInstance.Width;
            }
            set
            {
                CollisionInstance.Width = value;
            }
        }
        public float CollisionInstanceHeight
        {
            get
            {
                return CollisionInstance.Height;
            }
            set
            {
                CollisionInstance.Height = value;
            }
        }
        public float SpriteInstanceY
        {
            get
            {
                if (SpriteInstance.Parent == null)
                {
                    return SpriteInstance.Y;
                }
                else
                {
                    return SpriteInstance.RelativeY;
                }
            }
            set
            {
                if (SpriteInstance.Parent == null)
                {
                    SpriteInstance.Y = value;
                }
                else
                {
                    SpriteInstance.RelativeY = value;
                }
            }
        }
        public float CollisionInstanceY
        {
            get
            {
                if (CollisionInstance.Parent == null)
                {
                    return CollisionInstance.Y;
                }
                else
                {
                    return CollisionInstance.RelativeY;
                }
            }
            set
            {
                if (CollisionInstance.Parent == null)
                {
                    CollisionInstance.Y = value;
                }
                else
                {
                    CollisionInstance.RelativeY = value;
                }
            }
        }
        public int Index { get; set; }
        public bool Used { get; set; }
        private FlatRedBall.Math.Geometry.ShapeCollection mGeneratedCollision;
        public FlatRedBall.Math.Geometry.ShapeCollection Collision
        {
            get
            {
                return mGeneratedCollision;
            }
        }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Door () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Door (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Door (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            SoundDoorOpen = door_lock;
            mCollisionInstance = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mCollisionInstance.Name = "mCollisionInstance";
            mSpriteInstance = new FlatRedBall.Sprite();
            mSpriteInstance.Name = "mSpriteInstance";
            
            PostInitialize();
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceXReset = CollisionInstance.X;
            }
            else
            {
                CollisionInstanceXReset = CollisionInstance.RelativeX;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceYReset = CollisionInstance.Y;
            }
            else
            {
                CollisionInstanceYReset = CollisionInstance.RelativeY;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceZReset = CollisionInstance.Z;
            }
            else
            {
                CollisionInstanceZReset = CollisionInstance.RelativeZ;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceXVelocityReset = CollisionInstance.XVelocity;
            }
            else
            {
                CollisionInstanceXVelocityReset = CollisionInstance.RelativeXVelocity;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceYVelocityReset = CollisionInstance.YVelocity;
            }
            else
            {
                CollisionInstanceYVelocityReset = CollisionInstance.RelativeYVelocity;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceZVelocityReset = CollisionInstance.ZVelocity;
            }
            else
            {
                CollisionInstanceZVelocityReset = CollisionInstance.RelativeZVelocity;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationXReset = CollisionInstance.RotationX;
            }
            else
            {
                CollisionInstanceRotationXReset = CollisionInstance.RelativeRotationX;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationYReset = CollisionInstance.RotationY;
            }
            else
            {
                CollisionInstanceRotationYReset = CollisionInstance.RelativeRotationY;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationZReset = CollisionInstance.RotationZ;
            }
            else
            {
                CollisionInstanceRotationZReset = CollisionInstance.RelativeRotationZ;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationXVelocityReset = CollisionInstance.RotationXVelocity;
            }
            else
            {
                CollisionInstanceRotationXVelocityReset = CollisionInstance.RelativeRotationXVelocity;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationYVelocityReset = CollisionInstance.RotationYVelocity;
            }
            else
            {
                CollisionInstanceRotationYVelocityReset = CollisionInstance.RelativeRotationYVelocity;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationZVelocityReset = CollisionInstance.RotationZVelocity;
            }
            else
            {
                CollisionInstanceRotationZVelocityReset = CollisionInstance.RelativeRotationZVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceXReset = SpriteInstance.X;
            }
            else
            {
                SpriteInstanceXReset = SpriteInstance.RelativeX;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceYReset = SpriteInstance.Y;
            }
            else
            {
                SpriteInstanceYReset = SpriteInstance.RelativeY;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceZReset = SpriteInstance.Z;
            }
            else
            {
                SpriteInstanceZReset = SpriteInstance.RelativeZ;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceXVelocityReset = SpriteInstance.XVelocity;
            }
            else
            {
                SpriteInstanceXVelocityReset = SpriteInstance.RelativeXVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceYVelocityReset = SpriteInstance.YVelocity;
            }
            else
            {
                SpriteInstanceYVelocityReset = SpriteInstance.RelativeYVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceZVelocityReset = SpriteInstance.ZVelocity;
            }
            else
            {
                SpriteInstanceZVelocityReset = SpriteInstance.RelativeZVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationXReset = SpriteInstance.RotationX;
            }
            else
            {
                SpriteInstanceRotationXReset = SpriteInstance.RelativeRotationX;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationYReset = SpriteInstance.RotationY;
            }
            else
            {
                SpriteInstanceRotationYReset = SpriteInstance.RelativeRotationY;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationZReset = SpriteInstance.RotationZ;
            }
            else
            {
                SpriteInstanceRotationZReset = SpriteInstance.RelativeRotationZ;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationXVelocityReset = SpriteInstance.RotationXVelocity;
            }
            else
            {
                SpriteInstanceRotationXVelocityReset = SpriteInstance.RelativeRotationXVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationYVelocityReset = SpriteInstance.RotationYVelocity;
            }
            else
            {
                SpriteInstanceRotationYVelocityReset = SpriteInstance.RelativeRotationYVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationZVelocityReset = SpriteInstance.RotationZVelocity;
            }
            else
            {
                SpriteInstanceRotationZVelocityReset = SpriteInstance.RelativeRotationZVelocity;
            }
            SpriteInstanceAlphaReset = SpriteInstance.Alpha;
            SpriteInstanceAlphaRateReset = SpriteInstance.AlphaRate;
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionInstance, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(mSpriteInstance, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionInstance, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(mSpriteInstance, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.DoorFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            if (mdoor_lock != null)
            {
            }
            
            if (CollisionInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionInstance);
            }
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (mCollisionInstance.Parent == null)
            {
                mCollisionInstance.CopyAbsoluteToRelative();
                mCollisionInstance.AttachTo(this, false);
            }
            CollisionInstance.Width = 32f;
            CollisionInstance.Height = 32f;
            CollisionInstance.Visible = false;
            CollisionInstance.Color = Color.Turquoise;
            CollisionInstance.IgnoreParentPosition = false;
            CollisionInstance.IgnoresParentVisibility = false;
            CollisionInstance.ParentRotationChangesPosition = true;
            if (mSpriteInstance.Parent == null)
            {
                mSpriteInstance.CopyAbsoluteToRelative();
                mSpriteInstance.AttachTo(this, false);
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = 0f;
            }
            else
            {
                SpriteInstance.RelativeY = 0f;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Z = 0f;
            }
            else
            {
                SpriteInstance.RelativeZ = 0f;
            }
            SpriteInstance.Texture = Wallshort;
            SpriteInstance.TextureScale = 1f;
            SpriteInstance.IgnoreParentPosition = false;
            SpriteInstance.IgnoresParentVisibility = false;
            mGeneratedCollision = new FlatRedBall.Math.Geometry.ShapeCollection();
            mGeneratedCollision.AxisAlignedRectangles.AddOneWay(mCollisionInstance);
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (CollisionInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionInstance);
            }
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            CollisionInstance.Width = 32f;
            CollisionInstance.Height = 32f;
            CollisionInstance.Visible = false;
            CollisionInstance.Color = Color.Turquoise;
            CollisionInstance.IgnoreParentPosition = false;
            CollisionInstance.IgnoresParentVisibility = false;
            CollisionInstance.ParentRotationChangesPosition = true;
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.X = CollisionInstanceXReset;
            }
            else
            {
                CollisionInstance.RelativeX = CollisionInstanceXReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.Y = CollisionInstanceYReset;
            }
            else
            {
                CollisionInstance.RelativeY = CollisionInstanceYReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.Z = CollisionInstanceZReset;
            }
            else
            {
                CollisionInstance.RelativeZ = CollisionInstanceZReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.XVelocity = CollisionInstanceXVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeXVelocity = CollisionInstanceXVelocityReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.YVelocity = CollisionInstanceYVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeYVelocity = CollisionInstanceYVelocityReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.ZVelocity = CollisionInstanceZVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeZVelocity = CollisionInstanceZVelocityReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationX = CollisionInstanceRotationXReset;
            }
            else
            {
                CollisionInstance.RelativeRotationX = CollisionInstanceRotationXReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationY = CollisionInstanceRotationYReset;
            }
            else
            {
                CollisionInstance.RelativeRotationY = CollisionInstanceRotationYReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationZ = CollisionInstanceRotationZReset;
            }
            else
            {
                CollisionInstance.RelativeRotationZ = CollisionInstanceRotationZReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationXVelocity = CollisionInstanceRotationXVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeRotationXVelocity = CollisionInstanceRotationXVelocityReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationYVelocity = CollisionInstanceRotationYVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeRotationYVelocity = CollisionInstanceRotationYVelocityReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationZVelocity = CollisionInstanceRotationZVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeRotationZVelocity = CollisionInstanceRotationZVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = 0f;
            }
            else
            {
                SpriteInstance.RelativeY = 0f;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Z = 0f;
            }
            else
            {
                SpriteInstance.RelativeZ = 0f;
            }
            SpriteInstance.Texture = Wallshort;
            SpriteInstance.TextureScale = 1f;
            SpriteInstance.IgnoreParentPosition = false;
            SpriteInstance.IgnoresParentVisibility = false;
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.X = SpriteInstanceXReset;
            }
            else
            {
                SpriteInstance.RelativeX = SpriteInstanceXReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = SpriteInstanceYReset;
            }
            else
            {
                SpriteInstance.RelativeY = SpriteInstanceYReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Z = SpriteInstanceZReset;
            }
            else
            {
                SpriteInstance.RelativeZ = SpriteInstanceZReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.XVelocity = SpriteInstanceXVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeXVelocity = SpriteInstanceXVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.YVelocity = SpriteInstanceYVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeYVelocity = SpriteInstanceYVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.ZVelocity = SpriteInstanceZVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeZVelocity = SpriteInstanceZVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationX = SpriteInstanceRotationXReset;
            }
            else
            {
                SpriteInstance.RelativeRotationX = SpriteInstanceRotationXReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationY = SpriteInstanceRotationYReset;
            }
            else
            {
                SpriteInstance.RelativeRotationY = SpriteInstanceRotationYReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationZ = SpriteInstanceRotationZReset;
            }
            else
            {
                SpriteInstance.RelativeRotationZ = SpriteInstanceRotationZReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationXVelocity = SpriteInstanceRotationXVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeRotationXVelocity = SpriteInstanceRotationXVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationYVelocity = SpriteInstanceRotationYVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeRotationYVelocity = SpriteInstanceRotationYVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationZVelocity = SpriteInstanceRotationZVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeRotationZVelocity = SpriteInstanceRotationZVelocityReset;
            }
            SpriteInstance.Alpha = SpriteInstanceAlphaReset;
            SpriteInstance.AlphaRate = SpriteInstanceAlphaRateReset;
            if (Parent == null)
            {
                Z = 5f;
            }
            else if (Parent is FlatRedBall.Camera)
            {
                RelativeZ = 5f - 40.0f;
            }
            else
            {
                RelativeZ = 5f;
            }
            LevelToGo = "";
            SpriteInstanceTexture = Wallshort;
            CollisionInstanceWidth = 32f;
            CollisionInstanceHeight = 32f;
            if (Parent == null)
            {
                RotationZ = 0f;
            }
            else
            {
                RelativeRotationZ = 0f;
            }
            SpriteInstanceY = 0f;
            CollisionInstanceY = 0f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteInstance);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("DoorStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/screen/build_atlas.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                build_atlas = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/screen/build_atlas.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/wall.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Wall = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/wall.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/doorshort.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                DoorShort = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/doorshort.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/wallshort.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Wallshort = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/wallshort.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/doorsprite.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                DoorSprite = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/doorsprite.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/none.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                none = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/none.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/wallshortbottom.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                WallshortBottom = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/wallshortbottom.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/door/dooropensprite.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                DoorOpenSprite = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/door/dooropensprite.png", ContentManagerName);
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("DoorStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (build_atlas != null)
                {
                    build_atlas= null;
                }
                if (Wall != null)
                {
                    Wall= null;
                }
                if (DoorShort != null)
                {
                    DoorShort= null;
                }
                if (Wallshort != null)
                {
                    Wallshort= null;
                }
                if (DoorSprite != null)
                {
                    DoorSprite= null;
                }
                if (none != null)
                {
                    none= null;
                }
                if (WallshortBottom != null)
                {
                    WallshortBottom= null;
                }
                if (DoorOpenSprite != null)
                {
                    DoorOpenSprite= null;
                }
                if (mdoor_lock != null)
                {
                    if(mdoor_lock.IsDisposed == false) { mdoor_lock.Stop();  mdoor_lock.Dispose(); };
                    mdoor_lock= null;
                }
            }
        }
        static VariableState mLoadingState = VariableState.Uninitialized;
        public static VariableState LoadingState
        {
            get
            {
                return mLoadingState;
            }
            set
            {
                mLoadingState = value;
            }
        }
        public FlatRedBall.Instructions.Instruction InterpolateToState (VariableState stateToInterpolateTo, double secondsToTake) 
        {
            switch(stateToInterpolateTo)
            {
                case  VariableState.DoorTop:
                    if (SpriteInstance.Parent != null)
                    {
                        SpriteInstance.RelativeYVelocity = (16f - SpriteInstance.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        SpriteInstance.YVelocity = (16f - SpriteInstance.Y) / (float)secondsToTake;
                    }
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeYVelocity = (16f - CollisionInstance.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionInstance.YVelocity = (16f - CollisionInstance.Y) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.DoorNoTop:
                    if (SpriteInstance.Parent != null)
                    {
                        SpriteInstance.RelativeYVelocity = (0f - SpriteInstance.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        SpriteInstance.YVelocity = (0f - SpriteInstance.Y) / (float)secondsToTake;
                    }
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeYVelocity = (0f - CollisionInstance.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionInstance.YVelocity = (0f - CollisionInstance.Y) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.DoorTopOpen:
                    break;
                case  VariableState.DoorNoTopOpen:
                    break;
                case  VariableState.DoorBotton:
                    if (this.Parent != null)
                    {
                        RelativeRotationZVelocity = (1.55f - RelativeRotationZ) / (float)secondsToTake;
                    }
                    else
                    {
                        RotationZVelocity = (1.55f - RotationZ) / (float)secondsToTake;
                    }
                    if (SpriteInstance.Parent != null)
                    {
                        SpriteInstance.RelativeYVelocity = (0f - SpriteInstance.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        SpriteInstance.YVelocity = (0f - SpriteInstance.Y) / (float)secondsToTake;
                    }
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeYVelocity = (0f - CollisionInstance.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionInstance.YVelocity = (0f - CollisionInstance.Y) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.DoorBottonOpen:
                    if (this.Parent != null)
                    {
                        RelativeRotationZVelocity = (1.55f - RelativeRotationZ) / (float)secondsToTake;
                    }
                    else
                    {
                        RotationZVelocity = (1.55f - RotationZ) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.WallTop:
                    break;
                case  VariableState.WallNotop:
                    break;
                case  VariableState.WallBottom:
                    break;
            }
            var instruction = new FlatRedBall.Instructions.DelegateInstruction<VariableState>(StopStateInterpolation, stateToInterpolateTo);
            instruction.TimeToExecute = FlatRedBall.TimeManager.CurrentTime + secondsToTake;
            this.Instructions.Add(instruction);
            return instruction;
        }
        public void StopStateInterpolation (VariableState stateToStop) 
        {
            switch(stateToStop)
            {
                case  VariableState.DoorTop:
                    if (SpriteInstance.Parent != null)
                    {
                        SpriteInstance.RelativeYVelocity =  0;
                    }
                    else
                    {
                        SpriteInstance.YVelocity =  0;
                    }
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionInstance.YVelocity =  0;
                    }
                    break;
                case  VariableState.DoorNoTop:
                    if (SpriteInstance.Parent != null)
                    {
                        SpriteInstance.RelativeYVelocity =  0;
                    }
                    else
                    {
                        SpriteInstance.YVelocity =  0;
                    }
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionInstance.YVelocity =  0;
                    }
                    break;
                case  VariableState.DoorTopOpen:
                    break;
                case  VariableState.DoorNoTopOpen:
                    break;
                case  VariableState.DoorBotton:
                    if (this.Parent != null)
                    {
                        RelativeRotationZVelocity =  0;
                    }
                    else
                    {
                        RotationZVelocity =  0;
                    }
                    if (SpriteInstance.Parent != null)
                    {
                        SpriteInstance.RelativeYVelocity =  0;
                    }
                    else
                    {
                        SpriteInstance.YVelocity =  0;
                    }
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionInstance.YVelocity =  0;
                    }
                    break;
                case  VariableState.DoorBottonOpen:
                    if (this.Parent != null)
                    {
                        RelativeRotationZVelocity =  0;
                    }
                    else
                    {
                        RotationZVelocity =  0;
                    }
                    break;
                case  VariableState.WallTop:
                    break;
                case  VariableState.WallNotop:
                    break;
                case  VariableState.WallBottom:
                    break;
            }
            CurrentState = stateToStop;
        }
        public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
        {
            #if DEBUG
            if (float.IsNaN(interpolationValue))
            {
                throw new System.Exception("interpolationValue cannot be NaN");
            }
            #endif
            bool setCollisionInstanceWidth = true;
            float CollisionInstanceWidthFirstValue= 0;
            float CollisionInstanceWidthSecondValue= 0;
            bool setCollisionInstanceHeight = true;
            float CollisionInstanceHeightFirstValue= 0;
            float CollisionInstanceHeightSecondValue= 0;
            bool setSpriteInstanceY = true;
            float SpriteInstanceYFirstValue= 0;
            float SpriteInstanceYSecondValue= 0;
            bool setCollisionInstanceY = true;
            float CollisionInstanceYFirstValue= 0;
            float CollisionInstanceYSecondValue= 0;
            bool setRotationZ = true;
            float RotationZFirstValue= 0;
            float RotationZSecondValue= 0;
            switch(firstState)
            {
                case  VariableState.DoorTop:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = DoorSprite;
                    }
                    CollisionInstanceWidthFirstValue = 32f;
                    CollisionInstanceHeightFirstValue = 20f;
                    SpriteInstanceYFirstValue = 16f;
                    CollisionInstanceYFirstValue = 16f;
                    break;
                case  VariableState.DoorNoTop:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = DoorShort;
                    }
                    CollisionInstanceWidthFirstValue = 10f;
                    CollisionInstanceHeightFirstValue = 32f;
                    SpriteInstanceYFirstValue = 0f;
                    CollisionInstanceYFirstValue = 0f;
                    break;
                case  VariableState.DoorTopOpen:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = DoorOpenSprite;
                    }
                    break;
                case  VariableState.DoorNoTopOpen:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = none;
                    }
                    CollisionInstanceWidthFirstValue = 10f;
                    CollisionInstanceHeightFirstValue = 32f;
                    break;
                case  VariableState.DoorBotton:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = DoorShort;
                    }
                    CollisionInstanceWidthFirstValue = 32f;
                    CollisionInstanceHeightFirstValue = 10f;
                    RotationZFirstValue = 1.55f;
                    SpriteInstanceYFirstValue = 0f;
                    CollisionInstanceYFirstValue = 0f;
                    break;
                case  VariableState.DoorBottonOpen:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = none;
                    }
                    CollisionInstanceWidthFirstValue = 32f;
                    CollisionInstanceHeightFirstValue = 10f;
                    RotationZFirstValue = 1.55f;
                    break;
                case  VariableState.WallTop:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = Wall;
                    }
                    CollisionInstanceWidthFirstValue = 32f;
                    CollisionInstanceHeightFirstValue = 64f;
                    break;
                case  VariableState.WallNotop:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = Wallshort;
                    }
                    CollisionInstanceWidthFirstValue = 32f;
                    CollisionInstanceHeightFirstValue = 32f;
                    break;
                case  VariableState.WallBottom:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = WallshortBottom;
                    }
                    CollisionInstanceWidthFirstValue = 32f;
                    CollisionInstanceHeightFirstValue = 32f;
                    break;
            }
            switch(secondState)
            {
                case  VariableState.DoorTop:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = DoorSprite;
                    }
                    CollisionInstanceWidthSecondValue = 32f;
                    CollisionInstanceHeightSecondValue = 20f;
                    SpriteInstanceYSecondValue = 16f;
                    CollisionInstanceYSecondValue = 16f;
                    break;
                case  VariableState.DoorNoTop:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = DoorShort;
                    }
                    CollisionInstanceWidthSecondValue = 10f;
                    CollisionInstanceHeightSecondValue = 32f;
                    SpriteInstanceYSecondValue = 0f;
                    CollisionInstanceYSecondValue = 0f;
                    break;
                case  VariableState.DoorTopOpen:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = DoorOpenSprite;
                    }
                    break;
                case  VariableState.DoorNoTopOpen:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = none;
                    }
                    CollisionInstanceWidthSecondValue = 10f;
                    CollisionInstanceHeightSecondValue = 32f;
                    break;
                case  VariableState.DoorBotton:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = DoorShort;
                    }
                    CollisionInstanceWidthSecondValue = 32f;
                    CollisionInstanceHeightSecondValue = 10f;
                    RotationZSecondValue = 1.55f;
                    SpriteInstanceYSecondValue = 0f;
                    CollisionInstanceYSecondValue = 0f;
                    break;
                case  VariableState.DoorBottonOpen:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = none;
                    }
                    CollisionInstanceWidthSecondValue = 32f;
                    CollisionInstanceHeightSecondValue = 10f;
                    RotationZSecondValue = 1.55f;
                    break;
                case  VariableState.WallTop:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = Wall;
                    }
                    CollisionInstanceWidthSecondValue = 32f;
                    CollisionInstanceHeightSecondValue = 64f;
                    break;
                case  VariableState.WallNotop:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = Wallshort;
                    }
                    CollisionInstanceWidthSecondValue = 32f;
                    CollisionInstanceHeightSecondValue = 32f;
                    break;
                case  VariableState.WallBottom:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = WallshortBottom;
                    }
                    CollisionInstanceWidthSecondValue = 32f;
                    CollisionInstanceHeightSecondValue = 32f;
                    break;
            }
            if (setCollisionInstanceWidth)
            {
                CollisionInstanceWidth = CollisionInstanceWidthFirstValue * (1 - interpolationValue) + CollisionInstanceWidthSecondValue * interpolationValue;
            }
            if (setCollisionInstanceHeight)
            {
                CollisionInstanceHeight = CollisionInstanceHeightFirstValue * (1 - interpolationValue) + CollisionInstanceHeightSecondValue * interpolationValue;
            }
            if (setSpriteInstanceY)
            {
                if (SpriteInstance.Parent != null)
                {
                    SpriteInstance.RelativeY = SpriteInstanceYFirstValue * (1 - interpolationValue) + SpriteInstanceYSecondValue * interpolationValue;
                }
                else
                {
                    SpriteInstanceY = SpriteInstanceYFirstValue * (1 - interpolationValue) + SpriteInstanceYSecondValue * interpolationValue;
                }
                if (setCollisionInstanceY)
                {
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeY = CollisionInstanceYFirstValue * (1 - interpolationValue) + CollisionInstanceYSecondValue * interpolationValue;
                    }
                    else
                    {
                        CollisionInstanceY = CollisionInstanceYFirstValue * (1 - interpolationValue) + CollisionInstanceYSecondValue * interpolationValue;
                    }
                    if (setRotationZ)
                    {
                        if (this.Parent != null)
                        {
                            RelativeRotationZ = RotationZFirstValue * (1 - interpolationValue) + RotationZSecondValue * interpolationValue;
                        }
                        else
                        {
                            RotationZ = RotationZFirstValue * (1 - interpolationValue) + RotationZSecondValue * interpolationValue;
                        }
                    }
                    if (interpolationValue < 1)
                    {
                        mCurrentState = (int)firstState;
                    }
                    else
                    {
                        mCurrentState = (int)secondState;
                    }
                }
            }
        }
        public static void PreloadStateContent (VariableState state, string contentManagerName) 
        {
            ContentManagerName = contentManagerName;
            switch(state)
            {
                case  VariableState.DoorTop:
                    {
                        object throwaway = DoorSprite;
                    }
                    break;
                case  VariableState.DoorNoTop:
                    {
                        object throwaway = DoorShort;
                    }
                    break;
                case  VariableState.DoorTopOpen:
                    {
                        object throwaway = DoorOpenSprite;
                    }
                    break;
                case  VariableState.DoorNoTopOpen:
                    {
                        object throwaway = none;
                    }
                    break;
                case  VariableState.DoorBotton:
                    {
                        object throwaway = DoorShort;
                    }
                    break;
                case  VariableState.DoorBottonOpen:
                    {
                        object throwaway = none;
                    }
                    break;
                case  VariableState.WallTop:
                    {
                        object throwaway = Wall;
                    }
                    break;
                case  VariableState.WallNotop:
                    {
                        object throwaway = Wallshort;
                    }
                    break;
                case  VariableState.WallBottom:
                    {
                        object throwaway = WallshortBottom;
                    }
                    break;
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "build_atlas":
                    return build_atlas;
                case  "Wall":
                    return Wall;
                case  "DoorShort":
                    return DoorShort;
                case  "Wallshort":
                    return Wallshort;
                case  "DoorSprite":
                    return DoorSprite;
                case  "none":
                    return none;
                case  "WallshortBottom":
                    return WallshortBottom;
                case  "DoorOpenSprite":
                    return DoorOpenSprite;
                case  "door_lock":
                    return door_lock;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "build_atlas":
                    return build_atlas;
                case  "Wall":
                    return Wall;
                case  "DoorShort":
                    return DoorShort;
                case  "Wallshort":
                    return Wallshort;
                case  "DoorSprite":
                    return DoorSprite;
                case  "none":
                    return none;
                case  "WallshortBottom":
                    return WallshortBottom;
                case  "DoorOpenSprite":
                    return DoorOpenSprite;
                case  "door_lock":
                    return door_lock;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "build_atlas":
                    return build_atlas;
                case  "Wall":
                    return Wall;
                case  "DoorShort":
                    return DoorShort;
                case  "Wallshort":
                    return Wallshort;
                case  "DoorSprite":
                    return DoorSprite;
                case  "none":
                    return none;
                case  "WallshortBottom":
                    return WallshortBottom;
                case  "DoorOpenSprite":
                    return DoorOpenSprite;
                case  "door_lock":
                    return door_lock;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
            if (SoundDoorOpen.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundDoorOpen.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundDoorOpen.Resume()));
            }
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CollisionInstance);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(SpriteInstance);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CollisionInstance);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CollisionInstance, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(SpriteInstance);
            }
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
