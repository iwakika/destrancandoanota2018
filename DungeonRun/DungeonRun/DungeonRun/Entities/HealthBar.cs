using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;

namespace DungeonRun.Entities
{
	public partial class HealthBar
	{
        

    float mRatioFull;
        public float RatioFull
        {
            get { return mRatioFull; }
            set
            {
                mRatioFull = value;
                InterpolateBetween(VariableState.Empty, VariableState.Full, mRatioFull);
            }
        }
        public Player player
        {
            get;
            set;
        }

        public Enemy enemy
        {
            get;
            set;
        }


        private void CustomInitialize()
		{
            

		}

		private void CustomActivity()
		{

           
            if(enemy != null)
            {
               // Console.WriteLine("vIDA inimigo:" + enemy.Health + "| Vida inicial:" + enemy.StartingHealth);
                RatioFull = enemy.Health / (float)enemy.StartingHealth;
               // Console.WriteLine(RatioFull +"|" + (float)enemy.Health / enemy.StartingHealth +"|"+ enemy.StartingHealth/ enemy.Health) ;         
                //ChangeBarcolor();
               SetHealthBarPosicion(enemy.CollisionHitBox, (enemy.CollisionHitBox.Height/2));
                if (enemy.Health == 0)
                {
                    this.Destroy();
                }
            }

           
            

        }
       

        private void SetHealthBarPosicion(PositionedObject obj, float altura)
        {
            this.X = obj.X ;
            this.Y = obj.Y + altura +16;
           // this.Z = 30;
        }

        private void ChangeBarcolor()
        {
            if (enemy.Health == enemy.StartingHealth / 2)
            {
                this.BarSpriteRed = 1;
                this.BarSpriteGreen = 0;
                this.BarSpriteBlue = 0;
            }

        }

        private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
	}
}
