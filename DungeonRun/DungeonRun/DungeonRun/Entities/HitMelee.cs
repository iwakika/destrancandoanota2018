using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework;
using DungeonRun.MinhasClasses.Data;

namespace DungeonRun.Entities
{
	public partial class HitMelee
	{


        public bool isAtackAnimate {
            get{

                return this.SpriteInstance.Animate;
            }
        }
        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{
            this.SpriteInstance.Visible = false;
            this.SpriteInstance.Animate = false;
            //Console.WriteLine("inicialize");
        }

        private void CustomActivity() { 
           // Console.WriteLine("custon");
            ActionAutoDestroy();

		}


        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }



        public void createHitMeleeAnimation(float x, float y, float z)
        {
            //HitMelee hit =  new HitMelee();
            switch (this.CurrentState)
            {
                 
              case HitMelee.VariableState.bite:

                    SoundMonster.Play();
                    this.CurrentState = HitMelee.VariableState.bite;
                    this.SpriteInstance.Visible = true;                    
                    this.SpriteInstance.Animate = true;
                    this.SpriteInstance.Position.X = x;
                    this.SpriteInstance.Position.Y = y;
                    this.SpriteInstance.Position.Z = z;

                    //Console.WriteLine("mordida");

                    break;
                    
                case HitMelee.VariableState.groundShock:
                    SoundMonster.Play();
                    this.CurrentState = HitMelee.VariableState.groundShock;
                    
                    this.SpriteInstance.Visible = true;
                    this.SpriteInstance.Animate = true;
                    this.SpriteInstance.Position.X = x;
                    this.SpriteInstance.Position.Y = y;
                    this.SpriteInstance.Position.Z = z;
                    break;

                case HitMelee.VariableState.sword:

                    SoundMonster.Play();
                    this.CurrentState = HitMelee.VariableState.sword;

                    this.SpriteInstance.Visible = true;
                    this.SpriteInstance.Animate = true;
                    this.SpriteInstance.Position.X = x;
                    this.SpriteInstance.Position.Y = y;
                    this.SpriteInstance.Position.Z = z;
                    break;



                default:
                    break;

              

    
            }

        }

        public void ActionAutoDestroy()
        {
            //Console.WriteLine(this.SpriteInstance.CurrentFrameIndex +"; " + this.SpriteInstance.CurrentChain.Count);
            if (this.autoDestroy)
            {

                if (this.SpriteInstance.CurrentFrameIndex == this.SpriteInstance.CurrentChain.Count - 1)
                {
                  //  Console.WriteLine("oculta sprite");
                    this.SpriteInstance.Visible = false;
                    this.SpriteInstance.Animate = false;
                    this.SpriteInstance.CurrentFrameIndex = 0;
                   // this.Destroy();
                }
            }
        }


        private void CustomDestroy()
        {


        }
    }
}
