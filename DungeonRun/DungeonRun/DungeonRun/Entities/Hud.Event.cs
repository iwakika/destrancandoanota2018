using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using DungeonRun.Entities;
using DungeonRun.Screens;
using DungeonRun.MinhasClasses.Data;
namespace DungeonRun.Entities
{
    public partial class Hud
    {
        void OnBtn_VoltarInicioClick(FlatRedBall.Gui.IWindow window)
        {
            GlobalData.gameOver = true;
            ScreenManager.CurrentScreen.MoveToScreen(typeof(MainMenuScreen).FullName);
        }
        void OnGameOver_btn_RankingClick(FlatRedBall.Gui.IWindow window)
        {
            //
             GameOver_DisplayInstance.Visible = false;
            
            loadRanking(true);
        }
        void OnGameOver_Btn_VoltarinicioClick(FlatRedBall.Gui.IWindow window)
        {
            GlobalData.gameOver = true;
            ScreenManager.CurrentScreen.MoveToScreen(typeof(MainMenuScreen).FullName);
        }



        void Onbtn_PointsClick(FlatRedBall.Gui.IWindow window)
        {
            loadPlayPoints();
        }
        
        void Onbtn_points_voltarInicioClick (FlatRedBall.Gui.IWindow window) 
        {
            GlobalData.gameOver = true;
            ScreenManager.CurrentScreen.MoveToScreen(typeof(MainMenuScreen).FullName);
        }

         
     
        void OnPoints_Btn_VoltarInicioClick (FlatRedBall.Gui.IWindow window) 
        {


            GlobalData.gameOver = true;
            ScreenManager.CurrentScreen.MoveToScreen(typeof(MainMenuScreen).FullName);

        }
        void Onbtn_GameOver_PointsClick (FlatRedBall.Gui.IWindow window) 
        {
            GameOver_DisplayInstance.Visible = false;
            loadPlayPoints();
        }
        

    }

    
}
