using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using DungeonRun.Entities;
using DungeonRun.Screens;
namespace DungeonRun.Entities
{
    public partial class Hud
    {
        void OnBtn_VoltarInicioClickTunnel (FlatRedBall.Gui.IWindow window) 
        {
            if (this.Btn_VoltarInicioClick != null)
            {
                Btn_VoltarInicioClick(window);
            }
        }
        void OnGameOver_btn_RankingClickTunnel (FlatRedBall.Gui.IWindow window) 
        {
            if (this.GameOver_btn_RankingClick != null)
            {
                GameOver_btn_RankingClick(window);
            }
        }
        void OnGameOver_Btn_VoltarinicioClickTunnel (FlatRedBall.Gui.IWindow window) 
        {
            if (this.GameOver_Btn_VoltarinicioClick != null)
            {
                GameOver_Btn_VoltarinicioClick(window);
            }
        }
        void Onbtn_GameOver_PointsClickTunnel (FlatRedBall.Gui.IWindow window) 
        {
            if (this.btn_GameOver_PointsClick != null)
            {
                btn_GameOver_PointsClick(window);
            }
        }
    }
}
