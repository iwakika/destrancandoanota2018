#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
namespace DungeonRun.Entities
{
    public partial class Hud : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static FlatRedBall.Gum.GumIdb GameHudGum;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D medium_box;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D full_heart;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D empty_heart;
        static FlatRedBall.Gum.GumIdb mPlayPoints_HUD;
        static string mLastContentManagerForPlayPoints_HUD;
        public static FlatRedBall.Gum.GumIdb PlayPoints_HUD
        {
            get
            {
                if (mPlayPoints_HUD == null || mLastContentManagerForPlayPoints_HUD != ContentManagerName)
                {
                    mLastContentManagerForPlayPoints_HUD = ContentManagerName;
                    Gum.Wireframe.GraphicalUiElement.IsAllLayoutSuspended = true;  mPlayPoints_HUD = new FlatRedBall.Gum.GumIdb();  mPlayPoints_HUD.LoadFromFile("content/gumproject/screens/playpoints_hud.gusx");  mPlayPoints_HUD.AssignReferences();Gum.Wireframe.GraphicalUiElement.IsAllLayoutSuspended = false; mPlayPoints_HUD.Element.UpdateLayout(); mPlayPoints_HUD.Element.UpdateLayout();
                    lock (mLockObject)
                    {
                        if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                        {
                            FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("HudStaticUnload", UnloadStaticContent);
                            mRegisteredUnloads.Add(ContentManagerName);
                        }
                    }
                }
                return mPlayPoints_HUD;
            }
        }
        static FlatRedBall.Gum.GumIdb mRakingHUD;
        static string mLastContentManagerForRakingHUD;
        public static FlatRedBall.Gum.GumIdb RakingHUD
        {
            get
            {
                if (mRakingHUD == null || mLastContentManagerForRakingHUD != ContentManagerName)
                {
                    mLastContentManagerForRakingHUD = ContentManagerName;
                    Gum.Wireframe.GraphicalUiElement.IsAllLayoutSuspended = true;  mRakingHUD = new FlatRedBall.Gum.GumIdb();  mRakingHUD.LoadFromFile("content/gumproject/screens/rakinghud.gusx");  mRakingHUD.AssignReferences();Gum.Wireframe.GraphicalUiElement.IsAllLayoutSuspended = false; mRakingHUD.Element.UpdateLayout(); mRakingHUD.Element.UpdateLayout();
                    lock (mLockObject)
                    {
                        if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                        {
                            FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("HudStaticUnload", UnloadStaticContent);
                            mRegisteredUnloads.Add(ContentManagerName);
                        }
                    }
                }
                return mRakingHUD;
            }
        }
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Galvisr;
        protected static Microsoft.Xna.Framework.Audio.SoundEffect click;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D GuiliandriAvatar;
        
        private FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.HealthBar> HealthBarList;
        private DungeonRun.GumRuntimes.ItemDisplayRuntime Item_HUD_0;
        private DungeonRun.GumRuntimes.ItemDisplayRuntime mItem_HUD_1;
        public DungeonRun.GumRuntimes.ItemDisplayRuntime Item_HUD_1
        {
            get
            {
                return mItem_HUD_1;
            }
            private set
            {
                mItem_HUD_1 = value;
            }
        }
        private DungeonRun.GumRuntimes.ItemDisplayRuntime mItem_HUD_2;
        public DungeonRun.GumRuntimes.ItemDisplayRuntime Item_HUD_2
        {
            get
            {
                return mItem_HUD_2;
            }
            private set
            {
                mItem_HUD_2 = value;
            }
        }
        private DungeonRun.GumRuntimes.LevelDisplayRuntime LevelDisplay_HUD;
        private DungeonRun.GumRuntimes.Name_DisplayRuntime Name_Display_HUD;
        private DungeonRun.GumRuntimes.TimeDisplayRuntime TimeDisplay_HUD;
        private DungeonRun.GumRuntimes.PlayerPoints_displayRuntime mPlayerPoints_displayInstance;
        public DungeonRun.GumRuntimes.PlayerPoints_displayRuntime PlayerPoints_displayInstance
        {
            get
            {
                return mPlayerPoints_displayInstance;
            }
            private set
            {
                mPlayerPoints_displayInstance = value;
            }
        }
        private DungeonRun.GumRuntimes.Panel_DisplayRuntime Panel_DisplayInstance;
        private DungeonRun.GumRuntimes.ButtonRuntime Ranking_Btn_VoltarInicio;
        private DungeonRun.GumRuntimes.Panel2_DisplayRuntime mControle_DisplayInstance;
        public DungeonRun.GumRuntimes.Panel2_DisplayRuntime Controle_DisplayInstance
        {
            get
            {
                return mControle_DisplayInstance;
            }
            private set
            {
                mControle_DisplayInstance = value;
            }
        }
        private DungeonRun.GumRuntimes.GameOver_DisplayRuntime GameOver_DisplayInstance;
        private DungeonRun.GumRuntimes.ButtonRuntime GameOver_btn_Ranking;
        private DungeonRun.GumRuntimes.ButtonRuntime GameOver_Btn_Voltarinicio;
        private FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.DashBar> dashBarList;
        private DungeonRun.GumRuntimes.SpriteRuntime BloodIntance;
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundClick;
        private DungeonRun.GumRuntimes.DialogBoxRuntime DialogBoxInstance;
        private DungeonRun.GumRuntimes.ButtonRuntime btn_GameOver_Points;
        private Microsoft.Xna.Framework.Graphics.Texture2D AvatarGui;
        public float HealthBarY = 250f;
        public event FlatRedBall.Gui.WindowEvent Btn_VoltarInicioClick;
        public event FlatRedBall.Gui.WindowEvent GameOver_btn_RankingClick;
        public event FlatRedBall.Gui.WindowEvent GameOver_Btn_VoltarinicioClick;
        public event FlatRedBall.Gui.WindowEvent btn_GameOver_PointsClick;
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Hud () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Hud (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Hud (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            SoundClick = click.CreateInstance();
            AvatarGui = GuiliandriAvatar;
            HealthBarList = new FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.HealthBar>();
            HealthBarList.Name = "HealthBarList";
            Item_HUD_0 = GameHudGum.GetGraphicalUiElementByName("Item_HUD_0") as DungeonRun.GumRuntimes.ItemDisplayRuntime;
            mItem_HUD_1 = GameHudGum.GetGraphicalUiElementByName("Item_HUD_1") as DungeonRun.GumRuntimes.ItemDisplayRuntime;
            mItem_HUD_2 = GameHudGum.GetGraphicalUiElementByName("Item_HUD_2") as DungeonRun.GumRuntimes.ItemDisplayRuntime;
            LevelDisplay_HUD = GameHudGum.GetGraphicalUiElementByName("LevelDisplay_HUD") as DungeonRun.GumRuntimes.LevelDisplayRuntime;
            Name_Display_HUD = GameHudGum.GetGraphicalUiElementByName("Name_DisplayInstance") as DungeonRun.GumRuntimes.Name_DisplayRuntime;
            TimeDisplay_HUD = GameHudGum.GetGraphicalUiElementByName("TimeDisplay") as DungeonRun.GumRuntimes.TimeDisplayRuntime;
            mPlayerPoints_displayInstance = PlayPoints_HUD.GetGraphicalUiElementByName("PlayerPoints_displayInstance") as DungeonRun.GumRuntimes.PlayerPoints_displayRuntime;
            Panel_DisplayInstance = RakingHUD.GetGraphicalUiElementByName("Panel_DisplayInstance") as DungeonRun.GumRuntimes.Panel_DisplayRuntime;
            Ranking_Btn_VoltarInicio = RakingHUD.GetGraphicalUiElementByName("ranking_VoltarInicio") as DungeonRun.GumRuntimes.ButtonRuntime;
            mControle_DisplayInstance = GameHudGum.GetGraphicalUiElementByName("Controle_DisplayInstance") as DungeonRun.GumRuntimes.Panel2_DisplayRuntime;
            GameOver_DisplayInstance = GameHudGum.GetGraphicalUiElementByName("GameOver_DisplayInstance") as DungeonRun.GumRuntimes.GameOver_DisplayRuntime;
            GameOver_btn_Ranking = GameHudGum.GetGraphicalUiElementByName("btn_Ranking") as DungeonRun.GumRuntimes.ButtonRuntime;
            GameOver_Btn_Voltarinicio = GameHudGum.GetGraphicalUiElementByName("Btn_VoltarInicio") as DungeonRun.GumRuntimes.ButtonRuntime;
            dashBarList = new FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.DashBar>();
            dashBarList.Name = "dashBarList";
            BloodIntance = GameHudGum.GetGraphicalUiElementByName("BloodIntance") as DungeonRun.GumRuntimes.SpriteRuntime;
            DialogBoxInstance = GameHudGum.GetGraphicalUiElementByName("DialogBoxInstance") as DungeonRun.GumRuntimes.DialogBoxRuntime;
            btn_GameOver_Points = GameHudGum.GetGraphicalUiElementByName("btn_GameOver_Points") as DungeonRun.GumRuntimes.ButtonRuntime;
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            Item_HUD_0.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            mItem_HUD_1.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            mItem_HUD_2.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            LevelDisplay_HUD.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            Name_Display_HUD.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            TimeDisplay_HUD.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            mPlayerPoints_displayInstance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            Panel_DisplayInstance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            Ranking_Btn_VoltarInicio.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            mControle_DisplayInstance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            GameOver_DisplayInstance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            GameOver_btn_Ranking.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            GameOver_Btn_Voltarinicio.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            BloodIntance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            DialogBoxInstance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            btn_GameOver_Points.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            Item_HUD_0.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            mItem_HUD_1.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            mItem_HUD_2.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            LevelDisplay_HUD.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            Name_Display_HUD.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            TimeDisplay_HUD.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            mPlayerPoints_displayInstance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            Panel_DisplayInstance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            Ranking_Btn_VoltarInicio.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            mControle_DisplayInstance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            GameOver_DisplayInstance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            GameOver_btn_Ranking.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            GameOver_Btn_Voltarinicio.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            BloodIntance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            DialogBoxInstance.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            btn_GameOver_Points.AddToManagers(RenderingLibrary.SystemManagers.Default, System.Linq.Enumerable.FirstOrDefault(FlatRedBall.Gum.GumIdb.AllGumLayersOnFrbLayer(LayerProvidedByContainer)));
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            for (int i = HealthBarList.Count - 1; i > -1; i--)
            {
                if (i < HealthBarList.Count)
                {
                    // We do the extra if-check because activity could destroy any number of entities
                    HealthBarList[i].Activity();
                }
            }
            for (int i = dashBarList.Count - 1; i > -1; i--)
            {
                if (i < dashBarList.Count)
                {
                    // We do the extra if-check because activity could destroy any number of entities
                    dashBarList[i].Activity();
                }
            }
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            if (mPlayPoints_HUD != null)
            {
            }
            if (mRakingHUD != null)
            {
            }
            
            HealthBarList.MakeOneWay();
            dashBarList.MakeOneWay();
            for (int i = HealthBarList.Count - 1; i > -1; i--)
            {
                HealthBarList[i].Destroy();
            }
            if (Item_HUD_0 != null)
            {
                Item_HUD_0.RemoveFromManagers();
            }
            if (Item_HUD_1 != null)
            {
                Item_HUD_1.RemoveFromManagers();
            }
            if (Item_HUD_2 != null)
            {
                Item_HUD_2.RemoveFromManagers();
            }
            if (LevelDisplay_HUD != null)
            {
                LevelDisplay_HUD.RemoveFromManagers();
            }
            if (Name_Display_HUD != null)
            {
                Name_Display_HUD.RemoveFromManagers();
            }
            if (TimeDisplay_HUD != null)
            {
                TimeDisplay_HUD.RemoveFromManagers();
            }
            if (PlayerPoints_displayInstance != null)
            {
                PlayerPoints_displayInstance.RemoveFromManagers();
            }
            if (Panel_DisplayInstance != null)
            {
                Panel_DisplayInstance.RemoveFromManagers();
            }
            if (Ranking_Btn_VoltarInicio != null)
            {
                Ranking_Btn_VoltarInicio.RemoveFromManagers();
            }
            if (Controle_DisplayInstance != null)
            {
                Controle_DisplayInstance.RemoveFromManagers();
            }
            if (GameOver_DisplayInstance != null)
            {
                GameOver_DisplayInstance.RemoveFromManagers();
            }
            if (GameOver_btn_Ranking != null)
            {
                GameOver_btn_Ranking.RemoveFromManagers();
            }
            if (GameOver_Btn_Voltarinicio != null)
            {
                GameOver_Btn_Voltarinicio.RemoveFromManagers();
            }
            for (int i = dashBarList.Count - 1; i > -1; i--)
            {
                dashBarList[i].Destroy();
            }
            if (BloodIntance != null)
            {
                BloodIntance.RemoveFromManagers();
            }
            if (DialogBoxInstance != null)
            {
                DialogBoxInstance.RemoveFromManagers();
            }
            if (btn_GameOver_Points != null)
            {
                btn_GameOver_Points.RemoveFromManagers();
            }
            HealthBarList.MakeTwoWay();
            dashBarList.MakeTwoWay();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            Ranking_Btn_VoltarInicio.Click += OnBtn_VoltarInicioClick;
            Ranking_Btn_VoltarInicio.Click += OnBtn_VoltarInicioClickTunnel;
            GameOver_btn_Ranking.Click += OnGameOver_btn_RankingClick;
            GameOver_btn_Ranking.Click += OnGameOver_btn_RankingClickTunnel;
            GameOver_Btn_Voltarinicio.Click += OnGameOver_Btn_VoltarinicioClick;
            GameOver_Btn_Voltarinicio.Click += OnGameOver_Btn_VoltarinicioClickTunnel;
            btn_GameOver_Points.Click += Onbtn_GameOver_PointsClick;
            btn_GameOver_Points.Click += Onbtn_GameOver_PointsClickTunnel;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            for (int i = HealthBarList.Count - 1; i > -1; i--)
            {
                HealthBarList[i].Destroy();
            }
            if (Item_HUD_0 != null)
            {
                Item_HUD_0.RemoveFromManagers();
            }
            if (Item_HUD_1 != null)
            {
                Item_HUD_1.RemoveFromManagers();
            }
            if (Item_HUD_2 != null)
            {
                Item_HUD_2.RemoveFromManagers();
            }
            if (LevelDisplay_HUD != null)
            {
                LevelDisplay_HUD.RemoveFromManagers();
            }
            if (Name_Display_HUD != null)
            {
                Name_Display_HUD.RemoveFromManagers();
            }
            if (TimeDisplay_HUD != null)
            {
                TimeDisplay_HUD.RemoveFromManagers();
            }
            if (PlayerPoints_displayInstance != null)
            {
                PlayerPoints_displayInstance.RemoveFromManagers();
            }
            if (Panel_DisplayInstance != null)
            {
                Panel_DisplayInstance.RemoveFromManagers();
            }
            if (Ranking_Btn_VoltarInicio != null)
            {
                Ranking_Btn_VoltarInicio.RemoveFromManagers();
            }
            if (Controle_DisplayInstance != null)
            {
                Controle_DisplayInstance.RemoveFromManagers();
            }
            if (GameOver_DisplayInstance != null)
            {
                GameOver_DisplayInstance.RemoveFromManagers();
            }
            if (GameOver_btn_Ranking != null)
            {
                GameOver_btn_Ranking.RemoveFromManagers();
            }
            if (GameOver_Btn_Voltarinicio != null)
            {
                GameOver_Btn_Voltarinicio.RemoveFromManagers();
            }
            for (int i = dashBarList.Count - 1; i > -1; i--)
            {
                dashBarList[i].Destroy();
            }
            if (BloodIntance != null)
            {
                BloodIntance.RemoveFromManagers();
            }
            if (DialogBoxInstance != null)
            {
                DialogBoxInstance.RemoveFromManagers();
            }
            if (btn_GameOver_Points != null)
            {
                btn_GameOver_Points.RemoveFromManagers();
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            if (Parent == null)
            {
                X = 0f;
            }
            else
            {
                RelativeX = 0f;
            }
            if (Parent == null)
            {
                Y = 0f;
            }
            else
            {
                RelativeY = 0f;
            }
            if (Parent == null)
            {
                Z = 30f;
            }
            else if (Parent is FlatRedBall.Camera)
            {
                RelativeZ = 30f - 40.0f;
            }
            else
            {
                RelativeZ = 30f;
            }
            HealthBarY = 250f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            for (int i = 0; i < HealthBarList.Count; i++)
            {
                HealthBarList[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < dashBarList.Count; i++)
            {
                dashBarList[i].ConvertToManuallyUpdated();
            }
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("HudStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Gum.GumIdb>(@"content/gumproject/screens/gamehudgum.gusx", ContentManagerName))
                {
                    registerUnload = true;
                }
                Gum.Wireframe.GraphicalUiElement.IsAllLayoutSuspended = true;  GameHudGum = new FlatRedBall.Gum.GumIdb();  GameHudGum.LoadFromFile("content/gumproject/screens/gamehudgum.gusx");  GameHudGum.AssignReferences();Gum.Wireframe.GraphicalUiElement.IsAllLayoutSuspended = false; GameHudGum.Element.UpdateLayout(); GameHudGum.Element.UpdateLayout();
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hud/medium_box.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                medium_box = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hud/medium_box.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hud/full_heart.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                full_heart = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hud/full_heart.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hud/empty_heart.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                empty_heart = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hud/empty_heart.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hud/galvisr.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Galvisr = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hud/galvisr.png", ContentManagerName);
                click = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/item/click", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hud/guiliandriavatar.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                GuiliandriAvatar = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hud/guiliandriavatar.png", ContentManagerName);
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("HudStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (GameHudGum != null)
                {
                    FlatRedBall.SpriteManager.RemoveDrawableBatch(GameHudGum); FlatRedBall.FlatRedBallServices.GraphicsOptions.SizeOrOrientationChanged -= GameHudGum.HandleResolutionChanged;
                    GameHudGum= null;
                }
                if (medium_box != null)
                {
                    medium_box= null;
                }
                if (full_heart != null)
                {
                    full_heart= null;
                }
                if (empty_heart != null)
                {
                    empty_heart= null;
                }
                if (mPlayPoints_HUD != null)
                {
                    FlatRedBall.SpriteManager.RemoveDrawableBatch(mPlayPoints_HUD); FlatRedBall.FlatRedBallServices.GraphicsOptions.SizeOrOrientationChanged -= mPlayPoints_HUD.HandleResolutionChanged;
                    mPlayPoints_HUD= null;
                }
                if (mRakingHUD != null)
                {
                    FlatRedBall.SpriteManager.RemoveDrawableBatch(mRakingHUD); FlatRedBall.FlatRedBallServices.GraphicsOptions.SizeOrOrientationChanged -= mRakingHUD.HandleResolutionChanged;
                    mRakingHUD= null;
                }
                if (Galvisr != null)
                {
                    Galvisr= null;
                }
                if (click != null)
                {
                    click= null;
                }
                if (GuiliandriAvatar != null)
                {
                    GuiliandriAvatar= null;
                }
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "GameHudGum":
                    return GameHudGum;
                case  "medium_box":
                    return medium_box;
                case  "full_heart":
                    return full_heart;
                case  "empty_heart":
                    return empty_heart;
                case  "PlayPoints_HUD":
                    return PlayPoints_HUD;
                case  "RakingHUD":
                    return RakingHUD;
                case  "Galvisr":
                    return Galvisr;
                case  "click":
                    return click;
                case  "GuiliandriAvatar":
                    return GuiliandriAvatar;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "GameHudGum":
                    return GameHudGum;
                case  "medium_box":
                    return medium_box;
                case  "full_heart":
                    return full_heart;
                case  "empty_heart":
                    return empty_heart;
                case  "PlayPoints_HUD":
                    return PlayPoints_HUD;
                case  "RakingHUD":
                    return RakingHUD;
                case  "Galvisr":
                    return Galvisr;
                case  "click":
                    return click;
                case  "GuiliandriAvatar":
                    return GuiliandriAvatar;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "GameHudGum":
                    return GameHudGum;
                case  "medium_box":
                    return medium_box;
                case  "full_heart":
                    return full_heart;
                case  "empty_heart":
                    return empty_heart;
                case  "PlayPoints_HUD":
                    return PlayPoints_HUD;
                case  "RakingHUD":
                    return RakingHUD;
                case  "Galvisr":
                    return Galvisr;
                case  "click":
                    return click;
                case  "GuiliandriAvatar":
                    return GuiliandriAvatar;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
            if (SoundClick.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundClick.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundClick.Resume()));
            }
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
