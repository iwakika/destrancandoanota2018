#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using DungeonRun.Performance;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
namespace DungeonRun.Entities
{
    public partial class Item : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable, FlatRedBall.Math.Geometry.ICollidable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        public enum VariableState
        {
            Uninitialized = 0, //This exists so that the first set call actually does something
            Unknown = 1, //This exists so that if the entity is actually a child entity and has set a child state, you will get this
            Key = 2, 
            FireBall = 3, 
            Key_Golden = 4
        }
        protected int mCurrentState = 0;
        public Entities.Item.VariableState CurrentState
        {
            get
            {
                if (mCurrentState >= 0 && mCurrentState <= 4)
                {
                    return (VariableState)mCurrentState;
                }
                else
                {
                    return VariableState.Unknown;
                }
            }
            set
            {
                mCurrentState = (int)value;
                switch(CurrentState)
                {
                    case  VariableState.Uninitialized:
                        break;
                    case  VariableState.Unknown:
                        break;
                    case  VariableState.Key:
                        SpriteInstanceTexture = key_silver;
                        IndexInventory = 1;
                        break;
                    case  VariableState.FireBall:
                        SpriteInstanceTexture = Fire_1;
                        itemQtd = 1;
                        IndexInventory = 2;
                        break;
                    case  VariableState.Key_Golden:
                        SpriteInstanceTexture = key_gold;
                        IndexInventory = 1;
                        break;
                }
            }
        }
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static Microsoft.Xna.Framework.Graphics.Texture2D key_silver;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D key_gold;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Fire_1;
        protected static Microsoft.Xna.Framework.Audio.SoundEffect click;
        
        private FlatRedBall.Sprite SpriteInstance;
        private FlatRedBall.Math.Geometry.Circle mCollisionInstance;
        public FlatRedBall.Math.Geometry.Circle CollisionInstance
        {
            get
            {
                return mCollisionInstance;
            }
            private set
            {
                mCollisionInstance = value;
            }
        }
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance mSoundItem;
        public Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundItem
        {
            get
            {
                return mSoundItem;
            }
            private set
            {
                mSoundItem = value;
            }
        }
        public Microsoft.Xna.Framework.Graphics.Texture2D SpriteInstanceTexture
        {
            get
            {
                return SpriteInstance.Texture;
            }
            set
            {
                SpriteInstance.Texture = value;
            }
        }
        public int itemQtd = 1;
        public int IndexInventory = 2;
        public int Index { get; set; }
        public bool Used { get; set; }
        private FlatRedBall.Math.Geometry.ShapeCollection mGeneratedCollision;
        public FlatRedBall.Math.Geometry.ShapeCollection Collision
        {
            get
            {
                return mGeneratedCollision;
            }
        }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Item () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Item (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Item (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            mSoundItem = click.CreateInstance();
            SpriteInstance = new FlatRedBall.Sprite();
            SpriteInstance.Name = "SpriteInstance";
            mCollisionInstance = new FlatRedBall.Math.Geometry.Circle();
            mCollisionInstance.Name = "mCollisionInstance";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionInstance, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionInstance, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.ItemFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            if (CollisionInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionInstance);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.CopyAbsoluteToRelative();
                SpriteInstance.AttachTo(this, false);
            }
            SpriteInstance.Texture = key_silver;
            SpriteInstance.TextureScale = 1f;
            if (mCollisionInstance.Parent == null)
            {
                mCollisionInstance.CopyAbsoluteToRelative();
                mCollisionInstance.AttachTo(this, false);
            }
            CollisionInstance.Radius = 10f;
            CollisionInstance.Visible = false;
            mGeneratedCollision = new FlatRedBall.Math.Geometry.ShapeCollection();
            mGeneratedCollision.Circles.AddOneWay(mCollisionInstance);
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            if (CollisionInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionInstance);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            SpriteInstance.Texture = key_silver;
            SpriteInstance.TextureScale = 1f;
            CollisionInstance.Radius = 10f;
            CollisionInstance.Visible = false;
            if (Parent == null)
            {
                Z = 5f;
            }
            else if (Parent is FlatRedBall.Camera)
            {
                RelativeZ = 5f - 40.0f;
            }
            else
            {
                RelativeZ = 5f;
            }
            SpriteInstanceTexture = key_silver;
            itemQtd = 1;
            IndexInventory = 2;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteInstance);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ItemStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/key_silver.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                key_silver = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/key_silver.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/key_gold.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                key_gold = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/key_gold.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/item/fire_1.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Fire_1 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/item/fire_1.png", ContentManagerName);
                click = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/item/click", ContentManagerName);
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ItemStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (key_silver != null)
                {
                    key_silver= null;
                }
                if (key_gold != null)
                {
                    key_gold= null;
                }
                if (Fire_1 != null)
                {
                    Fire_1= null;
                }
                if (click != null)
                {
                    click= null;
                }
            }
        }
        static VariableState mLoadingState = VariableState.Uninitialized;
        public static VariableState LoadingState
        {
            get
            {
                return mLoadingState;
            }
            set
            {
                mLoadingState = value;
            }
        }
        public FlatRedBall.Instructions.Instruction InterpolateToState (VariableState stateToInterpolateTo, double secondsToTake) 
        {
            switch(stateToInterpolateTo)
            {
                case  VariableState.Key:
                    break;
                case  VariableState.FireBall:
                    break;
                case  VariableState.Key_Golden:
                    break;
            }
            var instruction = new FlatRedBall.Instructions.DelegateInstruction<VariableState>(StopStateInterpolation, stateToInterpolateTo);
            instruction.TimeToExecute = FlatRedBall.TimeManager.CurrentTime + secondsToTake;
            this.Instructions.Add(instruction);
            return instruction;
        }
        public void StopStateInterpolation (VariableState stateToStop) 
        {
            switch(stateToStop)
            {
                case  VariableState.Key:
                    break;
                case  VariableState.FireBall:
                    break;
                case  VariableState.Key_Golden:
                    break;
            }
            CurrentState = stateToStop;
        }
        public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
        {
            #if DEBUG
            if (float.IsNaN(interpolationValue))
            {
                throw new System.Exception("interpolationValue cannot be NaN");
            }
            #endif
            bool setIndexInventory = true;
            int IndexInventoryFirstValue= 0;
            int IndexInventorySecondValue= 0;
            bool setitemQtd = true;
            int itemQtdFirstValue= 0;
            int itemQtdSecondValue= 0;
            switch(firstState)
            {
                case  VariableState.Key:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = key_silver;
                    }
                    IndexInventoryFirstValue = 1;
                    break;
                case  VariableState.FireBall:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = Fire_1;
                    }
                    itemQtdFirstValue = 1;
                    IndexInventoryFirstValue = 2;
                    break;
                case  VariableState.Key_Golden:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = key_gold;
                    }
                    IndexInventoryFirstValue = 1;
                    break;
            }
            switch(secondState)
            {
                case  VariableState.Key:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = key_silver;
                    }
                    IndexInventorySecondValue = 1;
                    break;
                case  VariableState.FireBall:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = Fire_1;
                    }
                    itemQtdSecondValue = 1;
                    IndexInventorySecondValue = 2;
                    break;
                case  VariableState.Key_Golden:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = key_gold;
                    }
                    IndexInventorySecondValue = 1;
                    break;
            }
            if (setIndexInventory)
            {
                IndexInventory = FlatRedBall.Math.MathFunctions.RoundToInt(IndexInventoryFirstValue* (1 - interpolationValue) + IndexInventorySecondValue * interpolationValue);
            }
            if (setitemQtd)
            {
                itemQtd = FlatRedBall.Math.MathFunctions.RoundToInt(itemQtdFirstValue* (1 - interpolationValue) + itemQtdSecondValue * interpolationValue);
            }
            if (interpolationValue < 1)
            {
                mCurrentState = (int)firstState;
            }
            else
            {
                mCurrentState = (int)secondState;
            }
        }
        public static void PreloadStateContent (VariableState state, string contentManagerName) 
        {
            ContentManagerName = contentManagerName;
            switch(state)
            {
                case  VariableState.Key:
                    {
                        object throwaway = key_silver;
                    }
                    break;
                case  VariableState.FireBall:
                    {
                        object throwaway = Fire_1;
                    }
                    break;
                case  VariableState.Key_Golden:
                    {
                        object throwaway = key_gold;
                    }
                    break;
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "key_silver":
                    return key_silver;
                case  "key_gold":
                    return key_gold;
                case  "Fire_1":
                    return Fire_1;
                case  "click":
                    return click;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "key_silver":
                    return key_silver;
                case  "key_gold":
                    return key_gold;
                case  "Fire_1":
                    return Fire_1;
                case  "click":
                    return click;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "key_silver":
                    return key_silver;
                case  "key_gold":
                    return key_gold;
                case  "Fire_1":
                    return Fire_1;
                case  "click":
                    return click;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
            if (SoundItem.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundItem.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundItem.Resume()));
            }
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(SpriteInstance);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CollisionInstance);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(SpriteInstance);
            }
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CollisionInstance);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CollisionInstance, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
