using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework;
using DungeonRun.MinhasClasses.Data;
using DungeonRun.Factories;

namespace DungeonRun.Entities
{
	public partial class ItemSpawer
	{
        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
        public double mLastSpawnTime { set; get; }
        public FlatRedBall.Math.PositionedObjectList<Item> mItemList;
        public FlatRedBall.Math.PositionedObjectList<Item> ItemList
        {
            set
            {
                mItemList = value;

            }
            get
            {
                return mItemList;
            }


        }
        public String outroLevel = "";


        bool IsTimeToSpawn
        {
            get
            {
                float spawnFrequency = 1 / SpawerPerSecond;
                return (GlobalData.currentLevel.levelTime - mLastSpawnTime) > spawnFrequency;
                
            }
        }

        private bool Initialize = false;  





        private void CustomInitialize()
        {
            
            
        }

        private void CustomActivity()
        {
            if (!Initialize) { 

                for (int i = 0; i < GlobalData.currentLevel.FireBallInitialize; i++)
                {
                    createItem(EnumItem.FIREBALL);
                }
                Initialize = true;
             }


            if (GlobalData.currentLevel.EnableSpawerItem)
            {

                if (IsTimeToSpawn)
                {
                    if (ItemList.Count <= GlobalData.currentLevel.FireBallSpawerMax)
                    {
                        createItem(EnumItem.FIREBALL);
                    }
                    else
                    {
                        mLastSpawnTime = GlobalData.currentLevel.levelTime;
                    }
                    //movementMonster();    
                }
            }


        }


        /// <summary>
        /// Cria o item em um tile aleat�rio do mapa
        /// </summary>
        public void createItem(EnumItem itemType)
        {//
            switch (itemType)
            {
                case EnumItem.FIREBALL:

                    Vector3 position = getFireBallPosition();
                    Item fireball = ItemFactory.CreateNew();
                    fireball.Position = position;
                    fireball.itemType = EnumItem.FIREBALL;
                    fireball.CurrentState = Item.VariableState.FireBall;
                       

                    break;

            }

            mLastSpawnTime = GlobalData.currentLevel.levelTime;            


        }

        /// <summary>
        ///Busca a posi��o atual para instaciar o item
        /// </summary>
        private Vector3 getFireBallPosition()
        {
            Vector3 position = new Vector3();
            Vector2 randomPosition = GlobalData.currentLevel.RandomGroundTilePosition();
            position.X = randomPosition.X;
            position.Y = randomPosition.Y;
            position.Z = 5;

            return position;

        }

        private void CustomDestroy()
        {

        }

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
    }
}
