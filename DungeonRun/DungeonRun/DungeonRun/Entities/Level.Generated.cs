#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using DungeonRun.Performance;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
namespace DungeonRun.Entities
{
    public partial class Level : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        public enum VariableState
        {
            Uninitialized = 0, //This exists so that the first set call actually does something
            Unknown = 1, //This exists so that if the entity is actually a child entity and has set a child state, you will get this
            level0 = 2, 
            level1 = 3, 
            level2 = 4, 
            level3 = 5, 
            levelBOSS = 6
        }
        protected int mCurrentState = 0;
        public Entities.Level.VariableState CurrentState
        {
            get
            {
                if (mCurrentState >= 0 && mCurrentState <= 6)
                {
                    return (VariableState)mCurrentState;
                }
                else
                {
                    return VariableState.Unknown;
                }
            }
            set
            {
                mCurrentState = (int)value;
                switch(CurrentState)
                {
                    case  VariableState.Uninitialized:
                        break;
                    case  VariableState.Unknown:
                        break;
                    case  VariableState.level0:
                        keyMin = 1;
                        keyMax = 1;
                        keyPathMinDistance = 10;
                        FireBallSpawerMax = 1;
                        MonsterSpawerMax = 0;
                        monsterInicialize = 0;
                        hasSpider = false;
                        hasSkeleton = false;
                        hasOgro = false;
                        hasTroll = false;
                        hasNpc = true;
                        isLevelBoss = false;
                        hasGalvis = false;
                        hasGuiliandri = false;
                        EnableSpawer = false;
                        EnableSpawerItem = false;
                        FireBallInitialize = 2;
                        break;
                    case  VariableState.level1:
                        keyMin = 1;
                        keyMax = 2;
                        FireBallSpawerMax = 3;
                        MonsterSpawerMax = 8;
                        monsterInicialize = 3;
                        hasSpider = true;
                        hasSkeleton = false;
                        hasOgro = false;
                        hasTroll = false;
                        hasNpc = false;
                        isLevelBoss = false;
                        hasGalvis = false;
                        hasGuiliandri = false;
                        EnableSpawer = true;
                        EnableSpawerItem = true;
                        break;
                    case  VariableState.level2:
                        keyMin = 1;
                        keyMax = 3;
                        FireBallSpawerMax = 4;
                        MonsterSpawerMax = 9;
                        monsterInicialize = 5;
                        hasSpider = true;
                        hasSkeleton = true;
                        hasOgro = false;
                        hasTroll = false;
                        hasNpc = false;
                        isLevelBoss = false;
                        hasGalvis = false;
                        hasGuiliandri = false;
                        EnableSpawer = true;
                        EnableSpawerItem = true;
                        break;
                    case  VariableState.level3:
                        keyMin = 2;
                        keyMax = 3;
                        keyPathMinDistance = 20;
                        FireBallSpawerMax = 4;
                        MonsterSpawerMax = 13;
                        monsterInicialize = 8;
                        hasSpider = true;
                        hasSkeleton = true;
                        hasOgro = true;
                        hasTroll = false;
                        hasNpc = false;
                        isLevelBoss = false;
                        hasGalvis = false;
                        hasGuiliandri = false;
                        EnableSpawer = true;
                        EnableSpawerItem = true;
                        break;
                    case  VariableState.levelBOSS:
                        keyMin = 0;
                        keyMax = 0;
                        keyPathMinDistance = 0;
                        FireBallSpawerMax = 5;
                        MonsterSpawerMax = 0;
                        monsterInicialize = 1;
                        hasSpider = false;
                        hasSkeleton = false;
                        hasOgro = false;
                        hasTroll = false;
                        hasNpc = false;
                        isLevelBoss = true;
                        hasGalvis = false;
                        hasGuiliandri = false;
                        EnableSpawer = false;
                        EnableSpawerItem = true;
                        FireBallInitialize = 5;
                        break;
                }
            }
        }
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        
        public string name;
        public int keyMin = 1;
        public int keyMax = 2;
        public int keyPathMinDistance = 10;
        public int FireBallSpawerMax = 2;
        public int MonsterSpawerMax = 25;
        public int monsterInicialize = 5;
        public bool hasSpider = true;
        public bool hasSkeleton = true;
        public bool hasOgro = true;
        public bool hasTroll = false;
        public bool hasNpc;
        public bool isLevelBoss;
        public bool hasGalvis = false;
        public bool hasGuiliandri = false;
        public bool EnableSpawer = true;
        public bool EnableSpawerItem = true;
        public int FireBallInitialize = 1;
        public int Index { get; set; }
        public bool Used { get; set; }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Level () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Level (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Level (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.LevelFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            keyMin = 1;
            keyMax = 2;
            keyPathMinDistance = 10;
            FireBallSpawerMax = 2;
            MonsterSpawerMax = 25;
            monsterInicialize = 5;
            hasSpider = true;
            hasSkeleton = true;
            hasOgro = true;
            hasTroll = false;
            hasGalvis = false;
            hasGuiliandri = false;
            EnableSpawer = true;
            EnableSpawerItem = true;
            FireBallInitialize = 1;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("LevelStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("LevelStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
            }
        }
        static VariableState mLoadingState = VariableState.Uninitialized;
        public static VariableState LoadingState
        {
            get
            {
                return mLoadingState;
            }
            set
            {
                mLoadingState = value;
            }
        }
        public FlatRedBall.Instructions.Instruction InterpolateToState (VariableState stateToInterpolateTo, double secondsToTake) 
        {
            switch(stateToInterpolateTo)
            {
                case  VariableState.level0:
                    break;
                case  VariableState.level1:
                    break;
                case  VariableState.level2:
                    break;
                case  VariableState.level3:
                    break;
                case  VariableState.levelBOSS:
                    break;
            }
            var instruction = new FlatRedBall.Instructions.DelegateInstruction<VariableState>(StopStateInterpolation, stateToInterpolateTo);
            instruction.TimeToExecute = FlatRedBall.TimeManager.CurrentTime + secondsToTake;
            this.Instructions.Add(instruction);
            return instruction;
        }
        public void StopStateInterpolation (VariableState stateToStop) 
        {
            switch(stateToStop)
            {
                case  VariableState.level0:
                    break;
                case  VariableState.level1:
                    break;
                case  VariableState.level2:
                    break;
                case  VariableState.level3:
                    break;
                case  VariableState.levelBOSS:
                    break;
            }
            CurrentState = stateToStop;
        }
        public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
        {
            #if DEBUG
            if (float.IsNaN(interpolationValue))
            {
                throw new System.Exception("interpolationValue cannot be NaN");
            }
            #endif
            bool setkeyMin = true;
            int keyMinFirstValue= 0;
            int keyMinSecondValue= 0;
            bool setkeyMax = true;
            int keyMaxFirstValue= 0;
            int keyMaxSecondValue= 0;
            bool setkeyPathMinDistance = true;
            int keyPathMinDistanceFirstValue= 0;
            int keyPathMinDistanceSecondValue= 0;
            bool setFireBallSpawerMax = true;
            int FireBallSpawerMaxFirstValue= 0;
            int FireBallSpawerMaxSecondValue= 0;
            bool setMonsterSpawerMax = true;
            int MonsterSpawerMaxFirstValue= 0;
            int MonsterSpawerMaxSecondValue= 0;
            bool setmonsterInicialize = true;
            int monsterInicializeFirstValue= 0;
            int monsterInicializeSecondValue= 0;
            bool setFireBallInitialize = true;
            int FireBallInitializeFirstValue= 0;
            int FireBallInitializeSecondValue= 0;
            switch(firstState)
            {
                case  VariableState.level0:
                    keyMinFirstValue = 1;
                    keyMaxFirstValue = 1;
                    keyPathMinDistanceFirstValue = 10;
                    FireBallSpawerMaxFirstValue = 1;
                    MonsterSpawerMaxFirstValue = 0;
                    monsterInicializeFirstValue = 0;
                    if (interpolationValue < 1)
                    {
                        this.hasSpider = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasSkeleton = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasOgro = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasTroll = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasNpc = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isLevelBoss = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGalvis = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGuiliandri = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.EnableSpawer = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.EnableSpawerItem = false;
                    }
                    FireBallInitializeFirstValue = 2;
                    break;
                case  VariableState.level1:
                    keyMinFirstValue = 1;
                    keyMaxFirstValue = 2;
                    FireBallSpawerMaxFirstValue = 3;
                    MonsterSpawerMaxFirstValue = 8;
                    monsterInicializeFirstValue = 3;
                    if (interpolationValue < 1)
                    {
                        this.hasSpider = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasSkeleton = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasOgro = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasTroll = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasNpc = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isLevelBoss = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGalvis = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGuiliandri = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.EnableSpawer = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.EnableSpawerItem = true;
                    }
                    break;
                case  VariableState.level2:
                    keyMinFirstValue = 1;
                    keyMaxFirstValue = 3;
                    FireBallSpawerMaxFirstValue = 4;
                    MonsterSpawerMaxFirstValue = 9;
                    monsterInicializeFirstValue = 5;
                    if (interpolationValue < 1)
                    {
                        this.hasSpider = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasSkeleton = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasOgro = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasTroll = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasNpc = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isLevelBoss = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGalvis = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGuiliandri = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.EnableSpawer = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.EnableSpawerItem = true;
                    }
                    break;
                case  VariableState.level3:
                    keyMinFirstValue = 2;
                    keyMaxFirstValue = 3;
                    keyPathMinDistanceFirstValue = 20;
                    FireBallSpawerMaxFirstValue = 4;
                    MonsterSpawerMaxFirstValue = 13;
                    monsterInicializeFirstValue = 8;
                    if (interpolationValue < 1)
                    {
                        this.hasSpider = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasSkeleton = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasOgro = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasTroll = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasNpc = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isLevelBoss = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGalvis = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGuiliandri = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.EnableSpawer = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.EnableSpawerItem = true;
                    }
                    break;
                case  VariableState.levelBOSS:
                    keyMinFirstValue = 0;
                    keyMaxFirstValue = 0;
                    keyPathMinDistanceFirstValue = 0;
                    FireBallSpawerMaxFirstValue = 5;
                    MonsterSpawerMaxFirstValue = 0;
                    monsterInicializeFirstValue = 1;
                    if (interpolationValue < 1)
                    {
                        this.hasSpider = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasSkeleton = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasOgro = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasTroll = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasNpc = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isLevelBoss = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGalvis = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGuiliandri = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.EnableSpawer = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.EnableSpawerItem = true;
                    }
                    FireBallInitializeFirstValue = 5;
                    break;
            }
            switch(secondState)
            {
                case  VariableState.level0:
                    keyMinSecondValue = 1;
                    keyMaxSecondValue = 1;
                    keyPathMinDistanceSecondValue = 10;
                    FireBallSpawerMaxSecondValue = 1;
                    MonsterSpawerMaxSecondValue = 0;
                    monsterInicializeSecondValue = 0;
                    if (interpolationValue >= 1)
                    {
                        this.hasSpider = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasSkeleton = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasOgro = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasTroll = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasNpc = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isLevelBoss = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGalvis = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGuiliandri = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.EnableSpawer = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.EnableSpawerItem = false;
                    }
                    FireBallInitializeSecondValue = 2;
                    break;
                case  VariableState.level1:
                    keyMinSecondValue = 1;
                    keyMaxSecondValue = 2;
                    FireBallSpawerMaxSecondValue = 3;
                    MonsterSpawerMaxSecondValue = 8;
                    monsterInicializeSecondValue = 3;
                    if (interpolationValue >= 1)
                    {
                        this.hasSpider = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasSkeleton = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasOgro = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasTroll = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasNpc = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isLevelBoss = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGalvis = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGuiliandri = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.EnableSpawer = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.EnableSpawerItem = true;
                    }
                    break;
                case  VariableState.level2:
                    keyMinSecondValue = 1;
                    keyMaxSecondValue = 3;
                    FireBallSpawerMaxSecondValue = 4;
                    MonsterSpawerMaxSecondValue = 9;
                    monsterInicializeSecondValue = 5;
                    if (interpolationValue >= 1)
                    {
                        this.hasSpider = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasSkeleton = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasOgro = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasTroll = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasNpc = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isLevelBoss = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGalvis = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGuiliandri = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.EnableSpawer = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.EnableSpawerItem = true;
                    }
                    break;
                case  VariableState.level3:
                    keyMinSecondValue = 2;
                    keyMaxSecondValue = 3;
                    keyPathMinDistanceSecondValue = 20;
                    FireBallSpawerMaxSecondValue = 4;
                    MonsterSpawerMaxSecondValue = 13;
                    monsterInicializeSecondValue = 8;
                    if (interpolationValue >= 1)
                    {
                        this.hasSpider = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasSkeleton = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasOgro = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasTroll = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasNpc = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isLevelBoss = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGalvis = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGuiliandri = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.EnableSpawer = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.EnableSpawerItem = true;
                    }
                    break;
                case  VariableState.levelBOSS:
                    keyMinSecondValue = 0;
                    keyMaxSecondValue = 0;
                    keyPathMinDistanceSecondValue = 0;
                    FireBallSpawerMaxSecondValue = 5;
                    MonsterSpawerMaxSecondValue = 0;
                    monsterInicializeSecondValue = 1;
                    if (interpolationValue >= 1)
                    {
                        this.hasSpider = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasSkeleton = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasOgro = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasTroll = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasNpc = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isLevelBoss = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGalvis = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGuiliandri = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.EnableSpawer = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.EnableSpawerItem = true;
                    }
                    FireBallInitializeSecondValue = 5;
                    break;
            }
            if (setkeyMin)
            {
                keyMin = FlatRedBall.Math.MathFunctions.RoundToInt(keyMinFirstValue* (1 - interpolationValue) + keyMinSecondValue * interpolationValue);
            }
            if (setkeyMax)
            {
                keyMax = FlatRedBall.Math.MathFunctions.RoundToInt(keyMaxFirstValue* (1 - interpolationValue) + keyMaxSecondValue * interpolationValue);
            }
            if (setkeyPathMinDistance)
            {
                keyPathMinDistance = FlatRedBall.Math.MathFunctions.RoundToInt(keyPathMinDistanceFirstValue* (1 - interpolationValue) + keyPathMinDistanceSecondValue * interpolationValue);
            }
            if (setFireBallSpawerMax)
            {
                FireBallSpawerMax = FlatRedBall.Math.MathFunctions.RoundToInt(FireBallSpawerMaxFirstValue* (1 - interpolationValue) + FireBallSpawerMaxSecondValue * interpolationValue);
            }
            if (setMonsterSpawerMax)
            {
                MonsterSpawerMax = FlatRedBall.Math.MathFunctions.RoundToInt(MonsterSpawerMaxFirstValue* (1 - interpolationValue) + MonsterSpawerMaxSecondValue * interpolationValue);
            }
            if (setmonsterInicialize)
            {
                monsterInicialize = FlatRedBall.Math.MathFunctions.RoundToInt(monsterInicializeFirstValue* (1 - interpolationValue) + monsterInicializeSecondValue * interpolationValue);
            }
            if (setFireBallInitialize)
            {
                FireBallInitialize = FlatRedBall.Math.MathFunctions.RoundToInt(FireBallInitializeFirstValue* (1 - interpolationValue) + FireBallInitializeSecondValue * interpolationValue);
            }
            if (interpolationValue < 1)
            {
                mCurrentState = (int)firstState;
            }
            else
            {
                mCurrentState = (int)secondState;
            }
        }
        public static void PreloadStateContent (VariableState state, string contentManagerName) 
        {
            ContentManagerName = contentManagerName;
            switch(state)
            {
                case  VariableState.level0:
                    break;
                case  VariableState.level1:
                    break;
                case  VariableState.level2:
                    break;
                case  VariableState.level3:
                    break;
                case  VariableState.levelBOSS:
                    break;
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
