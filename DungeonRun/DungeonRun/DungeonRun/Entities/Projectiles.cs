using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework;
using DungeonRun.MinhasClasses.Data;
namespace DungeonRun.Entities
{
	public partial class Projectiles
	{


        public bool ishit { get; set; }
        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{
            //Console.WriteLine("PRJECTILE");
            ishit = false;

		}

		private void CustomActivity()
		{
            ActionAutoDestroy();

        }

		private void CustomDestroy()
		{
           

		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public static void createProjectiles(Projectiles.VariableState projectile, float origemX, float origemY, I2DInput ShotInput, Boolean isPlayAtack)
        {
            createProjectiles( projectile, origemX, origemY, ShotInput, isPlayAtack, 0, 0);
        }

        public static void createProjectiles(Projectiles.VariableState projectile, float origemX, float origemY, I2DInput ShotInput, Boolean isPlayAtack, float alvoX, float alvoY)
        {
            switch (projectile)
            {
                case Projectiles.VariableState.FireBall:
                    
                    
                    Projectiles shot = Factories.ProjectilesFactory.CreateNew();
                    shot.CurrentState = Projectiles.VariableState.FireBall;
                    shot.X = origemX;
                    shot.Y = origemY - 10;
                    shot.XVelocity = ShotInput.X * (shot.MovementSpeed);
                    shot.YVelocity = ShotInput.Y * (shot.MovementSpeed);
                    shot.isPlayAtack = isPlayAtack;
                    shot.SoundFire.Play();
                    
                    Vector3 v = new Vector3(shot.XVelocity, shot.YVelocity, 0);
                    v.Normalize();
         
                    float angulo = 90 * v.Y;
                    if (v.X != 0) {
                        angulo = 90 * (v.X - 1);
                   
                        if(v.Y > 0)
                        {
                            angulo = angulo * -1;
                          //  shot.CollisionWallInstance.Y *= -1;
                        }

                    }

                    /*if (v.X < 0 )
                    {
                        shot.CollisionWallInstanceY *=-1;
                    }
                    if(v.Y> 0)
                    {
                        shot.CollisionWallInstanceY *= -1;
                    }
                   */
                    
                    float radians = angulo * ((float) Math.PI / 180);

                   // Console.WriteLine(radians);
                    //Console.WriteLine(angulo);
                    shot.RotationZ = radians;
                   

                    break;

                case Projectiles.VariableState.AtackWater1:
                    shot = Factories.ProjectilesFactory.CreateNew();
                    shot.CurrentState = Projectiles.VariableState.AtackWater1;
                    shot.X = origemX;
                    shot.Y = origemY;
                    shot.Z = 10;
                    shot.isPlayAtack = isPlayAtack;
                    shot.SoundFlaunch.Play();
                   
                    break;

                case Projectiles.VariableState.SwordAtack:
                    shot = Factories.ProjectilesFactory.CreateNew();
                    shot.CurrentState = Projectiles.VariableState.SwordAtack;
                    Vector3 shotdirectionx = new Vector3(alvoX - origemX, alvoY - origemY, 0);
                    shot.X = origemX;
                    shot.Y = origemY;
                    shot.Z = GlobalData.PlayerData.playerInstance.Position.Z + 5;                    
                    shotdirectionx.Normalize();
                    shot.XVelocity = shotdirectionx.X * shot.MovementSpeed;
                    shot.YVelocity = shotdirectionx.Y * shot.MovementSpeed;
                    shot.isPlayAtack = isPlayAtack;
                    shot.SoundFlaunch.Play();

                    break;

                case Projectiles.VariableState.Arrow:
                    shot = Factories.ProjectilesFactory.CreateNew();
                    shot.CurrentState = Projectiles.VariableState.Arrow;
                    shot.X = origemX;
                    shot.Y = origemY;
                    shot.Z = GlobalData.PlayerData.playerInstance.Position.Z+5;
                    Vector3 shotdirection = new Vector3(alvoX - origemX, alvoY - origemY, 0);
                    shotdirection.Normalize();
                    shot.XVelocity = shotdirection.X * shot.MovementSpeed;
                    shot.YVelocity = shotdirection.Y *shot.MovementSpeed;
                    shot.isPlayAtack = isPlayAtack;
                    shot.SoundArrow.Play();

                    v = new Vector3(shot.XVelocity, shot.YVelocity, 0);
                    v.Normalize();

                    
                  
                    angulo = 90 * v.Y;
                    if (v.X != 0)
                    {
                        angulo = 90 * (v.X - 1);
        
                        if (v.Y > 0)
                        {
                            angulo = -angulo;
                        }

                    }
                    angulo -= 90;
                    radians = angulo * ((float)Math.PI / 180);


                    shot.RotationZ = radians;



                    break;
            }

        }

        public void ActionAutoDestroy()
        {
            
            switch (this.CurrentState)
            {
                case Projectiles.VariableState.AtackWater1:
                    if (this.SpriteInstance.CurrentFrameIndex == this.SpriteInstance.CurrentChain.Count - 1)
                    {
                        this.Destroy();
                    }
                    break;
                case Projectiles.VariableState.Arrow:
                  //  Console.WriteLine(ishit);
                    if (ishit)
                    {
                     //   Console.WriteLine("Arrow+ destroy");
                        this.Destroy();

                    }
                    break;
            }
        }
	}
}
