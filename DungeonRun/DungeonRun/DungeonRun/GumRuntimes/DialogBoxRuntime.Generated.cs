    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class DialogBoxRuntime : DungeonRun.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            texBox.Blue = 0;
                            texBox.Green = 0;
                            texBox.Height = 107f;
                            texBox.Red = 0;
                            texBox.Visible = true;
                            texBox.Width = 419f;
                            texBox.X = 0f;
                            texBox.Y = 0f;
                            NpcTextinstance.Height = -21f;
                            NpcTextinstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            NpcTextinstance.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                            NpcTextinstance.OutlineThickness = 1;
                            NpcTextinstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "texBox");
                            NpcTextinstance.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Top;
                            NpcTextinstance.Width = -128f;
                            NpcTextinstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            NpcTextinstance.X = 110f;
                            NpcTextinstance.Y = 9f;
                            RectangleInstance.Height = -9f;
                            RectangleInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            RectangleInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "texBox");
                            RectangleInstance.Width = -7f;
                            RectangleInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            RectangleInstance.X = 3f;
                            RectangleInstance.Y = 5f;
                            Avatar.Height = 86.66667f;
                            Avatar.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            Avatar.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "texBox");
                            SetProperty("Avatar.SourceFile", "Galvisr.png");
                            Avatar.TextureAddress = Gum.Managers.TextureAddress.EntireTexture;
                            Avatar.TextureHeight = 57;
                            Avatar.TextureHeightScale = 1f;
                            Avatar.TextureLeft = 0;
                            Avatar.TextureTop = 0;
                            Avatar.TextureWidth = 55;
                            Avatar.TextureWidthScale = 1f;
                            Avatar.Width = 21.71837f;
                            Avatar.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            Avatar.Wrap = false;
                            Avatar.X = -1f;
                            Avatar.Y = 5f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setAvatarHeightFirstValue = false;
                bool setAvatarHeightSecondValue = false;
                float AvatarHeightFirstValue= 0;
                float AvatarHeightSecondValue= 0;
                bool setAvatarTextureHeightFirstValue = false;
                bool setAvatarTextureHeightSecondValue = false;
                int AvatarTextureHeightFirstValue= 0;
                int AvatarTextureHeightSecondValue= 0;
                bool setAvatarTextureHeightScaleFirstValue = false;
                bool setAvatarTextureHeightScaleSecondValue = false;
                float AvatarTextureHeightScaleFirstValue= 0;
                float AvatarTextureHeightScaleSecondValue= 0;
                bool setAvatarTextureLeftFirstValue = false;
                bool setAvatarTextureLeftSecondValue = false;
                int AvatarTextureLeftFirstValue= 0;
                int AvatarTextureLeftSecondValue= 0;
                bool setAvatarTextureTopFirstValue = false;
                bool setAvatarTextureTopSecondValue = false;
                int AvatarTextureTopFirstValue= 0;
                int AvatarTextureTopSecondValue= 0;
                bool setAvatarTextureWidthFirstValue = false;
                bool setAvatarTextureWidthSecondValue = false;
                int AvatarTextureWidthFirstValue= 0;
                int AvatarTextureWidthSecondValue= 0;
                bool setAvatarTextureWidthScaleFirstValue = false;
                bool setAvatarTextureWidthScaleSecondValue = false;
                float AvatarTextureWidthScaleFirstValue= 0;
                float AvatarTextureWidthScaleSecondValue= 0;
                bool setAvatarWidthFirstValue = false;
                bool setAvatarWidthSecondValue = false;
                float AvatarWidthFirstValue= 0;
                float AvatarWidthSecondValue= 0;
                bool setAvatarXFirstValue = false;
                bool setAvatarXSecondValue = false;
                float AvatarXFirstValue= 0;
                float AvatarXSecondValue= 0;
                bool setAvatarYFirstValue = false;
                bool setAvatarYSecondValue = false;
                float AvatarYFirstValue= 0;
                float AvatarYSecondValue= 0;
                bool setNpcTextinstanceHeightFirstValue = false;
                bool setNpcTextinstanceHeightSecondValue = false;
                float NpcTextinstanceHeightFirstValue= 0;
                float NpcTextinstanceHeightSecondValue= 0;
                bool setNpcTextinstanceOutlineThicknessFirstValue = false;
                bool setNpcTextinstanceOutlineThicknessSecondValue = false;
                int NpcTextinstanceOutlineThicknessFirstValue= 0;
                int NpcTextinstanceOutlineThicknessSecondValue= 0;
                bool setNpcTextinstanceWidthFirstValue = false;
                bool setNpcTextinstanceWidthSecondValue = false;
                float NpcTextinstanceWidthFirstValue= 0;
                float NpcTextinstanceWidthSecondValue= 0;
                bool setNpcTextinstanceXFirstValue = false;
                bool setNpcTextinstanceXSecondValue = false;
                float NpcTextinstanceXFirstValue= 0;
                float NpcTextinstanceXSecondValue= 0;
                bool setNpcTextinstanceYFirstValue = false;
                bool setNpcTextinstanceYSecondValue = false;
                float NpcTextinstanceYFirstValue= 0;
                float NpcTextinstanceYSecondValue= 0;
                bool setRectangleInstanceHeightFirstValue = false;
                bool setRectangleInstanceHeightSecondValue = false;
                float RectangleInstanceHeightFirstValue= 0;
                float RectangleInstanceHeightSecondValue= 0;
                bool setRectangleInstanceWidthFirstValue = false;
                bool setRectangleInstanceWidthSecondValue = false;
                float RectangleInstanceWidthFirstValue= 0;
                float RectangleInstanceWidthSecondValue= 0;
                bool setRectangleInstanceXFirstValue = false;
                bool setRectangleInstanceXSecondValue = false;
                float RectangleInstanceXFirstValue= 0;
                float RectangleInstanceXSecondValue= 0;
                bool setRectangleInstanceYFirstValue = false;
                bool setRectangleInstanceYSecondValue = false;
                float RectangleInstanceYFirstValue= 0;
                float RectangleInstanceYSecondValue= 0;
                bool settexBoxBlueFirstValue = false;
                bool settexBoxBlueSecondValue = false;
                int texBoxBlueFirstValue= 0;
                int texBoxBlueSecondValue= 0;
                bool settexBoxGreenFirstValue = false;
                bool settexBoxGreenSecondValue = false;
                int texBoxGreenFirstValue= 0;
                int texBoxGreenSecondValue= 0;
                bool settexBoxHeightFirstValue = false;
                bool settexBoxHeightSecondValue = false;
                float texBoxHeightFirstValue= 0;
                float texBoxHeightSecondValue= 0;
                bool settexBoxRedFirstValue = false;
                bool settexBoxRedSecondValue = false;
                int texBoxRedFirstValue= 0;
                int texBoxRedSecondValue= 0;
                bool settexBoxWidthFirstValue = false;
                bool settexBoxWidthSecondValue = false;
                float texBoxWidthFirstValue= 0;
                float texBoxWidthSecondValue= 0;
                bool settexBoxXFirstValue = false;
                bool settexBoxXSecondValue = false;
                float texBoxXFirstValue= 0;
                float texBoxXSecondValue= 0;
                bool settexBoxYFirstValue = false;
                bool settexBoxYSecondValue = false;
                float texBoxYFirstValue= 0;
                float texBoxYSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setAvatarHeightFirstValue = true;
                        AvatarHeightFirstValue = 86.66667f;
                        if (interpolationValue < 1)
                        {
                            this.Avatar.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        if (interpolationValue < 1)
                        {
                            this.Avatar.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "texBox");
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("Avatar.SourceFile", "Galvisr.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Avatar.TextureAddress = Gum.Managers.TextureAddress.EntireTexture;
                        }
                        setAvatarTextureHeightFirstValue = true;
                        AvatarTextureHeightFirstValue = 57;
                        setAvatarTextureHeightScaleFirstValue = true;
                        AvatarTextureHeightScaleFirstValue = 1f;
                        setAvatarTextureLeftFirstValue = true;
                        AvatarTextureLeftFirstValue = 0;
                        setAvatarTextureTopFirstValue = true;
                        AvatarTextureTopFirstValue = 0;
                        setAvatarTextureWidthFirstValue = true;
                        AvatarTextureWidthFirstValue = 55;
                        setAvatarTextureWidthScaleFirstValue = true;
                        AvatarTextureWidthScaleFirstValue = 1f;
                        setAvatarWidthFirstValue = true;
                        AvatarWidthFirstValue = 21.71837f;
                        if (interpolationValue < 1)
                        {
                            this.Avatar.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        if (interpolationValue < 1)
                        {
                            this.Avatar.Wrap = false;
                        }
                        setAvatarXFirstValue = true;
                        AvatarXFirstValue = -1f;
                        setAvatarYFirstValue = true;
                        AvatarYFirstValue = 5f;
                        setNpcTextinstanceHeightFirstValue = true;
                        NpcTextinstanceHeightFirstValue = -21f;
                        if (interpolationValue < 1)
                        {
                            this.NpcTextinstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.NpcTextinstance.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        setNpcTextinstanceOutlineThicknessFirstValue = true;
                        NpcTextinstanceOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.NpcTextinstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "texBox");
                        }
                        if (interpolationValue < 1)
                        {
                            this.NpcTextinstance.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        setNpcTextinstanceWidthFirstValue = true;
                        NpcTextinstanceWidthFirstValue = -128f;
                        if (interpolationValue < 1)
                        {
                            this.NpcTextinstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setNpcTextinstanceXFirstValue = true;
                        NpcTextinstanceXFirstValue = 110f;
                        setNpcTextinstanceYFirstValue = true;
                        NpcTextinstanceYFirstValue = 9f;
                        setRectangleInstanceHeightFirstValue = true;
                        RectangleInstanceHeightFirstValue = -9f;
                        if (interpolationValue < 1)
                        {
                            this.RectangleInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.RectangleInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "texBox");
                        }
                        setRectangleInstanceWidthFirstValue = true;
                        RectangleInstanceWidthFirstValue = -7f;
                        if (interpolationValue < 1)
                        {
                            this.RectangleInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setRectangleInstanceXFirstValue = true;
                        RectangleInstanceXFirstValue = 3f;
                        setRectangleInstanceYFirstValue = true;
                        RectangleInstanceYFirstValue = 5f;
                        settexBoxBlueFirstValue = true;
                        texBoxBlueFirstValue = 0;
                        settexBoxGreenFirstValue = true;
                        texBoxGreenFirstValue = 0;
                        settexBoxHeightFirstValue = true;
                        texBoxHeightFirstValue = 107f;
                        settexBoxRedFirstValue = true;
                        texBoxRedFirstValue = 0;
                        if (interpolationValue < 1)
                        {
                            this.texBox.Visible = true;
                        }
                        settexBoxWidthFirstValue = true;
                        texBoxWidthFirstValue = 419f;
                        settexBoxXFirstValue = true;
                        texBoxXFirstValue = 0f;
                        settexBoxYFirstValue = true;
                        texBoxYFirstValue = 0f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setAvatarHeightSecondValue = true;
                        AvatarHeightSecondValue = 86.66667f;
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "texBox");
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("Avatar.SourceFile", "Galvisr.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.TextureAddress = Gum.Managers.TextureAddress.EntireTexture;
                        }
                        setAvatarTextureHeightSecondValue = true;
                        AvatarTextureHeightSecondValue = 57;
                        setAvatarTextureHeightScaleSecondValue = true;
                        AvatarTextureHeightScaleSecondValue = 1f;
                        setAvatarTextureLeftSecondValue = true;
                        AvatarTextureLeftSecondValue = 0;
                        setAvatarTextureTopSecondValue = true;
                        AvatarTextureTopSecondValue = 0;
                        setAvatarTextureWidthSecondValue = true;
                        AvatarTextureWidthSecondValue = 55;
                        setAvatarTextureWidthScaleSecondValue = true;
                        AvatarTextureWidthScaleSecondValue = 1f;
                        setAvatarWidthSecondValue = true;
                        AvatarWidthSecondValue = 21.71837f;
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.Wrap = false;
                        }
                        setAvatarXSecondValue = true;
                        AvatarXSecondValue = -1f;
                        setAvatarYSecondValue = true;
                        AvatarYSecondValue = 5f;
                        setNpcTextinstanceHeightSecondValue = true;
                        NpcTextinstanceHeightSecondValue = -21f;
                        if (interpolationValue >= 1)
                        {
                            this.NpcTextinstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.NpcTextinstance.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        setNpcTextinstanceOutlineThicknessSecondValue = true;
                        NpcTextinstanceOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.NpcTextinstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "texBox");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.NpcTextinstance.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        setNpcTextinstanceWidthSecondValue = true;
                        NpcTextinstanceWidthSecondValue = -128f;
                        if (interpolationValue >= 1)
                        {
                            this.NpcTextinstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setNpcTextinstanceXSecondValue = true;
                        NpcTextinstanceXSecondValue = 110f;
                        setNpcTextinstanceYSecondValue = true;
                        NpcTextinstanceYSecondValue = 9f;
                        setRectangleInstanceHeightSecondValue = true;
                        RectangleInstanceHeightSecondValue = -9f;
                        if (interpolationValue >= 1)
                        {
                            this.RectangleInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.RectangleInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "texBox");
                        }
                        setRectangleInstanceWidthSecondValue = true;
                        RectangleInstanceWidthSecondValue = -7f;
                        if (interpolationValue >= 1)
                        {
                            this.RectangleInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setRectangleInstanceXSecondValue = true;
                        RectangleInstanceXSecondValue = 3f;
                        setRectangleInstanceYSecondValue = true;
                        RectangleInstanceYSecondValue = 5f;
                        settexBoxBlueSecondValue = true;
                        texBoxBlueSecondValue = 0;
                        settexBoxGreenSecondValue = true;
                        texBoxGreenSecondValue = 0;
                        settexBoxHeightSecondValue = true;
                        texBoxHeightSecondValue = 107f;
                        settexBoxRedSecondValue = true;
                        texBoxRedSecondValue = 0;
                        if (interpolationValue >= 1)
                        {
                            this.texBox.Visible = true;
                        }
                        settexBoxWidthSecondValue = true;
                        texBoxWidthSecondValue = 419f;
                        settexBoxXSecondValue = true;
                        texBoxXSecondValue = 0f;
                        settexBoxYSecondValue = true;
                        texBoxYSecondValue = 0f;
                        break;
                }
                if (setAvatarHeightFirstValue && setAvatarHeightSecondValue)
                {
                    Avatar.Height = AvatarHeightFirstValue * (1 - interpolationValue) + AvatarHeightSecondValue * interpolationValue;
                }
                if (setAvatarTextureHeightFirstValue && setAvatarTextureHeightSecondValue)
                {
                    Avatar.TextureHeight = FlatRedBall.Math.MathFunctions.RoundToInt(AvatarTextureHeightFirstValue* (1 - interpolationValue) + AvatarTextureHeightSecondValue * interpolationValue);
                }
                if (setAvatarTextureHeightScaleFirstValue && setAvatarTextureHeightScaleSecondValue)
                {
                    Avatar.TextureHeightScale = AvatarTextureHeightScaleFirstValue * (1 - interpolationValue) + AvatarTextureHeightScaleSecondValue * interpolationValue;
                }
                if (setAvatarTextureLeftFirstValue && setAvatarTextureLeftSecondValue)
                {
                    Avatar.TextureLeft = FlatRedBall.Math.MathFunctions.RoundToInt(AvatarTextureLeftFirstValue* (1 - interpolationValue) + AvatarTextureLeftSecondValue * interpolationValue);
                }
                if (setAvatarTextureTopFirstValue && setAvatarTextureTopSecondValue)
                {
                    Avatar.TextureTop = FlatRedBall.Math.MathFunctions.RoundToInt(AvatarTextureTopFirstValue* (1 - interpolationValue) + AvatarTextureTopSecondValue * interpolationValue);
                }
                if (setAvatarTextureWidthFirstValue && setAvatarTextureWidthSecondValue)
                {
                    Avatar.TextureWidth = FlatRedBall.Math.MathFunctions.RoundToInt(AvatarTextureWidthFirstValue* (1 - interpolationValue) + AvatarTextureWidthSecondValue * interpolationValue);
                }
                if (setAvatarTextureWidthScaleFirstValue && setAvatarTextureWidthScaleSecondValue)
                {
                    Avatar.TextureWidthScale = AvatarTextureWidthScaleFirstValue * (1 - interpolationValue) + AvatarTextureWidthScaleSecondValue * interpolationValue;
                }
                if (setAvatarWidthFirstValue && setAvatarWidthSecondValue)
                {
                    Avatar.Width = AvatarWidthFirstValue * (1 - interpolationValue) + AvatarWidthSecondValue * interpolationValue;
                }
                if (setAvatarXFirstValue && setAvatarXSecondValue)
                {
                    Avatar.X = AvatarXFirstValue * (1 - interpolationValue) + AvatarXSecondValue * interpolationValue;
                }
                if (setAvatarYFirstValue && setAvatarYSecondValue)
                {
                    Avatar.Y = AvatarYFirstValue * (1 - interpolationValue) + AvatarYSecondValue * interpolationValue;
                }
                if (setNpcTextinstanceHeightFirstValue && setNpcTextinstanceHeightSecondValue)
                {
                    NpcTextinstance.Height = NpcTextinstanceHeightFirstValue * (1 - interpolationValue) + NpcTextinstanceHeightSecondValue * interpolationValue;
                }
                if (setNpcTextinstanceOutlineThicknessFirstValue && setNpcTextinstanceOutlineThicknessSecondValue)
                {
                    NpcTextinstance.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(NpcTextinstanceOutlineThicknessFirstValue* (1 - interpolationValue) + NpcTextinstanceOutlineThicknessSecondValue * interpolationValue);
                }
                if (setNpcTextinstanceWidthFirstValue && setNpcTextinstanceWidthSecondValue)
                {
                    NpcTextinstance.Width = NpcTextinstanceWidthFirstValue * (1 - interpolationValue) + NpcTextinstanceWidthSecondValue * interpolationValue;
                }
                if (setNpcTextinstanceXFirstValue && setNpcTextinstanceXSecondValue)
                {
                    NpcTextinstance.X = NpcTextinstanceXFirstValue * (1 - interpolationValue) + NpcTextinstanceXSecondValue * interpolationValue;
                }
                if (setNpcTextinstanceYFirstValue && setNpcTextinstanceYSecondValue)
                {
                    NpcTextinstance.Y = NpcTextinstanceYFirstValue * (1 - interpolationValue) + NpcTextinstanceYSecondValue * interpolationValue;
                }
                if (setRectangleInstanceHeightFirstValue && setRectangleInstanceHeightSecondValue)
                {
                    RectangleInstance.Height = RectangleInstanceHeightFirstValue * (1 - interpolationValue) + RectangleInstanceHeightSecondValue * interpolationValue;
                }
                if (setRectangleInstanceWidthFirstValue && setRectangleInstanceWidthSecondValue)
                {
                    RectangleInstance.Width = RectangleInstanceWidthFirstValue * (1 - interpolationValue) + RectangleInstanceWidthSecondValue * interpolationValue;
                }
                if (setRectangleInstanceXFirstValue && setRectangleInstanceXSecondValue)
                {
                    RectangleInstance.X = RectangleInstanceXFirstValue * (1 - interpolationValue) + RectangleInstanceXSecondValue * interpolationValue;
                }
                if (setRectangleInstanceYFirstValue && setRectangleInstanceYSecondValue)
                {
                    RectangleInstance.Y = RectangleInstanceYFirstValue * (1 - interpolationValue) + RectangleInstanceYSecondValue * interpolationValue;
                }
                if (settexBoxBlueFirstValue && settexBoxBlueSecondValue)
                {
                    texBox.Blue = FlatRedBall.Math.MathFunctions.RoundToInt(texBoxBlueFirstValue* (1 - interpolationValue) + texBoxBlueSecondValue * interpolationValue);
                }
                if (settexBoxGreenFirstValue && settexBoxGreenSecondValue)
                {
                    texBox.Green = FlatRedBall.Math.MathFunctions.RoundToInt(texBoxGreenFirstValue* (1 - interpolationValue) + texBoxGreenSecondValue * interpolationValue);
                }
                if (settexBoxHeightFirstValue && settexBoxHeightSecondValue)
                {
                    texBox.Height = texBoxHeightFirstValue * (1 - interpolationValue) + texBoxHeightSecondValue * interpolationValue;
                }
                if (settexBoxRedFirstValue && settexBoxRedSecondValue)
                {
                    texBox.Red = FlatRedBall.Math.MathFunctions.RoundToInt(texBoxRedFirstValue* (1 - interpolationValue) + texBoxRedSecondValue * interpolationValue);
                }
                if (settexBoxWidthFirstValue && settexBoxWidthSecondValue)
                {
                    texBox.Width = texBoxWidthFirstValue * (1 - interpolationValue) + texBoxWidthSecondValue * interpolationValue;
                }
                if (settexBoxXFirstValue && settexBoxXSecondValue)
                {
                    texBox.X = texBoxXFirstValue * (1 - interpolationValue) + texBoxXSecondValue * interpolationValue;
                }
                if (settexBoxYFirstValue && settexBoxYSecondValue)
                {
                    texBox.Y = texBoxYFirstValue * (1 - interpolationValue) + texBoxYSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.DialogBoxRuntime.VariableState fromState,DungeonRun.GumRuntimes.DialogBoxRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Blue",
                            Type = "int",
                            Value = texBox.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Green",
                            Type = "int",
                            Value = texBox.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Height",
                            Type = "float",
                            Value = texBox.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Red",
                            Type = "int",
                            Value = texBox.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Visible",
                            Type = "bool",
                            Value = texBox.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Width",
                            Type = "float",
                            Value = texBox.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.X",
                            Type = "float",
                            Value = texBox.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Y",
                            Type = "float",
                            Value = texBox.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Height",
                            Type = "float",
                            Value = NpcTextinstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = NpcTextinstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = NpcTextinstance.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.OutlineThickness",
                            Type = "int",
                            Value = NpcTextinstance.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Parent",
                            Type = "string",
                            Value = NpcTextinstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = NpcTextinstance.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Width",
                            Type = "float",
                            Value = NpcTextinstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = NpcTextinstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.X",
                            Type = "float",
                            Value = NpcTextinstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Y",
                            Type = "float",
                            Value = NpcTextinstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Height",
                            Type = "float",
                            Value = RectangleInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Parent",
                            Type = "string",
                            Value = RectangleInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Width",
                            Type = "float",
                            Value = RectangleInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.X",
                            Type = "float",
                            Value = RectangleInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Y",
                            Type = "float",
                            Value = RectangleInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Height",
                            Type = "float",
                            Value = Avatar.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Height Units",
                            Type = "DimensionUnitType",
                            Value = Avatar.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Parent",
                            Type = "string",
                            Value = Avatar.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.SourceFile",
                            Type = "string",
                            Value = Avatar.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Address",
                            Type = "TextureAddress",
                            Value = Avatar.TextureAddress
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Height",
                            Type = "int",
                            Value = Avatar.TextureHeight
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Height Scale",
                            Type = "float",
                            Value = Avatar.TextureHeightScale
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Left",
                            Type = "int",
                            Value = Avatar.TextureLeft
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Top",
                            Type = "int",
                            Value = Avatar.TextureTop
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Width",
                            Type = "int",
                            Value = Avatar.TextureWidth
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Width Scale",
                            Type = "float",
                            Value = Avatar.TextureWidthScale
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Width",
                            Type = "float",
                            Value = Avatar.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Width Units",
                            Type = "DimensionUnitType",
                            Value = Avatar.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Wrap",
                            Type = "bool",
                            Value = Avatar.Wrap
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.X",
                            Type = "float",
                            Value = Avatar.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Y",
                            Type = "float",
                            Value = Avatar.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Blue",
                            Type = "int",
                            Value = texBox.Blue + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Green",
                            Type = "int",
                            Value = texBox.Green + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Height",
                            Type = "float",
                            Value = texBox.Height + 107f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Red",
                            Type = "int",
                            Value = texBox.Red + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Visible",
                            Type = "bool",
                            Value = texBox.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Width",
                            Type = "float",
                            Value = texBox.Width + 419f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.X",
                            Type = "float",
                            Value = texBox.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "texBox.Y",
                            Type = "float",
                            Value = texBox.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Height",
                            Type = "float",
                            Value = NpcTextinstance.Height + -21f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = NpcTextinstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = NpcTextinstance.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.OutlineThickness",
                            Type = "int",
                            Value = NpcTextinstance.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Parent",
                            Type = "string",
                            Value = NpcTextinstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = NpcTextinstance.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Width",
                            Type = "float",
                            Value = NpcTextinstance.Width + -128f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = NpcTextinstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.X",
                            Type = "float",
                            Value = NpcTextinstance.X + 110f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "NpcTextinstance.Y",
                            Type = "float",
                            Value = NpcTextinstance.Y + 9f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Height",
                            Type = "float",
                            Value = RectangleInstance.Height + -9f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Parent",
                            Type = "string",
                            Value = RectangleInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Width",
                            Type = "float",
                            Value = RectangleInstance.Width + -7f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.X",
                            Type = "float",
                            Value = RectangleInstance.X + 3f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Y",
                            Type = "float",
                            Value = RectangleInstance.Y + 5f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Height",
                            Type = "float",
                            Value = Avatar.Height + 86.66667f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Height Units",
                            Type = "DimensionUnitType",
                            Value = Avatar.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Parent",
                            Type = "string",
                            Value = Avatar.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.SourceFile",
                            Type = "string",
                            Value = Avatar.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Address",
                            Type = "TextureAddress",
                            Value = Avatar.TextureAddress
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Height",
                            Type = "int",
                            Value = Avatar.TextureHeight + 57
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Height Scale",
                            Type = "float",
                            Value = Avatar.TextureHeightScale + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Left",
                            Type = "int",
                            Value = Avatar.TextureLeft + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Top",
                            Type = "int",
                            Value = Avatar.TextureTop + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Width",
                            Type = "int",
                            Value = Avatar.TextureWidth + 55
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Width Scale",
                            Type = "float",
                            Value = Avatar.TextureWidthScale + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Width",
                            Type = "float",
                            Value = Avatar.Width + 21.71837f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Width Units",
                            Type = "DimensionUnitType",
                            Value = Avatar.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Wrap",
                            Type = "bool",
                            Value = Avatar.Wrap
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.X",
                            Type = "float",
                            Value = Avatar.X + -1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Y",
                            Type = "float",
                            Value = Avatar.Y + 5f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.ColoredRectangleRuntime texBox { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime NpcTextinstance { get; set; }
            private DungeonRun.GumRuntimes.RectangleRuntime RectangleInstance { get; set; }
            private DungeonRun.GumRuntimes.SpriteRuntime Avatar { get; set; }
            public string Text
            {
                get
                {
                    return NpcTextinstance.Text;
                }
                set
                {
                    if (NpcTextinstance.Text != value)
                    {
                        NpcTextinstance.Text = value;
                        TextChanged?.Invoke(this, null);
                    }
                }
            }
            public event System.EventHandler TextChanged;
            public DialogBoxRuntime (bool fullInstantiation = true) 
            	: base(false)
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "DialogBox");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                texBox = this.GetGraphicalUiElementByName("texBox") as DungeonRun.GumRuntimes.ColoredRectangleRuntime;
                NpcTextinstance = this.GetGraphicalUiElementByName("NpcTextinstance") as DungeonRun.GumRuntimes.TextRuntime;
                RectangleInstance = this.GetGraphicalUiElementByName("RectangleInstance") as DungeonRun.GumRuntimes.RectangleRuntime;
                Avatar = this.GetGraphicalUiElementByName("Avatar") as DungeonRun.GumRuntimes.SpriteRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
