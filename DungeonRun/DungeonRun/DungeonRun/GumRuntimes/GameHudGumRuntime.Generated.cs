    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class GameHudGumRuntime : Gum.Wireframe.GraphicalUiElement
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            BloodIntance.Height = 80.63882f;
                            SetProperty("BloodIntance.SourceFile", "Blood.png");
                            BloodIntance.Visible = false;
                            BloodIntance.Width = 100.7296f;
                            BloodIntance.X = -2f;
                            BloodIntance.Y = -8f;
                            LevelDisplay_HUD.Height = -552f;
                            LevelDisplay_HUD.Visible = false;
                            LevelDisplay_HUD.Width = -736f;
                            LevelDisplay_HUD.X = 14f;
                            LevelDisplay_HUD.Y = 12f;
                            InvetoryDisplay.Height = 38f;
                            InvetoryDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "LevelDisplay_HUD");
                            InvetoryDisplay.Width = 232f;
                            InvetoryDisplay.X = 160f;
                            InvetoryDisplay.Y = -1f;
                            SetProperty("Item_HUD_1.ItemSprite_hud", "../Entities/Item/key_silver.png");
                            Item_HUD_1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                            Item_HUD_1.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                            Item_HUD_1.X = 53f;
                            Item_HUD_1.Y = 0f;
                            SetProperty("Item_HUD_2.ItemSprite_hud", "../Entities/Item/Fire_1.png");
                            Item_HUD_2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                            Item_HUD_2.X = 102f;
                            Item_HUD_2.Y = 0f;
                            SetProperty("Item_HUD_0.ItemSprite_hud", "../Entities/Hud/full_heart.png");
                            Item_HUD_0.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                            Item_HUD_0.X = 7f;
                            Item_HUD_0.Y = 0f;
                            Item_HUD_3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                            Item_HUD_3.Visible = false;
                            Item_HUD_3.X = 145f;
                            Item_HUD_3.Y = 0f;
                            Name_DisplayInstance.Height = 35f;
                            Name_DisplayInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "LevelDisplay_HUD");
                            Name_DisplayInstance.Width = 243f;
                            Name_DisplayInstance.X = 417f;
                            Name_DisplayInstance.Y = -1f;
                            TimeDisplay.Height = 34f;
                            TimeDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "LevelDisplay_HUD");
                            TimeDisplay.X = 70f;
                            TimeDisplay.Y = 1f;
                            Controle_DisplayInstance.Visible = false;
                            Controle_DisplayInstance.X = -2f;
                            Controle_DisplayInstance.Y = 10f;
                            GameOver_DisplayInstance.Height = 423f;
                            GameOver_DisplayInstance.Visible = true;
                            GameOver_DisplayInstance.X = 170f;
                            GameOver_DisplayInstance.Y = 96f;
                            btn_Ranking.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            btn_Ranking.Text = "Ranking";
                            btn_Ranking.Visible = true;
                            btn_Ranking.X = 160f;
                            btn_Ranking.Y = 0f;
                            Btn_VoltarInicio.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            Btn_VoltarInicio.Text = "VoltarInicio";
                            Btn_VoltarInicio.Visible = true;
                            Btn_VoltarInicio.X = 320f;
                            Btn_VoltarInicio.Y = 0f;
                            ContainerInstance.Height = 79f;
                            ContainerInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "GameOver_DisplayInstance");
                            ContainerInstance.Visible = true;
                            ContainerInstance.Width = 480f;
                            ContainerInstance.X = 14f;
                            ContainerInstance.Y = 332f;
                            DialogBoxInstance.Height = 109f;
                            DialogBoxInstance.Text = "Yep";
                            DialogBoxInstance.Visible = false;
                            DialogBoxInstance.Width = 423f;
                            DialogBoxInstance.X = 0f;
                            DialogBoxInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            DialogBoxInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            DialogBoxInstance.Y = -145f;
                            DialogBoxInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                            DialogBoxInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                            btn_GameOver_Points.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            btn_GameOver_Points.Text = "Pontos";
                            btn_GameOver_Points.X = 0f;
                            btn_GameOver_Points.Y = 0f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setBloodIntanceHeightFirstValue = false;
                bool setBloodIntanceHeightSecondValue = false;
                float BloodIntanceHeightFirstValue= 0;
                float BloodIntanceHeightSecondValue= 0;
                bool setBloodIntanceWidthFirstValue = false;
                bool setBloodIntanceWidthSecondValue = false;
                float BloodIntanceWidthFirstValue= 0;
                float BloodIntanceWidthSecondValue= 0;
                bool setBloodIntanceXFirstValue = false;
                bool setBloodIntanceXSecondValue = false;
                float BloodIntanceXFirstValue= 0;
                float BloodIntanceXSecondValue= 0;
                bool setBloodIntanceYFirstValue = false;
                bool setBloodIntanceYSecondValue = false;
                float BloodIntanceYFirstValue= 0;
                float BloodIntanceYSecondValue= 0;
                bool setbtn_GameOver_PointsXFirstValue = false;
                bool setbtn_GameOver_PointsXSecondValue = false;
                float btn_GameOver_PointsXFirstValue= 0;
                float btn_GameOver_PointsXSecondValue= 0;
                bool setbtn_GameOver_PointsYFirstValue = false;
                bool setbtn_GameOver_PointsYSecondValue = false;
                float btn_GameOver_PointsYFirstValue= 0;
                float btn_GameOver_PointsYSecondValue= 0;
                bool setbtn_RankingXFirstValue = false;
                bool setbtn_RankingXSecondValue = false;
                float btn_RankingXFirstValue= 0;
                float btn_RankingXSecondValue= 0;
                bool setbtn_RankingYFirstValue = false;
                bool setbtn_RankingYSecondValue = false;
                float btn_RankingYFirstValue= 0;
                float btn_RankingYSecondValue= 0;
                bool setBtn_VoltarInicioXFirstValue = false;
                bool setBtn_VoltarInicioXSecondValue = false;
                float Btn_VoltarInicioXFirstValue= 0;
                float Btn_VoltarInicioXSecondValue= 0;
                bool setBtn_VoltarInicioYFirstValue = false;
                bool setBtn_VoltarInicioYSecondValue = false;
                float Btn_VoltarInicioYFirstValue= 0;
                float Btn_VoltarInicioYSecondValue= 0;
                bool setContainerInstanceHeightFirstValue = false;
                bool setContainerInstanceHeightSecondValue = false;
                float ContainerInstanceHeightFirstValue= 0;
                float ContainerInstanceHeightSecondValue= 0;
                bool setContainerInstanceWidthFirstValue = false;
                bool setContainerInstanceWidthSecondValue = false;
                float ContainerInstanceWidthFirstValue= 0;
                float ContainerInstanceWidthSecondValue= 0;
                bool setContainerInstanceXFirstValue = false;
                bool setContainerInstanceXSecondValue = false;
                float ContainerInstanceXFirstValue= 0;
                float ContainerInstanceXSecondValue= 0;
                bool setContainerInstanceYFirstValue = false;
                bool setContainerInstanceYSecondValue = false;
                float ContainerInstanceYFirstValue= 0;
                float ContainerInstanceYSecondValue= 0;
                bool setControle_DisplayInstanceXFirstValue = false;
                bool setControle_DisplayInstanceXSecondValue = false;
                float Controle_DisplayInstanceXFirstValue= 0;
                float Controle_DisplayInstanceXSecondValue= 0;
                bool setControle_DisplayInstanceYFirstValue = false;
                bool setControle_DisplayInstanceYSecondValue = false;
                float Controle_DisplayInstanceYFirstValue= 0;
                float Controle_DisplayInstanceYSecondValue= 0;
                bool setDialogBoxInstanceHeightFirstValue = false;
                bool setDialogBoxInstanceHeightSecondValue = false;
                float DialogBoxInstanceHeightFirstValue= 0;
                float DialogBoxInstanceHeightSecondValue= 0;
                bool setDialogBoxInstanceWidthFirstValue = false;
                bool setDialogBoxInstanceWidthSecondValue = false;
                float DialogBoxInstanceWidthFirstValue= 0;
                float DialogBoxInstanceWidthSecondValue= 0;
                bool setDialogBoxInstanceXFirstValue = false;
                bool setDialogBoxInstanceXSecondValue = false;
                float DialogBoxInstanceXFirstValue= 0;
                float DialogBoxInstanceXSecondValue= 0;
                bool setDialogBoxInstanceYFirstValue = false;
                bool setDialogBoxInstanceYSecondValue = false;
                float DialogBoxInstanceYFirstValue= 0;
                float DialogBoxInstanceYSecondValue= 0;
                bool setGameOver_DisplayInstanceHeightFirstValue = false;
                bool setGameOver_DisplayInstanceHeightSecondValue = false;
                float GameOver_DisplayInstanceHeightFirstValue= 0;
                float GameOver_DisplayInstanceHeightSecondValue= 0;
                bool setGameOver_DisplayInstanceXFirstValue = false;
                bool setGameOver_DisplayInstanceXSecondValue = false;
                float GameOver_DisplayInstanceXFirstValue= 0;
                float GameOver_DisplayInstanceXSecondValue= 0;
                bool setGameOver_DisplayInstanceYFirstValue = false;
                bool setGameOver_DisplayInstanceYSecondValue = false;
                float GameOver_DisplayInstanceYFirstValue= 0;
                float GameOver_DisplayInstanceYSecondValue= 0;
                bool setInvetoryDisplayHeightFirstValue = false;
                bool setInvetoryDisplayHeightSecondValue = false;
                float InvetoryDisplayHeightFirstValue= 0;
                float InvetoryDisplayHeightSecondValue= 0;
                bool setInvetoryDisplayWidthFirstValue = false;
                bool setInvetoryDisplayWidthSecondValue = false;
                float InvetoryDisplayWidthFirstValue= 0;
                float InvetoryDisplayWidthSecondValue= 0;
                bool setInvetoryDisplayXFirstValue = false;
                bool setInvetoryDisplayXSecondValue = false;
                float InvetoryDisplayXFirstValue= 0;
                float InvetoryDisplayXSecondValue= 0;
                bool setInvetoryDisplayYFirstValue = false;
                bool setInvetoryDisplayYSecondValue = false;
                float InvetoryDisplayYFirstValue= 0;
                float InvetoryDisplayYSecondValue= 0;
                bool setItem_HUD_0XFirstValue = false;
                bool setItem_HUD_0XSecondValue = false;
                float Item_HUD_0XFirstValue= 0;
                float Item_HUD_0XSecondValue= 0;
                bool setItem_HUD_0YFirstValue = false;
                bool setItem_HUD_0YSecondValue = false;
                float Item_HUD_0YFirstValue= 0;
                float Item_HUD_0YSecondValue= 0;
                bool setItem_HUD_1XFirstValue = false;
                bool setItem_HUD_1XSecondValue = false;
                float Item_HUD_1XFirstValue= 0;
                float Item_HUD_1XSecondValue= 0;
                bool setItem_HUD_1YFirstValue = false;
                bool setItem_HUD_1YSecondValue = false;
                float Item_HUD_1YFirstValue= 0;
                float Item_HUD_1YSecondValue= 0;
                bool setItem_HUD_2XFirstValue = false;
                bool setItem_HUD_2XSecondValue = false;
                float Item_HUD_2XFirstValue= 0;
                float Item_HUD_2XSecondValue= 0;
                bool setItem_HUD_2YFirstValue = false;
                bool setItem_HUD_2YSecondValue = false;
                float Item_HUD_2YFirstValue= 0;
                float Item_HUD_2YSecondValue= 0;
                bool setItem_HUD_3XFirstValue = false;
                bool setItem_HUD_3XSecondValue = false;
                float Item_HUD_3XFirstValue= 0;
                float Item_HUD_3XSecondValue= 0;
                bool setItem_HUD_3YFirstValue = false;
                bool setItem_HUD_3YSecondValue = false;
                float Item_HUD_3YFirstValue= 0;
                float Item_HUD_3YSecondValue= 0;
                bool setLevelDisplay_HUDHeightFirstValue = false;
                bool setLevelDisplay_HUDHeightSecondValue = false;
                float LevelDisplay_HUDHeightFirstValue= 0;
                float LevelDisplay_HUDHeightSecondValue= 0;
                bool setLevelDisplay_HUDWidthFirstValue = false;
                bool setLevelDisplay_HUDWidthSecondValue = false;
                float LevelDisplay_HUDWidthFirstValue= 0;
                float LevelDisplay_HUDWidthSecondValue= 0;
                bool setLevelDisplay_HUDXFirstValue = false;
                bool setLevelDisplay_HUDXSecondValue = false;
                float LevelDisplay_HUDXFirstValue= 0;
                float LevelDisplay_HUDXSecondValue= 0;
                bool setLevelDisplay_HUDYFirstValue = false;
                bool setLevelDisplay_HUDYSecondValue = false;
                float LevelDisplay_HUDYFirstValue= 0;
                float LevelDisplay_HUDYSecondValue= 0;
                bool setName_DisplayInstanceHeightFirstValue = false;
                bool setName_DisplayInstanceHeightSecondValue = false;
                float Name_DisplayInstanceHeightFirstValue= 0;
                float Name_DisplayInstanceHeightSecondValue= 0;
                bool setName_DisplayInstanceWidthFirstValue = false;
                bool setName_DisplayInstanceWidthSecondValue = false;
                float Name_DisplayInstanceWidthFirstValue= 0;
                float Name_DisplayInstanceWidthSecondValue= 0;
                bool setName_DisplayInstanceXFirstValue = false;
                bool setName_DisplayInstanceXSecondValue = false;
                float Name_DisplayInstanceXFirstValue= 0;
                float Name_DisplayInstanceXSecondValue= 0;
                bool setName_DisplayInstanceYFirstValue = false;
                bool setName_DisplayInstanceYSecondValue = false;
                float Name_DisplayInstanceYFirstValue= 0;
                float Name_DisplayInstanceYSecondValue= 0;
                bool setTimeDisplayHeightFirstValue = false;
                bool setTimeDisplayHeightSecondValue = false;
                float TimeDisplayHeightFirstValue= 0;
                float TimeDisplayHeightSecondValue= 0;
                bool setTimeDisplayXFirstValue = false;
                bool setTimeDisplayXSecondValue = false;
                float TimeDisplayXFirstValue= 0;
                float TimeDisplayXSecondValue= 0;
                bool setTimeDisplayYFirstValue = false;
                bool setTimeDisplayYSecondValue = false;
                float TimeDisplayYFirstValue= 0;
                float TimeDisplayYSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setBloodIntanceHeightFirstValue = true;
                        BloodIntanceHeightFirstValue = 80.63882f;
                        if (interpolationValue < 1)
                        {
                            SetProperty("BloodIntance.SourceFile", "Blood.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.BloodIntance.Visible = false;
                        }
                        setBloodIntanceWidthFirstValue = true;
                        BloodIntanceWidthFirstValue = 100.7296f;
                        setBloodIntanceXFirstValue = true;
                        BloodIntanceXFirstValue = -2f;
                        setBloodIntanceYFirstValue = true;
                        BloodIntanceYFirstValue = -8f;
                        if (interpolationValue < 1)
                        {
                            this.btn_GameOver_Points.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.btn_GameOver_Points.Text = "Pontos";
                        }
                        setbtn_GameOver_PointsXFirstValue = true;
                        btn_GameOver_PointsXFirstValue = 0f;
                        setbtn_GameOver_PointsYFirstValue = true;
                        btn_GameOver_PointsYFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.btn_Ranking.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.btn_Ranking.Text = "Ranking";
                        }
                        if (interpolationValue < 1)
                        {
                            this.btn_Ranking.Visible = true;
                        }
                        setbtn_RankingXFirstValue = true;
                        btn_RankingXFirstValue = 160f;
                        setbtn_RankingYFirstValue = true;
                        btn_RankingYFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.Btn_VoltarInicio.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Btn_VoltarInicio.Text = "VoltarInicio";
                        }
                        if (interpolationValue < 1)
                        {
                            this.Btn_VoltarInicio.Visible = true;
                        }
                        setBtn_VoltarInicioXFirstValue = true;
                        Btn_VoltarInicioXFirstValue = 320f;
                        setBtn_VoltarInicioYFirstValue = true;
                        Btn_VoltarInicioYFirstValue = 0f;
                        setContainerInstanceHeightFirstValue = true;
                        ContainerInstanceHeightFirstValue = 79f;
                        if (interpolationValue < 1)
                        {
                            this.ContainerInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "GameOver_DisplayInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.ContainerInstance.Visible = true;
                        }
                        setContainerInstanceWidthFirstValue = true;
                        ContainerInstanceWidthFirstValue = 480f;
                        setContainerInstanceXFirstValue = true;
                        ContainerInstanceXFirstValue = 14f;
                        setContainerInstanceYFirstValue = true;
                        ContainerInstanceYFirstValue = 332f;
                        if (interpolationValue < 1)
                        {
                            this.Controle_DisplayInstance.Visible = false;
                        }
                        setControle_DisplayInstanceXFirstValue = true;
                        Controle_DisplayInstanceXFirstValue = -2f;
                        setControle_DisplayInstanceYFirstValue = true;
                        Controle_DisplayInstanceYFirstValue = 10f;
                        setDialogBoxInstanceHeightFirstValue = true;
                        DialogBoxInstanceHeightFirstValue = 109f;
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.Text = "Yep";
                        }
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.Visible = false;
                        }
                        setDialogBoxInstanceWidthFirstValue = true;
                        DialogBoxInstanceWidthFirstValue = 423f;
                        setDialogBoxInstanceXFirstValue = true;
                        DialogBoxInstanceXFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setDialogBoxInstanceYFirstValue = true;
                        DialogBoxInstanceYFirstValue = -145f;
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                        }
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                        }
                        setGameOver_DisplayInstanceHeightFirstValue = true;
                        GameOver_DisplayInstanceHeightFirstValue = 423f;
                        if (interpolationValue < 1)
                        {
                            this.GameOver_DisplayInstance.Visible = true;
                        }
                        setGameOver_DisplayInstanceXFirstValue = true;
                        GameOver_DisplayInstanceXFirstValue = 170f;
                        setGameOver_DisplayInstanceYFirstValue = true;
                        GameOver_DisplayInstanceYFirstValue = 96f;
                        setInvetoryDisplayHeightFirstValue = true;
                        InvetoryDisplayHeightFirstValue = 38f;
                        if (interpolationValue < 1)
                        {
                            this.InvetoryDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "LevelDisplay_HUD");
                        }
                        setInvetoryDisplayWidthFirstValue = true;
                        InvetoryDisplayWidthFirstValue = 232f;
                        setInvetoryDisplayXFirstValue = true;
                        InvetoryDisplayXFirstValue = 160f;
                        setInvetoryDisplayYFirstValue = true;
                        InvetoryDisplayYFirstValue = -1f;
                        if (interpolationValue < 1)
                        {
                            SetProperty("Item_HUD_0.ItemSprite_hud", "../Entities/Hud/full_heart.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Item_HUD_0.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                        }
                        setItem_HUD_0XFirstValue = true;
                        Item_HUD_0XFirstValue = 7f;
                        setItem_HUD_0YFirstValue = true;
                        Item_HUD_0YFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            SetProperty("Item_HUD_1.ItemSprite_hud", "../Entities/Item/key_silver.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Item_HUD_1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Item_HUD_1.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setItem_HUD_1XFirstValue = true;
                        Item_HUD_1XFirstValue = 53f;
                        setItem_HUD_1YFirstValue = true;
                        Item_HUD_1YFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            SetProperty("Item_HUD_2.ItemSprite_hud", "../Entities/Item/Fire_1.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Item_HUD_2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                        }
                        setItem_HUD_2XFirstValue = true;
                        Item_HUD_2XFirstValue = 102f;
                        setItem_HUD_2YFirstValue = true;
                        Item_HUD_2YFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.Item_HUD_3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Item_HUD_3.Visible = false;
                        }
                        setItem_HUD_3XFirstValue = true;
                        Item_HUD_3XFirstValue = 145f;
                        setItem_HUD_3YFirstValue = true;
                        Item_HUD_3YFirstValue = 0f;
                        setLevelDisplay_HUDHeightFirstValue = true;
                        LevelDisplay_HUDHeightFirstValue = -552f;
                        if (interpolationValue < 1)
                        {
                            this.LevelDisplay_HUD.Visible = false;
                        }
                        setLevelDisplay_HUDWidthFirstValue = true;
                        LevelDisplay_HUDWidthFirstValue = -736f;
                        setLevelDisplay_HUDXFirstValue = true;
                        LevelDisplay_HUDXFirstValue = 14f;
                        setLevelDisplay_HUDYFirstValue = true;
                        LevelDisplay_HUDYFirstValue = 12f;
                        setName_DisplayInstanceHeightFirstValue = true;
                        Name_DisplayInstanceHeightFirstValue = 35f;
                        if (interpolationValue < 1)
                        {
                            this.Name_DisplayInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "LevelDisplay_HUD");
                        }
                        setName_DisplayInstanceWidthFirstValue = true;
                        Name_DisplayInstanceWidthFirstValue = 243f;
                        setName_DisplayInstanceXFirstValue = true;
                        Name_DisplayInstanceXFirstValue = 417f;
                        setName_DisplayInstanceYFirstValue = true;
                        Name_DisplayInstanceYFirstValue = -1f;
                        setTimeDisplayHeightFirstValue = true;
                        TimeDisplayHeightFirstValue = 34f;
                        if (interpolationValue < 1)
                        {
                            this.TimeDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "LevelDisplay_HUD");
                        }
                        setTimeDisplayXFirstValue = true;
                        TimeDisplayXFirstValue = 70f;
                        setTimeDisplayYFirstValue = true;
                        TimeDisplayYFirstValue = 1f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setBloodIntanceHeightSecondValue = true;
                        BloodIntanceHeightSecondValue = 80.63882f;
                        if (interpolationValue >= 1)
                        {
                            SetProperty("BloodIntance.SourceFile", "Blood.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.BloodIntance.Visible = false;
                        }
                        setBloodIntanceWidthSecondValue = true;
                        BloodIntanceWidthSecondValue = 100.7296f;
                        setBloodIntanceXSecondValue = true;
                        BloodIntanceXSecondValue = -2f;
                        setBloodIntanceYSecondValue = true;
                        BloodIntanceYSecondValue = -8f;
                        if (interpolationValue >= 1)
                        {
                            this.btn_GameOver_Points.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.btn_GameOver_Points.Text = "Pontos";
                        }
                        setbtn_GameOver_PointsXSecondValue = true;
                        btn_GameOver_PointsXSecondValue = 0f;
                        setbtn_GameOver_PointsYSecondValue = true;
                        btn_GameOver_PointsYSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.btn_Ranking.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.btn_Ranking.Text = "Ranking";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.btn_Ranking.Visible = true;
                        }
                        setbtn_RankingXSecondValue = true;
                        btn_RankingXSecondValue = 160f;
                        setbtn_RankingYSecondValue = true;
                        btn_RankingYSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.Btn_VoltarInicio.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Btn_VoltarInicio.Text = "VoltarInicio";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Btn_VoltarInicio.Visible = true;
                        }
                        setBtn_VoltarInicioXSecondValue = true;
                        Btn_VoltarInicioXSecondValue = 320f;
                        setBtn_VoltarInicioYSecondValue = true;
                        Btn_VoltarInicioYSecondValue = 0f;
                        setContainerInstanceHeightSecondValue = true;
                        ContainerInstanceHeightSecondValue = 79f;
                        if (interpolationValue >= 1)
                        {
                            this.ContainerInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "GameOver_DisplayInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ContainerInstance.Visible = true;
                        }
                        setContainerInstanceWidthSecondValue = true;
                        ContainerInstanceWidthSecondValue = 480f;
                        setContainerInstanceXSecondValue = true;
                        ContainerInstanceXSecondValue = 14f;
                        setContainerInstanceYSecondValue = true;
                        ContainerInstanceYSecondValue = 332f;
                        if (interpolationValue >= 1)
                        {
                            this.Controle_DisplayInstance.Visible = false;
                        }
                        setControle_DisplayInstanceXSecondValue = true;
                        Controle_DisplayInstanceXSecondValue = -2f;
                        setControle_DisplayInstanceYSecondValue = true;
                        Controle_DisplayInstanceYSecondValue = 10f;
                        setDialogBoxInstanceHeightSecondValue = true;
                        DialogBoxInstanceHeightSecondValue = 109f;
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.Text = "Yep";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.Visible = false;
                        }
                        setDialogBoxInstanceWidthSecondValue = true;
                        DialogBoxInstanceWidthSecondValue = 423f;
                        setDialogBoxInstanceXSecondValue = true;
                        DialogBoxInstanceXSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setDialogBoxInstanceYSecondValue = true;
                        DialogBoxInstanceYSecondValue = -145f;
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                        }
                        setGameOver_DisplayInstanceHeightSecondValue = true;
                        GameOver_DisplayInstanceHeightSecondValue = 423f;
                        if (interpolationValue >= 1)
                        {
                            this.GameOver_DisplayInstance.Visible = true;
                        }
                        setGameOver_DisplayInstanceXSecondValue = true;
                        GameOver_DisplayInstanceXSecondValue = 170f;
                        setGameOver_DisplayInstanceYSecondValue = true;
                        GameOver_DisplayInstanceYSecondValue = 96f;
                        setInvetoryDisplayHeightSecondValue = true;
                        InvetoryDisplayHeightSecondValue = 38f;
                        if (interpolationValue >= 1)
                        {
                            this.InvetoryDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "LevelDisplay_HUD");
                        }
                        setInvetoryDisplayWidthSecondValue = true;
                        InvetoryDisplayWidthSecondValue = 232f;
                        setInvetoryDisplayXSecondValue = true;
                        InvetoryDisplayXSecondValue = 160f;
                        setInvetoryDisplayYSecondValue = true;
                        InvetoryDisplayYSecondValue = -1f;
                        if (interpolationValue >= 1)
                        {
                            SetProperty("Item_HUD_0.ItemSprite_hud", "../Entities/Hud/full_heart.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Item_HUD_0.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                        }
                        setItem_HUD_0XSecondValue = true;
                        Item_HUD_0XSecondValue = 7f;
                        setItem_HUD_0YSecondValue = true;
                        Item_HUD_0YSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            SetProperty("Item_HUD_1.ItemSprite_hud", "../Entities/Item/key_silver.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Item_HUD_1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Item_HUD_1.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setItem_HUD_1XSecondValue = true;
                        Item_HUD_1XSecondValue = 53f;
                        setItem_HUD_1YSecondValue = true;
                        Item_HUD_1YSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            SetProperty("Item_HUD_2.ItemSprite_hud", "../Entities/Item/Fire_1.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Item_HUD_2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                        }
                        setItem_HUD_2XSecondValue = true;
                        Item_HUD_2XSecondValue = 102f;
                        setItem_HUD_2YSecondValue = true;
                        Item_HUD_2YSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.Item_HUD_3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "InvetoryDisplay");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Item_HUD_3.Visible = false;
                        }
                        setItem_HUD_3XSecondValue = true;
                        Item_HUD_3XSecondValue = 145f;
                        setItem_HUD_3YSecondValue = true;
                        Item_HUD_3YSecondValue = 0f;
                        setLevelDisplay_HUDHeightSecondValue = true;
                        LevelDisplay_HUDHeightSecondValue = -552f;
                        if (interpolationValue >= 1)
                        {
                            this.LevelDisplay_HUD.Visible = false;
                        }
                        setLevelDisplay_HUDWidthSecondValue = true;
                        LevelDisplay_HUDWidthSecondValue = -736f;
                        setLevelDisplay_HUDXSecondValue = true;
                        LevelDisplay_HUDXSecondValue = 14f;
                        setLevelDisplay_HUDYSecondValue = true;
                        LevelDisplay_HUDYSecondValue = 12f;
                        setName_DisplayInstanceHeightSecondValue = true;
                        Name_DisplayInstanceHeightSecondValue = 35f;
                        if (interpolationValue >= 1)
                        {
                            this.Name_DisplayInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "LevelDisplay_HUD");
                        }
                        setName_DisplayInstanceWidthSecondValue = true;
                        Name_DisplayInstanceWidthSecondValue = 243f;
                        setName_DisplayInstanceXSecondValue = true;
                        Name_DisplayInstanceXSecondValue = 417f;
                        setName_DisplayInstanceYSecondValue = true;
                        Name_DisplayInstanceYSecondValue = -1f;
                        setTimeDisplayHeightSecondValue = true;
                        TimeDisplayHeightSecondValue = 34f;
                        if (interpolationValue >= 1)
                        {
                            this.TimeDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "LevelDisplay_HUD");
                        }
                        setTimeDisplayXSecondValue = true;
                        TimeDisplayXSecondValue = 70f;
                        setTimeDisplayYSecondValue = true;
                        TimeDisplayYSecondValue = 1f;
                        break;
                }
                if (setBloodIntanceHeightFirstValue && setBloodIntanceHeightSecondValue)
                {
                    BloodIntance.Height = BloodIntanceHeightFirstValue * (1 - interpolationValue) + BloodIntanceHeightSecondValue * interpolationValue;
                }
                if (setBloodIntanceWidthFirstValue && setBloodIntanceWidthSecondValue)
                {
                    BloodIntance.Width = BloodIntanceWidthFirstValue * (1 - interpolationValue) + BloodIntanceWidthSecondValue * interpolationValue;
                }
                if (setBloodIntanceXFirstValue && setBloodIntanceXSecondValue)
                {
                    BloodIntance.X = BloodIntanceXFirstValue * (1 - interpolationValue) + BloodIntanceXSecondValue * interpolationValue;
                }
                if (setBloodIntanceYFirstValue && setBloodIntanceYSecondValue)
                {
                    BloodIntance.Y = BloodIntanceYFirstValue * (1 - interpolationValue) + BloodIntanceYSecondValue * interpolationValue;
                }
                if (setbtn_GameOver_PointsXFirstValue && setbtn_GameOver_PointsXSecondValue)
                {
                    btn_GameOver_Points.X = btn_GameOver_PointsXFirstValue * (1 - interpolationValue) + btn_GameOver_PointsXSecondValue * interpolationValue;
                }
                if (setbtn_GameOver_PointsYFirstValue && setbtn_GameOver_PointsYSecondValue)
                {
                    btn_GameOver_Points.Y = btn_GameOver_PointsYFirstValue * (1 - interpolationValue) + btn_GameOver_PointsYSecondValue * interpolationValue;
                }
                if (setbtn_RankingXFirstValue && setbtn_RankingXSecondValue)
                {
                    btn_Ranking.X = btn_RankingXFirstValue * (1 - interpolationValue) + btn_RankingXSecondValue * interpolationValue;
                }
                if (setbtn_RankingYFirstValue && setbtn_RankingYSecondValue)
                {
                    btn_Ranking.Y = btn_RankingYFirstValue * (1 - interpolationValue) + btn_RankingYSecondValue * interpolationValue;
                }
                if (setBtn_VoltarInicioXFirstValue && setBtn_VoltarInicioXSecondValue)
                {
                    Btn_VoltarInicio.X = Btn_VoltarInicioXFirstValue * (1 - interpolationValue) + Btn_VoltarInicioXSecondValue * interpolationValue;
                }
                if (setBtn_VoltarInicioYFirstValue && setBtn_VoltarInicioYSecondValue)
                {
                    Btn_VoltarInicio.Y = Btn_VoltarInicioYFirstValue * (1 - interpolationValue) + Btn_VoltarInicioYSecondValue * interpolationValue;
                }
                if (setContainerInstanceHeightFirstValue && setContainerInstanceHeightSecondValue)
                {
                    ContainerInstance.Height = ContainerInstanceHeightFirstValue * (1 - interpolationValue) + ContainerInstanceHeightSecondValue * interpolationValue;
                }
                if (setContainerInstanceWidthFirstValue && setContainerInstanceWidthSecondValue)
                {
                    ContainerInstance.Width = ContainerInstanceWidthFirstValue * (1 - interpolationValue) + ContainerInstanceWidthSecondValue * interpolationValue;
                }
                if (setContainerInstanceXFirstValue && setContainerInstanceXSecondValue)
                {
                    ContainerInstance.X = ContainerInstanceXFirstValue * (1 - interpolationValue) + ContainerInstanceXSecondValue * interpolationValue;
                }
                if (setContainerInstanceYFirstValue && setContainerInstanceYSecondValue)
                {
                    ContainerInstance.Y = ContainerInstanceYFirstValue * (1 - interpolationValue) + ContainerInstanceYSecondValue * interpolationValue;
                }
                if (setControle_DisplayInstanceXFirstValue && setControle_DisplayInstanceXSecondValue)
                {
                    Controle_DisplayInstance.X = Controle_DisplayInstanceXFirstValue * (1 - interpolationValue) + Controle_DisplayInstanceXSecondValue * interpolationValue;
                }
                if (setControle_DisplayInstanceYFirstValue && setControle_DisplayInstanceYSecondValue)
                {
                    Controle_DisplayInstance.Y = Controle_DisplayInstanceYFirstValue * (1 - interpolationValue) + Controle_DisplayInstanceYSecondValue * interpolationValue;
                }
                if (setDialogBoxInstanceHeightFirstValue && setDialogBoxInstanceHeightSecondValue)
                {
                    DialogBoxInstance.Height = DialogBoxInstanceHeightFirstValue * (1 - interpolationValue) + DialogBoxInstanceHeightSecondValue * interpolationValue;
                }
                if (setDialogBoxInstanceWidthFirstValue && setDialogBoxInstanceWidthSecondValue)
                {
                    DialogBoxInstance.Width = DialogBoxInstanceWidthFirstValue * (1 - interpolationValue) + DialogBoxInstanceWidthSecondValue * interpolationValue;
                }
                if (setDialogBoxInstanceXFirstValue && setDialogBoxInstanceXSecondValue)
                {
                    DialogBoxInstance.X = DialogBoxInstanceXFirstValue * (1 - interpolationValue) + DialogBoxInstanceXSecondValue * interpolationValue;
                }
                if (setDialogBoxInstanceYFirstValue && setDialogBoxInstanceYSecondValue)
                {
                    DialogBoxInstance.Y = DialogBoxInstanceYFirstValue * (1 - interpolationValue) + DialogBoxInstanceYSecondValue * interpolationValue;
                }
                if (setGameOver_DisplayInstanceHeightFirstValue && setGameOver_DisplayInstanceHeightSecondValue)
                {
                    GameOver_DisplayInstance.Height = GameOver_DisplayInstanceHeightFirstValue * (1 - interpolationValue) + GameOver_DisplayInstanceHeightSecondValue * interpolationValue;
                }
                if (setGameOver_DisplayInstanceXFirstValue && setGameOver_DisplayInstanceXSecondValue)
                {
                    GameOver_DisplayInstance.X = GameOver_DisplayInstanceXFirstValue * (1 - interpolationValue) + GameOver_DisplayInstanceXSecondValue * interpolationValue;
                }
                if (setGameOver_DisplayInstanceYFirstValue && setGameOver_DisplayInstanceYSecondValue)
                {
                    GameOver_DisplayInstance.Y = GameOver_DisplayInstanceYFirstValue * (1 - interpolationValue) + GameOver_DisplayInstanceYSecondValue * interpolationValue;
                }
                if (setInvetoryDisplayHeightFirstValue && setInvetoryDisplayHeightSecondValue)
                {
                    InvetoryDisplay.Height = InvetoryDisplayHeightFirstValue * (1 - interpolationValue) + InvetoryDisplayHeightSecondValue * interpolationValue;
                }
                if (setInvetoryDisplayWidthFirstValue && setInvetoryDisplayWidthSecondValue)
                {
                    InvetoryDisplay.Width = InvetoryDisplayWidthFirstValue * (1 - interpolationValue) + InvetoryDisplayWidthSecondValue * interpolationValue;
                }
                if (setInvetoryDisplayXFirstValue && setInvetoryDisplayXSecondValue)
                {
                    InvetoryDisplay.X = InvetoryDisplayXFirstValue * (1 - interpolationValue) + InvetoryDisplayXSecondValue * interpolationValue;
                }
                if (setInvetoryDisplayYFirstValue && setInvetoryDisplayYSecondValue)
                {
                    InvetoryDisplay.Y = InvetoryDisplayYFirstValue * (1 - interpolationValue) + InvetoryDisplayYSecondValue * interpolationValue;
                }
                if (setItem_HUD_0XFirstValue && setItem_HUD_0XSecondValue)
                {
                    Item_HUD_0.X = Item_HUD_0XFirstValue * (1 - interpolationValue) + Item_HUD_0XSecondValue * interpolationValue;
                }
                if (setItem_HUD_0YFirstValue && setItem_HUD_0YSecondValue)
                {
                    Item_HUD_0.Y = Item_HUD_0YFirstValue * (1 - interpolationValue) + Item_HUD_0YSecondValue * interpolationValue;
                }
                if (setItem_HUD_1XFirstValue && setItem_HUD_1XSecondValue)
                {
                    Item_HUD_1.X = Item_HUD_1XFirstValue * (1 - interpolationValue) + Item_HUD_1XSecondValue * interpolationValue;
                }
                if (setItem_HUD_1YFirstValue && setItem_HUD_1YSecondValue)
                {
                    Item_HUD_1.Y = Item_HUD_1YFirstValue * (1 - interpolationValue) + Item_HUD_1YSecondValue * interpolationValue;
                }
                if (setItem_HUD_2XFirstValue && setItem_HUD_2XSecondValue)
                {
                    Item_HUD_2.X = Item_HUD_2XFirstValue * (1 - interpolationValue) + Item_HUD_2XSecondValue * interpolationValue;
                }
                if (setItem_HUD_2YFirstValue && setItem_HUD_2YSecondValue)
                {
                    Item_HUD_2.Y = Item_HUD_2YFirstValue * (1 - interpolationValue) + Item_HUD_2YSecondValue * interpolationValue;
                }
                if (setItem_HUD_3XFirstValue && setItem_HUD_3XSecondValue)
                {
                    Item_HUD_3.X = Item_HUD_3XFirstValue * (1 - interpolationValue) + Item_HUD_3XSecondValue * interpolationValue;
                }
                if (setItem_HUD_3YFirstValue && setItem_HUD_3YSecondValue)
                {
                    Item_HUD_3.Y = Item_HUD_3YFirstValue * (1 - interpolationValue) + Item_HUD_3YSecondValue * interpolationValue;
                }
                if (setLevelDisplay_HUDHeightFirstValue && setLevelDisplay_HUDHeightSecondValue)
                {
                    LevelDisplay_HUD.Height = LevelDisplay_HUDHeightFirstValue * (1 - interpolationValue) + LevelDisplay_HUDHeightSecondValue * interpolationValue;
                }
                if (setLevelDisplay_HUDWidthFirstValue && setLevelDisplay_HUDWidthSecondValue)
                {
                    LevelDisplay_HUD.Width = LevelDisplay_HUDWidthFirstValue * (1 - interpolationValue) + LevelDisplay_HUDWidthSecondValue * interpolationValue;
                }
                if (setLevelDisplay_HUDXFirstValue && setLevelDisplay_HUDXSecondValue)
                {
                    LevelDisplay_HUD.X = LevelDisplay_HUDXFirstValue * (1 - interpolationValue) + LevelDisplay_HUDXSecondValue * interpolationValue;
                }
                if (setLevelDisplay_HUDYFirstValue && setLevelDisplay_HUDYSecondValue)
                {
                    LevelDisplay_HUD.Y = LevelDisplay_HUDYFirstValue * (1 - interpolationValue) + LevelDisplay_HUDYSecondValue * interpolationValue;
                }
                if (setName_DisplayInstanceHeightFirstValue && setName_DisplayInstanceHeightSecondValue)
                {
                    Name_DisplayInstance.Height = Name_DisplayInstanceHeightFirstValue * (1 - interpolationValue) + Name_DisplayInstanceHeightSecondValue * interpolationValue;
                }
                if (setName_DisplayInstanceWidthFirstValue && setName_DisplayInstanceWidthSecondValue)
                {
                    Name_DisplayInstance.Width = Name_DisplayInstanceWidthFirstValue * (1 - interpolationValue) + Name_DisplayInstanceWidthSecondValue * interpolationValue;
                }
                if (setName_DisplayInstanceXFirstValue && setName_DisplayInstanceXSecondValue)
                {
                    Name_DisplayInstance.X = Name_DisplayInstanceXFirstValue * (1 - interpolationValue) + Name_DisplayInstanceXSecondValue * interpolationValue;
                }
                if (setName_DisplayInstanceYFirstValue && setName_DisplayInstanceYSecondValue)
                {
                    Name_DisplayInstance.Y = Name_DisplayInstanceYFirstValue * (1 - interpolationValue) + Name_DisplayInstanceYSecondValue * interpolationValue;
                }
                if (setTimeDisplayHeightFirstValue && setTimeDisplayHeightSecondValue)
                {
                    TimeDisplay.Height = TimeDisplayHeightFirstValue * (1 - interpolationValue) + TimeDisplayHeightSecondValue * interpolationValue;
                }
                if (setTimeDisplayXFirstValue && setTimeDisplayXSecondValue)
                {
                    TimeDisplay.X = TimeDisplayXFirstValue * (1 - interpolationValue) + TimeDisplayXSecondValue * interpolationValue;
                }
                if (setTimeDisplayYFirstValue && setTimeDisplayYSecondValue)
                {
                    TimeDisplay.Y = TimeDisplayYFirstValue * (1 - interpolationValue) + TimeDisplayYSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.GameHudGumRuntime.VariableState fromState,DungeonRun.GumRuntimes.GameHudGumRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
                LevelDisplay_HUD.StopAnimations();
                Item_HUD_1.StopAnimations();
                Item_HUD_2.StopAnimations();
                Item_HUD_0.StopAnimations();
                Item_HUD_3.StopAnimations();
                Name_DisplayInstance.StopAnimations();
                TimeDisplay.StopAnimations();
                Controle_DisplayInstance.StopAnimations();
                GameOver_DisplayInstance.StopAnimations();
                btn_Ranking.StopAnimations();
                Btn_VoltarInicio.StopAnimations();
                DialogBoxInstance.StopAnimations();
                btn_GameOver_Points.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.Height",
                            Type = "float",
                            Value = BloodIntance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.SourceFile",
                            Type = "string",
                            Value = BloodIntance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.Visible",
                            Type = "bool",
                            Value = BloodIntance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.Width",
                            Type = "float",
                            Value = BloodIntance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.X",
                            Type = "float",
                            Value = BloodIntance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.Y",
                            Type = "float",
                            Value = BloodIntance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelDisplay_HUD.Height",
                            Type = "float",
                            Value = LevelDisplay_HUD.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelDisplay_HUD.Visible",
                            Type = "bool",
                            Value = LevelDisplay_HUD.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelDisplay_HUD.Width",
                            Type = "float",
                            Value = LevelDisplay_HUD.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelDisplay_HUD.X",
                            Type = "float",
                            Value = LevelDisplay_HUD.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelDisplay_HUD.Y",
                            Type = "float",
                            Value = LevelDisplay_HUD.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "InvetoryDisplay.Height",
                            Type = "float",
                            Value = InvetoryDisplay.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "InvetoryDisplay.Parent",
                            Type = "string",
                            Value = InvetoryDisplay.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "InvetoryDisplay.Width",
                            Type = "float",
                            Value = InvetoryDisplay.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "InvetoryDisplay.X",
                            Type = "float",
                            Value = InvetoryDisplay.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "InvetoryDisplay.Y",
                            Type = "float",
                            Value = InvetoryDisplay.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_1.ItemSprite_hud",
                            Type = "string",
                            Value = Item_HUD_1.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_1.Parent",
                            Type = "string",
                            Value = Item_HUD_1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_1.Width Units",
                            Type = "DimensionUnitType",
                            Value = Item_HUD_1.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_1.X",
                            Type = "float",
                            Value = Item_HUD_1.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_1.Y",
                            Type = "float",
                            Value = Item_HUD_1.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_2.ItemSprite_hud",
                            Type = "string",
                            Value = Item_HUD_2.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_2.Parent",
                            Type = "string",
                            Value = Item_HUD_2.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_2.X",
                            Type = "float",
                            Value = Item_HUD_2.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_2.Y",
                            Type = "float",
                            Value = Item_HUD_2.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_0.ItemSprite_hud",
                            Type = "string",
                            Value = Item_HUD_0.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_0.Parent",
                            Type = "string",
                            Value = Item_HUD_0.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_0.X",
                            Type = "float",
                            Value = Item_HUD_0.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_0.Y",
                            Type = "float",
                            Value = Item_HUD_0.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_3.Parent",
                            Type = "string",
                            Value = Item_HUD_3.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_3.Visible",
                            Type = "bool",
                            Value = Item_HUD_3.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_3.X",
                            Type = "float",
                            Value = Item_HUD_3.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_3.Y",
                            Type = "float",
                            Value = Item_HUD_3.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Name_DisplayInstance.Height",
                            Type = "float",
                            Value = Name_DisplayInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Name_DisplayInstance.Parent",
                            Type = "string",
                            Value = Name_DisplayInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Name_DisplayInstance.Width",
                            Type = "float",
                            Value = Name_DisplayInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Name_DisplayInstance.X",
                            Type = "float",
                            Value = Name_DisplayInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Name_DisplayInstance.Y",
                            Type = "float",
                            Value = Name_DisplayInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimeDisplay.Height",
                            Type = "float",
                            Value = TimeDisplay.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimeDisplay.Parent",
                            Type = "string",
                            Value = TimeDisplay.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimeDisplay.X",
                            Type = "float",
                            Value = TimeDisplay.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimeDisplay.Y",
                            Type = "float",
                            Value = TimeDisplay.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Controle_DisplayInstance.Visible",
                            Type = "bool",
                            Value = Controle_DisplayInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Controle_DisplayInstance.X",
                            Type = "float",
                            Value = Controle_DisplayInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Controle_DisplayInstance.Y",
                            Type = "float",
                            Value = Controle_DisplayInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOver_DisplayInstance.Height",
                            Type = "float",
                            Value = GameOver_DisplayInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOver_DisplayInstance.Visible",
                            Type = "bool",
                            Value = GameOver_DisplayInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOver_DisplayInstance.X",
                            Type = "float",
                            Value = GameOver_DisplayInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOver_DisplayInstance.Y",
                            Type = "float",
                            Value = GameOver_DisplayInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_Ranking.Parent",
                            Type = "string",
                            Value = btn_Ranking.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_Ranking.Text",
                            Type = "string",
                            Value = btn_Ranking.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_Ranking.Visible",
                            Type = "bool",
                            Value = btn_Ranking.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_Ranking.X",
                            Type = "float",
                            Value = btn_Ranking.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_Ranking.Y",
                            Type = "float",
                            Value = btn_Ranking.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Btn_VoltarInicio.Parent",
                            Type = "string",
                            Value = Btn_VoltarInicio.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Btn_VoltarInicio.Text",
                            Type = "string",
                            Value = Btn_VoltarInicio.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Btn_VoltarInicio.Visible",
                            Type = "bool",
                            Value = Btn_VoltarInicio.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Btn_VoltarInicio.X",
                            Type = "float",
                            Value = Btn_VoltarInicio.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Btn_VoltarInicio.Y",
                            Type = "float",
                            Value = Btn_VoltarInicio.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height",
                            Type = "float",
                            Value = ContainerInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Parent",
                            Type = "string",
                            Value = ContainerInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Visible",
                            Type = "bool",
                            Value = ContainerInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width",
                            Type = "float",
                            Value = ContainerInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.X",
                            Type = "float",
                            Value = ContainerInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Y",
                            Type = "float",
                            Value = ContainerInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Height",
                            Type = "float",
                            Value = DialogBoxInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Text",
                            Type = "string",
                            Value = DialogBoxInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Visible",
                            Type = "bool",
                            Value = DialogBoxInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Width",
                            Type = "float",
                            Value = DialogBoxInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X",
                            Type = "float",
                            Value = DialogBoxInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = DialogBoxInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X Units",
                            Type = "PositionUnitType",
                            Value = DialogBoxInstance.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y",
                            Type = "float",
                            Value = DialogBoxInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y Origin",
                            Type = "VerticalAlignment",
                            Value = DialogBoxInstance.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y Units",
                            Type = "PositionUnitType",
                            Value = DialogBoxInstance.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_GameOver_Points.Parent",
                            Type = "string",
                            Value = btn_GameOver_Points.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_GameOver_Points.Text",
                            Type = "string",
                            Value = btn_GameOver_Points.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_GameOver_Points.X",
                            Type = "float",
                            Value = btn_GameOver_Points.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_GameOver_Points.Y",
                            Type = "float",
                            Value = btn_GameOver_Points.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.Height",
                            Type = "float",
                            Value = BloodIntance.Height + 80.63882f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.SourceFile",
                            Type = "string",
                            Value = BloodIntance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.Visible",
                            Type = "bool",
                            Value = BloodIntance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.Width",
                            Type = "float",
                            Value = BloodIntance.Width + 100.7296f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.X",
                            Type = "float",
                            Value = BloodIntance.X + -2f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "BloodIntance.Y",
                            Type = "float",
                            Value = BloodIntance.Y + -8f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelDisplay_HUD.Height",
                            Type = "float",
                            Value = LevelDisplay_HUD.Height + -552f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelDisplay_HUD.Visible",
                            Type = "bool",
                            Value = LevelDisplay_HUD.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelDisplay_HUD.Width",
                            Type = "float",
                            Value = LevelDisplay_HUD.Width + -736f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelDisplay_HUD.X",
                            Type = "float",
                            Value = LevelDisplay_HUD.X + 14f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelDisplay_HUD.Y",
                            Type = "float",
                            Value = LevelDisplay_HUD.Y + 12f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "InvetoryDisplay.Height",
                            Type = "float",
                            Value = InvetoryDisplay.Height + 38f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "InvetoryDisplay.Parent",
                            Type = "string",
                            Value = InvetoryDisplay.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "InvetoryDisplay.Width",
                            Type = "float",
                            Value = InvetoryDisplay.Width + 232f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "InvetoryDisplay.X",
                            Type = "float",
                            Value = InvetoryDisplay.X + 160f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "InvetoryDisplay.Y",
                            Type = "float",
                            Value = InvetoryDisplay.Y + -1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_1.ItemSprite_hud",
                            Type = "string",
                            Value = Item_HUD_1.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_1.Parent",
                            Type = "string",
                            Value = Item_HUD_1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_1.Width Units",
                            Type = "DimensionUnitType",
                            Value = Item_HUD_1.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_1.X",
                            Type = "float",
                            Value = Item_HUD_1.X + 53f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_1.Y",
                            Type = "float",
                            Value = Item_HUD_1.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_2.ItemSprite_hud",
                            Type = "string",
                            Value = Item_HUD_2.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_2.Parent",
                            Type = "string",
                            Value = Item_HUD_2.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_2.X",
                            Type = "float",
                            Value = Item_HUD_2.X + 102f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_2.Y",
                            Type = "float",
                            Value = Item_HUD_2.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_0.ItemSprite_hud",
                            Type = "string",
                            Value = Item_HUD_0.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_0.Parent",
                            Type = "string",
                            Value = Item_HUD_0.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_0.X",
                            Type = "float",
                            Value = Item_HUD_0.X + 7f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_0.Y",
                            Type = "float",
                            Value = Item_HUD_0.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_3.Parent",
                            Type = "string",
                            Value = Item_HUD_3.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_3.Visible",
                            Type = "bool",
                            Value = Item_HUD_3.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_3.X",
                            Type = "float",
                            Value = Item_HUD_3.X + 145f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_HUD_3.Y",
                            Type = "float",
                            Value = Item_HUD_3.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Name_DisplayInstance.Height",
                            Type = "float",
                            Value = Name_DisplayInstance.Height + 35f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Name_DisplayInstance.Parent",
                            Type = "string",
                            Value = Name_DisplayInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Name_DisplayInstance.Width",
                            Type = "float",
                            Value = Name_DisplayInstance.Width + 243f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Name_DisplayInstance.X",
                            Type = "float",
                            Value = Name_DisplayInstance.X + 417f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Name_DisplayInstance.Y",
                            Type = "float",
                            Value = Name_DisplayInstance.Y + -1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimeDisplay.Height",
                            Type = "float",
                            Value = TimeDisplay.Height + 34f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimeDisplay.Parent",
                            Type = "string",
                            Value = TimeDisplay.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimeDisplay.X",
                            Type = "float",
                            Value = TimeDisplay.X + 70f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimeDisplay.Y",
                            Type = "float",
                            Value = TimeDisplay.Y + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Controle_DisplayInstance.Visible",
                            Type = "bool",
                            Value = Controle_DisplayInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Controle_DisplayInstance.X",
                            Type = "float",
                            Value = Controle_DisplayInstance.X + -2f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Controle_DisplayInstance.Y",
                            Type = "float",
                            Value = Controle_DisplayInstance.Y + 10f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOver_DisplayInstance.Height",
                            Type = "float",
                            Value = GameOver_DisplayInstance.Height + 423f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOver_DisplayInstance.Visible",
                            Type = "bool",
                            Value = GameOver_DisplayInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOver_DisplayInstance.X",
                            Type = "float",
                            Value = GameOver_DisplayInstance.X + 170f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOver_DisplayInstance.Y",
                            Type = "float",
                            Value = GameOver_DisplayInstance.Y + 96f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_Ranking.Parent",
                            Type = "string",
                            Value = btn_Ranking.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_Ranking.Text",
                            Type = "string",
                            Value = btn_Ranking.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_Ranking.Visible",
                            Type = "bool",
                            Value = btn_Ranking.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_Ranking.X",
                            Type = "float",
                            Value = btn_Ranking.X + 160f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_Ranking.Y",
                            Type = "float",
                            Value = btn_Ranking.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Btn_VoltarInicio.Parent",
                            Type = "string",
                            Value = Btn_VoltarInicio.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Btn_VoltarInicio.Text",
                            Type = "string",
                            Value = Btn_VoltarInicio.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Btn_VoltarInicio.Visible",
                            Type = "bool",
                            Value = Btn_VoltarInicio.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Btn_VoltarInicio.X",
                            Type = "float",
                            Value = Btn_VoltarInicio.X + 320f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Btn_VoltarInicio.Y",
                            Type = "float",
                            Value = Btn_VoltarInicio.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height",
                            Type = "float",
                            Value = ContainerInstance.Height + 79f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Parent",
                            Type = "string",
                            Value = ContainerInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Visible",
                            Type = "bool",
                            Value = ContainerInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width",
                            Type = "float",
                            Value = ContainerInstance.Width + 480f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.X",
                            Type = "float",
                            Value = ContainerInstance.X + 14f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Y",
                            Type = "float",
                            Value = ContainerInstance.Y + 332f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Height",
                            Type = "float",
                            Value = DialogBoxInstance.Height + 109f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Text",
                            Type = "string",
                            Value = DialogBoxInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Visible",
                            Type = "bool",
                            Value = DialogBoxInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Width",
                            Type = "float",
                            Value = DialogBoxInstance.Width + 423f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X",
                            Type = "float",
                            Value = DialogBoxInstance.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = DialogBoxInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X Units",
                            Type = "PositionUnitType",
                            Value = DialogBoxInstance.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y",
                            Type = "float",
                            Value = DialogBoxInstance.Y + -145f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y Origin",
                            Type = "VerticalAlignment",
                            Value = DialogBoxInstance.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y Units",
                            Type = "PositionUnitType",
                            Value = DialogBoxInstance.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_GameOver_Points.Parent",
                            Type = "string",
                            Value = btn_GameOver_Points.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_GameOver_Points.Text",
                            Type = "string",
                            Value = btn_GameOver_Points.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_GameOver_Points.X",
                            Type = "float",
                            Value = btn_GameOver_Points.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btn_GameOver_Points.Y",
                            Type = "float",
                            Value = btn_GameOver_Points.Y + 0f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.SpriteRuntime BloodIntance { get; set; }
            private DungeonRun.GumRuntimes.LevelDisplayRuntime LevelDisplay_HUD { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime InvetoryDisplay { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplayRuntime Item_HUD_1 { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplayRuntime Item_HUD_2 { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplayRuntime Item_HUD_0 { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplayRuntime Item_HUD_3 { get; set; }
            private DungeonRun.GumRuntimes.Name_DisplayRuntime Name_DisplayInstance { get; set; }
            private DungeonRun.GumRuntimes.TimeDisplayRuntime TimeDisplay { get; set; }
            private DungeonRun.GumRuntimes.Panel2_DisplayRuntime Controle_DisplayInstance { get; set; }
            private DungeonRun.GumRuntimes.GameOver_DisplayRuntime GameOver_DisplayInstance { get; set; }
            private DungeonRun.GumRuntimes.ButtonRuntime btn_Ranking { get; set; }
            private DungeonRun.GumRuntimes.ButtonRuntime Btn_VoltarInicio { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime ContainerInstance { get; set; }
            private DungeonRun.GumRuntimes.DialogBoxRuntime DialogBoxInstance { get; set; }
            private DungeonRun.GumRuntimes.ButtonRuntime btn_GameOver_Points { get; set; }
            public GameHudGumRuntime (bool fullInstantiation = true) 
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Screens.First(item => item.Name == "GameHudGum");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                BloodIntance = this.GetGraphicalUiElementByName("BloodIntance") as DungeonRun.GumRuntimes.SpriteRuntime;
                LevelDisplay_HUD = this.GetGraphicalUiElementByName("LevelDisplay_HUD") as DungeonRun.GumRuntimes.LevelDisplayRuntime;
                InvetoryDisplay = this.GetGraphicalUiElementByName("InvetoryDisplay") as DungeonRun.GumRuntimes.ContainerRuntime;
                Item_HUD_1 = this.GetGraphicalUiElementByName("Item_HUD_1") as DungeonRun.GumRuntimes.ItemDisplayRuntime;
                Item_HUD_2 = this.GetGraphicalUiElementByName("Item_HUD_2") as DungeonRun.GumRuntimes.ItemDisplayRuntime;
                Item_HUD_0 = this.GetGraphicalUiElementByName("Item_HUD_0") as DungeonRun.GumRuntimes.ItemDisplayRuntime;
                Item_HUD_3 = this.GetGraphicalUiElementByName("Item_HUD_3") as DungeonRun.GumRuntimes.ItemDisplayRuntime;
                Name_DisplayInstance = this.GetGraphicalUiElementByName("Name_DisplayInstance") as DungeonRun.GumRuntimes.Name_DisplayRuntime;
                TimeDisplay = this.GetGraphicalUiElementByName("TimeDisplay") as DungeonRun.GumRuntimes.TimeDisplayRuntime;
                Controle_DisplayInstance = this.GetGraphicalUiElementByName("Controle_DisplayInstance") as DungeonRun.GumRuntimes.Panel2_DisplayRuntime;
                GameOver_DisplayInstance = this.GetGraphicalUiElementByName("GameOver_DisplayInstance") as DungeonRun.GumRuntimes.GameOver_DisplayRuntime;
                btn_Ranking = this.GetGraphicalUiElementByName("btn_Ranking") as DungeonRun.GumRuntimes.ButtonRuntime;
                Btn_VoltarInicio = this.GetGraphicalUiElementByName("Btn_VoltarInicio") as DungeonRun.GumRuntimes.ButtonRuntime;
                ContainerInstance = this.GetGraphicalUiElementByName("ContainerInstance") as DungeonRun.GumRuntimes.ContainerRuntime;
                DialogBoxInstance = this.GetGraphicalUiElementByName("DialogBoxInstance") as DungeonRun.GumRuntimes.DialogBoxRuntime;
                btn_GameOver_Points = this.GetGraphicalUiElementByName("btn_GameOver_Points") as DungeonRun.GumRuntimes.ButtonRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
