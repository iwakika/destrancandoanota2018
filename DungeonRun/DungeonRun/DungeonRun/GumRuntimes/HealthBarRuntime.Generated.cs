    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class HealthBarRuntime : DungeonRun.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default,
                Full,
                Empty
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            Height = 65f;
                            Width = 227f;
                            X = 1f;
                            Y = 25f;
                            Health_Background.Blue = 0;
                            Health_Background.Green = 0;
                            Health_Background.Height = -30f;
                            Health_Background.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Health_Background.Red = 128;
                            Health_Background.Width = -45f;
                            Health_Background.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Health_Background.X = 23f;
                            Health_Background.Y = 10f;
                            Health_Bar.Blue = 50;
                            Health_Bar.Green = 205;
                            Health_Bar.Height = 30f;
                            Health_Bar.HeightUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                            Health_Bar.Red = 50;
                            Health_Bar.Width = 175f;
                            Health_Bar.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                            Health_Bar.X = 27f;
                            Health_Bar.Y = 14f;
                            health_Frame.Height = -12f;
                            health_Frame.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            SetProperty("health_Frame.SourceFile", "../resources/enemy_health_bar_foreground_003.png");
                            health_Frame.Visible = true;
                            health_Frame.Width = 0f;
                            health_Frame.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            health_Frame.X = 3f;
                            health_Frame.Y = -19f;
                            break;
                        case  VariableState.Full:
                            Health_Bar.Blue = 0;
                            Health_Bar.Green = 255;
                            Health_Bar.Height = 30f;
                            Health_Bar.Red = 0;
                            break;
                        case  VariableState.Empty:
                            Health_Bar.Blue = 0;
                            Health_Bar.Green = 0;
                            Health_Bar.Red = 255;
                            Health_Bar.Width = 10f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setHealth_BackgroundBlueFirstValue = false;
                bool setHealth_BackgroundBlueSecondValue = false;
                int Health_BackgroundBlueFirstValue= 0;
                int Health_BackgroundBlueSecondValue= 0;
                bool setHealth_BackgroundGreenFirstValue = false;
                bool setHealth_BackgroundGreenSecondValue = false;
                int Health_BackgroundGreenFirstValue= 0;
                int Health_BackgroundGreenSecondValue= 0;
                bool setHealth_BackgroundHeightFirstValue = false;
                bool setHealth_BackgroundHeightSecondValue = false;
                float Health_BackgroundHeightFirstValue= 0;
                float Health_BackgroundHeightSecondValue= 0;
                bool setHealth_BackgroundRedFirstValue = false;
                bool setHealth_BackgroundRedSecondValue = false;
                int Health_BackgroundRedFirstValue= 0;
                int Health_BackgroundRedSecondValue= 0;
                bool setHealth_BackgroundWidthFirstValue = false;
                bool setHealth_BackgroundWidthSecondValue = false;
                float Health_BackgroundWidthFirstValue= 0;
                float Health_BackgroundWidthSecondValue= 0;
                bool setHealth_BackgroundXFirstValue = false;
                bool setHealth_BackgroundXSecondValue = false;
                float Health_BackgroundXFirstValue= 0;
                float Health_BackgroundXSecondValue= 0;
                bool setHealth_BackgroundYFirstValue = false;
                bool setHealth_BackgroundYSecondValue = false;
                float Health_BackgroundYFirstValue= 0;
                float Health_BackgroundYSecondValue= 0;
                bool setHealth_BarBlueFirstValue = false;
                bool setHealth_BarBlueSecondValue = false;
                int Health_BarBlueFirstValue= 0;
                int Health_BarBlueSecondValue= 0;
                bool setHealth_BarGreenFirstValue = false;
                bool setHealth_BarGreenSecondValue = false;
                int Health_BarGreenFirstValue= 0;
                int Health_BarGreenSecondValue= 0;
                bool setHealth_BarHeightFirstValue = false;
                bool setHealth_BarHeightSecondValue = false;
                float Health_BarHeightFirstValue= 0;
                float Health_BarHeightSecondValue= 0;
                bool setHealth_BarRedFirstValue = false;
                bool setHealth_BarRedSecondValue = false;
                int Health_BarRedFirstValue= 0;
                int Health_BarRedSecondValue= 0;
                bool setHealth_BarWidthFirstValue = false;
                bool setHealth_BarWidthSecondValue = false;
                float Health_BarWidthFirstValue= 0;
                float Health_BarWidthSecondValue= 0;
                bool setHealth_BarXFirstValue = false;
                bool setHealth_BarXSecondValue = false;
                float Health_BarXFirstValue= 0;
                float Health_BarXSecondValue= 0;
                bool setHealth_BarYFirstValue = false;
                bool setHealth_BarYSecondValue = false;
                float Health_BarYFirstValue= 0;
                float Health_BarYSecondValue= 0;
                bool sethealth_FrameHeightFirstValue = false;
                bool sethealth_FrameHeightSecondValue = false;
                float health_FrameHeightFirstValue= 0;
                float health_FrameHeightSecondValue= 0;
                bool sethealth_FrameWidthFirstValue = false;
                bool sethealth_FrameWidthSecondValue = false;
                float health_FrameWidthFirstValue= 0;
                float health_FrameWidthSecondValue= 0;
                bool sethealth_FrameXFirstValue = false;
                bool sethealth_FrameXSecondValue = false;
                float health_FrameXFirstValue= 0;
                float health_FrameXSecondValue= 0;
                bool sethealth_FrameYFirstValue = false;
                bool sethealth_FrameYSecondValue = false;
                float health_FrameYFirstValue= 0;
                float health_FrameYSecondValue= 0;
                bool setHeightFirstValue = false;
                bool setHeightSecondValue = false;
                float HeightFirstValue= 0;
                float HeightSecondValue= 0;
                bool setWidthFirstValue = false;
                bool setWidthSecondValue = false;
                float WidthFirstValue= 0;
                float WidthSecondValue= 0;
                bool setXFirstValue = false;
                bool setXSecondValue = false;
                float XFirstValue= 0;
                float XSecondValue= 0;
                bool setYFirstValue = false;
                bool setYSecondValue = false;
                float YFirstValue= 0;
                float YSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setHealth_BackgroundBlueFirstValue = true;
                        Health_BackgroundBlueFirstValue = 0;
                        setHealth_BackgroundGreenFirstValue = true;
                        Health_BackgroundGreenFirstValue = 0;
                        setHealth_BackgroundHeightFirstValue = true;
                        Health_BackgroundHeightFirstValue = -30f;
                        if (interpolationValue < 1)
                        {
                            this.Health_Background.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHealth_BackgroundRedFirstValue = true;
                        Health_BackgroundRedFirstValue = 128;
                        setHealth_BackgroundWidthFirstValue = true;
                        Health_BackgroundWidthFirstValue = -45f;
                        if (interpolationValue < 1)
                        {
                            this.Health_Background.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHealth_BackgroundXFirstValue = true;
                        Health_BackgroundXFirstValue = 23f;
                        setHealth_BackgroundYFirstValue = true;
                        Health_BackgroundYFirstValue = 10f;
                        setHealth_BarBlueFirstValue = true;
                        Health_BarBlueFirstValue = 50;
                        setHealth_BarGreenFirstValue = true;
                        Health_BarGreenFirstValue = 205;
                        setHealth_BarHeightFirstValue = true;
                        Health_BarHeightFirstValue = 30f;
                        if (interpolationValue < 1)
                        {
                            this.Health_Bar.HeightUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setHealth_BarRedFirstValue = true;
                        Health_BarRedFirstValue = 50;
                        setHealth_BarWidthFirstValue = true;
                        Health_BarWidthFirstValue = 175f;
                        if (interpolationValue < 1)
                        {
                            this.Health_Bar.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setHealth_BarXFirstValue = true;
                        Health_BarXFirstValue = 27f;
                        setHealth_BarYFirstValue = true;
                        Health_BarYFirstValue = 14f;
                        sethealth_FrameHeightFirstValue = true;
                        health_FrameHeightFirstValue = -12f;
                        if (interpolationValue < 1)
                        {
                            this.health_Frame.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("health_Frame.SourceFile", "../resources/enemy_health_bar_foreground_003.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.health_Frame.Visible = true;
                        }
                        sethealth_FrameWidthFirstValue = true;
                        health_FrameWidthFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.health_Frame.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        sethealth_FrameXFirstValue = true;
                        health_FrameXFirstValue = 3f;
                        sethealth_FrameYFirstValue = true;
                        health_FrameYFirstValue = -19f;
                        setHeightFirstValue = true;
                        HeightFirstValue = 65f;
                        setWidthFirstValue = true;
                        WidthFirstValue = 227f;
                        setXFirstValue = true;
                        XFirstValue = 1f;
                        setYFirstValue = true;
                        YFirstValue = 25f;
                        break;
                    case  VariableState.Full:
                        setHealth_BarBlueFirstValue = true;
                        Health_BarBlueFirstValue = 0;
                        setHealth_BarGreenFirstValue = true;
                        Health_BarGreenFirstValue = 255;
                        setHealth_BarHeightFirstValue = true;
                        Health_BarHeightFirstValue = 30f;
                        setHealth_BarRedFirstValue = true;
                        Health_BarRedFirstValue = 0;
                        break;
                    case  VariableState.Empty:
                        setHealth_BarBlueFirstValue = true;
                        Health_BarBlueFirstValue = 0;
                        setHealth_BarGreenFirstValue = true;
                        Health_BarGreenFirstValue = 0;
                        setHealth_BarRedFirstValue = true;
                        Health_BarRedFirstValue = 255;
                        setHealth_BarWidthFirstValue = true;
                        Health_BarWidthFirstValue = 10f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setHealth_BackgroundBlueSecondValue = true;
                        Health_BackgroundBlueSecondValue = 0;
                        setHealth_BackgroundGreenSecondValue = true;
                        Health_BackgroundGreenSecondValue = 0;
                        setHealth_BackgroundHeightSecondValue = true;
                        Health_BackgroundHeightSecondValue = -30f;
                        if (interpolationValue >= 1)
                        {
                            this.Health_Background.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHealth_BackgroundRedSecondValue = true;
                        Health_BackgroundRedSecondValue = 128;
                        setHealth_BackgroundWidthSecondValue = true;
                        Health_BackgroundWidthSecondValue = -45f;
                        if (interpolationValue >= 1)
                        {
                            this.Health_Background.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHealth_BackgroundXSecondValue = true;
                        Health_BackgroundXSecondValue = 23f;
                        setHealth_BackgroundYSecondValue = true;
                        Health_BackgroundYSecondValue = 10f;
                        setHealth_BarBlueSecondValue = true;
                        Health_BarBlueSecondValue = 50;
                        setHealth_BarGreenSecondValue = true;
                        Health_BarGreenSecondValue = 205;
                        setHealth_BarHeightSecondValue = true;
                        Health_BarHeightSecondValue = 30f;
                        if (interpolationValue >= 1)
                        {
                            this.Health_Bar.HeightUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setHealth_BarRedSecondValue = true;
                        Health_BarRedSecondValue = 50;
                        setHealth_BarWidthSecondValue = true;
                        Health_BarWidthSecondValue = 175f;
                        if (interpolationValue >= 1)
                        {
                            this.Health_Bar.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setHealth_BarXSecondValue = true;
                        Health_BarXSecondValue = 27f;
                        setHealth_BarYSecondValue = true;
                        Health_BarYSecondValue = 14f;
                        sethealth_FrameHeightSecondValue = true;
                        health_FrameHeightSecondValue = -12f;
                        if (interpolationValue >= 1)
                        {
                            this.health_Frame.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("health_Frame.SourceFile", "../resources/enemy_health_bar_foreground_003.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.health_Frame.Visible = true;
                        }
                        sethealth_FrameWidthSecondValue = true;
                        health_FrameWidthSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.health_Frame.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        sethealth_FrameXSecondValue = true;
                        health_FrameXSecondValue = 3f;
                        sethealth_FrameYSecondValue = true;
                        health_FrameYSecondValue = -19f;
                        setHeightSecondValue = true;
                        HeightSecondValue = 65f;
                        setWidthSecondValue = true;
                        WidthSecondValue = 227f;
                        setXSecondValue = true;
                        XSecondValue = 1f;
                        setYSecondValue = true;
                        YSecondValue = 25f;
                        break;
                    case  VariableState.Full:
                        setHealth_BarBlueSecondValue = true;
                        Health_BarBlueSecondValue = 0;
                        setHealth_BarGreenSecondValue = true;
                        Health_BarGreenSecondValue = 255;
                        setHealth_BarHeightSecondValue = true;
                        Health_BarHeightSecondValue = 30f;
                        setHealth_BarRedSecondValue = true;
                        Health_BarRedSecondValue = 0;
                        break;
                    case  VariableState.Empty:
                        setHealth_BarBlueSecondValue = true;
                        Health_BarBlueSecondValue = 0;
                        setHealth_BarGreenSecondValue = true;
                        Health_BarGreenSecondValue = 0;
                        setHealth_BarRedSecondValue = true;
                        Health_BarRedSecondValue = 255;
                        setHealth_BarWidthSecondValue = true;
                        Health_BarWidthSecondValue = 10f;
                        break;
                }
                if (setHealth_BackgroundBlueFirstValue && setHealth_BackgroundBlueSecondValue)
                {
                    Health_Background.Blue = FlatRedBall.Math.MathFunctions.RoundToInt(Health_BackgroundBlueFirstValue* (1 - interpolationValue) + Health_BackgroundBlueSecondValue * interpolationValue);
                }
                if (setHealth_BackgroundGreenFirstValue && setHealth_BackgroundGreenSecondValue)
                {
                    Health_Background.Green = FlatRedBall.Math.MathFunctions.RoundToInt(Health_BackgroundGreenFirstValue* (1 - interpolationValue) + Health_BackgroundGreenSecondValue * interpolationValue);
                }
                if (setHealth_BackgroundHeightFirstValue && setHealth_BackgroundHeightSecondValue)
                {
                    Health_Background.Height = Health_BackgroundHeightFirstValue * (1 - interpolationValue) + Health_BackgroundHeightSecondValue * interpolationValue;
                }
                if (setHealth_BackgroundRedFirstValue && setHealth_BackgroundRedSecondValue)
                {
                    Health_Background.Red = FlatRedBall.Math.MathFunctions.RoundToInt(Health_BackgroundRedFirstValue* (1 - interpolationValue) + Health_BackgroundRedSecondValue * interpolationValue);
                }
                if (setHealth_BackgroundWidthFirstValue && setHealth_BackgroundWidthSecondValue)
                {
                    Health_Background.Width = Health_BackgroundWidthFirstValue * (1 - interpolationValue) + Health_BackgroundWidthSecondValue * interpolationValue;
                }
                if (setHealth_BackgroundXFirstValue && setHealth_BackgroundXSecondValue)
                {
                    Health_Background.X = Health_BackgroundXFirstValue * (1 - interpolationValue) + Health_BackgroundXSecondValue * interpolationValue;
                }
                if (setHealth_BackgroundYFirstValue && setHealth_BackgroundYSecondValue)
                {
                    Health_Background.Y = Health_BackgroundYFirstValue * (1 - interpolationValue) + Health_BackgroundYSecondValue * interpolationValue;
                }
                if (setHealth_BarBlueFirstValue && setHealth_BarBlueSecondValue)
                {
                    Health_Bar.Blue = FlatRedBall.Math.MathFunctions.RoundToInt(Health_BarBlueFirstValue* (1 - interpolationValue) + Health_BarBlueSecondValue * interpolationValue);
                }
                if (setHealth_BarGreenFirstValue && setHealth_BarGreenSecondValue)
                {
                    Health_Bar.Green = FlatRedBall.Math.MathFunctions.RoundToInt(Health_BarGreenFirstValue* (1 - interpolationValue) + Health_BarGreenSecondValue * interpolationValue);
                }
                if (setHealth_BarHeightFirstValue && setHealth_BarHeightSecondValue)
                {
                    Health_Bar.Height = Health_BarHeightFirstValue * (1 - interpolationValue) + Health_BarHeightSecondValue * interpolationValue;
                }
                if (setHealth_BarRedFirstValue && setHealth_BarRedSecondValue)
                {
                    Health_Bar.Red = FlatRedBall.Math.MathFunctions.RoundToInt(Health_BarRedFirstValue* (1 - interpolationValue) + Health_BarRedSecondValue * interpolationValue);
                }
                if (setHealth_BarWidthFirstValue && setHealth_BarWidthSecondValue)
                {
                    Health_Bar.Width = Health_BarWidthFirstValue * (1 - interpolationValue) + Health_BarWidthSecondValue * interpolationValue;
                }
                if (setHealth_BarXFirstValue && setHealth_BarXSecondValue)
                {
                    Health_Bar.X = Health_BarXFirstValue * (1 - interpolationValue) + Health_BarXSecondValue * interpolationValue;
                }
                if (setHealth_BarYFirstValue && setHealth_BarYSecondValue)
                {
                    Health_Bar.Y = Health_BarYFirstValue * (1 - interpolationValue) + Health_BarYSecondValue * interpolationValue;
                }
                if (sethealth_FrameHeightFirstValue && sethealth_FrameHeightSecondValue)
                {
                    health_Frame.Height = health_FrameHeightFirstValue * (1 - interpolationValue) + health_FrameHeightSecondValue * interpolationValue;
                }
                if (sethealth_FrameWidthFirstValue && sethealth_FrameWidthSecondValue)
                {
                    health_Frame.Width = health_FrameWidthFirstValue * (1 - interpolationValue) + health_FrameWidthSecondValue * interpolationValue;
                }
                if (sethealth_FrameXFirstValue && sethealth_FrameXSecondValue)
                {
                    health_Frame.X = health_FrameXFirstValue * (1 - interpolationValue) + health_FrameXSecondValue * interpolationValue;
                }
                if (sethealth_FrameYFirstValue && sethealth_FrameYSecondValue)
                {
                    health_Frame.Y = health_FrameYFirstValue * (1 - interpolationValue) + health_FrameYSecondValue * interpolationValue;
                }
                if (setHeightFirstValue && setHeightSecondValue)
                {
                    Height = HeightFirstValue * (1 - interpolationValue) + HeightSecondValue * interpolationValue;
                }
                if (setWidthFirstValue && setWidthSecondValue)
                {
                    Width = WidthFirstValue * (1 - interpolationValue) + WidthSecondValue * interpolationValue;
                }
                if (setXFirstValue && setXSecondValue)
                {
                    X = XFirstValue * (1 - interpolationValue) + XSecondValue * interpolationValue;
                }
                if (setYFirstValue && setYSecondValue)
                {
                    Y = YFirstValue * (1 - interpolationValue) + YSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.HealthBarRuntime.VariableState fromState,DungeonRun.GumRuntimes.HealthBarRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "X",
                            Type = "float",
                            Value = X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Y",
                            Type = "float",
                            Value = Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Blue",
                            Type = "int",
                            Value = Health_Background.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Green",
                            Type = "int",
                            Value = Health_Background.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Height",
                            Type = "float",
                            Value = Health_Background.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Height Units",
                            Type = "DimensionUnitType",
                            Value = Health_Background.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Red",
                            Type = "int",
                            Value = Health_Background.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Width",
                            Type = "float",
                            Value = Health_Background.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Width Units",
                            Type = "DimensionUnitType",
                            Value = Health_Background.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.X",
                            Type = "float",
                            Value = Health_Background.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Y",
                            Type = "float",
                            Value = Health_Background.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Blue",
                            Type = "int",
                            Value = Health_Bar.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Green",
                            Type = "int",
                            Value = Health_Bar.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Height",
                            Type = "float",
                            Value = Health_Bar.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Height Units",
                            Type = "DimensionUnitType",
                            Value = Health_Bar.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Red",
                            Type = "int",
                            Value = Health_Bar.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Width",
                            Type = "float",
                            Value = Health_Bar.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Width Units",
                            Type = "DimensionUnitType",
                            Value = Health_Bar.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.X",
                            Type = "float",
                            Value = Health_Bar.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Y",
                            Type = "float",
                            Value = Health_Bar.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Height",
                            Type = "float",
                            Value = health_Frame.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Height Units",
                            Type = "DimensionUnitType",
                            Value = health_Frame.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.SourceFile",
                            Type = "string",
                            Value = health_Frame.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Visible",
                            Type = "bool",
                            Value = health_Frame.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Width",
                            Type = "float",
                            Value = health_Frame.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Width Units",
                            Type = "DimensionUnitType",
                            Value = health_Frame.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.X",
                            Type = "float",
                            Value = health_Frame.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Y",
                            Type = "float",
                            Value = health_Frame.Y
                        }
                        );
                        break;
                    case  VariableState.Full:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Blue",
                            Type = "int",
                            Value = Health_Bar.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Green",
                            Type = "int",
                            Value = Health_Bar.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Height",
                            Type = "float",
                            Value = Health_Bar.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Red",
                            Type = "int",
                            Value = Health_Bar.Red
                        }
                        );
                        break;
                    case  VariableState.Empty:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Blue",
                            Type = "int",
                            Value = Health_Bar.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Green",
                            Type = "int",
                            Value = Health_Bar.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Red",
                            Type = "int",
                            Value = Health_Bar.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Width",
                            Type = "float",
                            Value = Health_Bar.Width
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height + 65f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width + 227f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "X",
                            Type = "float",
                            Value = X + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Y",
                            Type = "float",
                            Value = Y + 25f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Blue",
                            Type = "int",
                            Value = Health_Background.Blue + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Green",
                            Type = "int",
                            Value = Health_Background.Green + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Height",
                            Type = "float",
                            Value = Health_Background.Height + -30f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Height Units",
                            Type = "DimensionUnitType",
                            Value = Health_Background.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Red",
                            Type = "int",
                            Value = Health_Background.Red + 128
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Width",
                            Type = "float",
                            Value = Health_Background.Width + -45f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Width Units",
                            Type = "DimensionUnitType",
                            Value = Health_Background.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.X",
                            Type = "float",
                            Value = Health_Background.X + 23f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Background.Y",
                            Type = "float",
                            Value = Health_Background.Y + 10f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Blue",
                            Type = "int",
                            Value = Health_Bar.Blue + 50
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Green",
                            Type = "int",
                            Value = Health_Bar.Green + 205
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Height",
                            Type = "float",
                            Value = Health_Bar.Height + 30f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Height Units",
                            Type = "DimensionUnitType",
                            Value = Health_Bar.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Red",
                            Type = "int",
                            Value = Health_Bar.Red + 50
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Width",
                            Type = "float",
                            Value = Health_Bar.Width + 175f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Width Units",
                            Type = "DimensionUnitType",
                            Value = Health_Bar.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.X",
                            Type = "float",
                            Value = Health_Bar.X + 27f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Y",
                            Type = "float",
                            Value = Health_Bar.Y + 14f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Height",
                            Type = "float",
                            Value = health_Frame.Height + -12f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Height Units",
                            Type = "DimensionUnitType",
                            Value = health_Frame.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.SourceFile",
                            Type = "string",
                            Value = health_Frame.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Visible",
                            Type = "bool",
                            Value = health_Frame.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Width",
                            Type = "float",
                            Value = health_Frame.Width + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Width Units",
                            Type = "DimensionUnitType",
                            Value = health_Frame.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.X",
                            Type = "float",
                            Value = health_Frame.X + 3f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "health_Frame.Y",
                            Type = "float",
                            Value = health_Frame.Y + -19f
                        }
                        );
                        break;
                    case  VariableState.Full:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Blue",
                            Type = "int",
                            Value = Health_Bar.Blue + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Green",
                            Type = "int",
                            Value = Health_Bar.Green + 255
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Height",
                            Type = "float",
                            Value = Health_Bar.Height + 30f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Red",
                            Type = "int",
                            Value = Health_Bar.Red + 0
                        }
                        );
                        break;
                    case  VariableState.Empty:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Blue",
                            Type = "int",
                            Value = Health_Bar.Blue + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Green",
                            Type = "int",
                            Value = Health_Bar.Green + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Red",
                            Type = "int",
                            Value = Health_Bar.Red + 255
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Health_Bar.Width",
                            Type = "float",
                            Value = Health_Bar.Width + 10f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                        if (state.Name == "Full") this.mCurrentVariableState = VariableState.Full;
                        if (state.Name == "Empty") this.mCurrentVariableState = VariableState.Empty;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.ColoredRectangleRuntime Health_Background { get; set; }
            private DungeonRun.GumRuntimes.ColoredRectangleRuntime Health_Bar { get; set; }
            private DungeonRun.GumRuntimes.SpriteRuntime health_Frame { get; set; }
            public int Health_BarBlue
            {
                get
                {
                    return Health_Bar.Blue;
                }
                set
                {
                    if (Health_Bar.Blue != value)
                    {
                        Health_Bar.Blue = value;
                        Health_BarBlueChanged?.Invoke(this, null);
                    }
                }
            }
            public int Health_BarGreen
            {
                get
                {
                    return Health_Bar.Green;
                }
                set
                {
                    if (Health_Bar.Green != value)
                    {
                        Health_Bar.Green = value;
                        Health_BarGreenChanged?.Invoke(this, null);
                    }
                }
            }
            public float Health_BarHeight
            {
                get
                {
                    return Health_Bar.Height;
                }
                set
                {
                    if (Health_Bar.Height != value)
                    {
                        Health_Bar.Height = value;
                        Health_BarHeightChanged?.Invoke(this, null);
                    }
                }
            }
            public int Health_BarRed
            {
                get
                {
                    return Health_Bar.Red;
                }
                set
                {
                    if (Health_Bar.Red != value)
                    {
                        Health_Bar.Red = value;
                        Health_BarRedChanged?.Invoke(this, null);
                    }
                }
            }
            public float Health_BarWidth
            {
                get
                {
                    return Health_Bar.Width;
                }
                set
                {
                    if (Health_Bar.Width != value)
                    {
                        Health_Bar.Width = value;
                        Health_BarWidthChanged?.Invoke(this, null);
                    }
                }
            }
            public event System.EventHandler Health_BarBlueChanged;
            public event System.EventHandler Health_BarGreenChanged;
            public event System.EventHandler Health_BarHeightChanged;
            public event System.EventHandler Health_BarRedChanged;
            public event System.EventHandler Health_BarWidthChanged;
            public HealthBarRuntime (bool fullInstantiation = true) 
            	: base(false)
            {
                this.HasEvents = false;
                this.ExposeChildrenEvents = false;
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "HealthBar");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                Health_Background = this.GetGraphicalUiElementByName("Health_Background") as DungeonRun.GumRuntimes.ColoredRectangleRuntime;
                Health_Bar = this.GetGraphicalUiElementByName("Health_Bar") as DungeonRun.GumRuntimes.ColoredRectangleRuntime;
                health_Frame = this.GetGraphicalUiElementByName("health_Frame") as DungeonRun.GumRuntimes.SpriteRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
