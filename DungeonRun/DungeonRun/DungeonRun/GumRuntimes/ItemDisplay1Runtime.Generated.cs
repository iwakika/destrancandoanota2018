    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class ItemDisplay1Runtime : DungeonRun.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            Height = 64f;
                            Width = 165f;
                            SpriteInstance.Height = 100f;
                            SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            SpriteInstance.Width = 100f;
                            SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            SpriteInstance.X = 192f;
                            SpriteInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Right;
                            SpriteInstance.Y = 0f;
                            ItemText.FontSize = 24;
                            ItemText.Height = 17f;
                            ItemText.HeightUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                            ItemText.OutlineThickness = 1;
                            ItemText.Text = "aqui  vai o nome do bagulho";
                            ItemText.Width = 440f;
                            ItemText.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                            ItemText.X = 200f;
                            ItemText.XUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                            ItemText.Y = 47.47686f;
                            ItemText.YUnits = Gum.Converters.GeneralUnitType.Percentage;
                            Key.Blue = 0;
                            Key.FontSize = 32;
                            Key.Green = 0;
                            Key.Height = 0f;
                            Key.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Key.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            Key.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "SpriteInstance");
                            Key.Red = 0;
                            Key.Text = "";
                            Key.UseCustomFont = false;
                            Key.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            Key.Width = 0f;
                            Key.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setHeightFirstValue = false;
                bool setHeightSecondValue = false;
                float HeightFirstValue= 0;
                float HeightSecondValue= 0;
                bool setItemTextFontSizeFirstValue = false;
                bool setItemTextFontSizeSecondValue = false;
                int ItemTextFontSizeFirstValue= 0;
                int ItemTextFontSizeSecondValue= 0;
                bool setItemTextHeightFirstValue = false;
                bool setItemTextHeightSecondValue = false;
                float ItemTextHeightFirstValue= 0;
                float ItemTextHeightSecondValue= 0;
                bool setItemTextOutlineThicknessFirstValue = false;
                bool setItemTextOutlineThicknessSecondValue = false;
                int ItemTextOutlineThicknessFirstValue= 0;
                int ItemTextOutlineThicknessSecondValue= 0;
                bool setItemTextWidthFirstValue = false;
                bool setItemTextWidthSecondValue = false;
                float ItemTextWidthFirstValue= 0;
                float ItemTextWidthSecondValue= 0;
                bool setItemTextXFirstValue = false;
                bool setItemTextXSecondValue = false;
                float ItemTextXFirstValue= 0;
                float ItemTextXSecondValue= 0;
                bool setItemTextYFirstValue = false;
                bool setItemTextYSecondValue = false;
                float ItemTextYFirstValue= 0;
                float ItemTextYSecondValue= 0;
                bool setKeyBlueFirstValue = false;
                bool setKeyBlueSecondValue = false;
                int KeyBlueFirstValue= 0;
                int KeyBlueSecondValue= 0;
                bool setKeyFontSizeFirstValue = false;
                bool setKeyFontSizeSecondValue = false;
                int KeyFontSizeFirstValue= 0;
                int KeyFontSizeSecondValue= 0;
                bool setKeyGreenFirstValue = false;
                bool setKeyGreenSecondValue = false;
                int KeyGreenFirstValue= 0;
                int KeyGreenSecondValue= 0;
                bool setKeyHeightFirstValue = false;
                bool setKeyHeightSecondValue = false;
                float KeyHeightFirstValue= 0;
                float KeyHeightSecondValue= 0;
                bool setKeyRedFirstValue = false;
                bool setKeyRedSecondValue = false;
                int KeyRedFirstValue= 0;
                int KeyRedSecondValue= 0;
                bool setKeyWidthFirstValue = false;
                bool setKeyWidthSecondValue = false;
                float KeyWidthFirstValue= 0;
                float KeyWidthSecondValue= 0;
                bool setSpriteInstanceHeightFirstValue = false;
                bool setSpriteInstanceHeightSecondValue = false;
                float SpriteInstanceHeightFirstValue= 0;
                float SpriteInstanceHeightSecondValue= 0;
                bool setSpriteInstanceWidthFirstValue = false;
                bool setSpriteInstanceWidthSecondValue = false;
                float SpriteInstanceWidthFirstValue= 0;
                float SpriteInstanceWidthSecondValue= 0;
                bool setSpriteInstanceXFirstValue = false;
                bool setSpriteInstanceXSecondValue = false;
                float SpriteInstanceXFirstValue= 0;
                float SpriteInstanceXSecondValue= 0;
                bool setSpriteInstanceYFirstValue = false;
                bool setSpriteInstanceYSecondValue = false;
                float SpriteInstanceYFirstValue= 0;
                float SpriteInstanceYSecondValue= 0;
                bool setWidthFirstValue = false;
                bool setWidthSecondValue = false;
                float WidthFirstValue= 0;
                float WidthSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setHeightFirstValue = true;
                        HeightFirstValue = 64f;
                        setItemTextFontSizeFirstValue = true;
                        ItemTextFontSizeFirstValue = 24;
                        setItemTextHeightFirstValue = true;
                        ItemTextHeightFirstValue = 17f;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.HeightUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setItemTextOutlineThicknessFirstValue = true;
                        ItemTextOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.Text = "aqui  vai o nome do bagulho";
                        }
                        setItemTextWidthFirstValue = true;
                        ItemTextWidthFirstValue = 440f;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setItemTextXFirstValue = true;
                        ItemTextXFirstValue = 200f;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.XUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        setItemTextYFirstValue = true;
                        ItemTextYFirstValue = 47.47686f;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.YUnits = Gum.Converters.GeneralUnitType.Percentage;
                        }
                        setKeyBlueFirstValue = true;
                        KeyBlueFirstValue = 0;
                        setKeyFontSizeFirstValue = true;
                        KeyFontSizeFirstValue = 32;
                        setKeyGreenFirstValue = true;
                        KeyGreenFirstValue = 0;
                        setKeyHeightFirstValue = true;
                        KeyHeightFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.Key.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.Key.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.Key.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "SpriteInstance");
                        }
                        setKeyRedFirstValue = true;
                        KeyRedFirstValue = 0;
                        if (interpolationValue < 1)
                        {
                            this.Key.Text = "";
                        }
                        if (interpolationValue < 1)
                        {
                            this.Key.UseCustomFont = false;
                        }
                        if (interpolationValue < 1)
                        {
                            this.Key.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setKeyWidthFirstValue = true;
                        KeyWidthFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.Key.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setSpriteInstanceHeightFirstValue = true;
                        SpriteInstanceHeightFirstValue = 100f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setSpriteInstanceWidthFirstValue = true;
                        SpriteInstanceWidthFirstValue = 100f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setSpriteInstanceXFirstValue = true;
                        SpriteInstanceXFirstValue = 192f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Right;
                        }
                        setSpriteInstanceYFirstValue = true;
                        SpriteInstanceYFirstValue = 0f;
                        setWidthFirstValue = true;
                        WidthFirstValue = 165f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setHeightSecondValue = true;
                        HeightSecondValue = 64f;
                        setItemTextFontSizeSecondValue = true;
                        ItemTextFontSizeSecondValue = 24;
                        setItemTextHeightSecondValue = true;
                        ItemTextHeightSecondValue = 17f;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.HeightUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setItemTextOutlineThicknessSecondValue = true;
                        ItemTextOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.Text = "aqui  vai o nome do bagulho";
                        }
                        setItemTextWidthSecondValue = true;
                        ItemTextWidthSecondValue = 440f;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setItemTextXSecondValue = true;
                        ItemTextXSecondValue = 200f;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.XUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        setItemTextYSecondValue = true;
                        ItemTextYSecondValue = 47.47686f;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.YUnits = Gum.Converters.GeneralUnitType.Percentage;
                        }
                        setKeyBlueSecondValue = true;
                        KeyBlueSecondValue = 0;
                        setKeyFontSizeSecondValue = true;
                        KeyFontSizeSecondValue = 32;
                        setKeyGreenSecondValue = true;
                        KeyGreenSecondValue = 0;
                        setKeyHeightSecondValue = true;
                        KeyHeightSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.Key.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Key.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Key.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "SpriteInstance");
                        }
                        setKeyRedSecondValue = true;
                        KeyRedSecondValue = 0;
                        if (interpolationValue >= 1)
                        {
                            this.Key.Text = "";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Key.UseCustomFont = false;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Key.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setKeyWidthSecondValue = true;
                        KeyWidthSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.Key.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setSpriteInstanceHeightSecondValue = true;
                        SpriteInstanceHeightSecondValue = 100f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setSpriteInstanceWidthSecondValue = true;
                        SpriteInstanceWidthSecondValue = 100f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setSpriteInstanceXSecondValue = true;
                        SpriteInstanceXSecondValue = 192f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Right;
                        }
                        setSpriteInstanceYSecondValue = true;
                        SpriteInstanceYSecondValue = 0f;
                        setWidthSecondValue = true;
                        WidthSecondValue = 165f;
                        break;
                }
                if (setHeightFirstValue && setHeightSecondValue)
                {
                    Height = HeightFirstValue * (1 - interpolationValue) + HeightSecondValue * interpolationValue;
                }
                if (setItemTextFontSizeFirstValue && setItemTextFontSizeSecondValue)
                {
                    ItemText.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(ItemTextFontSizeFirstValue* (1 - interpolationValue) + ItemTextFontSizeSecondValue * interpolationValue);
                }
                if (setItemTextHeightFirstValue && setItemTextHeightSecondValue)
                {
                    ItemText.Height = ItemTextHeightFirstValue * (1 - interpolationValue) + ItemTextHeightSecondValue * interpolationValue;
                }
                if (setItemTextOutlineThicknessFirstValue && setItemTextOutlineThicknessSecondValue)
                {
                    ItemText.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(ItemTextOutlineThicknessFirstValue* (1 - interpolationValue) + ItemTextOutlineThicknessSecondValue * interpolationValue);
                }
                if (setItemTextWidthFirstValue && setItemTextWidthSecondValue)
                {
                    ItemText.Width = ItemTextWidthFirstValue * (1 - interpolationValue) + ItemTextWidthSecondValue * interpolationValue;
                }
                if (setItemTextXFirstValue && setItemTextXSecondValue)
                {
                    ItemText.X = ItemTextXFirstValue * (1 - interpolationValue) + ItemTextXSecondValue * interpolationValue;
                }
                if (setItemTextYFirstValue && setItemTextYSecondValue)
                {
                    ItemText.Y = ItemTextYFirstValue * (1 - interpolationValue) + ItemTextYSecondValue * interpolationValue;
                }
                if (setKeyBlueFirstValue && setKeyBlueSecondValue)
                {
                    Key.Blue = FlatRedBall.Math.MathFunctions.RoundToInt(KeyBlueFirstValue* (1 - interpolationValue) + KeyBlueSecondValue * interpolationValue);
                }
                if (setKeyFontSizeFirstValue && setKeyFontSizeSecondValue)
                {
                    Key.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(KeyFontSizeFirstValue* (1 - interpolationValue) + KeyFontSizeSecondValue * interpolationValue);
                }
                if (setKeyGreenFirstValue && setKeyGreenSecondValue)
                {
                    Key.Green = FlatRedBall.Math.MathFunctions.RoundToInt(KeyGreenFirstValue* (1 - interpolationValue) + KeyGreenSecondValue * interpolationValue);
                }
                if (setKeyHeightFirstValue && setKeyHeightSecondValue)
                {
                    Key.Height = KeyHeightFirstValue * (1 - interpolationValue) + KeyHeightSecondValue * interpolationValue;
                }
                if (setKeyRedFirstValue && setKeyRedSecondValue)
                {
                    Key.Red = FlatRedBall.Math.MathFunctions.RoundToInt(KeyRedFirstValue* (1 - interpolationValue) + KeyRedSecondValue * interpolationValue);
                }
                if (setKeyWidthFirstValue && setKeyWidthSecondValue)
                {
                    Key.Width = KeyWidthFirstValue * (1 - interpolationValue) + KeyWidthSecondValue * interpolationValue;
                }
                if (setSpriteInstanceHeightFirstValue && setSpriteInstanceHeightSecondValue)
                {
                    SpriteInstance.Height = SpriteInstanceHeightFirstValue * (1 - interpolationValue) + SpriteInstanceHeightSecondValue * interpolationValue;
                }
                if (setSpriteInstanceWidthFirstValue && setSpriteInstanceWidthSecondValue)
                {
                    SpriteInstance.Width = SpriteInstanceWidthFirstValue * (1 - interpolationValue) + SpriteInstanceWidthSecondValue * interpolationValue;
                }
                if (setSpriteInstanceXFirstValue && setSpriteInstanceXSecondValue)
                {
                    SpriteInstance.X = SpriteInstanceXFirstValue * (1 - interpolationValue) + SpriteInstanceXSecondValue * interpolationValue;
                }
                if (setSpriteInstanceYFirstValue && setSpriteInstanceYSecondValue)
                {
                    SpriteInstance.Y = SpriteInstanceYFirstValue * (1 - interpolationValue) + SpriteInstanceYSecondValue * interpolationValue;
                }
                if (setWidthFirstValue && setWidthSecondValue)
                {
                    Width = WidthFirstValue * (1 - interpolationValue) + WidthSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.ItemDisplay1Runtime.VariableState fromState,DungeonRun.GumRuntimes.ItemDisplay1Runtime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = SpriteInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Y",
                            Type = "float",
                            Value = SpriteInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.FontSize",
                            Type = "int",
                            Value = ItemText.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height",
                            Type = "float",
                            Value = ItemText.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.OutlineThickness",
                            Type = "int",
                            Value = ItemText.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Text",
                            Type = "string",
                            Value = ItemText.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width",
                            Type = "float",
                            Value = ItemText.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.X",
                            Type = "float",
                            Value = ItemText.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.X Units",
                            Type = "PositionUnitType",
                            Value = ItemText.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Y",
                            Type = "float",
                            Value = ItemText.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Y Units",
                            Type = "PositionUnitType",
                            Value = ItemText.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Blue",
                            Type = "int",
                            Value = Key.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.FontSize",
                            Type = "int",
                            Value = Key.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Green",
                            Type = "int",
                            Value = Key.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Height",
                            Type = "float",
                            Value = Key.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Height Units",
                            Type = "DimensionUnitType",
                            Value = Key.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = Key.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Parent",
                            Type = "string",
                            Value = Key.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Red",
                            Type = "int",
                            Value = Key.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Text",
                            Type = "string",
                            Value = Key.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.UseCustomFont",
                            Type = "bool",
                            Value = Key.UseCustomFont
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = Key.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Width",
                            Type = "float",
                            Value = Key.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Width Units",
                            Type = "DimensionUnitType",
                            Value = Key.WidthUnits
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width + 165f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height + 100f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width + 100f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X + 192f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = SpriteInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Y",
                            Type = "float",
                            Value = SpriteInstance.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.FontSize",
                            Type = "int",
                            Value = ItemText.FontSize + 24
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height",
                            Type = "float",
                            Value = ItemText.Height + 17f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.OutlineThickness",
                            Type = "int",
                            Value = ItemText.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Text",
                            Type = "string",
                            Value = ItemText.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width",
                            Type = "float",
                            Value = ItemText.Width + 440f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.X",
                            Type = "float",
                            Value = ItemText.X + 200f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.X Units",
                            Type = "PositionUnitType",
                            Value = ItemText.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Y",
                            Type = "float",
                            Value = ItemText.Y + 47.47686f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Y Units",
                            Type = "PositionUnitType",
                            Value = ItemText.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Blue",
                            Type = "int",
                            Value = Key.Blue + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.FontSize",
                            Type = "int",
                            Value = Key.FontSize + 32
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Green",
                            Type = "int",
                            Value = Key.Green + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Height",
                            Type = "float",
                            Value = Key.Height + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Height Units",
                            Type = "DimensionUnitType",
                            Value = Key.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = Key.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Parent",
                            Type = "string",
                            Value = Key.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Red",
                            Type = "int",
                            Value = Key.Red + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Text",
                            Type = "string",
                            Value = Key.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.UseCustomFont",
                            Type = "bool",
                            Value = Key.UseCustomFont
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = Key.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Width",
                            Type = "float",
                            Value = Key.Width + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Key.Width Units",
                            Type = "DimensionUnitType",
                            Value = Key.WidthUnits
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.SpriteRuntime SpriteInstance { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime ItemText { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime Key { get; set; }
            public string ItemdescText
            {
                get
                {
                    return ItemText.Text;
                }
                set
                {
                    if (ItemText.Text != value)
                    {
                        ItemText.Text = value;
                        ItemdescTextChanged?.Invoke(this, null);
                    }
                }
            }
            public string KeyText
            {
                get
                {
                    return Key.Text;
                }
                set
                {
                    if (Key.Text != value)
                    {
                        Key.Text = value;
                        KeyTextChanged?.Invoke(this, null);
                    }
                }
            }
            public Microsoft.Xna.Framework.Graphics.Texture2D ItemSprite_hud
            {
                get
                {
                    return SpriteInstance.SourceFile;
                }
                set
                {
                    if (SpriteInstance.SourceFile != value)
                    {
                        SpriteInstance.SourceFile = value;
                        ItemSprite_hudChanged?.Invoke(this, null);
                    }
                }
            }
            public event System.EventHandler ItemdescTextChanged;
            public event System.EventHandler KeyTextChanged;
            public event System.EventHandler ItemSprite_hudChanged;
            public ItemDisplay1Runtime (bool fullInstantiation = true) 
            	: base(false)
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "ItemDisplay1");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                SpriteInstance = this.GetGraphicalUiElementByName("SpriteInstance") as DungeonRun.GumRuntimes.SpriteRuntime;
                ItemText = this.GetGraphicalUiElementByName("ItemText") as DungeonRun.GumRuntimes.TextRuntime;
                Key = this.GetGraphicalUiElementByName("Key") as DungeonRun.GumRuntimes.TextRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
