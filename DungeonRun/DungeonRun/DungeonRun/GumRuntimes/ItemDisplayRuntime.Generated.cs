    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class ItemDisplayRuntime : DungeonRun.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            Height = 36f;
                            Width = 36f;
                            SpriteInstance.Height = 82.53968f;
                            SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            SpriteInstance.Width = 73.01585f;
                            SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            SpriteInstance.X = 3f;
                            Item_Qtd.Height = 49.66666f;
                            Item_Qtd.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            Item_Qtd.Width = 61.71429f;
                            Item_Qtd.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            Item_Qtd.X = 16f;
                            Item_Qtd.Y = 19f;
                            ItemText.FontSize = 12;
                            ItemText.Height = 68.42546f;
                            ItemText.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            ItemText.OutlineThickness = 1;
                            ItemText.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Item_Qtd");
                            ItemText.Text = "0";
                            ItemText.Width = 55.83597f;
                            ItemText.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            ItemText.X = 4f;
                            ItemText.Y = 0f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setHeightFirstValue = false;
                bool setHeightSecondValue = false;
                float HeightFirstValue= 0;
                float HeightSecondValue= 0;
                bool setItem_QtdHeightFirstValue = false;
                bool setItem_QtdHeightSecondValue = false;
                float Item_QtdHeightFirstValue= 0;
                float Item_QtdHeightSecondValue= 0;
                bool setItem_QtdWidthFirstValue = false;
                bool setItem_QtdWidthSecondValue = false;
                float Item_QtdWidthFirstValue= 0;
                float Item_QtdWidthSecondValue= 0;
                bool setItem_QtdXFirstValue = false;
                bool setItem_QtdXSecondValue = false;
                float Item_QtdXFirstValue= 0;
                float Item_QtdXSecondValue= 0;
                bool setItem_QtdYFirstValue = false;
                bool setItem_QtdYSecondValue = false;
                float Item_QtdYFirstValue= 0;
                float Item_QtdYSecondValue= 0;
                bool setItemTextFontSizeFirstValue = false;
                bool setItemTextFontSizeSecondValue = false;
                int ItemTextFontSizeFirstValue= 0;
                int ItemTextFontSizeSecondValue= 0;
                bool setItemTextHeightFirstValue = false;
                bool setItemTextHeightSecondValue = false;
                float ItemTextHeightFirstValue= 0;
                float ItemTextHeightSecondValue= 0;
                bool setItemTextOutlineThicknessFirstValue = false;
                bool setItemTextOutlineThicknessSecondValue = false;
                int ItemTextOutlineThicknessFirstValue= 0;
                int ItemTextOutlineThicknessSecondValue= 0;
                bool setItemTextWidthFirstValue = false;
                bool setItemTextWidthSecondValue = false;
                float ItemTextWidthFirstValue= 0;
                float ItemTextWidthSecondValue= 0;
                bool setItemTextXFirstValue = false;
                bool setItemTextXSecondValue = false;
                float ItemTextXFirstValue= 0;
                float ItemTextXSecondValue= 0;
                bool setItemTextYFirstValue = false;
                bool setItemTextYSecondValue = false;
                float ItemTextYFirstValue= 0;
                float ItemTextYSecondValue= 0;
                bool setSpriteInstanceHeightFirstValue = false;
                bool setSpriteInstanceHeightSecondValue = false;
                float SpriteInstanceHeightFirstValue= 0;
                float SpriteInstanceHeightSecondValue= 0;
                bool setSpriteInstanceWidthFirstValue = false;
                bool setSpriteInstanceWidthSecondValue = false;
                float SpriteInstanceWidthFirstValue= 0;
                float SpriteInstanceWidthSecondValue= 0;
                bool setSpriteInstanceXFirstValue = false;
                bool setSpriteInstanceXSecondValue = false;
                float SpriteInstanceXFirstValue= 0;
                float SpriteInstanceXSecondValue= 0;
                bool setWidthFirstValue = false;
                bool setWidthSecondValue = false;
                float WidthFirstValue= 0;
                float WidthSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setHeightFirstValue = true;
                        HeightFirstValue = 36f;
                        setItem_QtdHeightFirstValue = true;
                        Item_QtdHeightFirstValue = 49.66666f;
                        if (interpolationValue < 1)
                        {
                            this.Item_Qtd.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setItem_QtdWidthFirstValue = true;
                        Item_QtdWidthFirstValue = 61.71429f;
                        if (interpolationValue < 1)
                        {
                            this.Item_Qtd.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setItem_QtdXFirstValue = true;
                        Item_QtdXFirstValue = 16f;
                        setItem_QtdYFirstValue = true;
                        Item_QtdYFirstValue = 19f;
                        setItemTextFontSizeFirstValue = true;
                        ItemTextFontSizeFirstValue = 12;
                        setItemTextHeightFirstValue = true;
                        ItemTextHeightFirstValue = 68.42546f;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setItemTextOutlineThicknessFirstValue = true;
                        ItemTextOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Item_Qtd");
                        }
                        if (interpolationValue < 1)
                        {
                            this.ItemText.Text = "0";
                        }
                        setItemTextWidthFirstValue = true;
                        ItemTextWidthFirstValue = 55.83597f;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setItemTextXFirstValue = true;
                        ItemTextXFirstValue = 4f;
                        setItemTextYFirstValue = true;
                        ItemTextYFirstValue = 0f;
                        setSpriteInstanceHeightFirstValue = true;
                        SpriteInstanceHeightFirstValue = 82.53968f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setSpriteInstanceWidthFirstValue = true;
                        SpriteInstanceWidthFirstValue = 73.01585f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setSpriteInstanceXFirstValue = true;
                        SpriteInstanceXFirstValue = 3f;
                        setWidthFirstValue = true;
                        WidthFirstValue = 36f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setHeightSecondValue = true;
                        HeightSecondValue = 36f;
                        setItem_QtdHeightSecondValue = true;
                        Item_QtdHeightSecondValue = 49.66666f;
                        if (interpolationValue >= 1)
                        {
                            this.Item_Qtd.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setItem_QtdWidthSecondValue = true;
                        Item_QtdWidthSecondValue = 61.71429f;
                        if (interpolationValue >= 1)
                        {
                            this.Item_Qtd.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setItem_QtdXSecondValue = true;
                        Item_QtdXSecondValue = 16f;
                        setItem_QtdYSecondValue = true;
                        Item_QtdYSecondValue = 19f;
                        setItemTextFontSizeSecondValue = true;
                        ItemTextFontSizeSecondValue = 12;
                        setItemTextHeightSecondValue = true;
                        ItemTextHeightSecondValue = 68.42546f;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setItemTextOutlineThicknessSecondValue = true;
                        ItemTextOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Item_Qtd");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.Text = "0";
                        }
                        setItemTextWidthSecondValue = true;
                        ItemTextWidthSecondValue = 55.83597f;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setItemTextXSecondValue = true;
                        ItemTextXSecondValue = 4f;
                        setItemTextYSecondValue = true;
                        ItemTextYSecondValue = 0f;
                        setSpriteInstanceHeightSecondValue = true;
                        SpriteInstanceHeightSecondValue = 82.53968f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setSpriteInstanceWidthSecondValue = true;
                        SpriteInstanceWidthSecondValue = 73.01585f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setSpriteInstanceXSecondValue = true;
                        SpriteInstanceXSecondValue = 3f;
                        setWidthSecondValue = true;
                        WidthSecondValue = 36f;
                        break;
                }
                if (setHeightFirstValue && setHeightSecondValue)
                {
                    Height = HeightFirstValue * (1 - interpolationValue) + HeightSecondValue * interpolationValue;
                }
                if (setItem_QtdHeightFirstValue && setItem_QtdHeightSecondValue)
                {
                    Item_Qtd.Height = Item_QtdHeightFirstValue * (1 - interpolationValue) + Item_QtdHeightSecondValue * interpolationValue;
                }
                if (setItem_QtdWidthFirstValue && setItem_QtdWidthSecondValue)
                {
                    Item_Qtd.Width = Item_QtdWidthFirstValue * (1 - interpolationValue) + Item_QtdWidthSecondValue * interpolationValue;
                }
                if (setItem_QtdXFirstValue && setItem_QtdXSecondValue)
                {
                    Item_Qtd.X = Item_QtdXFirstValue * (1 - interpolationValue) + Item_QtdXSecondValue * interpolationValue;
                }
                if (setItem_QtdYFirstValue && setItem_QtdYSecondValue)
                {
                    Item_Qtd.Y = Item_QtdYFirstValue * (1 - interpolationValue) + Item_QtdYSecondValue * interpolationValue;
                }
                if (setItemTextFontSizeFirstValue && setItemTextFontSizeSecondValue)
                {
                    ItemText.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(ItemTextFontSizeFirstValue* (1 - interpolationValue) + ItemTextFontSizeSecondValue * interpolationValue);
                }
                if (setItemTextHeightFirstValue && setItemTextHeightSecondValue)
                {
                    ItemText.Height = ItemTextHeightFirstValue * (1 - interpolationValue) + ItemTextHeightSecondValue * interpolationValue;
                }
                if (setItemTextOutlineThicknessFirstValue && setItemTextOutlineThicknessSecondValue)
                {
                    ItemText.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(ItemTextOutlineThicknessFirstValue* (1 - interpolationValue) + ItemTextOutlineThicknessSecondValue * interpolationValue);
                }
                if (setItemTextWidthFirstValue && setItemTextWidthSecondValue)
                {
                    ItemText.Width = ItemTextWidthFirstValue * (1 - interpolationValue) + ItemTextWidthSecondValue * interpolationValue;
                }
                if (setItemTextXFirstValue && setItemTextXSecondValue)
                {
                    ItemText.X = ItemTextXFirstValue * (1 - interpolationValue) + ItemTextXSecondValue * interpolationValue;
                }
                if (setItemTextYFirstValue && setItemTextYSecondValue)
                {
                    ItemText.Y = ItemTextYFirstValue * (1 - interpolationValue) + ItemTextYSecondValue * interpolationValue;
                }
                if (setSpriteInstanceHeightFirstValue && setSpriteInstanceHeightSecondValue)
                {
                    SpriteInstance.Height = SpriteInstanceHeightFirstValue * (1 - interpolationValue) + SpriteInstanceHeightSecondValue * interpolationValue;
                }
                if (setSpriteInstanceWidthFirstValue && setSpriteInstanceWidthSecondValue)
                {
                    SpriteInstance.Width = SpriteInstanceWidthFirstValue * (1 - interpolationValue) + SpriteInstanceWidthSecondValue * interpolationValue;
                }
                if (setSpriteInstanceXFirstValue && setSpriteInstanceXSecondValue)
                {
                    SpriteInstance.X = SpriteInstanceXFirstValue * (1 - interpolationValue) + SpriteInstanceXSecondValue * interpolationValue;
                }
                if (setWidthFirstValue && setWidthSecondValue)
                {
                    Width = WidthFirstValue * (1 - interpolationValue) + WidthSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.ItemDisplayRuntime.VariableState fromState,DungeonRun.GumRuntimes.ItemDisplayRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.Height",
                            Type = "float",
                            Value = Item_Qtd.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.Height Units",
                            Type = "DimensionUnitType",
                            Value = Item_Qtd.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.Width",
                            Type = "float",
                            Value = Item_Qtd.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.Width Units",
                            Type = "DimensionUnitType",
                            Value = Item_Qtd.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.X",
                            Type = "float",
                            Value = Item_Qtd.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.Y",
                            Type = "float",
                            Value = Item_Qtd.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.FontSize",
                            Type = "int",
                            Value = ItemText.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height",
                            Type = "float",
                            Value = ItemText.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.OutlineThickness",
                            Type = "int",
                            Value = ItemText.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Parent",
                            Type = "string",
                            Value = ItemText.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Text",
                            Type = "string",
                            Value = ItemText.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width",
                            Type = "float",
                            Value = ItemText.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.X",
                            Type = "float",
                            Value = ItemText.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Y",
                            Type = "float",
                            Value = ItemText.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height + 36f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width + 36f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height + 82.53968f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width + 73.01585f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X + 3f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.Height",
                            Type = "float",
                            Value = Item_Qtd.Height + 49.66666f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.Height Units",
                            Type = "DimensionUnitType",
                            Value = Item_Qtd.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.Width",
                            Type = "float",
                            Value = Item_Qtd.Width + 61.71429f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.Width Units",
                            Type = "DimensionUnitType",
                            Value = Item_Qtd.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.X",
                            Type = "float",
                            Value = Item_Qtd.X + 16f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Item_Qtd.Y",
                            Type = "float",
                            Value = Item_Qtd.Y + 19f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.FontSize",
                            Type = "int",
                            Value = ItemText.FontSize + 12
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height",
                            Type = "float",
                            Value = ItemText.Height + 68.42546f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.OutlineThickness",
                            Type = "int",
                            Value = ItemText.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Parent",
                            Type = "string",
                            Value = ItemText.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Text",
                            Type = "string",
                            Value = ItemText.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width",
                            Type = "float",
                            Value = ItemText.Width + 55.83597f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.X",
                            Type = "float",
                            Value = ItemText.X + 4f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Y",
                            Type = "float",
                            Value = ItemText.Y + 0f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.SpriteRuntime SpriteInstance { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime Item_Qtd { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime ItemText { get; set; }
            public string Item_qtd_hud
            {
                get
                {
                    return ItemText.Text;
                }
                set
                {
                    if (ItemText.Text != value)
                    {
                        ItemText.Text = value;
                        Item_qtd_hudChanged?.Invoke(this, null);
                    }
                }
            }
            public Microsoft.Xna.Framework.Graphics.Texture2D ItemSprite_hud
            {
                get
                {
                    return SpriteInstance.SourceFile;
                }
                set
                {
                    if (SpriteInstance.SourceFile != value)
                    {
                        SpriteInstance.SourceFile = value;
                        ItemSprite_hudChanged?.Invoke(this, null);
                    }
                }
            }
            public event System.EventHandler Item_qtd_hudChanged;
            public event System.EventHandler ItemSprite_hudChanged;
            public ItemDisplayRuntime (bool fullInstantiation = true) 
            	: base(false)
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "ItemDisplay");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                SpriteInstance = this.GetGraphicalUiElementByName("SpriteInstance") as DungeonRun.GumRuntimes.SpriteRuntime;
                Item_Qtd = this.GetGraphicalUiElementByName("Item_Qtd") as DungeonRun.GumRuntimes.ContainerRuntime;
                ItemText = this.GetGraphicalUiElementByName("ItemText") as DungeonRun.GumRuntimes.TextRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
