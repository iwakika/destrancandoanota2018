    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class LevelDisplayRuntime : DungeonRun.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            Height = -546f;
                            HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Width = -735f;
                            WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            X = 0f;
                            Y = 2f;
                            panelinset_brown.Height = 0f;
                            panelinset_brown.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            SetProperty("panelinset_brown.SourceFile", "../screens/mainmenuscreen/panelinset_brown.png");
                            panelinset_brown.Width = -7f;
                            panelinset_brown.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            panelinset_brown.X = 0f;
                            panelinset_brown.Y = 0f;
                            LabelLv.Font = "Arial Narrow";
                            LabelLv.Height = -31f;
                            LabelLv.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            LabelLv.OutlineThickness = 1;
                            LabelLv.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "panelinset_brown");
                            LabelLv.Text = "Nv";
                            LabelLv.UseCustomFont = false;
                            LabelLv.Width = -13f;
                            LabelLv.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            LabelLv.X = 4f;
                            LabelLv.Y = 6f;
                            Hud_Nivel.FontSize = 22;
                            Hud_Nivel.Height = -28f;
                            Hud_Nivel.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Hud_Nivel.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            Hud_Nivel.OutlineThickness = 1;
                            Hud_Nivel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "panelinset_brown");
                            Hud_Nivel.Text = "0";
                            Hud_Nivel.Width = -23f;
                            Hud_Nivel.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Hud_Nivel.X = 14f;
                            Hud_Nivel.Y = 9f;
                            TextInstance.FontSize = 12;
                            TextInstance.Height = 16f;
                            TextInstance.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            TextInstance.OutlineThickness = 1;
                            TextInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Sala");
                            TextInstance.Text = "00";
                            TextInstance.Width = 19f;
                            TextInstance.X = 6f;
                            TextInstance.Y = 4f;
                            TextInstance1.FontSize = 12;
                            TextInstance1.Height = 18f;
                            TextInstance1.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            TextInstance1.OutlineThickness = 1;
                            TextInstance1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Sala");
                            TextInstance1.Text = "00";
                            TextInstance1.Width = 20f;
                            TextInstance1.X = 28f;
                            TextInstance1.Y = 4f;
                            Sala.Height = -34f;
                            Sala.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Sala.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "panelinset_brown");
                            Sala.Width = -27f;
                            Sala.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Sala.X = 7f;
                            Sala.Y = 30f;
                            TextInstance2.Height = 23f;
                            TextInstance2.OutlineThickness = 1;
                            TextInstance2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Sala");
                            TextInstance2.Text = "/";
                            TextInstance2.Width = 10f;
                            TextInstance2.X = 22f;
                            TextInstance2.Y = 1f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setHeightFirstValue = false;
                bool setHeightSecondValue = false;
                float HeightFirstValue= 0;
                float HeightSecondValue= 0;
                bool setHud_NivelFontSizeFirstValue = false;
                bool setHud_NivelFontSizeSecondValue = false;
                int Hud_NivelFontSizeFirstValue= 0;
                int Hud_NivelFontSizeSecondValue= 0;
                bool setHud_NivelHeightFirstValue = false;
                bool setHud_NivelHeightSecondValue = false;
                float Hud_NivelHeightFirstValue= 0;
                float Hud_NivelHeightSecondValue= 0;
                bool setHud_NivelOutlineThicknessFirstValue = false;
                bool setHud_NivelOutlineThicknessSecondValue = false;
                int Hud_NivelOutlineThicknessFirstValue= 0;
                int Hud_NivelOutlineThicknessSecondValue= 0;
                bool setHud_NivelWidthFirstValue = false;
                bool setHud_NivelWidthSecondValue = false;
                float Hud_NivelWidthFirstValue= 0;
                float Hud_NivelWidthSecondValue= 0;
                bool setHud_NivelXFirstValue = false;
                bool setHud_NivelXSecondValue = false;
                float Hud_NivelXFirstValue= 0;
                float Hud_NivelXSecondValue= 0;
                bool setHud_NivelYFirstValue = false;
                bool setHud_NivelYSecondValue = false;
                float Hud_NivelYFirstValue= 0;
                float Hud_NivelYSecondValue= 0;
                bool setLabelLvHeightFirstValue = false;
                bool setLabelLvHeightSecondValue = false;
                float LabelLvHeightFirstValue= 0;
                float LabelLvHeightSecondValue= 0;
                bool setLabelLvOutlineThicknessFirstValue = false;
                bool setLabelLvOutlineThicknessSecondValue = false;
                int LabelLvOutlineThicknessFirstValue= 0;
                int LabelLvOutlineThicknessSecondValue= 0;
                bool setLabelLvWidthFirstValue = false;
                bool setLabelLvWidthSecondValue = false;
                float LabelLvWidthFirstValue= 0;
                float LabelLvWidthSecondValue= 0;
                bool setLabelLvXFirstValue = false;
                bool setLabelLvXSecondValue = false;
                float LabelLvXFirstValue= 0;
                float LabelLvXSecondValue= 0;
                bool setLabelLvYFirstValue = false;
                bool setLabelLvYSecondValue = false;
                float LabelLvYFirstValue= 0;
                float LabelLvYSecondValue= 0;
                bool setpanelinset_brownHeightFirstValue = false;
                bool setpanelinset_brownHeightSecondValue = false;
                float panelinset_brownHeightFirstValue= 0;
                float panelinset_brownHeightSecondValue= 0;
                bool setpanelinset_brownWidthFirstValue = false;
                bool setpanelinset_brownWidthSecondValue = false;
                float panelinset_brownWidthFirstValue= 0;
                float panelinset_brownWidthSecondValue= 0;
                bool setpanelinset_brownXFirstValue = false;
                bool setpanelinset_brownXSecondValue = false;
                float panelinset_brownXFirstValue= 0;
                float panelinset_brownXSecondValue= 0;
                bool setpanelinset_brownYFirstValue = false;
                bool setpanelinset_brownYSecondValue = false;
                float panelinset_brownYFirstValue= 0;
                float panelinset_brownYSecondValue= 0;
                bool setSalaHeightFirstValue = false;
                bool setSalaHeightSecondValue = false;
                float SalaHeightFirstValue= 0;
                float SalaHeightSecondValue= 0;
                bool setSalaWidthFirstValue = false;
                bool setSalaWidthSecondValue = false;
                float SalaWidthFirstValue= 0;
                float SalaWidthSecondValue= 0;
                bool setSalaXFirstValue = false;
                bool setSalaXSecondValue = false;
                float SalaXFirstValue= 0;
                float SalaXSecondValue= 0;
                bool setSalaYFirstValue = false;
                bool setSalaYSecondValue = false;
                float SalaYFirstValue= 0;
                float SalaYSecondValue= 0;
                bool setTextInstanceFontSizeFirstValue = false;
                bool setTextInstanceFontSizeSecondValue = false;
                int TextInstanceFontSizeFirstValue= 0;
                int TextInstanceFontSizeSecondValue= 0;
                bool setTextInstanceHeightFirstValue = false;
                bool setTextInstanceHeightSecondValue = false;
                float TextInstanceHeightFirstValue= 0;
                float TextInstanceHeightSecondValue= 0;
                bool setTextInstanceOutlineThicknessFirstValue = false;
                bool setTextInstanceOutlineThicknessSecondValue = false;
                int TextInstanceOutlineThicknessFirstValue= 0;
                int TextInstanceOutlineThicknessSecondValue= 0;
                bool setTextInstanceWidthFirstValue = false;
                bool setTextInstanceWidthSecondValue = false;
                float TextInstanceWidthFirstValue= 0;
                float TextInstanceWidthSecondValue= 0;
                bool setTextInstanceXFirstValue = false;
                bool setTextInstanceXSecondValue = false;
                float TextInstanceXFirstValue= 0;
                float TextInstanceXSecondValue= 0;
                bool setTextInstanceYFirstValue = false;
                bool setTextInstanceYSecondValue = false;
                float TextInstanceYFirstValue= 0;
                float TextInstanceYSecondValue= 0;
                bool setTextInstance1FontSizeFirstValue = false;
                bool setTextInstance1FontSizeSecondValue = false;
                int TextInstance1FontSizeFirstValue= 0;
                int TextInstance1FontSizeSecondValue= 0;
                bool setTextInstance1HeightFirstValue = false;
                bool setTextInstance1HeightSecondValue = false;
                float TextInstance1HeightFirstValue= 0;
                float TextInstance1HeightSecondValue= 0;
                bool setTextInstance1OutlineThicknessFirstValue = false;
                bool setTextInstance1OutlineThicknessSecondValue = false;
                int TextInstance1OutlineThicknessFirstValue= 0;
                int TextInstance1OutlineThicknessSecondValue= 0;
                bool setTextInstance1WidthFirstValue = false;
                bool setTextInstance1WidthSecondValue = false;
                float TextInstance1WidthFirstValue= 0;
                float TextInstance1WidthSecondValue= 0;
                bool setTextInstance1XFirstValue = false;
                bool setTextInstance1XSecondValue = false;
                float TextInstance1XFirstValue= 0;
                float TextInstance1XSecondValue= 0;
                bool setTextInstance1YFirstValue = false;
                bool setTextInstance1YSecondValue = false;
                float TextInstance1YFirstValue= 0;
                float TextInstance1YSecondValue= 0;
                bool setTextInstance2HeightFirstValue = false;
                bool setTextInstance2HeightSecondValue = false;
                float TextInstance2HeightFirstValue= 0;
                float TextInstance2HeightSecondValue= 0;
                bool setTextInstance2OutlineThicknessFirstValue = false;
                bool setTextInstance2OutlineThicknessSecondValue = false;
                int TextInstance2OutlineThicknessFirstValue= 0;
                int TextInstance2OutlineThicknessSecondValue= 0;
                bool setTextInstance2WidthFirstValue = false;
                bool setTextInstance2WidthSecondValue = false;
                float TextInstance2WidthFirstValue= 0;
                float TextInstance2WidthSecondValue= 0;
                bool setTextInstance2XFirstValue = false;
                bool setTextInstance2XSecondValue = false;
                float TextInstance2XFirstValue= 0;
                float TextInstance2XSecondValue= 0;
                bool setTextInstance2YFirstValue = false;
                bool setTextInstance2YSecondValue = false;
                float TextInstance2YFirstValue= 0;
                float TextInstance2YSecondValue= 0;
                bool setWidthFirstValue = false;
                bool setWidthSecondValue = false;
                float WidthFirstValue= 0;
                float WidthSecondValue= 0;
                bool setXFirstValue = false;
                bool setXSecondValue = false;
                float XFirstValue= 0;
                float XSecondValue= 0;
                bool setYFirstValue = false;
                bool setYSecondValue = false;
                float YFirstValue= 0;
                float YSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setHeightFirstValue = true;
                        HeightFirstValue = -546f;
                        if (interpolationValue < 1)
                        {
                            this.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHud_NivelFontSizeFirstValue = true;
                        Hud_NivelFontSizeFirstValue = 22;
                        setHud_NivelHeightFirstValue = true;
                        Hud_NivelHeightFirstValue = -28f;
                        if (interpolationValue < 1)
                        {
                            this.Hud_Nivel.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.Hud_Nivel.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setHud_NivelOutlineThicknessFirstValue = true;
                        Hud_NivelOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.Hud_Nivel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "panelinset_brown");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Hud_Nivel.Text = "0";
                        }
                        setHud_NivelWidthFirstValue = true;
                        Hud_NivelWidthFirstValue = -23f;
                        if (interpolationValue < 1)
                        {
                            this.Hud_Nivel.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHud_NivelXFirstValue = true;
                        Hud_NivelXFirstValue = 14f;
                        setHud_NivelYFirstValue = true;
                        Hud_NivelYFirstValue = 9f;
                        if (interpolationValue < 1)
                        {
                            this.LabelLv.Font = "Arial Narrow";
                        }
                        setLabelLvHeightFirstValue = true;
                        LabelLvHeightFirstValue = -31f;
                        if (interpolationValue < 1)
                        {
                            this.LabelLv.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setLabelLvOutlineThicknessFirstValue = true;
                        LabelLvOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.LabelLv.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "panelinset_brown");
                        }
                        if (interpolationValue < 1)
                        {
                            this.LabelLv.Text = "Nv";
                        }
                        if (interpolationValue < 1)
                        {
                            this.LabelLv.UseCustomFont = false;
                        }
                        setLabelLvWidthFirstValue = true;
                        LabelLvWidthFirstValue = -13f;
                        if (interpolationValue < 1)
                        {
                            this.LabelLv.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setLabelLvXFirstValue = true;
                        LabelLvXFirstValue = 4f;
                        setLabelLvYFirstValue = true;
                        LabelLvYFirstValue = 6f;
                        setpanelinset_brownHeightFirstValue = true;
                        panelinset_brownHeightFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.panelinset_brown.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("panelinset_brown.SourceFile", "../screens/mainmenuscreen/panelinset_brown.png");
                        }
                        setpanelinset_brownWidthFirstValue = true;
                        panelinset_brownWidthFirstValue = -7f;
                        if (interpolationValue < 1)
                        {
                            this.panelinset_brown.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setpanelinset_brownXFirstValue = true;
                        panelinset_brownXFirstValue = 0f;
                        setpanelinset_brownYFirstValue = true;
                        panelinset_brownYFirstValue = 0f;
                        setSalaHeightFirstValue = true;
                        SalaHeightFirstValue = -34f;
                        if (interpolationValue < 1)
                        {
                            this.Sala.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.Sala.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "panelinset_brown");
                        }
                        setSalaWidthFirstValue = true;
                        SalaWidthFirstValue = -27f;
                        if (interpolationValue < 1)
                        {
                            this.Sala.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setSalaXFirstValue = true;
                        SalaXFirstValue = 7f;
                        setSalaYFirstValue = true;
                        SalaYFirstValue = 30f;
                        setTextInstanceFontSizeFirstValue = true;
                        TextInstanceFontSizeFirstValue = 12;
                        setTextInstanceHeightFirstValue = true;
                        TextInstanceHeightFirstValue = 16f;
                        if (interpolationValue < 1)
                        {
                            this.TextInstance.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setTextInstanceOutlineThicknessFirstValue = true;
                        TextInstanceOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.TextInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Sala");
                        }
                        if (interpolationValue < 1)
                        {
                            this.TextInstance.Text = "00";
                        }
                        setTextInstanceWidthFirstValue = true;
                        TextInstanceWidthFirstValue = 19f;
                        setTextInstanceXFirstValue = true;
                        TextInstanceXFirstValue = 6f;
                        setTextInstanceYFirstValue = true;
                        TextInstanceYFirstValue = 4f;
                        setTextInstance1FontSizeFirstValue = true;
                        TextInstance1FontSizeFirstValue = 12;
                        setTextInstance1HeightFirstValue = true;
                        TextInstance1HeightFirstValue = 18f;
                        if (interpolationValue < 1)
                        {
                            this.TextInstance1.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setTextInstance1OutlineThicknessFirstValue = true;
                        TextInstance1OutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.TextInstance1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Sala");
                        }
                        if (interpolationValue < 1)
                        {
                            this.TextInstance1.Text = "00";
                        }
                        setTextInstance1WidthFirstValue = true;
                        TextInstance1WidthFirstValue = 20f;
                        setTextInstance1XFirstValue = true;
                        TextInstance1XFirstValue = 28f;
                        setTextInstance1YFirstValue = true;
                        TextInstance1YFirstValue = 4f;
                        setTextInstance2HeightFirstValue = true;
                        TextInstance2HeightFirstValue = 23f;
                        setTextInstance2OutlineThicknessFirstValue = true;
                        TextInstance2OutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.TextInstance2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Sala");
                        }
                        if (interpolationValue < 1)
                        {
                            this.TextInstance2.Text = "/";
                        }
                        setTextInstance2WidthFirstValue = true;
                        TextInstance2WidthFirstValue = 10f;
                        setTextInstance2XFirstValue = true;
                        TextInstance2XFirstValue = 22f;
                        setTextInstance2YFirstValue = true;
                        TextInstance2YFirstValue = 1f;
                        setWidthFirstValue = true;
                        WidthFirstValue = -735f;
                        if (interpolationValue < 1)
                        {
                            this.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setXFirstValue = true;
                        XFirstValue = 0f;
                        setYFirstValue = true;
                        YFirstValue = 2f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setHeightSecondValue = true;
                        HeightSecondValue = -546f;
                        if (interpolationValue >= 1)
                        {
                            this.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHud_NivelFontSizeSecondValue = true;
                        Hud_NivelFontSizeSecondValue = 22;
                        setHud_NivelHeightSecondValue = true;
                        Hud_NivelHeightSecondValue = -28f;
                        if (interpolationValue >= 1)
                        {
                            this.Hud_Nivel.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Hud_Nivel.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setHud_NivelOutlineThicknessSecondValue = true;
                        Hud_NivelOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.Hud_Nivel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "panelinset_brown");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Hud_Nivel.Text = "0";
                        }
                        setHud_NivelWidthSecondValue = true;
                        Hud_NivelWidthSecondValue = -23f;
                        if (interpolationValue >= 1)
                        {
                            this.Hud_Nivel.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHud_NivelXSecondValue = true;
                        Hud_NivelXSecondValue = 14f;
                        setHud_NivelYSecondValue = true;
                        Hud_NivelYSecondValue = 9f;
                        if (interpolationValue >= 1)
                        {
                            this.LabelLv.Font = "Arial Narrow";
                        }
                        setLabelLvHeightSecondValue = true;
                        LabelLvHeightSecondValue = -31f;
                        if (interpolationValue >= 1)
                        {
                            this.LabelLv.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setLabelLvOutlineThicknessSecondValue = true;
                        LabelLvOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.LabelLv.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "panelinset_brown");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.LabelLv.Text = "Nv";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.LabelLv.UseCustomFont = false;
                        }
                        setLabelLvWidthSecondValue = true;
                        LabelLvWidthSecondValue = -13f;
                        if (interpolationValue >= 1)
                        {
                            this.LabelLv.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setLabelLvXSecondValue = true;
                        LabelLvXSecondValue = 4f;
                        setLabelLvYSecondValue = true;
                        LabelLvYSecondValue = 6f;
                        setpanelinset_brownHeightSecondValue = true;
                        panelinset_brownHeightSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.panelinset_brown.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("panelinset_brown.SourceFile", "../screens/mainmenuscreen/panelinset_brown.png");
                        }
                        setpanelinset_brownWidthSecondValue = true;
                        panelinset_brownWidthSecondValue = -7f;
                        if (interpolationValue >= 1)
                        {
                            this.panelinset_brown.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setpanelinset_brownXSecondValue = true;
                        panelinset_brownXSecondValue = 0f;
                        setpanelinset_brownYSecondValue = true;
                        panelinset_brownYSecondValue = 0f;
                        setSalaHeightSecondValue = true;
                        SalaHeightSecondValue = -34f;
                        if (interpolationValue >= 1)
                        {
                            this.Sala.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Sala.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "panelinset_brown");
                        }
                        setSalaWidthSecondValue = true;
                        SalaWidthSecondValue = -27f;
                        if (interpolationValue >= 1)
                        {
                            this.Sala.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setSalaXSecondValue = true;
                        SalaXSecondValue = 7f;
                        setSalaYSecondValue = true;
                        SalaYSecondValue = 30f;
                        setTextInstanceFontSizeSecondValue = true;
                        TextInstanceFontSizeSecondValue = 12;
                        setTextInstanceHeightSecondValue = true;
                        TextInstanceHeightSecondValue = 16f;
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setTextInstanceOutlineThicknessSecondValue = true;
                        TextInstanceOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Sala");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance.Text = "00";
                        }
                        setTextInstanceWidthSecondValue = true;
                        TextInstanceWidthSecondValue = 19f;
                        setTextInstanceXSecondValue = true;
                        TextInstanceXSecondValue = 6f;
                        setTextInstanceYSecondValue = true;
                        TextInstanceYSecondValue = 4f;
                        setTextInstance1FontSizeSecondValue = true;
                        TextInstance1FontSizeSecondValue = 12;
                        setTextInstance1HeightSecondValue = true;
                        TextInstance1HeightSecondValue = 18f;
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance1.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setTextInstance1OutlineThicknessSecondValue = true;
                        TextInstance1OutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Sala");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance1.Text = "00";
                        }
                        setTextInstance1WidthSecondValue = true;
                        TextInstance1WidthSecondValue = 20f;
                        setTextInstance1XSecondValue = true;
                        TextInstance1XSecondValue = 28f;
                        setTextInstance1YSecondValue = true;
                        TextInstance1YSecondValue = 4f;
                        setTextInstance2HeightSecondValue = true;
                        TextInstance2HeightSecondValue = 23f;
                        setTextInstance2OutlineThicknessSecondValue = true;
                        TextInstance2OutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Sala");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance2.Text = "/";
                        }
                        setTextInstance2WidthSecondValue = true;
                        TextInstance2WidthSecondValue = 10f;
                        setTextInstance2XSecondValue = true;
                        TextInstance2XSecondValue = 22f;
                        setTextInstance2YSecondValue = true;
                        TextInstance2YSecondValue = 1f;
                        setWidthSecondValue = true;
                        WidthSecondValue = -735f;
                        if (interpolationValue >= 1)
                        {
                            this.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setXSecondValue = true;
                        XSecondValue = 0f;
                        setYSecondValue = true;
                        YSecondValue = 2f;
                        break;
                }
                if (setHeightFirstValue && setHeightSecondValue)
                {
                    Height = HeightFirstValue * (1 - interpolationValue) + HeightSecondValue * interpolationValue;
                }
                if (setHud_NivelFontSizeFirstValue && setHud_NivelFontSizeSecondValue)
                {
                    Hud_Nivel.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(Hud_NivelFontSizeFirstValue* (1 - interpolationValue) + Hud_NivelFontSizeSecondValue * interpolationValue);
                }
                if (setHud_NivelHeightFirstValue && setHud_NivelHeightSecondValue)
                {
                    Hud_Nivel.Height = Hud_NivelHeightFirstValue * (1 - interpolationValue) + Hud_NivelHeightSecondValue * interpolationValue;
                }
                if (setHud_NivelOutlineThicknessFirstValue && setHud_NivelOutlineThicknessSecondValue)
                {
                    Hud_Nivel.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(Hud_NivelOutlineThicknessFirstValue* (1 - interpolationValue) + Hud_NivelOutlineThicknessSecondValue * interpolationValue);
                }
                if (setHud_NivelWidthFirstValue && setHud_NivelWidthSecondValue)
                {
                    Hud_Nivel.Width = Hud_NivelWidthFirstValue * (1 - interpolationValue) + Hud_NivelWidthSecondValue * interpolationValue;
                }
                if (setHud_NivelXFirstValue && setHud_NivelXSecondValue)
                {
                    Hud_Nivel.X = Hud_NivelXFirstValue * (1 - interpolationValue) + Hud_NivelXSecondValue * interpolationValue;
                }
                if (setHud_NivelYFirstValue && setHud_NivelYSecondValue)
                {
                    Hud_Nivel.Y = Hud_NivelYFirstValue * (1 - interpolationValue) + Hud_NivelYSecondValue * interpolationValue;
                }
                if (setLabelLvHeightFirstValue && setLabelLvHeightSecondValue)
                {
                    LabelLv.Height = LabelLvHeightFirstValue * (1 - interpolationValue) + LabelLvHeightSecondValue * interpolationValue;
                }
                if (setLabelLvOutlineThicknessFirstValue && setLabelLvOutlineThicknessSecondValue)
                {
                    LabelLv.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(LabelLvOutlineThicknessFirstValue* (1 - interpolationValue) + LabelLvOutlineThicknessSecondValue * interpolationValue);
                }
                if (setLabelLvWidthFirstValue && setLabelLvWidthSecondValue)
                {
                    LabelLv.Width = LabelLvWidthFirstValue * (1 - interpolationValue) + LabelLvWidthSecondValue * interpolationValue;
                }
                if (setLabelLvXFirstValue && setLabelLvXSecondValue)
                {
                    LabelLv.X = LabelLvXFirstValue * (1 - interpolationValue) + LabelLvXSecondValue * interpolationValue;
                }
                if (setLabelLvYFirstValue && setLabelLvYSecondValue)
                {
                    LabelLv.Y = LabelLvYFirstValue * (1 - interpolationValue) + LabelLvYSecondValue * interpolationValue;
                }
                if (setpanelinset_brownHeightFirstValue && setpanelinset_brownHeightSecondValue)
                {
                    panelinset_brown.Height = panelinset_brownHeightFirstValue * (1 - interpolationValue) + panelinset_brownHeightSecondValue * interpolationValue;
                }
                if (setpanelinset_brownWidthFirstValue && setpanelinset_brownWidthSecondValue)
                {
                    panelinset_brown.Width = panelinset_brownWidthFirstValue * (1 - interpolationValue) + panelinset_brownWidthSecondValue * interpolationValue;
                }
                if (setpanelinset_brownXFirstValue && setpanelinset_brownXSecondValue)
                {
                    panelinset_brown.X = panelinset_brownXFirstValue * (1 - interpolationValue) + panelinset_brownXSecondValue * interpolationValue;
                }
                if (setpanelinset_brownYFirstValue && setpanelinset_brownYSecondValue)
                {
                    panelinset_brown.Y = panelinset_brownYFirstValue * (1 - interpolationValue) + panelinset_brownYSecondValue * interpolationValue;
                }
                if (setSalaHeightFirstValue && setSalaHeightSecondValue)
                {
                    Sala.Height = SalaHeightFirstValue * (1 - interpolationValue) + SalaHeightSecondValue * interpolationValue;
                }
                if (setSalaWidthFirstValue && setSalaWidthSecondValue)
                {
                    Sala.Width = SalaWidthFirstValue * (1 - interpolationValue) + SalaWidthSecondValue * interpolationValue;
                }
                if (setSalaXFirstValue && setSalaXSecondValue)
                {
                    Sala.X = SalaXFirstValue * (1 - interpolationValue) + SalaXSecondValue * interpolationValue;
                }
                if (setSalaYFirstValue && setSalaYSecondValue)
                {
                    Sala.Y = SalaYFirstValue * (1 - interpolationValue) + SalaYSecondValue * interpolationValue;
                }
                if (setTextInstanceFontSizeFirstValue && setTextInstanceFontSizeSecondValue)
                {
                    TextInstance.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstanceFontSizeFirstValue* (1 - interpolationValue) + TextInstanceFontSizeSecondValue * interpolationValue);
                }
                if (setTextInstanceHeightFirstValue && setTextInstanceHeightSecondValue)
                {
                    TextInstance.Height = TextInstanceHeightFirstValue * (1 - interpolationValue) + TextInstanceHeightSecondValue * interpolationValue;
                }
                if (setTextInstanceOutlineThicknessFirstValue && setTextInstanceOutlineThicknessSecondValue)
                {
                    TextInstance.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstanceOutlineThicknessFirstValue* (1 - interpolationValue) + TextInstanceOutlineThicknessSecondValue * interpolationValue);
                }
                if (setTextInstanceWidthFirstValue && setTextInstanceWidthSecondValue)
                {
                    TextInstance.Width = TextInstanceWidthFirstValue * (1 - interpolationValue) + TextInstanceWidthSecondValue * interpolationValue;
                }
                if (setTextInstanceXFirstValue && setTextInstanceXSecondValue)
                {
                    TextInstance.X = TextInstanceXFirstValue * (1 - interpolationValue) + TextInstanceXSecondValue * interpolationValue;
                }
                if (setTextInstanceYFirstValue && setTextInstanceYSecondValue)
                {
                    TextInstance.Y = TextInstanceYFirstValue * (1 - interpolationValue) + TextInstanceYSecondValue * interpolationValue;
                }
                if (setTextInstance1FontSizeFirstValue && setTextInstance1FontSizeSecondValue)
                {
                    TextInstance1.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstance1FontSizeFirstValue* (1 - interpolationValue) + TextInstance1FontSizeSecondValue * interpolationValue);
                }
                if (setTextInstance1HeightFirstValue && setTextInstance1HeightSecondValue)
                {
                    TextInstance1.Height = TextInstance1HeightFirstValue * (1 - interpolationValue) + TextInstance1HeightSecondValue * interpolationValue;
                }
                if (setTextInstance1OutlineThicknessFirstValue && setTextInstance1OutlineThicknessSecondValue)
                {
                    TextInstance1.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstance1OutlineThicknessFirstValue* (1 - interpolationValue) + TextInstance1OutlineThicknessSecondValue * interpolationValue);
                }
                if (setTextInstance1WidthFirstValue && setTextInstance1WidthSecondValue)
                {
                    TextInstance1.Width = TextInstance1WidthFirstValue * (1 - interpolationValue) + TextInstance1WidthSecondValue * interpolationValue;
                }
                if (setTextInstance1XFirstValue && setTextInstance1XSecondValue)
                {
                    TextInstance1.X = TextInstance1XFirstValue * (1 - interpolationValue) + TextInstance1XSecondValue * interpolationValue;
                }
                if (setTextInstance1YFirstValue && setTextInstance1YSecondValue)
                {
                    TextInstance1.Y = TextInstance1YFirstValue * (1 - interpolationValue) + TextInstance1YSecondValue * interpolationValue;
                }
                if (setTextInstance2HeightFirstValue && setTextInstance2HeightSecondValue)
                {
                    TextInstance2.Height = TextInstance2HeightFirstValue * (1 - interpolationValue) + TextInstance2HeightSecondValue * interpolationValue;
                }
                if (setTextInstance2OutlineThicknessFirstValue && setTextInstance2OutlineThicknessSecondValue)
                {
                    TextInstance2.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstance2OutlineThicknessFirstValue* (1 - interpolationValue) + TextInstance2OutlineThicknessSecondValue * interpolationValue);
                }
                if (setTextInstance2WidthFirstValue && setTextInstance2WidthSecondValue)
                {
                    TextInstance2.Width = TextInstance2WidthFirstValue * (1 - interpolationValue) + TextInstance2WidthSecondValue * interpolationValue;
                }
                if (setTextInstance2XFirstValue && setTextInstance2XSecondValue)
                {
                    TextInstance2.X = TextInstance2XFirstValue * (1 - interpolationValue) + TextInstance2XSecondValue * interpolationValue;
                }
                if (setTextInstance2YFirstValue && setTextInstance2YSecondValue)
                {
                    TextInstance2.Y = TextInstance2YFirstValue * (1 - interpolationValue) + TextInstance2YSecondValue * interpolationValue;
                }
                if (setWidthFirstValue && setWidthSecondValue)
                {
                    Width = WidthFirstValue * (1 - interpolationValue) + WidthSecondValue * interpolationValue;
                }
                if (setXFirstValue && setXSecondValue)
                {
                    X = XFirstValue * (1 - interpolationValue) + XSecondValue * interpolationValue;
                }
                if (setYFirstValue && setYSecondValue)
                {
                    Y = YFirstValue * (1 - interpolationValue) + YSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.LevelDisplayRuntime.VariableState fromState,DungeonRun.GumRuntimes.LevelDisplayRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height Units",
                            Type = "DimensionUnitType",
                            Value = HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width Units",
                            Type = "DimensionUnitType",
                            Value = WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "X",
                            Type = "float",
                            Value = X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Y",
                            Type = "float",
                            Value = Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.Height",
                            Type = "float",
                            Value = panelinset_brown.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.Height Units",
                            Type = "DimensionUnitType",
                            Value = panelinset_brown.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.SourceFile",
                            Type = "string",
                            Value = panelinset_brown.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.Width",
                            Type = "float",
                            Value = panelinset_brown.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.Width Units",
                            Type = "DimensionUnitType",
                            Value = panelinset_brown.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.X",
                            Type = "float",
                            Value = panelinset_brown.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.Y",
                            Type = "float",
                            Value = panelinset_brown.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Font",
                            Type = "string",
                            Value = LabelLv.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Height",
                            Type = "float",
                            Value = LabelLv.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Height Units",
                            Type = "DimensionUnitType",
                            Value = LabelLv.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.OutlineThickness",
                            Type = "int",
                            Value = LabelLv.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Parent",
                            Type = "string",
                            Value = LabelLv.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Text",
                            Type = "string",
                            Value = LabelLv.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.UseCustomFont",
                            Type = "bool",
                            Value = LabelLv.UseCustomFont
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Width",
                            Type = "float",
                            Value = LabelLv.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Width Units",
                            Type = "DimensionUnitType",
                            Value = LabelLv.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.X",
                            Type = "float",
                            Value = LabelLv.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Y",
                            Type = "float",
                            Value = LabelLv.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.FontSize",
                            Type = "int",
                            Value = Hud_Nivel.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Height",
                            Type = "float",
                            Value = Hud_Nivel.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Height Units",
                            Type = "DimensionUnitType",
                            Value = Hud_Nivel.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = Hud_Nivel.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.OutlineThickness",
                            Type = "int",
                            Value = Hud_Nivel.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Parent",
                            Type = "string",
                            Value = Hud_Nivel.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Text",
                            Type = "string",
                            Value = Hud_Nivel.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Width",
                            Type = "float",
                            Value = Hud_Nivel.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Width Units",
                            Type = "DimensionUnitType",
                            Value = Hud_Nivel.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.X",
                            Type = "float",
                            Value = Hud_Nivel.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Y",
                            Type = "float",
                            Value = Hud_Nivel.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.FontSize",
                            Type = "int",
                            Value = TextInstance.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Height",
                            Type = "float",
                            Value = TextInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = TextInstance.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.OutlineThickness",
                            Type = "int",
                            Value = TextInstance.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Parent",
                            Type = "string",
                            Value = TextInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Text",
                            Type = "string",
                            Value = TextInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Width",
                            Type = "float",
                            Value = TextInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.X",
                            Type = "float",
                            Value = TextInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Y",
                            Type = "float",
                            Value = TextInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.FontSize",
                            Type = "int",
                            Value = TextInstance1.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.Height",
                            Type = "float",
                            Value = TextInstance1.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = TextInstance1.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.OutlineThickness",
                            Type = "int",
                            Value = TextInstance1.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.Parent",
                            Type = "string",
                            Value = TextInstance1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.Text",
                            Type = "string",
                            Value = TextInstance1.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.Width",
                            Type = "float",
                            Value = TextInstance1.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.X",
                            Type = "float",
                            Value = TextInstance1.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.Y",
                            Type = "float",
                            Value = TextInstance1.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Height",
                            Type = "float",
                            Value = Sala.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Height Units",
                            Type = "DimensionUnitType",
                            Value = Sala.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Parent",
                            Type = "string",
                            Value = Sala.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Width",
                            Type = "float",
                            Value = Sala.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Width Units",
                            Type = "DimensionUnitType",
                            Value = Sala.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.X",
                            Type = "float",
                            Value = Sala.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Y",
                            Type = "float",
                            Value = Sala.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.Height",
                            Type = "float",
                            Value = TextInstance2.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.OutlineThickness",
                            Type = "int",
                            Value = TextInstance2.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.Parent",
                            Type = "string",
                            Value = TextInstance2.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.Text",
                            Type = "string",
                            Value = TextInstance2.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.Width",
                            Type = "float",
                            Value = TextInstance2.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.X",
                            Type = "float",
                            Value = TextInstance2.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.Y",
                            Type = "float",
                            Value = TextInstance2.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height + -546f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height Units",
                            Type = "DimensionUnitType",
                            Value = HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width + -735f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width Units",
                            Type = "DimensionUnitType",
                            Value = WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "X",
                            Type = "float",
                            Value = X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Y",
                            Type = "float",
                            Value = Y + 2f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.Height",
                            Type = "float",
                            Value = panelinset_brown.Height + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.Height Units",
                            Type = "DimensionUnitType",
                            Value = panelinset_brown.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.SourceFile",
                            Type = "string",
                            Value = panelinset_brown.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.Width",
                            Type = "float",
                            Value = panelinset_brown.Width + -7f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.Width Units",
                            Type = "DimensionUnitType",
                            Value = panelinset_brown.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.X",
                            Type = "float",
                            Value = panelinset_brown.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "panelinset_brown.Y",
                            Type = "float",
                            Value = panelinset_brown.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Font",
                            Type = "string",
                            Value = LabelLv.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Height",
                            Type = "float",
                            Value = LabelLv.Height + -31f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Height Units",
                            Type = "DimensionUnitType",
                            Value = LabelLv.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.OutlineThickness",
                            Type = "int",
                            Value = LabelLv.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Parent",
                            Type = "string",
                            Value = LabelLv.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Text",
                            Type = "string",
                            Value = LabelLv.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.UseCustomFont",
                            Type = "bool",
                            Value = LabelLv.UseCustomFont
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Width",
                            Type = "float",
                            Value = LabelLv.Width + -13f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Width Units",
                            Type = "DimensionUnitType",
                            Value = LabelLv.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.X",
                            Type = "float",
                            Value = LabelLv.X + 4f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LabelLv.Y",
                            Type = "float",
                            Value = LabelLv.Y + 6f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.FontSize",
                            Type = "int",
                            Value = Hud_Nivel.FontSize + 22
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Height",
                            Type = "float",
                            Value = Hud_Nivel.Height + -28f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Height Units",
                            Type = "DimensionUnitType",
                            Value = Hud_Nivel.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = Hud_Nivel.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.OutlineThickness",
                            Type = "int",
                            Value = Hud_Nivel.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Parent",
                            Type = "string",
                            Value = Hud_Nivel.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Text",
                            Type = "string",
                            Value = Hud_Nivel.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Width",
                            Type = "float",
                            Value = Hud_Nivel.Width + -23f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Width Units",
                            Type = "DimensionUnitType",
                            Value = Hud_Nivel.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.X",
                            Type = "float",
                            Value = Hud_Nivel.X + 14f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Hud_Nivel.Y",
                            Type = "float",
                            Value = Hud_Nivel.Y + 9f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.FontSize",
                            Type = "int",
                            Value = TextInstance.FontSize + 12
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Height",
                            Type = "float",
                            Value = TextInstance.Height + 16f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = TextInstance.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.OutlineThickness",
                            Type = "int",
                            Value = TextInstance.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Parent",
                            Type = "string",
                            Value = TextInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Text",
                            Type = "string",
                            Value = TextInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Width",
                            Type = "float",
                            Value = TextInstance.Width + 19f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.X",
                            Type = "float",
                            Value = TextInstance.X + 6f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Y",
                            Type = "float",
                            Value = TextInstance.Y + 4f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.FontSize",
                            Type = "int",
                            Value = TextInstance1.FontSize + 12
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.Height",
                            Type = "float",
                            Value = TextInstance1.Height + 18f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = TextInstance1.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.OutlineThickness",
                            Type = "int",
                            Value = TextInstance1.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.Parent",
                            Type = "string",
                            Value = TextInstance1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.Text",
                            Type = "string",
                            Value = TextInstance1.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.Width",
                            Type = "float",
                            Value = TextInstance1.Width + 20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.X",
                            Type = "float",
                            Value = TextInstance1.X + 28f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance1.Y",
                            Type = "float",
                            Value = TextInstance1.Y + 4f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Height",
                            Type = "float",
                            Value = Sala.Height + -34f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Height Units",
                            Type = "DimensionUnitType",
                            Value = Sala.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Parent",
                            Type = "string",
                            Value = Sala.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Width",
                            Type = "float",
                            Value = Sala.Width + -27f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Width Units",
                            Type = "DimensionUnitType",
                            Value = Sala.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.X",
                            Type = "float",
                            Value = Sala.X + 7f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Sala.Y",
                            Type = "float",
                            Value = Sala.Y + 30f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.Height",
                            Type = "float",
                            Value = TextInstance2.Height + 23f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.OutlineThickness",
                            Type = "int",
                            Value = TextInstance2.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.Parent",
                            Type = "string",
                            Value = TextInstance2.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.Text",
                            Type = "string",
                            Value = TextInstance2.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.Width",
                            Type = "float",
                            Value = TextInstance2.Width + 10f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.X",
                            Type = "float",
                            Value = TextInstance2.X + 22f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance2.Y",
                            Type = "float",
                            Value = TextInstance2.Y + 1f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.SpriteRuntime panelinset_brown { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime LabelLv { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime Hud_Nivel { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TextInstance { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TextInstance1 { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime Sala { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TextInstance2 { get; set; }
            public string LevelDisplay_txt_Hud
            {
                get
                {
                    return Hud_Nivel.Text;
                }
                set
                {
                    if (Hud_Nivel.Text != value)
                    {
                        Hud_Nivel.Text = value;
                        LevelDisplay_txt_HudChanged?.Invoke(this, null);
                    }
                }
            }
            public string RomIn_HUD
            {
                get
                {
                    return TextInstance.Text;
                }
                set
                {
                    if (TextInstance.Text != value)
                    {
                        TextInstance.Text = value;
                        RomIn_HUDChanged?.Invoke(this, null);
                    }
                }
            }
            public string RomOf_HUD
            {
                get
                {
                    return TextInstance1.Text;
                }
                set
                {
                    if (TextInstance1.Text != value)
                    {
                        TextInstance1.Text = value;
                        RomOf_HUDChanged?.Invoke(this, null);
                    }
                }
            }
            public event System.EventHandler LevelDisplay_txt_HudChanged;
            public event System.EventHandler RomIn_HUDChanged;
            public event System.EventHandler RomOf_HUDChanged;
            public LevelDisplayRuntime (bool fullInstantiation = true) 
            	: base(false)
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "LevelDisplay");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                panelinset_brown = this.GetGraphicalUiElementByName("panelinset_brown") as DungeonRun.GumRuntimes.SpriteRuntime;
                LabelLv = this.GetGraphicalUiElementByName("LabelLv") as DungeonRun.GumRuntimes.TextRuntime;
                Hud_Nivel = this.GetGraphicalUiElementByName("Hud_Nivel") as DungeonRun.GumRuntimes.TextRuntime;
                TextInstance = this.GetGraphicalUiElementByName("TextInstance") as DungeonRun.GumRuntimes.TextRuntime;
                TextInstance1 = this.GetGraphicalUiElementByName("TextInstance1") as DungeonRun.GumRuntimes.TextRuntime;
                Sala = this.GetGraphicalUiElementByName("Sala") as DungeonRun.GumRuntimes.ContainerRuntime;
                TextInstance2 = this.GetGraphicalUiElementByName("TextInstance2") as DungeonRun.GumRuntimes.TextRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
