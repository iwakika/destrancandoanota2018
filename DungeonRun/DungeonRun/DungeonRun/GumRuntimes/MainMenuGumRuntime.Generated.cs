    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class MainMenuGumRuntime : Gum.Wireframe.GraphicalUiElement
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            ButtonGum1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            ButtonGum1.Text = "Iniciar";
                            ButtonGum1.X = 0f;
                            ButtonGum1.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            ButtonGum1.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            ButtonGum1.Y = 0f;
                            ButtonGum1.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            ButtonGum1.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            EditTextBoxInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            EditTextBoxInstance.X = 0f;
                            EditTextBoxInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            EditTextBoxInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            EditTextBoxInstance.Y = -60f;
                            EditTextBoxInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            EditTextBoxInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            ButtonGum2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            ButtonGum2.Text = "Controles";
                            ButtonGum2.X = 0f;
                            ButtonGum2.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            ButtonGum2.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            ButtonGum2.Y = 70f;
                            ButtonGum2.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            ButtonGum2.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            ButtonGum3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            ButtonGum3.Text = "Rankings";
                            ButtonGum3.X = 0f;
                            ButtonGum3.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            ButtonGum3.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            ButtonGum3.Y = 140f;
                            ButtonGum3.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            ButtonGum3.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            ContainerInstance.Height = 320f;
                            ContainerInstance.Visible = true;
                            ContainerInstance.Width = 261f;
                            ContainerInstance.X = 283f;
                            ContainerInstance.Y = 230f;
                            SpriteInstance.Height = 75.14699f;
                            SetProperty("SpriteInstance.SourceFile", "DestrancandoNota3.png");
                            SpriteInstance.Width = 78.31094f;
                            SpriteInstance.X = 83f;
                            SpriteInstance.Y = -49f;
                            DialogBoxInstance.Height = 109f;
                            DialogBoxInstance.Text = "Yep";
                            DialogBoxInstance.Visible = false;
                            DialogBoxInstance.Width = 423f;
                            DialogBoxInstance.X = 0f;
                            DialogBoxInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            DialogBoxInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            DialogBoxInstance.Y = -145f;
                            DialogBoxInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                            DialogBoxInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setButtonGum1XFirstValue = false;
                bool setButtonGum1XSecondValue = false;
                float ButtonGum1XFirstValue= 0;
                float ButtonGum1XSecondValue= 0;
                bool setButtonGum1YFirstValue = false;
                bool setButtonGum1YSecondValue = false;
                float ButtonGum1YFirstValue= 0;
                float ButtonGum1YSecondValue= 0;
                bool setButtonGum2XFirstValue = false;
                bool setButtonGum2XSecondValue = false;
                float ButtonGum2XFirstValue= 0;
                float ButtonGum2XSecondValue= 0;
                bool setButtonGum2YFirstValue = false;
                bool setButtonGum2YSecondValue = false;
                float ButtonGum2YFirstValue= 0;
                float ButtonGum2YSecondValue= 0;
                bool setButtonGum3XFirstValue = false;
                bool setButtonGum3XSecondValue = false;
                float ButtonGum3XFirstValue= 0;
                float ButtonGum3XSecondValue= 0;
                bool setButtonGum3YFirstValue = false;
                bool setButtonGum3YSecondValue = false;
                float ButtonGum3YFirstValue= 0;
                float ButtonGum3YSecondValue= 0;
                bool setContainerInstanceHeightFirstValue = false;
                bool setContainerInstanceHeightSecondValue = false;
                float ContainerInstanceHeightFirstValue= 0;
                float ContainerInstanceHeightSecondValue= 0;
                bool setContainerInstanceWidthFirstValue = false;
                bool setContainerInstanceWidthSecondValue = false;
                float ContainerInstanceWidthFirstValue= 0;
                float ContainerInstanceWidthSecondValue= 0;
                bool setContainerInstanceXFirstValue = false;
                bool setContainerInstanceXSecondValue = false;
                float ContainerInstanceXFirstValue= 0;
                float ContainerInstanceXSecondValue= 0;
                bool setContainerInstanceYFirstValue = false;
                bool setContainerInstanceYSecondValue = false;
                float ContainerInstanceYFirstValue= 0;
                float ContainerInstanceYSecondValue= 0;
                bool setDialogBoxInstanceHeightFirstValue = false;
                bool setDialogBoxInstanceHeightSecondValue = false;
                float DialogBoxInstanceHeightFirstValue= 0;
                float DialogBoxInstanceHeightSecondValue= 0;
                bool setDialogBoxInstanceWidthFirstValue = false;
                bool setDialogBoxInstanceWidthSecondValue = false;
                float DialogBoxInstanceWidthFirstValue= 0;
                float DialogBoxInstanceWidthSecondValue= 0;
                bool setDialogBoxInstanceXFirstValue = false;
                bool setDialogBoxInstanceXSecondValue = false;
                float DialogBoxInstanceXFirstValue= 0;
                float DialogBoxInstanceXSecondValue= 0;
                bool setDialogBoxInstanceYFirstValue = false;
                bool setDialogBoxInstanceYSecondValue = false;
                float DialogBoxInstanceYFirstValue= 0;
                float DialogBoxInstanceYSecondValue= 0;
                bool setEditTextBoxInstanceXFirstValue = false;
                bool setEditTextBoxInstanceXSecondValue = false;
                float EditTextBoxInstanceXFirstValue= 0;
                float EditTextBoxInstanceXSecondValue= 0;
                bool setEditTextBoxInstanceYFirstValue = false;
                bool setEditTextBoxInstanceYSecondValue = false;
                float EditTextBoxInstanceYFirstValue= 0;
                float EditTextBoxInstanceYSecondValue= 0;
                bool setSpriteInstanceHeightFirstValue = false;
                bool setSpriteInstanceHeightSecondValue = false;
                float SpriteInstanceHeightFirstValue= 0;
                float SpriteInstanceHeightSecondValue= 0;
                bool setSpriteInstanceWidthFirstValue = false;
                bool setSpriteInstanceWidthSecondValue = false;
                float SpriteInstanceWidthFirstValue= 0;
                float SpriteInstanceWidthSecondValue= 0;
                bool setSpriteInstanceXFirstValue = false;
                bool setSpriteInstanceXSecondValue = false;
                float SpriteInstanceXFirstValue= 0;
                float SpriteInstanceXSecondValue= 0;
                bool setSpriteInstanceYFirstValue = false;
                bool setSpriteInstanceYSecondValue = false;
                float SpriteInstanceYFirstValue= 0;
                float SpriteInstanceYSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum1.Text = "Iniciar";
                        }
                        setButtonGum1XFirstValue = true;
                        ButtonGum1XFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum1.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum1.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setButtonGum1YFirstValue = true;
                        ButtonGum1YFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum1.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum1.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum2.Text = "Controles";
                        }
                        setButtonGum2XFirstValue = true;
                        ButtonGum2XFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum2.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum2.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setButtonGum2YFirstValue = true;
                        ButtonGum2YFirstValue = 70f;
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum2.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum2.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum3.Text = "Rankings";
                        }
                        setButtonGum3XFirstValue = true;
                        ButtonGum3XFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum3.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum3.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setButtonGum3YFirstValue = true;
                        ButtonGum3YFirstValue = 140f;
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum3.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ButtonGum3.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setContainerInstanceHeightFirstValue = true;
                        ContainerInstanceHeightFirstValue = 320f;
                        if (interpolationValue < 1)
                        {
                            this.ContainerInstance.Visible = true;
                        }
                        setContainerInstanceWidthFirstValue = true;
                        ContainerInstanceWidthFirstValue = 261f;
                        setContainerInstanceXFirstValue = true;
                        ContainerInstanceXFirstValue = 283f;
                        setContainerInstanceYFirstValue = true;
                        ContainerInstanceYFirstValue = 230f;
                        setDialogBoxInstanceHeightFirstValue = true;
                        DialogBoxInstanceHeightFirstValue = 109f;
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.Text = "Yep";
                        }
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.Visible = false;
                        }
                        setDialogBoxInstanceWidthFirstValue = true;
                        DialogBoxInstanceWidthFirstValue = 423f;
                        setDialogBoxInstanceXFirstValue = true;
                        DialogBoxInstanceXFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setDialogBoxInstanceYFirstValue = true;
                        DialogBoxInstanceYFirstValue = -145f;
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                        }
                        if (interpolationValue < 1)
                        {
                            this.DialogBoxInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                        }
                        if (interpolationValue < 1)
                        {
                            this.EditTextBoxInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setEditTextBoxInstanceXFirstValue = true;
                        EditTextBoxInstanceXFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.EditTextBoxInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.EditTextBoxInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setEditTextBoxInstanceYFirstValue = true;
                        EditTextBoxInstanceYFirstValue = -60f;
                        if (interpolationValue < 1)
                        {
                            this.EditTextBoxInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.EditTextBoxInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setSpriteInstanceHeightFirstValue = true;
                        SpriteInstanceHeightFirstValue = 75.14699f;
                        if (interpolationValue < 1)
                        {
                            SetProperty("SpriteInstance.SourceFile", "DestrancandoNota3.png");
                        }
                        setSpriteInstanceWidthFirstValue = true;
                        SpriteInstanceWidthFirstValue = 78.31094f;
                        setSpriteInstanceXFirstValue = true;
                        SpriteInstanceXFirstValue = 83f;
                        setSpriteInstanceYFirstValue = true;
                        SpriteInstanceYFirstValue = -49f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum1.Text = "Iniciar";
                        }
                        setButtonGum1XSecondValue = true;
                        ButtonGum1XSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum1.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum1.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setButtonGum1YSecondValue = true;
                        ButtonGum1YSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum1.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum1.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum2.Text = "Controles";
                        }
                        setButtonGum2XSecondValue = true;
                        ButtonGum2XSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum2.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum2.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setButtonGum2YSecondValue = true;
                        ButtonGum2YSecondValue = 70f;
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum2.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum2.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum3.Text = "Rankings";
                        }
                        setButtonGum3XSecondValue = true;
                        ButtonGum3XSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum3.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum3.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setButtonGum3YSecondValue = true;
                        ButtonGum3YSecondValue = 140f;
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum3.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ButtonGum3.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setContainerInstanceHeightSecondValue = true;
                        ContainerInstanceHeightSecondValue = 320f;
                        if (interpolationValue >= 1)
                        {
                            this.ContainerInstance.Visible = true;
                        }
                        setContainerInstanceWidthSecondValue = true;
                        ContainerInstanceWidthSecondValue = 261f;
                        setContainerInstanceXSecondValue = true;
                        ContainerInstanceXSecondValue = 283f;
                        setContainerInstanceYSecondValue = true;
                        ContainerInstanceYSecondValue = 230f;
                        setDialogBoxInstanceHeightSecondValue = true;
                        DialogBoxInstanceHeightSecondValue = 109f;
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.Text = "Yep";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.Visible = false;
                        }
                        setDialogBoxInstanceWidthSecondValue = true;
                        DialogBoxInstanceWidthSecondValue = 423f;
                        setDialogBoxInstanceXSecondValue = true;
                        DialogBoxInstanceXSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setDialogBoxInstanceYSecondValue = true;
                        DialogBoxInstanceYSecondValue = -145f;
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.DialogBoxInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.EditTextBoxInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setEditTextBoxInstanceXSecondValue = true;
                        EditTextBoxInstanceXSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.EditTextBoxInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.EditTextBoxInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setEditTextBoxInstanceYSecondValue = true;
                        EditTextBoxInstanceYSecondValue = -60f;
                        if (interpolationValue >= 1)
                        {
                            this.EditTextBoxInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.EditTextBoxInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setSpriteInstanceHeightSecondValue = true;
                        SpriteInstanceHeightSecondValue = 75.14699f;
                        if (interpolationValue >= 1)
                        {
                            SetProperty("SpriteInstance.SourceFile", "DestrancandoNota3.png");
                        }
                        setSpriteInstanceWidthSecondValue = true;
                        SpriteInstanceWidthSecondValue = 78.31094f;
                        setSpriteInstanceXSecondValue = true;
                        SpriteInstanceXSecondValue = 83f;
                        setSpriteInstanceYSecondValue = true;
                        SpriteInstanceYSecondValue = -49f;
                        break;
                }
                if (setButtonGum1XFirstValue && setButtonGum1XSecondValue)
                {
                    ButtonGum1.X = ButtonGum1XFirstValue * (1 - interpolationValue) + ButtonGum1XSecondValue * interpolationValue;
                }
                if (setButtonGum1YFirstValue && setButtonGum1YSecondValue)
                {
                    ButtonGum1.Y = ButtonGum1YFirstValue * (1 - interpolationValue) + ButtonGum1YSecondValue * interpolationValue;
                }
                if (setButtonGum2XFirstValue && setButtonGum2XSecondValue)
                {
                    ButtonGum2.X = ButtonGum2XFirstValue * (1 - interpolationValue) + ButtonGum2XSecondValue * interpolationValue;
                }
                if (setButtonGum2YFirstValue && setButtonGum2YSecondValue)
                {
                    ButtonGum2.Y = ButtonGum2YFirstValue * (1 - interpolationValue) + ButtonGum2YSecondValue * interpolationValue;
                }
                if (setButtonGum3XFirstValue && setButtonGum3XSecondValue)
                {
                    ButtonGum3.X = ButtonGum3XFirstValue * (1 - interpolationValue) + ButtonGum3XSecondValue * interpolationValue;
                }
                if (setButtonGum3YFirstValue && setButtonGum3YSecondValue)
                {
                    ButtonGum3.Y = ButtonGum3YFirstValue * (1 - interpolationValue) + ButtonGum3YSecondValue * interpolationValue;
                }
                if (setContainerInstanceHeightFirstValue && setContainerInstanceHeightSecondValue)
                {
                    ContainerInstance.Height = ContainerInstanceHeightFirstValue * (1 - interpolationValue) + ContainerInstanceHeightSecondValue * interpolationValue;
                }
                if (setContainerInstanceWidthFirstValue && setContainerInstanceWidthSecondValue)
                {
                    ContainerInstance.Width = ContainerInstanceWidthFirstValue * (1 - interpolationValue) + ContainerInstanceWidthSecondValue * interpolationValue;
                }
                if (setContainerInstanceXFirstValue && setContainerInstanceXSecondValue)
                {
                    ContainerInstance.X = ContainerInstanceXFirstValue * (1 - interpolationValue) + ContainerInstanceXSecondValue * interpolationValue;
                }
                if (setContainerInstanceYFirstValue && setContainerInstanceYSecondValue)
                {
                    ContainerInstance.Y = ContainerInstanceYFirstValue * (1 - interpolationValue) + ContainerInstanceYSecondValue * interpolationValue;
                }
                if (setDialogBoxInstanceHeightFirstValue && setDialogBoxInstanceHeightSecondValue)
                {
                    DialogBoxInstance.Height = DialogBoxInstanceHeightFirstValue * (1 - interpolationValue) + DialogBoxInstanceHeightSecondValue * interpolationValue;
                }
                if (setDialogBoxInstanceWidthFirstValue && setDialogBoxInstanceWidthSecondValue)
                {
                    DialogBoxInstance.Width = DialogBoxInstanceWidthFirstValue * (1 - interpolationValue) + DialogBoxInstanceWidthSecondValue * interpolationValue;
                }
                if (setDialogBoxInstanceXFirstValue && setDialogBoxInstanceXSecondValue)
                {
                    DialogBoxInstance.X = DialogBoxInstanceXFirstValue * (1 - interpolationValue) + DialogBoxInstanceXSecondValue * interpolationValue;
                }
                if (setDialogBoxInstanceYFirstValue && setDialogBoxInstanceYSecondValue)
                {
                    DialogBoxInstance.Y = DialogBoxInstanceYFirstValue * (1 - interpolationValue) + DialogBoxInstanceYSecondValue * interpolationValue;
                }
                if (setEditTextBoxInstanceXFirstValue && setEditTextBoxInstanceXSecondValue)
                {
                    EditTextBoxInstance.X = EditTextBoxInstanceXFirstValue * (1 - interpolationValue) + EditTextBoxInstanceXSecondValue * interpolationValue;
                }
                if (setEditTextBoxInstanceYFirstValue && setEditTextBoxInstanceYSecondValue)
                {
                    EditTextBoxInstance.Y = EditTextBoxInstanceYFirstValue * (1 - interpolationValue) + EditTextBoxInstanceYSecondValue * interpolationValue;
                }
                if (setSpriteInstanceHeightFirstValue && setSpriteInstanceHeightSecondValue)
                {
                    SpriteInstance.Height = SpriteInstanceHeightFirstValue * (1 - interpolationValue) + SpriteInstanceHeightSecondValue * interpolationValue;
                }
                if (setSpriteInstanceWidthFirstValue && setSpriteInstanceWidthSecondValue)
                {
                    SpriteInstance.Width = SpriteInstanceWidthFirstValue * (1 - interpolationValue) + SpriteInstanceWidthSecondValue * interpolationValue;
                }
                if (setSpriteInstanceXFirstValue && setSpriteInstanceXSecondValue)
                {
                    SpriteInstance.X = SpriteInstanceXFirstValue * (1 - interpolationValue) + SpriteInstanceXSecondValue * interpolationValue;
                }
                if (setSpriteInstanceYFirstValue && setSpriteInstanceYSecondValue)
                {
                    SpriteInstance.Y = SpriteInstanceYFirstValue * (1 - interpolationValue) + SpriteInstanceYSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.MainMenuGumRuntime.VariableState fromState,DungeonRun.GumRuntimes.MainMenuGumRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
                ButtonGum1.StopAnimations();
                EditTextBoxInstance.StopAnimations();
                ButtonGum2.StopAnimations();
                ButtonGum3.StopAnimations();
                DialogBoxInstance.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.Parent",
                            Type = "string",
                            Value = ButtonGum1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.Text",
                            Type = "string",
                            Value = ButtonGum1.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.X",
                            Type = "float",
                            Value = ButtonGum1.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.X Origin",
                            Type = "HorizontalAlignment",
                            Value = ButtonGum1.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.X Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum1.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.Y",
                            Type = "float",
                            Value = ButtonGum1.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.Y Origin",
                            Type = "VerticalAlignment",
                            Value = ButtonGum1.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.Y Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum1.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.Parent",
                            Type = "string",
                            Value = EditTextBoxInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.X",
                            Type = "float",
                            Value = EditTextBoxInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = EditTextBoxInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.X Units",
                            Type = "PositionUnitType",
                            Value = EditTextBoxInstance.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.Y",
                            Type = "float",
                            Value = EditTextBoxInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.Y Origin",
                            Type = "VerticalAlignment",
                            Value = EditTextBoxInstance.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.Y Units",
                            Type = "PositionUnitType",
                            Value = EditTextBoxInstance.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.Parent",
                            Type = "string",
                            Value = ButtonGum2.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.Text",
                            Type = "string",
                            Value = ButtonGum2.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.X",
                            Type = "float",
                            Value = ButtonGum2.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.X Origin",
                            Type = "HorizontalAlignment",
                            Value = ButtonGum2.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.X Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum2.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.Y",
                            Type = "float",
                            Value = ButtonGum2.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.Y Origin",
                            Type = "VerticalAlignment",
                            Value = ButtonGum2.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.Y Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum2.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.Parent",
                            Type = "string",
                            Value = ButtonGum3.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.Text",
                            Type = "string",
                            Value = ButtonGum3.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.X",
                            Type = "float",
                            Value = ButtonGum3.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.X Origin",
                            Type = "HorizontalAlignment",
                            Value = ButtonGum3.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.X Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum3.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.Y",
                            Type = "float",
                            Value = ButtonGum3.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.Y Origin",
                            Type = "VerticalAlignment",
                            Value = ButtonGum3.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.Y Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum3.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height",
                            Type = "float",
                            Value = ContainerInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Visible",
                            Type = "bool",
                            Value = ContainerInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width",
                            Type = "float",
                            Value = ContainerInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.X",
                            Type = "float",
                            Value = ContainerInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Y",
                            Type = "float",
                            Value = ContainerInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.SourceFile",
                            Type = "string",
                            Value = SpriteInstance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Y",
                            Type = "float",
                            Value = SpriteInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Height",
                            Type = "float",
                            Value = DialogBoxInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Text",
                            Type = "string",
                            Value = DialogBoxInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Visible",
                            Type = "bool",
                            Value = DialogBoxInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Width",
                            Type = "float",
                            Value = DialogBoxInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X",
                            Type = "float",
                            Value = DialogBoxInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = DialogBoxInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X Units",
                            Type = "PositionUnitType",
                            Value = DialogBoxInstance.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y",
                            Type = "float",
                            Value = DialogBoxInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y Origin",
                            Type = "VerticalAlignment",
                            Value = DialogBoxInstance.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y Units",
                            Type = "PositionUnitType",
                            Value = DialogBoxInstance.YUnits
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.Parent",
                            Type = "string",
                            Value = ButtonGum1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.Text",
                            Type = "string",
                            Value = ButtonGum1.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.X",
                            Type = "float",
                            Value = ButtonGum1.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.X Origin",
                            Type = "HorizontalAlignment",
                            Value = ButtonGum1.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.X Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum1.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.Y",
                            Type = "float",
                            Value = ButtonGum1.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.Y Origin",
                            Type = "VerticalAlignment",
                            Value = ButtonGum1.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum1.Y Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum1.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.Parent",
                            Type = "string",
                            Value = EditTextBoxInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.X",
                            Type = "float",
                            Value = EditTextBoxInstance.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = EditTextBoxInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.X Units",
                            Type = "PositionUnitType",
                            Value = EditTextBoxInstance.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.Y",
                            Type = "float",
                            Value = EditTextBoxInstance.Y + -60f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.Y Origin",
                            Type = "VerticalAlignment",
                            Value = EditTextBoxInstance.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "EditTextBoxInstance.Y Units",
                            Type = "PositionUnitType",
                            Value = EditTextBoxInstance.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.Parent",
                            Type = "string",
                            Value = ButtonGum2.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.Text",
                            Type = "string",
                            Value = ButtonGum2.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.X",
                            Type = "float",
                            Value = ButtonGum2.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.X Origin",
                            Type = "HorizontalAlignment",
                            Value = ButtonGum2.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.X Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum2.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.Y",
                            Type = "float",
                            Value = ButtonGum2.Y + 70f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.Y Origin",
                            Type = "VerticalAlignment",
                            Value = ButtonGum2.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum2.Y Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum2.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.Parent",
                            Type = "string",
                            Value = ButtonGum3.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.Text",
                            Type = "string",
                            Value = ButtonGum3.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.X",
                            Type = "float",
                            Value = ButtonGum3.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.X Origin",
                            Type = "HorizontalAlignment",
                            Value = ButtonGum3.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.X Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum3.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.Y",
                            Type = "float",
                            Value = ButtonGum3.Y + 140f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.Y Origin",
                            Type = "VerticalAlignment",
                            Value = ButtonGum3.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ButtonGum3.Y Units",
                            Type = "PositionUnitType",
                            Value = ButtonGum3.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height",
                            Type = "float",
                            Value = ContainerInstance.Height + 320f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Visible",
                            Type = "bool",
                            Value = ContainerInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width",
                            Type = "float",
                            Value = ContainerInstance.Width + 261f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.X",
                            Type = "float",
                            Value = ContainerInstance.X + 283f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Y",
                            Type = "float",
                            Value = ContainerInstance.Y + 230f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height + 75.14699f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.SourceFile",
                            Type = "string",
                            Value = SpriteInstance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width + 78.31094f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X + 83f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Y",
                            Type = "float",
                            Value = SpriteInstance.Y + -49f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Height",
                            Type = "float",
                            Value = DialogBoxInstance.Height + 109f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Text",
                            Type = "string",
                            Value = DialogBoxInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Visible",
                            Type = "bool",
                            Value = DialogBoxInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Width",
                            Type = "float",
                            Value = DialogBoxInstance.Width + 423f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X",
                            Type = "float",
                            Value = DialogBoxInstance.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = DialogBoxInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.X Units",
                            Type = "PositionUnitType",
                            Value = DialogBoxInstance.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y",
                            Type = "float",
                            Value = DialogBoxInstance.Y + -145f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y Origin",
                            Type = "VerticalAlignment",
                            Value = DialogBoxInstance.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "DialogBoxInstance.Y Units",
                            Type = "PositionUnitType",
                            Value = DialogBoxInstance.YUnits
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.ButtonRuntime ButtonGum1 { get; set; }
            private DungeonRun.GumRuntimes.EditTextBoxRuntime EditTextBoxInstance { get; set; }
            private DungeonRun.GumRuntimes.ButtonRuntime ButtonGum2 { get; set; }
            private DungeonRun.GumRuntimes.ButtonRuntime ButtonGum3 { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime ContainerInstance { get; set; }
            private DungeonRun.GumRuntimes.SpriteRuntime SpriteInstance { get; set; }
            private DungeonRun.GumRuntimes.DialogBoxRuntime DialogBoxInstance { get; set; }
            public MainMenuGumRuntime (bool fullInstantiation = true) 
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Screens.First(item => item.Name == "MainMenuGum");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                ButtonGum1 = this.GetGraphicalUiElementByName("ButtonGum1") as DungeonRun.GumRuntimes.ButtonRuntime;
                EditTextBoxInstance = this.GetGraphicalUiElementByName("EditTextBoxInstance") as DungeonRun.GumRuntimes.EditTextBoxRuntime;
                ButtonGum2 = this.GetGraphicalUiElementByName("ButtonGum2") as DungeonRun.GumRuntimes.ButtonRuntime;
                ButtonGum3 = this.GetGraphicalUiElementByName("ButtonGum3") as DungeonRun.GumRuntimes.ButtonRuntime;
                ContainerInstance = this.GetGraphicalUiElementByName("ContainerInstance") as DungeonRun.GumRuntimes.ContainerRuntime;
                SpriteInstance = this.GetGraphicalUiElementByName("SpriteInstance") as DungeonRun.GumRuntimes.SpriteRuntime;
                DialogBoxInstance = this.GetGraphicalUiElementByName("DialogBoxInstance") as DungeonRun.GumRuntimes.DialogBoxRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
