    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class Panel_DisplayRuntime : DungeonRun.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            Height = 578f;
                            Width = 540f;
                            SpriteInstance.Height = 0f;
                            SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown.png");
                            SpriteInstance.Width = 0f;
                            SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            SpriteInstance.X = -2f;
                            SpriteInstance.Y = -1f;
                            ColoredRectangleInstance.Blue = 65;
                            ColoredRectangleInstance.Green = 86;
                            ColoredRectangleInstance.Height = 10f;
                            ColoredRectangleInstance.Red = 120;
                            ColoredRectangleInstance.Width = 362f;
                            ColoredRectangleInstance.X = 0f;
                            ColoredRectangleInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            ColoredRectangleInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            ColoredRectangleInstance.Y = 64f;
                            ColoredRectangleInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                            ColoredRectangleInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                            HeaderInstance.Blue = 245;
                            HeaderInstance.Font = "Impact";
                            HeaderInstance.FontSize = 30;
                            HeaderInstance.Green = 240;
                            HeaderInstance.Height = 17f;
                            HeaderInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            HeaderInstance.OutlineThickness = 2;
                            HeaderInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ColoredRectangleInstance");
                            HeaderInstance.Red = 255;
                            HeaderInstance.Text = "RANKING";
                            HeaderInstance.Width = -275f;
                            HeaderInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            HeaderInstance.X = 128f;
                            HeaderInstance.Y = -32f;
                            titleBar.Height = 29f;
                            titleBar.Width = 542f;
                            titleBar.X = 0f;
                            titleBar.Y = 74f;
                            Title1.Blue = 255;
                            Title1.Font = "Impact";
                            Title1.FontSize = 20;
                            Title1.Green = 255;
                            Title1.Height = -5f;
                            Title1.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Title1.OutlineThickness = 1;
                            Title1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                            Title1.Red = 255;
                            Title1.Text = "Nome";
                            Title1.Width = -481f;
                            Title1.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Title1.X = 75f;
                            Title1.Y = 1f;
                            Title2.Font = "Impact";
                            Title2.FontSize = 20;
                            Title2.Height = -3f;
                            Title2.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Title2.OutlineThickness = 1;
                            Title2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                            Title2.Text = "Pontos";
                            Title2.Width = -491f;
                            Title2.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Title2.X = 252f;
                            Title2.Y = 0f;
                            Title3.Font = "Impact";
                            Title3.FontSize = 20;
                            Title3.Height = -1f;
                            Title3.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Title3.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            Title3.OutlineThickness = 1;
                            Title3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                            Title3.Text = "Tempo Total";
                            Title3.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                            Title3.Width = -405f;
                            Title3.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Title3.X = 377f;
                            Title3.Y = 0f;
                            Ranking.Height = 474f;
                            Ranking.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                            Ranking.Width = 538f;
                            Ranking.X = 2f;
                            Ranking.Y = 27f;
                            Col1.Height = 461f;
                            Col1.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                            Col1.OutlineThickness = 1;
                            Col1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Ranking");
                            Col1.Text = "name\nname\nname\nname\nname\nname";
                            Col1.Width = 178f;
                            Col1.X = 17f;
                            Col1.Y = 0f;
                            Col2.Height = 466f;
                            Col2.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            Col2.OutlineThickness = 1;
                            Col2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Ranking");
                            Col2.Text = "Points";
                            Col2.Width = 178f;
                            Col2.X = 184f;
                            Col2.Y = 1f;
                            Col3.Height = 462f;
                            Col3.OutlineThickness = 1;
                            Col3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Ranking");
                            Col3.Text = "TIMETIMETIME\nTIM\n";
                            Col3.Width = 119f;
                            Col3.X = 368f;
                            Col3.Y = 0f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setCol1HeightFirstValue = false;
                bool setCol1HeightSecondValue = false;
                float Col1HeightFirstValue= 0;
                float Col1HeightSecondValue= 0;
                bool setCol1OutlineThicknessFirstValue = false;
                bool setCol1OutlineThicknessSecondValue = false;
                int Col1OutlineThicknessFirstValue= 0;
                int Col1OutlineThicknessSecondValue= 0;
                bool setCol1WidthFirstValue = false;
                bool setCol1WidthSecondValue = false;
                float Col1WidthFirstValue= 0;
                float Col1WidthSecondValue= 0;
                bool setCol1XFirstValue = false;
                bool setCol1XSecondValue = false;
                float Col1XFirstValue= 0;
                float Col1XSecondValue= 0;
                bool setCol1YFirstValue = false;
                bool setCol1YSecondValue = false;
                float Col1YFirstValue= 0;
                float Col1YSecondValue= 0;
                bool setCol2HeightFirstValue = false;
                bool setCol2HeightSecondValue = false;
                float Col2HeightFirstValue= 0;
                float Col2HeightSecondValue= 0;
                bool setCol2OutlineThicknessFirstValue = false;
                bool setCol2OutlineThicknessSecondValue = false;
                int Col2OutlineThicknessFirstValue= 0;
                int Col2OutlineThicknessSecondValue= 0;
                bool setCol2WidthFirstValue = false;
                bool setCol2WidthSecondValue = false;
                float Col2WidthFirstValue= 0;
                float Col2WidthSecondValue= 0;
                bool setCol2XFirstValue = false;
                bool setCol2XSecondValue = false;
                float Col2XFirstValue= 0;
                float Col2XSecondValue= 0;
                bool setCol2YFirstValue = false;
                bool setCol2YSecondValue = false;
                float Col2YFirstValue= 0;
                float Col2YSecondValue= 0;
                bool setCol3HeightFirstValue = false;
                bool setCol3HeightSecondValue = false;
                float Col3HeightFirstValue= 0;
                float Col3HeightSecondValue= 0;
                bool setCol3OutlineThicknessFirstValue = false;
                bool setCol3OutlineThicknessSecondValue = false;
                int Col3OutlineThicknessFirstValue= 0;
                int Col3OutlineThicknessSecondValue= 0;
                bool setCol3WidthFirstValue = false;
                bool setCol3WidthSecondValue = false;
                float Col3WidthFirstValue= 0;
                float Col3WidthSecondValue= 0;
                bool setCol3XFirstValue = false;
                bool setCol3XSecondValue = false;
                float Col3XFirstValue= 0;
                float Col3XSecondValue= 0;
                bool setCol3YFirstValue = false;
                bool setCol3YSecondValue = false;
                float Col3YFirstValue= 0;
                float Col3YSecondValue= 0;
                bool setColoredRectangleInstanceBlueFirstValue = false;
                bool setColoredRectangleInstanceBlueSecondValue = false;
                int ColoredRectangleInstanceBlueFirstValue= 0;
                int ColoredRectangleInstanceBlueSecondValue= 0;
                bool setColoredRectangleInstanceGreenFirstValue = false;
                bool setColoredRectangleInstanceGreenSecondValue = false;
                int ColoredRectangleInstanceGreenFirstValue= 0;
                int ColoredRectangleInstanceGreenSecondValue= 0;
                bool setColoredRectangleInstanceHeightFirstValue = false;
                bool setColoredRectangleInstanceHeightSecondValue = false;
                float ColoredRectangleInstanceHeightFirstValue= 0;
                float ColoredRectangleInstanceHeightSecondValue= 0;
                bool setColoredRectangleInstanceRedFirstValue = false;
                bool setColoredRectangleInstanceRedSecondValue = false;
                int ColoredRectangleInstanceRedFirstValue= 0;
                int ColoredRectangleInstanceRedSecondValue= 0;
                bool setColoredRectangleInstanceWidthFirstValue = false;
                bool setColoredRectangleInstanceWidthSecondValue = false;
                float ColoredRectangleInstanceWidthFirstValue= 0;
                float ColoredRectangleInstanceWidthSecondValue= 0;
                bool setColoredRectangleInstanceXFirstValue = false;
                bool setColoredRectangleInstanceXSecondValue = false;
                float ColoredRectangleInstanceXFirstValue= 0;
                float ColoredRectangleInstanceXSecondValue= 0;
                bool setColoredRectangleInstanceYFirstValue = false;
                bool setColoredRectangleInstanceYSecondValue = false;
                float ColoredRectangleInstanceYFirstValue= 0;
                float ColoredRectangleInstanceYSecondValue= 0;
                bool setHeaderInstanceBlueFirstValue = false;
                bool setHeaderInstanceBlueSecondValue = false;
                int HeaderInstanceBlueFirstValue= 0;
                int HeaderInstanceBlueSecondValue= 0;
                bool setHeaderInstanceFontSizeFirstValue = false;
                bool setHeaderInstanceFontSizeSecondValue = false;
                int HeaderInstanceFontSizeFirstValue= 0;
                int HeaderInstanceFontSizeSecondValue= 0;
                bool setHeaderInstanceGreenFirstValue = false;
                bool setHeaderInstanceGreenSecondValue = false;
                int HeaderInstanceGreenFirstValue= 0;
                int HeaderInstanceGreenSecondValue= 0;
                bool setHeaderInstanceHeightFirstValue = false;
                bool setHeaderInstanceHeightSecondValue = false;
                float HeaderInstanceHeightFirstValue= 0;
                float HeaderInstanceHeightSecondValue= 0;
                bool setHeaderInstanceOutlineThicknessFirstValue = false;
                bool setHeaderInstanceOutlineThicknessSecondValue = false;
                int HeaderInstanceOutlineThicknessFirstValue= 0;
                int HeaderInstanceOutlineThicknessSecondValue= 0;
                bool setHeaderInstanceRedFirstValue = false;
                bool setHeaderInstanceRedSecondValue = false;
                int HeaderInstanceRedFirstValue= 0;
                int HeaderInstanceRedSecondValue= 0;
                bool setHeaderInstanceWidthFirstValue = false;
                bool setHeaderInstanceWidthSecondValue = false;
                float HeaderInstanceWidthFirstValue= 0;
                float HeaderInstanceWidthSecondValue= 0;
                bool setHeaderInstanceXFirstValue = false;
                bool setHeaderInstanceXSecondValue = false;
                float HeaderInstanceXFirstValue= 0;
                float HeaderInstanceXSecondValue= 0;
                bool setHeaderInstanceYFirstValue = false;
                bool setHeaderInstanceYSecondValue = false;
                float HeaderInstanceYFirstValue= 0;
                float HeaderInstanceYSecondValue= 0;
                bool setHeightFirstValue = false;
                bool setHeightSecondValue = false;
                float HeightFirstValue= 0;
                float HeightSecondValue= 0;
                bool setRankingHeightFirstValue = false;
                bool setRankingHeightSecondValue = false;
                float RankingHeightFirstValue= 0;
                float RankingHeightSecondValue= 0;
                bool setRankingWidthFirstValue = false;
                bool setRankingWidthSecondValue = false;
                float RankingWidthFirstValue= 0;
                float RankingWidthSecondValue= 0;
                bool setRankingXFirstValue = false;
                bool setRankingXSecondValue = false;
                float RankingXFirstValue= 0;
                float RankingXSecondValue= 0;
                bool setRankingYFirstValue = false;
                bool setRankingYSecondValue = false;
                float RankingYFirstValue= 0;
                float RankingYSecondValue= 0;
                bool setSpriteInstanceHeightFirstValue = false;
                bool setSpriteInstanceHeightSecondValue = false;
                float SpriteInstanceHeightFirstValue= 0;
                float SpriteInstanceHeightSecondValue= 0;
                bool setSpriteInstanceWidthFirstValue = false;
                bool setSpriteInstanceWidthSecondValue = false;
                float SpriteInstanceWidthFirstValue= 0;
                float SpriteInstanceWidthSecondValue= 0;
                bool setSpriteInstanceXFirstValue = false;
                bool setSpriteInstanceXSecondValue = false;
                float SpriteInstanceXFirstValue= 0;
                float SpriteInstanceXSecondValue= 0;
                bool setSpriteInstanceYFirstValue = false;
                bool setSpriteInstanceYSecondValue = false;
                float SpriteInstanceYFirstValue= 0;
                float SpriteInstanceYSecondValue= 0;
                bool setTitle1BlueFirstValue = false;
                bool setTitle1BlueSecondValue = false;
                int Title1BlueFirstValue= 0;
                int Title1BlueSecondValue= 0;
                bool setTitle1FontSizeFirstValue = false;
                bool setTitle1FontSizeSecondValue = false;
                int Title1FontSizeFirstValue= 0;
                int Title1FontSizeSecondValue= 0;
                bool setTitle1GreenFirstValue = false;
                bool setTitle1GreenSecondValue = false;
                int Title1GreenFirstValue= 0;
                int Title1GreenSecondValue= 0;
                bool setTitle1HeightFirstValue = false;
                bool setTitle1HeightSecondValue = false;
                float Title1HeightFirstValue= 0;
                float Title1HeightSecondValue= 0;
                bool setTitle1OutlineThicknessFirstValue = false;
                bool setTitle1OutlineThicknessSecondValue = false;
                int Title1OutlineThicknessFirstValue= 0;
                int Title1OutlineThicknessSecondValue= 0;
                bool setTitle1RedFirstValue = false;
                bool setTitle1RedSecondValue = false;
                int Title1RedFirstValue= 0;
                int Title1RedSecondValue= 0;
                bool setTitle1WidthFirstValue = false;
                bool setTitle1WidthSecondValue = false;
                float Title1WidthFirstValue= 0;
                float Title1WidthSecondValue= 0;
                bool setTitle1XFirstValue = false;
                bool setTitle1XSecondValue = false;
                float Title1XFirstValue= 0;
                float Title1XSecondValue= 0;
                bool setTitle1YFirstValue = false;
                bool setTitle1YSecondValue = false;
                float Title1YFirstValue= 0;
                float Title1YSecondValue= 0;
                bool setTitle2FontSizeFirstValue = false;
                bool setTitle2FontSizeSecondValue = false;
                int Title2FontSizeFirstValue= 0;
                int Title2FontSizeSecondValue= 0;
                bool setTitle2HeightFirstValue = false;
                bool setTitle2HeightSecondValue = false;
                float Title2HeightFirstValue= 0;
                float Title2HeightSecondValue= 0;
                bool setTitle2OutlineThicknessFirstValue = false;
                bool setTitle2OutlineThicknessSecondValue = false;
                int Title2OutlineThicknessFirstValue= 0;
                int Title2OutlineThicknessSecondValue= 0;
                bool setTitle2WidthFirstValue = false;
                bool setTitle2WidthSecondValue = false;
                float Title2WidthFirstValue= 0;
                float Title2WidthSecondValue= 0;
                bool setTitle2XFirstValue = false;
                bool setTitle2XSecondValue = false;
                float Title2XFirstValue= 0;
                float Title2XSecondValue= 0;
                bool setTitle2YFirstValue = false;
                bool setTitle2YSecondValue = false;
                float Title2YFirstValue= 0;
                float Title2YSecondValue= 0;
                bool setTitle3FontSizeFirstValue = false;
                bool setTitle3FontSizeSecondValue = false;
                int Title3FontSizeFirstValue= 0;
                int Title3FontSizeSecondValue= 0;
                bool setTitle3HeightFirstValue = false;
                bool setTitle3HeightSecondValue = false;
                float Title3HeightFirstValue= 0;
                float Title3HeightSecondValue= 0;
                bool setTitle3OutlineThicknessFirstValue = false;
                bool setTitle3OutlineThicknessSecondValue = false;
                int Title3OutlineThicknessFirstValue= 0;
                int Title3OutlineThicknessSecondValue= 0;
                bool setTitle3WidthFirstValue = false;
                bool setTitle3WidthSecondValue = false;
                float Title3WidthFirstValue= 0;
                float Title3WidthSecondValue= 0;
                bool setTitle3XFirstValue = false;
                bool setTitle3XSecondValue = false;
                float Title3XFirstValue= 0;
                float Title3XSecondValue= 0;
                bool setTitle3YFirstValue = false;
                bool setTitle3YSecondValue = false;
                float Title3YFirstValue= 0;
                float Title3YSecondValue= 0;
                bool settitleBarHeightFirstValue = false;
                bool settitleBarHeightSecondValue = false;
                float titleBarHeightFirstValue= 0;
                float titleBarHeightSecondValue= 0;
                bool settitleBarWidthFirstValue = false;
                bool settitleBarWidthSecondValue = false;
                float titleBarWidthFirstValue= 0;
                float titleBarWidthSecondValue= 0;
                bool settitleBarXFirstValue = false;
                bool settitleBarXSecondValue = false;
                float titleBarXFirstValue= 0;
                float titleBarXSecondValue= 0;
                bool settitleBarYFirstValue = false;
                bool settitleBarYSecondValue = false;
                float titleBarYFirstValue= 0;
                float titleBarYSecondValue= 0;
                bool setWidthFirstValue = false;
                bool setWidthSecondValue = false;
                float WidthFirstValue= 0;
                float WidthSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setCol1HeightFirstValue = true;
                        Col1HeightFirstValue = 461f;
                        if (interpolationValue < 1)
                        {
                            this.Col1.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        setCol1OutlineThicknessFirstValue = true;
                        Col1OutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.Col1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Ranking");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Col1.Text = "name\nname\nname\nname\nname\nname";
                        }
                        setCol1WidthFirstValue = true;
                        Col1WidthFirstValue = 178f;
                        setCol1XFirstValue = true;
                        Col1XFirstValue = 17f;
                        setCol1YFirstValue = true;
                        Col1YFirstValue = 0f;
                        setCol2HeightFirstValue = true;
                        Col2HeightFirstValue = 466f;
                        if (interpolationValue < 1)
                        {
                            this.Col2.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setCol2OutlineThicknessFirstValue = true;
                        Col2OutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.Col2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Ranking");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Col2.Text = "Points";
                        }
                        setCol2WidthFirstValue = true;
                        Col2WidthFirstValue = 178f;
                        setCol2XFirstValue = true;
                        Col2XFirstValue = 184f;
                        setCol2YFirstValue = true;
                        Col2YFirstValue = 1f;
                        setCol3HeightFirstValue = true;
                        Col3HeightFirstValue = 462f;
                        setCol3OutlineThicknessFirstValue = true;
                        Col3OutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.Col3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Ranking");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Col3.Text = "TIMETIMETIME\nTIM\n";
                        }
                        setCol3WidthFirstValue = true;
                        Col3WidthFirstValue = 119f;
                        setCol3XFirstValue = true;
                        Col3XFirstValue = 368f;
                        setCol3YFirstValue = true;
                        Col3YFirstValue = 0f;
                        setColoredRectangleInstanceBlueFirstValue = true;
                        ColoredRectangleInstanceBlueFirstValue = 65;
                        setColoredRectangleInstanceGreenFirstValue = true;
                        ColoredRectangleInstanceGreenFirstValue = 86;
                        setColoredRectangleInstanceHeightFirstValue = true;
                        ColoredRectangleInstanceHeightFirstValue = 10f;
                        setColoredRectangleInstanceRedFirstValue = true;
                        ColoredRectangleInstanceRedFirstValue = 120;
                        setColoredRectangleInstanceWidthFirstValue = true;
                        ColoredRectangleInstanceWidthFirstValue = 362f;
                        setColoredRectangleInstanceXFirstValue = true;
                        ColoredRectangleInstanceXFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.ColoredRectangleInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ColoredRectangleInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setColoredRectangleInstanceYFirstValue = true;
                        ColoredRectangleInstanceYFirstValue = 64f;
                        if (interpolationValue < 1)
                        {
                            this.ColoredRectangleInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ColoredRectangleInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        setHeaderInstanceBlueFirstValue = true;
                        HeaderInstanceBlueFirstValue = 245;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.Font = "Impact";
                        }
                        setHeaderInstanceFontSizeFirstValue = true;
                        HeaderInstanceFontSizeFirstValue = 30;
                        setHeaderInstanceGreenFirstValue = true;
                        HeaderInstanceGreenFirstValue = 240;
                        setHeaderInstanceHeightFirstValue = true;
                        HeaderInstanceHeightFirstValue = 17f;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHeaderInstanceOutlineThicknessFirstValue = true;
                        HeaderInstanceOutlineThicknessFirstValue = 2;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ColoredRectangleInstance");
                        }
                        setHeaderInstanceRedFirstValue = true;
                        HeaderInstanceRedFirstValue = 255;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.Text = "RANKING";
                        }
                        setHeaderInstanceWidthFirstValue = true;
                        HeaderInstanceWidthFirstValue = -275f;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHeaderInstanceXFirstValue = true;
                        HeaderInstanceXFirstValue = 128f;
                        setHeaderInstanceYFirstValue = true;
                        HeaderInstanceYFirstValue = -32f;
                        setHeightFirstValue = true;
                        HeightFirstValue = 578f;
                        setRankingHeightFirstValue = true;
                        RankingHeightFirstValue = 474f;
                        if (interpolationValue < 1)
                        {
                            this.Ranking.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                        }
                        setRankingWidthFirstValue = true;
                        RankingWidthFirstValue = 538f;
                        setRankingXFirstValue = true;
                        RankingXFirstValue = 2f;
                        setRankingYFirstValue = true;
                        RankingYFirstValue = 27f;
                        setSpriteInstanceHeightFirstValue = true;
                        SpriteInstanceHeightFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown.png");
                        }
                        setSpriteInstanceWidthFirstValue = true;
                        SpriteInstanceWidthFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setSpriteInstanceXFirstValue = true;
                        SpriteInstanceXFirstValue = -2f;
                        setSpriteInstanceYFirstValue = true;
                        SpriteInstanceYFirstValue = -1f;
                        setTitle1BlueFirstValue = true;
                        Title1BlueFirstValue = 255;
                        if (interpolationValue < 1)
                        {
                            this.Title1.Font = "Impact";
                        }
                        setTitle1FontSizeFirstValue = true;
                        Title1FontSizeFirstValue = 20;
                        setTitle1GreenFirstValue = true;
                        Title1GreenFirstValue = 255;
                        setTitle1HeightFirstValue = true;
                        Title1HeightFirstValue = -5f;
                        if (interpolationValue < 1)
                        {
                            this.Title1.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTitle1OutlineThicknessFirstValue = true;
                        Title1OutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.Title1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                        }
                        setTitle1RedFirstValue = true;
                        Title1RedFirstValue = 255;
                        if (interpolationValue < 1)
                        {
                            this.Title1.Text = "Nome";
                        }
                        setTitle1WidthFirstValue = true;
                        Title1WidthFirstValue = -481f;
                        if (interpolationValue < 1)
                        {
                            this.Title1.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTitle1XFirstValue = true;
                        Title1XFirstValue = 75f;
                        setTitle1YFirstValue = true;
                        Title1YFirstValue = 1f;
                        if (interpolationValue < 1)
                        {
                            this.Title2.Font = "Impact";
                        }
                        setTitle2FontSizeFirstValue = true;
                        Title2FontSizeFirstValue = 20;
                        setTitle2HeightFirstValue = true;
                        Title2HeightFirstValue = -3f;
                        if (interpolationValue < 1)
                        {
                            this.Title2.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTitle2OutlineThicknessFirstValue = true;
                        Title2OutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.Title2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Title2.Text = "Pontos";
                        }
                        setTitle2WidthFirstValue = true;
                        Title2WidthFirstValue = -491f;
                        if (interpolationValue < 1)
                        {
                            this.Title2.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTitle2XFirstValue = true;
                        Title2XFirstValue = 252f;
                        setTitle2YFirstValue = true;
                        Title2YFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.Title3.Font = "Impact";
                        }
                        setTitle3FontSizeFirstValue = true;
                        Title3FontSizeFirstValue = 20;
                        setTitle3HeightFirstValue = true;
                        Title3HeightFirstValue = -1f;
                        if (interpolationValue < 1)
                        {
                            this.Title3.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.Title3.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setTitle3OutlineThicknessFirstValue = true;
                        Title3OutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.Title3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Title3.Text = "Tempo Total";
                        }
                        if (interpolationValue < 1)
                        {
                            this.Title3.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                        }
                        setTitle3WidthFirstValue = true;
                        Title3WidthFirstValue = -405f;
                        if (interpolationValue < 1)
                        {
                            this.Title3.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTitle3XFirstValue = true;
                        Title3XFirstValue = 377f;
                        setTitle3YFirstValue = true;
                        Title3YFirstValue = 0f;
                        settitleBarHeightFirstValue = true;
                        titleBarHeightFirstValue = 29f;
                        settitleBarWidthFirstValue = true;
                        titleBarWidthFirstValue = 542f;
                        settitleBarXFirstValue = true;
                        titleBarXFirstValue = 0f;
                        settitleBarYFirstValue = true;
                        titleBarYFirstValue = 74f;
                        setWidthFirstValue = true;
                        WidthFirstValue = 540f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setCol1HeightSecondValue = true;
                        Col1HeightSecondValue = 461f;
                        if (interpolationValue >= 1)
                        {
                            this.Col1.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        setCol1OutlineThicknessSecondValue = true;
                        Col1OutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.Col1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Ranking");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Col1.Text = "name\nname\nname\nname\nname\nname";
                        }
                        setCol1WidthSecondValue = true;
                        Col1WidthSecondValue = 178f;
                        setCol1XSecondValue = true;
                        Col1XSecondValue = 17f;
                        setCol1YSecondValue = true;
                        Col1YSecondValue = 0f;
                        setCol2HeightSecondValue = true;
                        Col2HeightSecondValue = 466f;
                        if (interpolationValue >= 1)
                        {
                            this.Col2.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setCol2OutlineThicknessSecondValue = true;
                        Col2OutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.Col2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Ranking");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Col2.Text = "Points";
                        }
                        setCol2WidthSecondValue = true;
                        Col2WidthSecondValue = 178f;
                        setCol2XSecondValue = true;
                        Col2XSecondValue = 184f;
                        setCol2YSecondValue = true;
                        Col2YSecondValue = 1f;
                        setCol3HeightSecondValue = true;
                        Col3HeightSecondValue = 462f;
                        setCol3OutlineThicknessSecondValue = true;
                        Col3OutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.Col3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Ranking");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Col3.Text = "TIMETIMETIME\nTIM\n";
                        }
                        setCol3WidthSecondValue = true;
                        Col3WidthSecondValue = 119f;
                        setCol3XSecondValue = true;
                        Col3XSecondValue = 368f;
                        setCol3YSecondValue = true;
                        Col3YSecondValue = 0f;
                        setColoredRectangleInstanceBlueSecondValue = true;
                        ColoredRectangleInstanceBlueSecondValue = 65;
                        setColoredRectangleInstanceGreenSecondValue = true;
                        ColoredRectangleInstanceGreenSecondValue = 86;
                        setColoredRectangleInstanceHeightSecondValue = true;
                        ColoredRectangleInstanceHeightSecondValue = 10f;
                        setColoredRectangleInstanceRedSecondValue = true;
                        ColoredRectangleInstanceRedSecondValue = 120;
                        setColoredRectangleInstanceWidthSecondValue = true;
                        ColoredRectangleInstanceWidthSecondValue = 362f;
                        setColoredRectangleInstanceXSecondValue = true;
                        ColoredRectangleInstanceXSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.ColoredRectangleInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ColoredRectangleInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setColoredRectangleInstanceYSecondValue = true;
                        ColoredRectangleInstanceYSecondValue = 64f;
                        if (interpolationValue >= 1)
                        {
                            this.ColoredRectangleInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ColoredRectangleInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        setHeaderInstanceBlueSecondValue = true;
                        HeaderInstanceBlueSecondValue = 245;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.Font = "Impact";
                        }
                        setHeaderInstanceFontSizeSecondValue = true;
                        HeaderInstanceFontSizeSecondValue = 30;
                        setHeaderInstanceGreenSecondValue = true;
                        HeaderInstanceGreenSecondValue = 240;
                        setHeaderInstanceHeightSecondValue = true;
                        HeaderInstanceHeightSecondValue = 17f;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHeaderInstanceOutlineThicknessSecondValue = true;
                        HeaderInstanceOutlineThicknessSecondValue = 2;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ColoredRectangleInstance");
                        }
                        setHeaderInstanceRedSecondValue = true;
                        HeaderInstanceRedSecondValue = 255;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.Text = "RANKING";
                        }
                        setHeaderInstanceWidthSecondValue = true;
                        HeaderInstanceWidthSecondValue = -275f;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHeaderInstanceXSecondValue = true;
                        HeaderInstanceXSecondValue = 128f;
                        setHeaderInstanceYSecondValue = true;
                        HeaderInstanceYSecondValue = -32f;
                        setHeightSecondValue = true;
                        HeightSecondValue = 578f;
                        setRankingHeightSecondValue = true;
                        RankingHeightSecondValue = 474f;
                        if (interpolationValue >= 1)
                        {
                            this.Ranking.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                        }
                        setRankingWidthSecondValue = true;
                        RankingWidthSecondValue = 538f;
                        setRankingXSecondValue = true;
                        RankingXSecondValue = 2f;
                        setRankingYSecondValue = true;
                        RankingYSecondValue = 27f;
                        setSpriteInstanceHeightSecondValue = true;
                        SpriteInstanceHeightSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown.png");
                        }
                        setSpriteInstanceWidthSecondValue = true;
                        SpriteInstanceWidthSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setSpriteInstanceXSecondValue = true;
                        SpriteInstanceXSecondValue = -2f;
                        setSpriteInstanceYSecondValue = true;
                        SpriteInstanceYSecondValue = -1f;
                        setTitle1BlueSecondValue = true;
                        Title1BlueSecondValue = 255;
                        if (interpolationValue >= 1)
                        {
                            this.Title1.Font = "Impact";
                        }
                        setTitle1FontSizeSecondValue = true;
                        Title1FontSizeSecondValue = 20;
                        setTitle1GreenSecondValue = true;
                        Title1GreenSecondValue = 255;
                        setTitle1HeightSecondValue = true;
                        Title1HeightSecondValue = -5f;
                        if (interpolationValue >= 1)
                        {
                            this.Title1.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTitle1OutlineThicknessSecondValue = true;
                        Title1OutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.Title1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                        }
                        setTitle1RedSecondValue = true;
                        Title1RedSecondValue = 255;
                        if (interpolationValue >= 1)
                        {
                            this.Title1.Text = "Nome";
                        }
                        setTitle1WidthSecondValue = true;
                        Title1WidthSecondValue = -481f;
                        if (interpolationValue >= 1)
                        {
                            this.Title1.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTitle1XSecondValue = true;
                        Title1XSecondValue = 75f;
                        setTitle1YSecondValue = true;
                        Title1YSecondValue = 1f;
                        if (interpolationValue >= 1)
                        {
                            this.Title2.Font = "Impact";
                        }
                        setTitle2FontSizeSecondValue = true;
                        Title2FontSizeSecondValue = 20;
                        setTitle2HeightSecondValue = true;
                        Title2HeightSecondValue = -3f;
                        if (interpolationValue >= 1)
                        {
                            this.Title2.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTitle2OutlineThicknessSecondValue = true;
                        Title2OutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.Title2.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Title2.Text = "Pontos";
                        }
                        setTitle2WidthSecondValue = true;
                        Title2WidthSecondValue = -491f;
                        if (interpolationValue >= 1)
                        {
                            this.Title2.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTitle2XSecondValue = true;
                        Title2XSecondValue = 252f;
                        setTitle2YSecondValue = true;
                        Title2YSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.Title3.Font = "Impact";
                        }
                        setTitle3FontSizeSecondValue = true;
                        Title3FontSizeSecondValue = 20;
                        setTitle3HeightSecondValue = true;
                        Title3HeightSecondValue = -1f;
                        if (interpolationValue >= 1)
                        {
                            this.Title3.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Title3.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setTitle3OutlineThicknessSecondValue = true;
                        Title3OutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.Title3.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "titleBar");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Title3.Text = "Tempo Total";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Title3.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                        }
                        setTitle3WidthSecondValue = true;
                        Title3WidthSecondValue = -405f;
                        if (interpolationValue >= 1)
                        {
                            this.Title3.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTitle3XSecondValue = true;
                        Title3XSecondValue = 377f;
                        setTitle3YSecondValue = true;
                        Title3YSecondValue = 0f;
                        settitleBarHeightSecondValue = true;
                        titleBarHeightSecondValue = 29f;
                        settitleBarWidthSecondValue = true;
                        titleBarWidthSecondValue = 542f;
                        settitleBarXSecondValue = true;
                        titleBarXSecondValue = 0f;
                        settitleBarYSecondValue = true;
                        titleBarYSecondValue = 74f;
                        setWidthSecondValue = true;
                        WidthSecondValue = 540f;
                        break;
                }
                if (setCol1HeightFirstValue && setCol1HeightSecondValue)
                {
                    Col1.Height = Col1HeightFirstValue * (1 - interpolationValue) + Col1HeightSecondValue * interpolationValue;
                }
                if (setCol1OutlineThicknessFirstValue && setCol1OutlineThicknessSecondValue)
                {
                    Col1.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(Col1OutlineThicknessFirstValue* (1 - interpolationValue) + Col1OutlineThicknessSecondValue * interpolationValue);
                }
                if (setCol1WidthFirstValue && setCol1WidthSecondValue)
                {
                    Col1.Width = Col1WidthFirstValue * (1 - interpolationValue) + Col1WidthSecondValue * interpolationValue;
                }
                if (setCol1XFirstValue && setCol1XSecondValue)
                {
                    Col1.X = Col1XFirstValue * (1 - interpolationValue) + Col1XSecondValue * interpolationValue;
                }
                if (setCol1YFirstValue && setCol1YSecondValue)
                {
                    Col1.Y = Col1YFirstValue * (1 - interpolationValue) + Col1YSecondValue * interpolationValue;
                }
                if (setCol2HeightFirstValue && setCol2HeightSecondValue)
                {
                    Col2.Height = Col2HeightFirstValue * (1 - interpolationValue) + Col2HeightSecondValue * interpolationValue;
                }
                if (setCol2OutlineThicknessFirstValue && setCol2OutlineThicknessSecondValue)
                {
                    Col2.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(Col2OutlineThicknessFirstValue* (1 - interpolationValue) + Col2OutlineThicknessSecondValue * interpolationValue);
                }
                if (setCol2WidthFirstValue && setCol2WidthSecondValue)
                {
                    Col2.Width = Col2WidthFirstValue * (1 - interpolationValue) + Col2WidthSecondValue * interpolationValue;
                }
                if (setCol2XFirstValue && setCol2XSecondValue)
                {
                    Col2.X = Col2XFirstValue * (1 - interpolationValue) + Col2XSecondValue * interpolationValue;
                }
                if (setCol2YFirstValue && setCol2YSecondValue)
                {
                    Col2.Y = Col2YFirstValue * (1 - interpolationValue) + Col2YSecondValue * interpolationValue;
                }
                if (setCol3HeightFirstValue && setCol3HeightSecondValue)
                {
                    Col3.Height = Col3HeightFirstValue * (1 - interpolationValue) + Col3HeightSecondValue * interpolationValue;
                }
                if (setCol3OutlineThicknessFirstValue && setCol3OutlineThicknessSecondValue)
                {
                    Col3.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(Col3OutlineThicknessFirstValue* (1 - interpolationValue) + Col3OutlineThicknessSecondValue * interpolationValue);
                }
                if (setCol3WidthFirstValue && setCol3WidthSecondValue)
                {
                    Col3.Width = Col3WidthFirstValue * (1 - interpolationValue) + Col3WidthSecondValue * interpolationValue;
                }
                if (setCol3XFirstValue && setCol3XSecondValue)
                {
                    Col3.X = Col3XFirstValue * (1 - interpolationValue) + Col3XSecondValue * interpolationValue;
                }
                if (setCol3YFirstValue && setCol3YSecondValue)
                {
                    Col3.Y = Col3YFirstValue * (1 - interpolationValue) + Col3YSecondValue * interpolationValue;
                }
                if (setColoredRectangleInstanceBlueFirstValue && setColoredRectangleInstanceBlueSecondValue)
                {
                    ColoredRectangleInstance.Blue = FlatRedBall.Math.MathFunctions.RoundToInt(ColoredRectangleInstanceBlueFirstValue* (1 - interpolationValue) + ColoredRectangleInstanceBlueSecondValue * interpolationValue);
                }
                if (setColoredRectangleInstanceGreenFirstValue && setColoredRectangleInstanceGreenSecondValue)
                {
                    ColoredRectangleInstance.Green = FlatRedBall.Math.MathFunctions.RoundToInt(ColoredRectangleInstanceGreenFirstValue* (1 - interpolationValue) + ColoredRectangleInstanceGreenSecondValue * interpolationValue);
                }
                if (setColoredRectangleInstanceHeightFirstValue && setColoredRectangleInstanceHeightSecondValue)
                {
                    ColoredRectangleInstance.Height = ColoredRectangleInstanceHeightFirstValue * (1 - interpolationValue) + ColoredRectangleInstanceHeightSecondValue * interpolationValue;
                }
                if (setColoredRectangleInstanceRedFirstValue && setColoredRectangleInstanceRedSecondValue)
                {
                    ColoredRectangleInstance.Red = FlatRedBall.Math.MathFunctions.RoundToInt(ColoredRectangleInstanceRedFirstValue* (1 - interpolationValue) + ColoredRectangleInstanceRedSecondValue * interpolationValue);
                }
                if (setColoredRectangleInstanceWidthFirstValue && setColoredRectangleInstanceWidthSecondValue)
                {
                    ColoredRectangleInstance.Width = ColoredRectangleInstanceWidthFirstValue * (1 - interpolationValue) + ColoredRectangleInstanceWidthSecondValue * interpolationValue;
                }
                if (setColoredRectangleInstanceXFirstValue && setColoredRectangleInstanceXSecondValue)
                {
                    ColoredRectangleInstance.X = ColoredRectangleInstanceXFirstValue * (1 - interpolationValue) + ColoredRectangleInstanceXSecondValue * interpolationValue;
                }
                if (setColoredRectangleInstanceYFirstValue && setColoredRectangleInstanceYSecondValue)
                {
                    ColoredRectangleInstance.Y = ColoredRectangleInstanceYFirstValue * (1 - interpolationValue) + ColoredRectangleInstanceYSecondValue * interpolationValue;
                }
                if (setHeaderInstanceBlueFirstValue && setHeaderInstanceBlueSecondValue)
                {
                    HeaderInstance.Blue = FlatRedBall.Math.MathFunctions.RoundToInt(HeaderInstanceBlueFirstValue* (1 - interpolationValue) + HeaderInstanceBlueSecondValue * interpolationValue);
                }
                if (setHeaderInstanceFontSizeFirstValue && setHeaderInstanceFontSizeSecondValue)
                {
                    HeaderInstance.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(HeaderInstanceFontSizeFirstValue* (1 - interpolationValue) + HeaderInstanceFontSizeSecondValue * interpolationValue);
                }
                if (setHeaderInstanceGreenFirstValue && setHeaderInstanceGreenSecondValue)
                {
                    HeaderInstance.Green = FlatRedBall.Math.MathFunctions.RoundToInt(HeaderInstanceGreenFirstValue* (1 - interpolationValue) + HeaderInstanceGreenSecondValue * interpolationValue);
                }
                if (setHeaderInstanceHeightFirstValue && setHeaderInstanceHeightSecondValue)
                {
                    HeaderInstance.Height = HeaderInstanceHeightFirstValue * (1 - interpolationValue) + HeaderInstanceHeightSecondValue * interpolationValue;
                }
                if (setHeaderInstanceOutlineThicknessFirstValue && setHeaderInstanceOutlineThicknessSecondValue)
                {
                    HeaderInstance.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(HeaderInstanceOutlineThicknessFirstValue* (1 - interpolationValue) + HeaderInstanceOutlineThicknessSecondValue * interpolationValue);
                }
                if (setHeaderInstanceRedFirstValue && setHeaderInstanceRedSecondValue)
                {
                    HeaderInstance.Red = FlatRedBall.Math.MathFunctions.RoundToInt(HeaderInstanceRedFirstValue* (1 - interpolationValue) + HeaderInstanceRedSecondValue * interpolationValue);
                }
                if (setHeaderInstanceWidthFirstValue && setHeaderInstanceWidthSecondValue)
                {
                    HeaderInstance.Width = HeaderInstanceWidthFirstValue * (1 - interpolationValue) + HeaderInstanceWidthSecondValue * interpolationValue;
                }
                if (setHeaderInstanceXFirstValue && setHeaderInstanceXSecondValue)
                {
                    HeaderInstance.X = HeaderInstanceXFirstValue * (1 - interpolationValue) + HeaderInstanceXSecondValue * interpolationValue;
                }
                if (setHeaderInstanceYFirstValue && setHeaderInstanceYSecondValue)
                {
                    HeaderInstance.Y = HeaderInstanceYFirstValue * (1 - interpolationValue) + HeaderInstanceYSecondValue * interpolationValue;
                }
                if (setHeightFirstValue && setHeightSecondValue)
                {
                    Height = HeightFirstValue * (1 - interpolationValue) + HeightSecondValue * interpolationValue;
                }
                if (setRankingHeightFirstValue && setRankingHeightSecondValue)
                {
                    Ranking.Height = RankingHeightFirstValue * (1 - interpolationValue) + RankingHeightSecondValue * interpolationValue;
                }
                if (setRankingWidthFirstValue && setRankingWidthSecondValue)
                {
                    Ranking.Width = RankingWidthFirstValue * (1 - interpolationValue) + RankingWidthSecondValue * interpolationValue;
                }
                if (setRankingXFirstValue && setRankingXSecondValue)
                {
                    Ranking.X = RankingXFirstValue * (1 - interpolationValue) + RankingXSecondValue * interpolationValue;
                }
                if (setRankingYFirstValue && setRankingYSecondValue)
                {
                    Ranking.Y = RankingYFirstValue * (1 - interpolationValue) + RankingYSecondValue * interpolationValue;
                }
                if (setSpriteInstanceHeightFirstValue && setSpriteInstanceHeightSecondValue)
                {
                    SpriteInstance.Height = SpriteInstanceHeightFirstValue * (1 - interpolationValue) + SpriteInstanceHeightSecondValue * interpolationValue;
                }
                if (setSpriteInstanceWidthFirstValue && setSpriteInstanceWidthSecondValue)
                {
                    SpriteInstance.Width = SpriteInstanceWidthFirstValue * (1 - interpolationValue) + SpriteInstanceWidthSecondValue * interpolationValue;
                }
                if (setSpriteInstanceXFirstValue && setSpriteInstanceXSecondValue)
                {
                    SpriteInstance.X = SpriteInstanceXFirstValue * (1 - interpolationValue) + SpriteInstanceXSecondValue * interpolationValue;
                }
                if (setSpriteInstanceYFirstValue && setSpriteInstanceYSecondValue)
                {
                    SpriteInstance.Y = SpriteInstanceYFirstValue * (1 - interpolationValue) + SpriteInstanceYSecondValue * interpolationValue;
                }
                if (setTitle1BlueFirstValue && setTitle1BlueSecondValue)
                {
                    Title1.Blue = FlatRedBall.Math.MathFunctions.RoundToInt(Title1BlueFirstValue* (1 - interpolationValue) + Title1BlueSecondValue * interpolationValue);
                }
                if (setTitle1FontSizeFirstValue && setTitle1FontSizeSecondValue)
                {
                    Title1.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(Title1FontSizeFirstValue* (1 - interpolationValue) + Title1FontSizeSecondValue * interpolationValue);
                }
                if (setTitle1GreenFirstValue && setTitle1GreenSecondValue)
                {
                    Title1.Green = FlatRedBall.Math.MathFunctions.RoundToInt(Title1GreenFirstValue* (1 - interpolationValue) + Title1GreenSecondValue * interpolationValue);
                }
                if (setTitle1HeightFirstValue && setTitle1HeightSecondValue)
                {
                    Title1.Height = Title1HeightFirstValue * (1 - interpolationValue) + Title1HeightSecondValue * interpolationValue;
                }
                if (setTitle1OutlineThicknessFirstValue && setTitle1OutlineThicknessSecondValue)
                {
                    Title1.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(Title1OutlineThicknessFirstValue* (1 - interpolationValue) + Title1OutlineThicknessSecondValue * interpolationValue);
                }
                if (setTitle1RedFirstValue && setTitle1RedSecondValue)
                {
                    Title1.Red = FlatRedBall.Math.MathFunctions.RoundToInt(Title1RedFirstValue* (1 - interpolationValue) + Title1RedSecondValue * interpolationValue);
                }
                if (setTitle1WidthFirstValue && setTitle1WidthSecondValue)
                {
                    Title1.Width = Title1WidthFirstValue * (1 - interpolationValue) + Title1WidthSecondValue * interpolationValue;
                }
                if (setTitle1XFirstValue && setTitle1XSecondValue)
                {
                    Title1.X = Title1XFirstValue * (1 - interpolationValue) + Title1XSecondValue * interpolationValue;
                }
                if (setTitle1YFirstValue && setTitle1YSecondValue)
                {
                    Title1.Y = Title1YFirstValue * (1 - interpolationValue) + Title1YSecondValue * interpolationValue;
                }
                if (setTitle2FontSizeFirstValue && setTitle2FontSizeSecondValue)
                {
                    Title2.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(Title2FontSizeFirstValue* (1 - interpolationValue) + Title2FontSizeSecondValue * interpolationValue);
                }
                if (setTitle2HeightFirstValue && setTitle2HeightSecondValue)
                {
                    Title2.Height = Title2HeightFirstValue * (1 - interpolationValue) + Title2HeightSecondValue * interpolationValue;
                }
                if (setTitle2OutlineThicknessFirstValue && setTitle2OutlineThicknessSecondValue)
                {
                    Title2.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(Title2OutlineThicknessFirstValue* (1 - interpolationValue) + Title2OutlineThicknessSecondValue * interpolationValue);
                }
                if (setTitle2WidthFirstValue && setTitle2WidthSecondValue)
                {
                    Title2.Width = Title2WidthFirstValue * (1 - interpolationValue) + Title2WidthSecondValue * interpolationValue;
                }
                if (setTitle2XFirstValue && setTitle2XSecondValue)
                {
                    Title2.X = Title2XFirstValue * (1 - interpolationValue) + Title2XSecondValue * interpolationValue;
                }
                if (setTitle2YFirstValue && setTitle2YSecondValue)
                {
                    Title2.Y = Title2YFirstValue * (1 - interpolationValue) + Title2YSecondValue * interpolationValue;
                }
                if (setTitle3FontSizeFirstValue && setTitle3FontSizeSecondValue)
                {
                    Title3.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(Title3FontSizeFirstValue* (1 - interpolationValue) + Title3FontSizeSecondValue * interpolationValue);
                }
                if (setTitle3HeightFirstValue && setTitle3HeightSecondValue)
                {
                    Title3.Height = Title3HeightFirstValue * (1 - interpolationValue) + Title3HeightSecondValue * interpolationValue;
                }
                if (setTitle3OutlineThicknessFirstValue && setTitle3OutlineThicknessSecondValue)
                {
                    Title3.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(Title3OutlineThicknessFirstValue* (1 - interpolationValue) + Title3OutlineThicknessSecondValue * interpolationValue);
                }
                if (setTitle3WidthFirstValue && setTitle3WidthSecondValue)
                {
                    Title3.Width = Title3WidthFirstValue * (1 - interpolationValue) + Title3WidthSecondValue * interpolationValue;
                }
                if (setTitle3XFirstValue && setTitle3XSecondValue)
                {
                    Title3.X = Title3XFirstValue * (1 - interpolationValue) + Title3XSecondValue * interpolationValue;
                }
                if (setTitle3YFirstValue && setTitle3YSecondValue)
                {
                    Title3.Y = Title3YFirstValue * (1 - interpolationValue) + Title3YSecondValue * interpolationValue;
                }
                if (settitleBarHeightFirstValue && settitleBarHeightSecondValue)
                {
                    titleBar.Height = titleBarHeightFirstValue * (1 - interpolationValue) + titleBarHeightSecondValue * interpolationValue;
                }
                if (settitleBarWidthFirstValue && settitleBarWidthSecondValue)
                {
                    titleBar.Width = titleBarWidthFirstValue * (1 - interpolationValue) + titleBarWidthSecondValue * interpolationValue;
                }
                if (settitleBarXFirstValue && settitleBarXSecondValue)
                {
                    titleBar.X = titleBarXFirstValue * (1 - interpolationValue) + titleBarXSecondValue * interpolationValue;
                }
                if (settitleBarYFirstValue && settitleBarYSecondValue)
                {
                    titleBar.Y = titleBarYFirstValue * (1 - interpolationValue) + titleBarYSecondValue * interpolationValue;
                }
                if (setWidthFirstValue && setWidthSecondValue)
                {
                    Width = WidthFirstValue * (1 - interpolationValue) + WidthSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.Panel_DisplayRuntime.VariableState fromState,DungeonRun.GumRuntimes.Panel_DisplayRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.SourceFile",
                            Type = "string",
                            Value = SpriteInstance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Y",
                            Type = "float",
                            Value = SpriteInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Blue",
                            Type = "int",
                            Value = ColoredRectangleInstance.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Green",
                            Type = "int",
                            Value = ColoredRectangleInstance.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Height",
                            Type = "float",
                            Value = ColoredRectangleInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Red",
                            Type = "int",
                            Value = ColoredRectangleInstance.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Width",
                            Type = "float",
                            Value = ColoredRectangleInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.X",
                            Type = "float",
                            Value = ColoredRectangleInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = ColoredRectangleInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.X Units",
                            Type = "PositionUnitType",
                            Value = ColoredRectangleInstance.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Y",
                            Type = "float",
                            Value = ColoredRectangleInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Y Origin",
                            Type = "VerticalAlignment",
                            Value = ColoredRectangleInstance.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Y Units",
                            Type = "PositionUnitType",
                            Value = ColoredRectangleInstance.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Blue",
                            Type = "int",
                            Value = HeaderInstance.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Font",
                            Type = "string",
                            Value = HeaderInstance.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.FontSize",
                            Type = "int",
                            Value = HeaderInstance.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Green",
                            Type = "int",
                            Value = HeaderInstance.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Height",
                            Type = "float",
                            Value = HeaderInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = HeaderInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.OutlineThickness",
                            Type = "int",
                            Value = HeaderInstance.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Parent",
                            Type = "string",
                            Value = HeaderInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Red",
                            Type = "int",
                            Value = HeaderInstance.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Text",
                            Type = "string",
                            Value = HeaderInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Width",
                            Type = "float",
                            Value = HeaderInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = HeaderInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.X",
                            Type = "float",
                            Value = HeaderInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Y",
                            Type = "float",
                            Value = HeaderInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "titleBar.Height",
                            Type = "float",
                            Value = titleBar.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "titleBar.Width",
                            Type = "float",
                            Value = titleBar.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "titleBar.X",
                            Type = "float",
                            Value = titleBar.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "titleBar.Y",
                            Type = "float",
                            Value = titleBar.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Blue",
                            Type = "int",
                            Value = Title1.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Font",
                            Type = "string",
                            Value = Title1.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.FontSize",
                            Type = "int",
                            Value = Title1.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Green",
                            Type = "int",
                            Value = Title1.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Height",
                            Type = "float",
                            Value = Title1.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Height Units",
                            Type = "DimensionUnitType",
                            Value = Title1.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.OutlineThickness",
                            Type = "int",
                            Value = Title1.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Parent",
                            Type = "string",
                            Value = Title1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Red",
                            Type = "int",
                            Value = Title1.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Text",
                            Type = "string",
                            Value = Title1.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Width",
                            Type = "float",
                            Value = Title1.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Width Units",
                            Type = "DimensionUnitType",
                            Value = Title1.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.X",
                            Type = "float",
                            Value = Title1.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Y",
                            Type = "float",
                            Value = Title1.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Font",
                            Type = "string",
                            Value = Title2.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.FontSize",
                            Type = "int",
                            Value = Title2.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Height",
                            Type = "float",
                            Value = Title2.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Height Units",
                            Type = "DimensionUnitType",
                            Value = Title2.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.OutlineThickness",
                            Type = "int",
                            Value = Title2.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Parent",
                            Type = "string",
                            Value = Title2.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Text",
                            Type = "string",
                            Value = Title2.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Width",
                            Type = "float",
                            Value = Title2.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Width Units",
                            Type = "DimensionUnitType",
                            Value = Title2.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.X",
                            Type = "float",
                            Value = Title2.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Y",
                            Type = "float",
                            Value = Title2.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Font",
                            Type = "string",
                            Value = Title3.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.FontSize",
                            Type = "int",
                            Value = Title3.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Height",
                            Type = "float",
                            Value = Title3.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Height Units",
                            Type = "DimensionUnitType",
                            Value = Title3.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = Title3.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.OutlineThickness",
                            Type = "int",
                            Value = Title3.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Parent",
                            Type = "string",
                            Value = Title3.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Text",
                            Type = "string",
                            Value = Title3.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = Title3.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Width",
                            Type = "float",
                            Value = Title3.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Width Units",
                            Type = "DimensionUnitType",
                            Value = Title3.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.X",
                            Type = "float",
                            Value = Title3.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Y",
                            Type = "float",
                            Value = Title3.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Ranking.Height",
                            Type = "float",
                            Value = Ranking.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Ranking.Parent",
                            Type = "string",
                            Value = Ranking.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Ranking.Width",
                            Type = "float",
                            Value = Ranking.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Ranking.X",
                            Type = "float",
                            Value = Ranking.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Ranking.Y",
                            Type = "float",
                            Value = Ranking.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.Height",
                            Type = "float",
                            Value = Col1.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = Col1.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.OutlineThickness",
                            Type = "int",
                            Value = Col1.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.Parent",
                            Type = "string",
                            Value = Col1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.Text",
                            Type = "string",
                            Value = Col1.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.Width",
                            Type = "float",
                            Value = Col1.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.X",
                            Type = "float",
                            Value = Col1.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.Y",
                            Type = "float",
                            Value = Col1.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.Height",
                            Type = "float",
                            Value = Col2.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = Col2.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.OutlineThickness",
                            Type = "int",
                            Value = Col2.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.Parent",
                            Type = "string",
                            Value = Col2.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.Text",
                            Type = "string",
                            Value = Col2.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.Width",
                            Type = "float",
                            Value = Col2.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.X",
                            Type = "float",
                            Value = Col2.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.Y",
                            Type = "float",
                            Value = Col2.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.Height",
                            Type = "float",
                            Value = Col3.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.OutlineThickness",
                            Type = "int",
                            Value = Col3.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.Parent",
                            Type = "string",
                            Value = Col3.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.Text",
                            Type = "string",
                            Value = Col3.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.Width",
                            Type = "float",
                            Value = Col3.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.X",
                            Type = "float",
                            Value = Col3.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.Y",
                            Type = "float",
                            Value = Col3.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height + 578f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width + 540f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.SourceFile",
                            Type = "string",
                            Value = SpriteInstance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X + -2f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Y",
                            Type = "float",
                            Value = SpriteInstance.Y + -1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Blue",
                            Type = "int",
                            Value = ColoredRectangleInstance.Blue + 65
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Green",
                            Type = "int",
                            Value = ColoredRectangleInstance.Green + 86
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Height",
                            Type = "float",
                            Value = ColoredRectangleInstance.Height + 10f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Red",
                            Type = "int",
                            Value = ColoredRectangleInstance.Red + 120
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Width",
                            Type = "float",
                            Value = ColoredRectangleInstance.Width + 362f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.X",
                            Type = "float",
                            Value = ColoredRectangleInstance.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = ColoredRectangleInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.X Units",
                            Type = "PositionUnitType",
                            Value = ColoredRectangleInstance.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Y",
                            Type = "float",
                            Value = ColoredRectangleInstance.Y + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Y Origin",
                            Type = "VerticalAlignment",
                            Value = ColoredRectangleInstance.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Y Units",
                            Type = "PositionUnitType",
                            Value = ColoredRectangleInstance.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Blue",
                            Type = "int",
                            Value = HeaderInstance.Blue + 245
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Font",
                            Type = "string",
                            Value = HeaderInstance.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.FontSize",
                            Type = "int",
                            Value = HeaderInstance.FontSize + 30
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Green",
                            Type = "int",
                            Value = HeaderInstance.Green + 240
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Height",
                            Type = "float",
                            Value = HeaderInstance.Height + 17f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = HeaderInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.OutlineThickness",
                            Type = "int",
                            Value = HeaderInstance.OutlineThickness + 2
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Parent",
                            Type = "string",
                            Value = HeaderInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Red",
                            Type = "int",
                            Value = HeaderInstance.Red + 255
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Text",
                            Type = "string",
                            Value = HeaderInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Width",
                            Type = "float",
                            Value = HeaderInstance.Width + -275f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = HeaderInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.X",
                            Type = "float",
                            Value = HeaderInstance.X + 128f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Y",
                            Type = "float",
                            Value = HeaderInstance.Y + -32f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "titleBar.Height",
                            Type = "float",
                            Value = titleBar.Height + 29f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "titleBar.Width",
                            Type = "float",
                            Value = titleBar.Width + 542f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "titleBar.X",
                            Type = "float",
                            Value = titleBar.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "titleBar.Y",
                            Type = "float",
                            Value = titleBar.Y + 74f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Blue",
                            Type = "int",
                            Value = Title1.Blue + 255
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Font",
                            Type = "string",
                            Value = Title1.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.FontSize",
                            Type = "int",
                            Value = Title1.FontSize + 20
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Green",
                            Type = "int",
                            Value = Title1.Green + 255
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Height",
                            Type = "float",
                            Value = Title1.Height + -5f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Height Units",
                            Type = "DimensionUnitType",
                            Value = Title1.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.OutlineThickness",
                            Type = "int",
                            Value = Title1.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Parent",
                            Type = "string",
                            Value = Title1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Red",
                            Type = "int",
                            Value = Title1.Red + 255
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Text",
                            Type = "string",
                            Value = Title1.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Width",
                            Type = "float",
                            Value = Title1.Width + -481f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Width Units",
                            Type = "DimensionUnitType",
                            Value = Title1.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.X",
                            Type = "float",
                            Value = Title1.X + 75f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title1.Y",
                            Type = "float",
                            Value = Title1.Y + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Font",
                            Type = "string",
                            Value = Title2.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.FontSize",
                            Type = "int",
                            Value = Title2.FontSize + 20
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Height",
                            Type = "float",
                            Value = Title2.Height + -3f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Height Units",
                            Type = "DimensionUnitType",
                            Value = Title2.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.OutlineThickness",
                            Type = "int",
                            Value = Title2.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Parent",
                            Type = "string",
                            Value = Title2.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Text",
                            Type = "string",
                            Value = Title2.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Width",
                            Type = "float",
                            Value = Title2.Width + -491f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Width Units",
                            Type = "DimensionUnitType",
                            Value = Title2.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.X",
                            Type = "float",
                            Value = Title2.X + 252f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title2.Y",
                            Type = "float",
                            Value = Title2.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Font",
                            Type = "string",
                            Value = Title3.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.FontSize",
                            Type = "int",
                            Value = Title3.FontSize + 20
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Height",
                            Type = "float",
                            Value = Title3.Height + -1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Height Units",
                            Type = "DimensionUnitType",
                            Value = Title3.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = Title3.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.OutlineThickness",
                            Type = "int",
                            Value = Title3.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Parent",
                            Type = "string",
                            Value = Title3.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Text",
                            Type = "string",
                            Value = Title3.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = Title3.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Width",
                            Type = "float",
                            Value = Title3.Width + -405f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Width Units",
                            Type = "DimensionUnitType",
                            Value = Title3.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.X",
                            Type = "float",
                            Value = Title3.X + 377f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Title3.Y",
                            Type = "float",
                            Value = Title3.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Ranking.Height",
                            Type = "float",
                            Value = Ranking.Height + 474f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Ranking.Parent",
                            Type = "string",
                            Value = Ranking.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Ranking.Width",
                            Type = "float",
                            Value = Ranking.Width + 538f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Ranking.X",
                            Type = "float",
                            Value = Ranking.X + 2f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Ranking.Y",
                            Type = "float",
                            Value = Ranking.Y + 27f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.Height",
                            Type = "float",
                            Value = Col1.Height + 461f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = Col1.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.OutlineThickness",
                            Type = "int",
                            Value = Col1.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.Parent",
                            Type = "string",
                            Value = Col1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.Text",
                            Type = "string",
                            Value = Col1.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.Width",
                            Type = "float",
                            Value = Col1.Width + 178f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.X",
                            Type = "float",
                            Value = Col1.X + 17f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col1.Y",
                            Type = "float",
                            Value = Col1.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.Height",
                            Type = "float",
                            Value = Col2.Height + 466f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = Col2.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.OutlineThickness",
                            Type = "int",
                            Value = Col2.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.Parent",
                            Type = "string",
                            Value = Col2.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.Text",
                            Type = "string",
                            Value = Col2.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.Width",
                            Type = "float",
                            Value = Col2.Width + 178f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.X",
                            Type = "float",
                            Value = Col2.X + 184f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col2.Y",
                            Type = "float",
                            Value = Col2.Y + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.Height",
                            Type = "float",
                            Value = Col3.Height + 462f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.OutlineThickness",
                            Type = "int",
                            Value = Col3.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.Parent",
                            Type = "string",
                            Value = Col3.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.Text",
                            Type = "string",
                            Value = Col3.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.Width",
                            Type = "float",
                            Value = Col3.Width + 119f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.X",
                            Type = "float",
                            Value = Col3.X + 368f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Col3.Y",
                            Type = "float",
                            Value = Col3.Y + 0f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.SpriteRuntime SpriteInstance { get; set; }
            private DungeonRun.GumRuntimes.ColoredRectangleRuntime ColoredRectangleInstance { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime HeaderInstance { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime titleBar { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime Title1 { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime Title2 { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime Title3 { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime Ranking { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime Col1 { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime Col2 { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime Col3 { get; set; }
            public string Text1
            {
                get
                {
                    return Col1.Text;
                }
                set
                {
                    if (Col1.Text != value)
                    {
                        Col1.Text = value;
                        Text1Changed?.Invoke(this, null);
                    }
                }
            }
            public string Text2
            {
                get
                {
                    return Col2.Text;
                }
                set
                {
                    if (Col2.Text != value)
                    {
                        Col2.Text = value;
                        Text2Changed?.Invoke(this, null);
                    }
                }
            }
            public string Text3
            {
                get
                {
                    return Col3.Text;
                }
                set
                {
                    if (Col3.Text != value)
                    {
                        Col3.Text = value;
                        Text3Changed?.Invoke(this, null);
                    }
                }
            }
            public event System.EventHandler Text1Changed;
            public event System.EventHandler Text2Changed;
            public event System.EventHandler Text3Changed;
            public Panel_DisplayRuntime (bool fullInstantiation = true) 
            	: base(false)
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "Panel_Display");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                SpriteInstance = this.GetGraphicalUiElementByName("SpriteInstance") as DungeonRun.GumRuntimes.SpriteRuntime;
                ColoredRectangleInstance = this.GetGraphicalUiElementByName("ColoredRectangleInstance") as DungeonRun.GumRuntimes.ColoredRectangleRuntime;
                HeaderInstance = this.GetGraphicalUiElementByName("HeaderInstance") as DungeonRun.GumRuntimes.TextRuntime;
                titleBar = this.GetGraphicalUiElementByName("titleBar") as DungeonRun.GumRuntimes.ContainerRuntime;
                Title1 = this.GetGraphicalUiElementByName("Title1") as DungeonRun.GumRuntimes.TextRuntime;
                Title2 = this.GetGraphicalUiElementByName("Title2") as DungeonRun.GumRuntimes.TextRuntime;
                Title3 = this.GetGraphicalUiElementByName("Title3") as DungeonRun.GumRuntimes.TextRuntime;
                Ranking = this.GetGraphicalUiElementByName("Ranking") as DungeonRun.GumRuntimes.ContainerRuntime;
                Col1 = this.GetGraphicalUiElementByName("Col1") as DungeonRun.GumRuntimes.TextRuntime;
                Col2 = this.GetGraphicalUiElementByName("Col2") as DungeonRun.GumRuntimes.TextRuntime;
                Col3 = this.GetGraphicalUiElementByName("Col3") as DungeonRun.GumRuntimes.TextRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
