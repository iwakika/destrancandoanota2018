    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class PlayPoints_HUDRuntime : Gum.Wireframe.GraphicalUiElement
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            PlayerPoints_displayInstance.Height = 127f;
                            PlayerPoints_displayInstance.Visible = true;
                            PlayerPoints_displayInstance.Width = 154f;
                            PlayerPoints_displayInstance.X = 183f;
                            PlayerPoints_displayInstance.Y = 92f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setPlayerPoints_displayInstanceHeightFirstValue = false;
                bool setPlayerPoints_displayInstanceHeightSecondValue = false;
                float PlayerPoints_displayInstanceHeightFirstValue= 0;
                float PlayerPoints_displayInstanceHeightSecondValue= 0;
                bool setPlayerPoints_displayInstanceWidthFirstValue = false;
                bool setPlayerPoints_displayInstanceWidthSecondValue = false;
                float PlayerPoints_displayInstanceWidthFirstValue= 0;
                float PlayerPoints_displayInstanceWidthSecondValue= 0;
                bool setPlayerPoints_displayInstanceXFirstValue = false;
                bool setPlayerPoints_displayInstanceXSecondValue = false;
                float PlayerPoints_displayInstanceXFirstValue= 0;
                float PlayerPoints_displayInstanceXSecondValue= 0;
                bool setPlayerPoints_displayInstanceYFirstValue = false;
                bool setPlayerPoints_displayInstanceYSecondValue = false;
                float PlayerPoints_displayInstanceYFirstValue= 0;
                float PlayerPoints_displayInstanceYSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setPlayerPoints_displayInstanceHeightFirstValue = true;
                        PlayerPoints_displayInstanceHeightFirstValue = 127f;
                        if (interpolationValue < 1)
                        {
                            this.PlayerPoints_displayInstance.Visible = true;
                        }
                        setPlayerPoints_displayInstanceWidthFirstValue = true;
                        PlayerPoints_displayInstanceWidthFirstValue = 154f;
                        setPlayerPoints_displayInstanceXFirstValue = true;
                        PlayerPoints_displayInstanceXFirstValue = 183f;
                        setPlayerPoints_displayInstanceYFirstValue = true;
                        PlayerPoints_displayInstanceYFirstValue = 92f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setPlayerPoints_displayInstanceHeightSecondValue = true;
                        PlayerPoints_displayInstanceHeightSecondValue = 127f;
                        if (interpolationValue >= 1)
                        {
                            this.PlayerPoints_displayInstance.Visible = true;
                        }
                        setPlayerPoints_displayInstanceWidthSecondValue = true;
                        PlayerPoints_displayInstanceWidthSecondValue = 154f;
                        setPlayerPoints_displayInstanceXSecondValue = true;
                        PlayerPoints_displayInstanceXSecondValue = 183f;
                        setPlayerPoints_displayInstanceYSecondValue = true;
                        PlayerPoints_displayInstanceYSecondValue = 92f;
                        break;
                }
                if (setPlayerPoints_displayInstanceHeightFirstValue && setPlayerPoints_displayInstanceHeightSecondValue)
                {
                    PlayerPoints_displayInstance.Height = PlayerPoints_displayInstanceHeightFirstValue * (1 - interpolationValue) + PlayerPoints_displayInstanceHeightSecondValue * interpolationValue;
                }
                if (setPlayerPoints_displayInstanceWidthFirstValue && setPlayerPoints_displayInstanceWidthSecondValue)
                {
                    PlayerPoints_displayInstance.Width = PlayerPoints_displayInstanceWidthFirstValue * (1 - interpolationValue) + PlayerPoints_displayInstanceWidthSecondValue * interpolationValue;
                }
                if (setPlayerPoints_displayInstanceXFirstValue && setPlayerPoints_displayInstanceXSecondValue)
                {
                    PlayerPoints_displayInstance.X = PlayerPoints_displayInstanceXFirstValue * (1 - interpolationValue) + PlayerPoints_displayInstanceXSecondValue * interpolationValue;
                }
                if (setPlayerPoints_displayInstanceYFirstValue && setPlayerPoints_displayInstanceYSecondValue)
                {
                    PlayerPoints_displayInstance.Y = PlayerPoints_displayInstanceYFirstValue * (1 - interpolationValue) + PlayerPoints_displayInstanceYSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.PlayPoints_HUDRuntime.VariableState fromState,DungeonRun.GumRuntimes.PlayPoints_HUDRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
                PlayerPoints_displayInstance.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayerPoints_displayInstance.Height",
                            Type = "float",
                            Value = PlayerPoints_displayInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayerPoints_displayInstance.Visible",
                            Type = "bool",
                            Value = PlayerPoints_displayInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayerPoints_displayInstance.Width",
                            Type = "float",
                            Value = PlayerPoints_displayInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayerPoints_displayInstance.X",
                            Type = "float",
                            Value = PlayerPoints_displayInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayerPoints_displayInstance.Y",
                            Type = "float",
                            Value = PlayerPoints_displayInstance.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayerPoints_displayInstance.Height",
                            Type = "float",
                            Value = PlayerPoints_displayInstance.Height + 127f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayerPoints_displayInstance.Visible",
                            Type = "bool",
                            Value = PlayerPoints_displayInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayerPoints_displayInstance.Width",
                            Type = "float",
                            Value = PlayerPoints_displayInstance.Width + 154f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayerPoints_displayInstance.X",
                            Type = "float",
                            Value = PlayerPoints_displayInstance.X + 183f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayerPoints_displayInstance.Y",
                            Type = "float",
                            Value = PlayerPoints_displayInstance.Y + 92f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.PlayerPoints_displayRuntime PlayerPoints_displayInstance { get; set; }
            public PlayPoints_HUDRuntime (bool fullInstantiation = true) 
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Screens.First(item => item.Name == "PlayPoints_HUD");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                PlayerPoints_displayInstance = this.GetGraphicalUiElementByName("PlayerPoints_displayInstance") as DungeonRun.GumRuntimes.PlayerPoints_displayRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
