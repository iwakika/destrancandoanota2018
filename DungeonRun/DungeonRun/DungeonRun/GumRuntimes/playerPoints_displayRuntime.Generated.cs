    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class PlayerPoints_displayRuntime : DungeonRun.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            SpriteInstance.Height = 243f;
                            SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown.png");
                            SpriteInstance.Width = 303f;
                            SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            SpriteInstance.X = -5f;
                            SpriteInstance.Y = 1f;
                            Avatar.Height = -59f;
                            Avatar.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Avatar.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            SetProperty("Avatar.SourceFile", "Xalakom.png");
                            Avatar.Width = -600f;
                            Avatar.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Avatar.X = 22f;
                            Avatar.Y = 30f;
                            PlayName.Font = "Impact";
                            PlayName.FontSize = 22;
                            PlayName.Height = 44f;
                            PlayName.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                            PlayName.OutlineThickness = 1;
                            PlayName.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            PlayName.Text = "Play name\n";
                            PlayName.Width = 178f;
                            PlayName.X = 98f;
                            PlayName.Y = 63f;
                            playTime.OutlineThickness = 1;
                            playTime.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            playTime.Text = "Play Time";
                            playTime.X = 99f;
                            playTime.Y = 97f;
                            ContainerInstance.Height = 14f;
                            ContainerInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            ContainerInstance.Width = 501f;
                            ContainerInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            ContainerInstance.X = 21f;
                            ContainerInstance.Y = 37f;
                            PointsDisplay.Height = -37f;
                            PointsDisplay.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            PointsDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            PointsDisplay.Width = -583f;
                            PointsDisplay.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            PointsDisplay.X = 16f;
                            PointsDisplay.Y = 193f;
                            TimePointsLabel.Height = 20f;
                            TimePointsLabel.OutlineThickness = 1;
                            TimePointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                            TimePointsLabel.Text = "Tempo";
                            TimePointsLabel.X = 0f;
                            TimePointsLabel.Y = 0f;
                            LifePointsLabel.Height = 20f;
                            LifePointsLabel.OutlineThickness = 1;
                            LifePointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                            LifePointsLabel.Text = "Vida\n";
                            LifePointsLabel.X = 0f;
                            LifePointsLabel.Y = 20f;
                            ExtrasPointsLabel.Height = 20f;
                            ExtrasPointsLabel.OutlineThickness = 1;
                            ExtrasPointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                            ExtrasPointsLabel.Text = "Extra";
                            ExtrasPointsLabel.X = 0f;
                            ExtrasPointsLabel.Y = 60f;
                            LevelPoints.Height = 20f;
                            LevelPoints.OutlineThickness = 1;
                            LevelPoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                            LevelPoints.Text = "0";
                            LevelPoints.Width = 100f;
                            LevelPoints.X = 0f;
                            LevelPoints.Y = 40f;
                            TimePoints.Height = 20f;
                            TimePoints.OutlineThickness = 1;
                            TimePoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                            TimePoints.Text = "0";
                            TimePoints.X = 0f;
                            TimePoints.Y = 0f;
                            LifePoints.Height = 20f;
                            LifePoints.OutlineThickness = 1;
                            LifePoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                            LifePoints.Text = "0";
                            LifePoints.X = 0f;
                            LifePoints.Y = 20f;
                            ExtrasPoints.Height = 0f;
                            ExtrasPoints.OutlineThickness = 1;
                            ExtrasPoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                            ExtrasPoints.Text = "0";
                            ExtrasPoints.Width = 100f;
                            ExtrasPoints.X = 0f;
                            ExtrasPoints.Y = 60f;
                            LevelPointsLabel.OutlineThickness = 1;
                            LevelPointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                            LevelPointsLabel.Text = "Level\n";
                            LevelPointsLabel.X = 0f;
                            LevelPointsLabel.Y = 40f;
                            TotalPoints.Height = 0f;
                            TotalPoints.OutlineThickness = 1;
                            TotalPoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                            TotalPoints.Text = "00\n\n";
                            TotalPoints.Width = 100f;
                            TotalPoints.X = 0f;
                            TotalPoints.Y = 80f;
                            TotalPointsDisplay.Height = 20f;
                            TotalPointsDisplay.OutlineThickness = 1;
                            TotalPointsDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                            TotalPointsDisplay.Text = "Total";
                            TotalPointsDisplay.X = 0f;
                            TotalPointsDisplay.Y = 80f;
                            PointsEdit.Height = -41f;
                            PointsEdit.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            PointsEdit.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            PointsEdit.Width = -572f;
                            PointsEdit.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            PointsEdit.X = 82f;
                            PointsEdit.Y = 195f;
                            TextInstance.Font = "Impact";
                            TextInstance.Height = 23f;
                            TextInstance.OutlineThickness = 1;
                            TextInstance.Text = "Pontos:\n";
                            TextInstance.Width = 72f;
                            TextInstance.X = 15f;
                            TextInstance.Y = 204f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setAvatarHeightFirstValue = false;
                bool setAvatarHeightSecondValue = false;
                float AvatarHeightFirstValue= 0;
                float AvatarHeightSecondValue= 0;
                bool setAvatarWidthFirstValue = false;
                bool setAvatarWidthSecondValue = false;
                float AvatarWidthFirstValue= 0;
                float AvatarWidthSecondValue= 0;
                bool setAvatarXFirstValue = false;
                bool setAvatarXSecondValue = false;
                float AvatarXFirstValue= 0;
                float AvatarXSecondValue= 0;
                bool setAvatarYFirstValue = false;
                bool setAvatarYSecondValue = false;
                float AvatarYFirstValue= 0;
                float AvatarYSecondValue= 0;
                bool setContainerInstanceHeightFirstValue = false;
                bool setContainerInstanceHeightSecondValue = false;
                float ContainerInstanceHeightFirstValue= 0;
                float ContainerInstanceHeightSecondValue= 0;
                bool setContainerInstanceWidthFirstValue = false;
                bool setContainerInstanceWidthSecondValue = false;
                float ContainerInstanceWidthFirstValue= 0;
                float ContainerInstanceWidthSecondValue= 0;
                bool setContainerInstanceXFirstValue = false;
                bool setContainerInstanceXSecondValue = false;
                float ContainerInstanceXFirstValue= 0;
                float ContainerInstanceXSecondValue= 0;
                bool setContainerInstanceYFirstValue = false;
                bool setContainerInstanceYSecondValue = false;
                float ContainerInstanceYFirstValue= 0;
                float ContainerInstanceYSecondValue= 0;
                bool setExtrasPointsHeightFirstValue = false;
                bool setExtrasPointsHeightSecondValue = false;
                float ExtrasPointsHeightFirstValue= 0;
                float ExtrasPointsHeightSecondValue= 0;
                bool setExtrasPointsOutlineThicknessFirstValue = false;
                bool setExtrasPointsOutlineThicknessSecondValue = false;
                int ExtrasPointsOutlineThicknessFirstValue= 0;
                int ExtrasPointsOutlineThicknessSecondValue= 0;
                bool setExtrasPointsWidthFirstValue = false;
                bool setExtrasPointsWidthSecondValue = false;
                float ExtrasPointsWidthFirstValue= 0;
                float ExtrasPointsWidthSecondValue= 0;
                bool setExtrasPointsXFirstValue = false;
                bool setExtrasPointsXSecondValue = false;
                float ExtrasPointsXFirstValue= 0;
                float ExtrasPointsXSecondValue= 0;
                bool setExtrasPointsYFirstValue = false;
                bool setExtrasPointsYSecondValue = false;
                float ExtrasPointsYFirstValue= 0;
                float ExtrasPointsYSecondValue= 0;
                bool setExtrasPointsLabelHeightFirstValue = false;
                bool setExtrasPointsLabelHeightSecondValue = false;
                float ExtrasPointsLabelHeightFirstValue= 0;
                float ExtrasPointsLabelHeightSecondValue= 0;
                bool setExtrasPointsLabelOutlineThicknessFirstValue = false;
                bool setExtrasPointsLabelOutlineThicknessSecondValue = false;
                int ExtrasPointsLabelOutlineThicknessFirstValue= 0;
                int ExtrasPointsLabelOutlineThicknessSecondValue= 0;
                bool setExtrasPointsLabelXFirstValue = false;
                bool setExtrasPointsLabelXSecondValue = false;
                float ExtrasPointsLabelXFirstValue= 0;
                float ExtrasPointsLabelXSecondValue= 0;
                bool setExtrasPointsLabelYFirstValue = false;
                bool setExtrasPointsLabelYSecondValue = false;
                float ExtrasPointsLabelYFirstValue= 0;
                float ExtrasPointsLabelYSecondValue= 0;
                bool setLevelPointsHeightFirstValue = false;
                bool setLevelPointsHeightSecondValue = false;
                float LevelPointsHeightFirstValue= 0;
                float LevelPointsHeightSecondValue= 0;
                bool setLevelPointsOutlineThicknessFirstValue = false;
                bool setLevelPointsOutlineThicknessSecondValue = false;
                int LevelPointsOutlineThicknessFirstValue= 0;
                int LevelPointsOutlineThicknessSecondValue= 0;
                bool setLevelPointsWidthFirstValue = false;
                bool setLevelPointsWidthSecondValue = false;
                float LevelPointsWidthFirstValue= 0;
                float LevelPointsWidthSecondValue= 0;
                bool setLevelPointsXFirstValue = false;
                bool setLevelPointsXSecondValue = false;
                float LevelPointsXFirstValue= 0;
                float LevelPointsXSecondValue= 0;
                bool setLevelPointsYFirstValue = false;
                bool setLevelPointsYSecondValue = false;
                float LevelPointsYFirstValue= 0;
                float LevelPointsYSecondValue= 0;
                bool setLevelPointsLabelOutlineThicknessFirstValue = false;
                bool setLevelPointsLabelOutlineThicknessSecondValue = false;
                int LevelPointsLabelOutlineThicknessFirstValue= 0;
                int LevelPointsLabelOutlineThicknessSecondValue= 0;
                bool setLevelPointsLabelXFirstValue = false;
                bool setLevelPointsLabelXSecondValue = false;
                float LevelPointsLabelXFirstValue= 0;
                float LevelPointsLabelXSecondValue= 0;
                bool setLevelPointsLabelYFirstValue = false;
                bool setLevelPointsLabelYSecondValue = false;
                float LevelPointsLabelYFirstValue= 0;
                float LevelPointsLabelYSecondValue= 0;
                bool setLifePointsHeightFirstValue = false;
                bool setLifePointsHeightSecondValue = false;
                float LifePointsHeightFirstValue= 0;
                float LifePointsHeightSecondValue= 0;
                bool setLifePointsOutlineThicknessFirstValue = false;
                bool setLifePointsOutlineThicknessSecondValue = false;
                int LifePointsOutlineThicknessFirstValue= 0;
                int LifePointsOutlineThicknessSecondValue= 0;
                bool setLifePointsXFirstValue = false;
                bool setLifePointsXSecondValue = false;
                float LifePointsXFirstValue= 0;
                float LifePointsXSecondValue= 0;
                bool setLifePointsYFirstValue = false;
                bool setLifePointsYSecondValue = false;
                float LifePointsYFirstValue= 0;
                float LifePointsYSecondValue= 0;
                bool setLifePointsLabelHeightFirstValue = false;
                bool setLifePointsLabelHeightSecondValue = false;
                float LifePointsLabelHeightFirstValue= 0;
                float LifePointsLabelHeightSecondValue= 0;
                bool setLifePointsLabelOutlineThicknessFirstValue = false;
                bool setLifePointsLabelOutlineThicknessSecondValue = false;
                int LifePointsLabelOutlineThicknessFirstValue= 0;
                int LifePointsLabelOutlineThicknessSecondValue= 0;
                bool setLifePointsLabelXFirstValue = false;
                bool setLifePointsLabelXSecondValue = false;
                float LifePointsLabelXFirstValue= 0;
                float LifePointsLabelXSecondValue= 0;
                bool setLifePointsLabelYFirstValue = false;
                bool setLifePointsLabelYSecondValue = false;
                float LifePointsLabelYFirstValue= 0;
                float LifePointsLabelYSecondValue= 0;
                bool setPlayNameFontSizeFirstValue = false;
                bool setPlayNameFontSizeSecondValue = false;
                int PlayNameFontSizeFirstValue= 0;
                int PlayNameFontSizeSecondValue= 0;
                bool setPlayNameHeightFirstValue = false;
                bool setPlayNameHeightSecondValue = false;
                float PlayNameHeightFirstValue= 0;
                float PlayNameHeightSecondValue= 0;
                bool setPlayNameOutlineThicknessFirstValue = false;
                bool setPlayNameOutlineThicknessSecondValue = false;
                int PlayNameOutlineThicknessFirstValue= 0;
                int PlayNameOutlineThicknessSecondValue= 0;
                bool setPlayNameWidthFirstValue = false;
                bool setPlayNameWidthSecondValue = false;
                float PlayNameWidthFirstValue= 0;
                float PlayNameWidthSecondValue= 0;
                bool setPlayNameXFirstValue = false;
                bool setPlayNameXSecondValue = false;
                float PlayNameXFirstValue= 0;
                float PlayNameXSecondValue= 0;
                bool setPlayNameYFirstValue = false;
                bool setPlayNameYSecondValue = false;
                float PlayNameYFirstValue= 0;
                float PlayNameYSecondValue= 0;
                bool setplayTimeOutlineThicknessFirstValue = false;
                bool setplayTimeOutlineThicknessSecondValue = false;
                int playTimeOutlineThicknessFirstValue= 0;
                int playTimeOutlineThicknessSecondValue= 0;
                bool setplayTimeXFirstValue = false;
                bool setplayTimeXSecondValue = false;
                float playTimeXFirstValue= 0;
                float playTimeXSecondValue= 0;
                bool setplayTimeYFirstValue = false;
                bool setplayTimeYSecondValue = false;
                float playTimeYFirstValue= 0;
                float playTimeYSecondValue= 0;
                bool setPointsDisplayHeightFirstValue = false;
                bool setPointsDisplayHeightSecondValue = false;
                float PointsDisplayHeightFirstValue= 0;
                float PointsDisplayHeightSecondValue= 0;
                bool setPointsDisplayWidthFirstValue = false;
                bool setPointsDisplayWidthSecondValue = false;
                float PointsDisplayWidthFirstValue= 0;
                float PointsDisplayWidthSecondValue= 0;
                bool setPointsDisplayXFirstValue = false;
                bool setPointsDisplayXSecondValue = false;
                float PointsDisplayXFirstValue= 0;
                float PointsDisplayXSecondValue= 0;
                bool setPointsDisplayYFirstValue = false;
                bool setPointsDisplayYSecondValue = false;
                float PointsDisplayYFirstValue= 0;
                float PointsDisplayYSecondValue= 0;
                bool setPointsEditHeightFirstValue = false;
                bool setPointsEditHeightSecondValue = false;
                float PointsEditHeightFirstValue= 0;
                float PointsEditHeightSecondValue= 0;
                bool setPointsEditWidthFirstValue = false;
                bool setPointsEditWidthSecondValue = false;
                float PointsEditWidthFirstValue= 0;
                float PointsEditWidthSecondValue= 0;
                bool setPointsEditXFirstValue = false;
                bool setPointsEditXSecondValue = false;
                float PointsEditXFirstValue= 0;
                float PointsEditXSecondValue= 0;
                bool setPointsEditYFirstValue = false;
                bool setPointsEditYSecondValue = false;
                float PointsEditYFirstValue= 0;
                float PointsEditYSecondValue= 0;
                bool setSpriteInstanceHeightFirstValue = false;
                bool setSpriteInstanceHeightSecondValue = false;
                float SpriteInstanceHeightFirstValue= 0;
                float SpriteInstanceHeightSecondValue= 0;
                bool setSpriteInstanceWidthFirstValue = false;
                bool setSpriteInstanceWidthSecondValue = false;
                float SpriteInstanceWidthFirstValue= 0;
                float SpriteInstanceWidthSecondValue= 0;
                bool setSpriteInstanceXFirstValue = false;
                bool setSpriteInstanceXSecondValue = false;
                float SpriteInstanceXFirstValue= 0;
                float SpriteInstanceXSecondValue= 0;
                bool setSpriteInstanceYFirstValue = false;
                bool setSpriteInstanceYSecondValue = false;
                float SpriteInstanceYFirstValue= 0;
                float SpriteInstanceYSecondValue= 0;
                bool setTextInstanceHeightFirstValue = false;
                bool setTextInstanceHeightSecondValue = false;
                float TextInstanceHeightFirstValue= 0;
                float TextInstanceHeightSecondValue= 0;
                bool setTextInstanceOutlineThicknessFirstValue = false;
                bool setTextInstanceOutlineThicknessSecondValue = false;
                int TextInstanceOutlineThicknessFirstValue= 0;
                int TextInstanceOutlineThicknessSecondValue= 0;
                bool setTextInstanceWidthFirstValue = false;
                bool setTextInstanceWidthSecondValue = false;
                float TextInstanceWidthFirstValue= 0;
                float TextInstanceWidthSecondValue= 0;
                bool setTextInstanceXFirstValue = false;
                bool setTextInstanceXSecondValue = false;
                float TextInstanceXFirstValue= 0;
                float TextInstanceXSecondValue= 0;
                bool setTextInstanceYFirstValue = false;
                bool setTextInstanceYSecondValue = false;
                float TextInstanceYFirstValue= 0;
                float TextInstanceYSecondValue= 0;
                bool setTimePointsHeightFirstValue = false;
                bool setTimePointsHeightSecondValue = false;
                float TimePointsHeightFirstValue= 0;
                float TimePointsHeightSecondValue= 0;
                bool setTimePointsOutlineThicknessFirstValue = false;
                bool setTimePointsOutlineThicknessSecondValue = false;
                int TimePointsOutlineThicknessFirstValue= 0;
                int TimePointsOutlineThicknessSecondValue= 0;
                bool setTimePointsXFirstValue = false;
                bool setTimePointsXSecondValue = false;
                float TimePointsXFirstValue= 0;
                float TimePointsXSecondValue= 0;
                bool setTimePointsYFirstValue = false;
                bool setTimePointsYSecondValue = false;
                float TimePointsYFirstValue= 0;
                float TimePointsYSecondValue= 0;
                bool setTimePointsLabelHeightFirstValue = false;
                bool setTimePointsLabelHeightSecondValue = false;
                float TimePointsLabelHeightFirstValue= 0;
                float TimePointsLabelHeightSecondValue= 0;
                bool setTimePointsLabelOutlineThicknessFirstValue = false;
                bool setTimePointsLabelOutlineThicknessSecondValue = false;
                int TimePointsLabelOutlineThicknessFirstValue= 0;
                int TimePointsLabelOutlineThicknessSecondValue= 0;
                bool setTimePointsLabelXFirstValue = false;
                bool setTimePointsLabelXSecondValue = false;
                float TimePointsLabelXFirstValue= 0;
                float TimePointsLabelXSecondValue= 0;
                bool setTimePointsLabelYFirstValue = false;
                bool setTimePointsLabelYSecondValue = false;
                float TimePointsLabelYFirstValue= 0;
                float TimePointsLabelYSecondValue= 0;
                bool setTotalPointsHeightFirstValue = false;
                bool setTotalPointsHeightSecondValue = false;
                float TotalPointsHeightFirstValue= 0;
                float TotalPointsHeightSecondValue= 0;
                bool setTotalPointsOutlineThicknessFirstValue = false;
                bool setTotalPointsOutlineThicknessSecondValue = false;
                int TotalPointsOutlineThicknessFirstValue= 0;
                int TotalPointsOutlineThicknessSecondValue= 0;
                bool setTotalPointsWidthFirstValue = false;
                bool setTotalPointsWidthSecondValue = false;
                float TotalPointsWidthFirstValue= 0;
                float TotalPointsWidthSecondValue= 0;
                bool setTotalPointsXFirstValue = false;
                bool setTotalPointsXSecondValue = false;
                float TotalPointsXFirstValue= 0;
                float TotalPointsXSecondValue= 0;
                bool setTotalPointsYFirstValue = false;
                bool setTotalPointsYSecondValue = false;
                float TotalPointsYFirstValue= 0;
                float TotalPointsYSecondValue= 0;
                bool setTotalPointsDisplayHeightFirstValue = false;
                bool setTotalPointsDisplayHeightSecondValue = false;
                float TotalPointsDisplayHeightFirstValue= 0;
                float TotalPointsDisplayHeightSecondValue= 0;
                bool setTotalPointsDisplayOutlineThicknessFirstValue = false;
                bool setTotalPointsDisplayOutlineThicknessSecondValue = false;
                int TotalPointsDisplayOutlineThicknessFirstValue= 0;
                int TotalPointsDisplayOutlineThicknessSecondValue= 0;
                bool setTotalPointsDisplayXFirstValue = false;
                bool setTotalPointsDisplayXSecondValue = false;
                float TotalPointsDisplayXFirstValue= 0;
                float TotalPointsDisplayXSecondValue= 0;
                bool setTotalPointsDisplayYFirstValue = false;
                bool setTotalPointsDisplayYSecondValue = false;
                float TotalPointsDisplayYFirstValue= 0;
                float TotalPointsDisplayYSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setAvatarHeightFirstValue = true;
                        AvatarHeightFirstValue = -59f;
                        if (interpolationValue < 1)
                        {
                            this.Avatar.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.Avatar.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("Avatar.SourceFile", "Xalakom.png");
                        }
                        setAvatarWidthFirstValue = true;
                        AvatarWidthFirstValue = -600f;
                        if (interpolationValue < 1)
                        {
                            this.Avatar.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setAvatarXFirstValue = true;
                        AvatarXFirstValue = 22f;
                        setAvatarYFirstValue = true;
                        AvatarYFirstValue = 30f;
                        setContainerInstanceHeightFirstValue = true;
                        ContainerInstanceHeightFirstValue = 14f;
                        if (interpolationValue < 1)
                        {
                            this.ContainerInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setContainerInstanceWidthFirstValue = true;
                        ContainerInstanceWidthFirstValue = 501f;
                        if (interpolationValue < 1)
                        {
                            this.ContainerInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setContainerInstanceXFirstValue = true;
                        ContainerInstanceXFirstValue = 21f;
                        setContainerInstanceYFirstValue = true;
                        ContainerInstanceYFirstValue = 37f;
                        setExtrasPointsHeightFirstValue = true;
                        ExtrasPointsHeightFirstValue = 0f;
                        setExtrasPointsOutlineThicknessFirstValue = true;
                        ExtrasPointsOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.ExtrasPoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                        }
                        if (interpolationValue < 1)
                        {
                            this.ExtrasPoints.Text = "0";
                        }
                        setExtrasPointsWidthFirstValue = true;
                        ExtrasPointsWidthFirstValue = 100f;
                        setExtrasPointsXFirstValue = true;
                        ExtrasPointsXFirstValue = 0f;
                        setExtrasPointsYFirstValue = true;
                        ExtrasPointsYFirstValue = 60f;
                        setExtrasPointsLabelHeightFirstValue = true;
                        ExtrasPointsLabelHeightFirstValue = 20f;
                        setExtrasPointsLabelOutlineThicknessFirstValue = true;
                        ExtrasPointsLabelOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.ExtrasPointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                        }
                        if (interpolationValue < 1)
                        {
                            this.ExtrasPointsLabel.Text = "Extra";
                        }
                        setExtrasPointsLabelXFirstValue = true;
                        ExtrasPointsLabelXFirstValue = 0f;
                        setExtrasPointsLabelYFirstValue = true;
                        ExtrasPointsLabelYFirstValue = 60f;
                        setLevelPointsHeightFirstValue = true;
                        LevelPointsHeightFirstValue = 20f;
                        setLevelPointsOutlineThicknessFirstValue = true;
                        LevelPointsOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.LevelPoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                        }
                        if (interpolationValue < 1)
                        {
                            this.LevelPoints.Text = "0";
                        }
                        setLevelPointsWidthFirstValue = true;
                        LevelPointsWidthFirstValue = 100f;
                        setLevelPointsXFirstValue = true;
                        LevelPointsXFirstValue = 0f;
                        setLevelPointsYFirstValue = true;
                        LevelPointsYFirstValue = 40f;
                        setLevelPointsLabelOutlineThicknessFirstValue = true;
                        LevelPointsLabelOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.LevelPointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                        }
                        if (interpolationValue < 1)
                        {
                            this.LevelPointsLabel.Text = "Level\n";
                        }
                        setLevelPointsLabelXFirstValue = true;
                        LevelPointsLabelXFirstValue = 0f;
                        setLevelPointsLabelYFirstValue = true;
                        LevelPointsLabelYFirstValue = 40f;
                        setLifePointsHeightFirstValue = true;
                        LifePointsHeightFirstValue = 20f;
                        setLifePointsOutlineThicknessFirstValue = true;
                        LifePointsOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.LifePoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                        }
                        if (interpolationValue < 1)
                        {
                            this.LifePoints.Text = "0";
                        }
                        setLifePointsXFirstValue = true;
                        LifePointsXFirstValue = 0f;
                        setLifePointsYFirstValue = true;
                        LifePointsYFirstValue = 20f;
                        setLifePointsLabelHeightFirstValue = true;
                        LifePointsLabelHeightFirstValue = 20f;
                        setLifePointsLabelOutlineThicknessFirstValue = true;
                        LifePointsLabelOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.LifePointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                        }
                        if (interpolationValue < 1)
                        {
                            this.LifePointsLabel.Text = "Vida\n";
                        }
                        setLifePointsLabelXFirstValue = true;
                        LifePointsLabelXFirstValue = 0f;
                        setLifePointsLabelYFirstValue = true;
                        LifePointsLabelYFirstValue = 20f;
                        if (interpolationValue < 1)
                        {
                            this.PlayName.Font = "Impact";
                        }
                        setPlayNameFontSizeFirstValue = true;
                        PlayNameFontSizeFirstValue = 22;
                        setPlayNameHeightFirstValue = true;
                        PlayNameHeightFirstValue = 44f;
                        if (interpolationValue < 1)
                        {
                            this.PlayName.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        setPlayNameOutlineThicknessFirstValue = true;
                        PlayNameOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.PlayName.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.PlayName.Text = "Play name\n";
                        }
                        setPlayNameWidthFirstValue = true;
                        PlayNameWidthFirstValue = 178f;
                        setPlayNameXFirstValue = true;
                        PlayNameXFirstValue = 98f;
                        setPlayNameYFirstValue = true;
                        PlayNameYFirstValue = 63f;
                        setplayTimeOutlineThicknessFirstValue = true;
                        playTimeOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.playTime.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.playTime.Text = "Play Time";
                        }
                        setplayTimeXFirstValue = true;
                        playTimeXFirstValue = 99f;
                        setplayTimeYFirstValue = true;
                        playTimeYFirstValue = 97f;
                        setPointsDisplayHeightFirstValue = true;
                        PointsDisplayHeightFirstValue = -37f;
                        if (interpolationValue < 1)
                        {
                            this.PointsDisplay.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.PointsDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setPointsDisplayWidthFirstValue = true;
                        PointsDisplayWidthFirstValue = -583f;
                        if (interpolationValue < 1)
                        {
                            this.PointsDisplay.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setPointsDisplayXFirstValue = true;
                        PointsDisplayXFirstValue = 16f;
                        setPointsDisplayYFirstValue = true;
                        PointsDisplayYFirstValue = 193f;
                        setPointsEditHeightFirstValue = true;
                        PointsEditHeightFirstValue = -41f;
                        if (interpolationValue < 1)
                        {
                            this.PointsEdit.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.PointsEdit.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setPointsEditWidthFirstValue = true;
                        PointsEditWidthFirstValue = -572f;
                        if (interpolationValue < 1)
                        {
                            this.PointsEdit.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setPointsEditXFirstValue = true;
                        PointsEditXFirstValue = 82f;
                        setPointsEditYFirstValue = true;
                        PointsEditYFirstValue = 195f;
                        setSpriteInstanceHeightFirstValue = true;
                        SpriteInstanceHeightFirstValue = 243f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown.png");
                        }
                        setSpriteInstanceWidthFirstValue = true;
                        SpriteInstanceWidthFirstValue = 303f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setSpriteInstanceXFirstValue = true;
                        SpriteInstanceXFirstValue = -5f;
                        setSpriteInstanceYFirstValue = true;
                        SpriteInstanceYFirstValue = 1f;
                        if (interpolationValue < 1)
                        {
                            this.TextInstance.Font = "Impact";
                        }
                        setTextInstanceHeightFirstValue = true;
                        TextInstanceHeightFirstValue = 23f;
                        setTextInstanceOutlineThicknessFirstValue = true;
                        TextInstanceOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.TextInstance.Text = "Pontos:\n";
                        }
                        setTextInstanceWidthFirstValue = true;
                        TextInstanceWidthFirstValue = 72f;
                        setTextInstanceXFirstValue = true;
                        TextInstanceXFirstValue = 15f;
                        setTextInstanceYFirstValue = true;
                        TextInstanceYFirstValue = 204f;
                        setTimePointsHeightFirstValue = true;
                        TimePointsHeightFirstValue = 20f;
                        setTimePointsOutlineThicknessFirstValue = true;
                        TimePointsOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.TimePoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                        }
                        if (interpolationValue < 1)
                        {
                            this.TimePoints.Text = "0";
                        }
                        setTimePointsXFirstValue = true;
                        TimePointsXFirstValue = 0f;
                        setTimePointsYFirstValue = true;
                        TimePointsYFirstValue = 0f;
                        setTimePointsLabelHeightFirstValue = true;
                        TimePointsLabelHeightFirstValue = 20f;
                        setTimePointsLabelOutlineThicknessFirstValue = true;
                        TimePointsLabelOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.TimePointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                        }
                        if (interpolationValue < 1)
                        {
                            this.TimePointsLabel.Text = "Tempo";
                        }
                        setTimePointsLabelXFirstValue = true;
                        TimePointsLabelXFirstValue = 0f;
                        setTimePointsLabelYFirstValue = true;
                        TimePointsLabelYFirstValue = 0f;
                        setTotalPointsHeightFirstValue = true;
                        TotalPointsHeightFirstValue = 0f;
                        setTotalPointsOutlineThicknessFirstValue = true;
                        TotalPointsOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.TotalPoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                        }
                        if (interpolationValue < 1)
                        {
                            this.TotalPoints.Text = "00\n\n";
                        }
                        setTotalPointsWidthFirstValue = true;
                        TotalPointsWidthFirstValue = 100f;
                        setTotalPointsXFirstValue = true;
                        TotalPointsXFirstValue = 0f;
                        setTotalPointsYFirstValue = true;
                        TotalPointsYFirstValue = 80f;
                        setTotalPointsDisplayHeightFirstValue = true;
                        TotalPointsDisplayHeightFirstValue = 20f;
                        setTotalPointsDisplayOutlineThicknessFirstValue = true;
                        TotalPointsDisplayOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.TotalPointsDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                        }
                        if (interpolationValue < 1)
                        {
                            this.TotalPointsDisplay.Text = "Total";
                        }
                        setTotalPointsDisplayXFirstValue = true;
                        TotalPointsDisplayXFirstValue = 0f;
                        setTotalPointsDisplayYFirstValue = true;
                        TotalPointsDisplayYFirstValue = 80f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setAvatarHeightSecondValue = true;
                        AvatarHeightSecondValue = -59f;
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("Avatar.SourceFile", "Xalakom.png");
                        }
                        setAvatarWidthSecondValue = true;
                        AvatarWidthSecondValue = -600f;
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setAvatarXSecondValue = true;
                        AvatarXSecondValue = 22f;
                        setAvatarYSecondValue = true;
                        AvatarYSecondValue = 30f;
                        setContainerInstanceHeightSecondValue = true;
                        ContainerInstanceHeightSecondValue = 14f;
                        if (interpolationValue >= 1)
                        {
                            this.ContainerInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setContainerInstanceWidthSecondValue = true;
                        ContainerInstanceWidthSecondValue = 501f;
                        if (interpolationValue >= 1)
                        {
                            this.ContainerInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setContainerInstanceXSecondValue = true;
                        ContainerInstanceXSecondValue = 21f;
                        setContainerInstanceYSecondValue = true;
                        ContainerInstanceYSecondValue = 37f;
                        setExtrasPointsHeightSecondValue = true;
                        ExtrasPointsHeightSecondValue = 0f;
                        setExtrasPointsOutlineThicknessSecondValue = true;
                        ExtrasPointsOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.ExtrasPoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ExtrasPoints.Text = "0";
                        }
                        setExtrasPointsWidthSecondValue = true;
                        ExtrasPointsWidthSecondValue = 100f;
                        setExtrasPointsXSecondValue = true;
                        ExtrasPointsXSecondValue = 0f;
                        setExtrasPointsYSecondValue = true;
                        ExtrasPointsYSecondValue = 60f;
                        setExtrasPointsLabelHeightSecondValue = true;
                        ExtrasPointsLabelHeightSecondValue = 20f;
                        setExtrasPointsLabelOutlineThicknessSecondValue = true;
                        ExtrasPointsLabelOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.ExtrasPointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ExtrasPointsLabel.Text = "Extra";
                        }
                        setExtrasPointsLabelXSecondValue = true;
                        ExtrasPointsLabelXSecondValue = 0f;
                        setExtrasPointsLabelYSecondValue = true;
                        ExtrasPointsLabelYSecondValue = 60f;
                        setLevelPointsHeightSecondValue = true;
                        LevelPointsHeightSecondValue = 20f;
                        setLevelPointsOutlineThicknessSecondValue = true;
                        LevelPointsOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.LevelPoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.LevelPoints.Text = "0";
                        }
                        setLevelPointsWidthSecondValue = true;
                        LevelPointsWidthSecondValue = 100f;
                        setLevelPointsXSecondValue = true;
                        LevelPointsXSecondValue = 0f;
                        setLevelPointsYSecondValue = true;
                        LevelPointsYSecondValue = 40f;
                        setLevelPointsLabelOutlineThicknessSecondValue = true;
                        LevelPointsLabelOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.LevelPointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.LevelPointsLabel.Text = "Level\n";
                        }
                        setLevelPointsLabelXSecondValue = true;
                        LevelPointsLabelXSecondValue = 0f;
                        setLevelPointsLabelYSecondValue = true;
                        LevelPointsLabelYSecondValue = 40f;
                        setLifePointsHeightSecondValue = true;
                        LifePointsHeightSecondValue = 20f;
                        setLifePointsOutlineThicknessSecondValue = true;
                        LifePointsOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.LifePoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.LifePoints.Text = "0";
                        }
                        setLifePointsXSecondValue = true;
                        LifePointsXSecondValue = 0f;
                        setLifePointsYSecondValue = true;
                        LifePointsYSecondValue = 20f;
                        setLifePointsLabelHeightSecondValue = true;
                        LifePointsLabelHeightSecondValue = 20f;
                        setLifePointsLabelOutlineThicknessSecondValue = true;
                        LifePointsLabelOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.LifePointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.LifePointsLabel.Text = "Vida\n";
                        }
                        setLifePointsLabelXSecondValue = true;
                        LifePointsLabelXSecondValue = 0f;
                        setLifePointsLabelYSecondValue = true;
                        LifePointsLabelYSecondValue = 20f;
                        if (interpolationValue >= 1)
                        {
                            this.PlayName.Font = "Impact";
                        }
                        setPlayNameFontSizeSecondValue = true;
                        PlayNameFontSizeSecondValue = 22;
                        setPlayNameHeightSecondValue = true;
                        PlayNameHeightSecondValue = 44f;
                        if (interpolationValue >= 1)
                        {
                            this.PlayName.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        setPlayNameOutlineThicknessSecondValue = true;
                        PlayNameOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.PlayName.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PlayName.Text = "Play name\n";
                        }
                        setPlayNameWidthSecondValue = true;
                        PlayNameWidthSecondValue = 178f;
                        setPlayNameXSecondValue = true;
                        PlayNameXSecondValue = 98f;
                        setPlayNameYSecondValue = true;
                        PlayNameYSecondValue = 63f;
                        setplayTimeOutlineThicknessSecondValue = true;
                        playTimeOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.playTime.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.playTime.Text = "Play Time";
                        }
                        setplayTimeXSecondValue = true;
                        playTimeXSecondValue = 99f;
                        setplayTimeYSecondValue = true;
                        playTimeYSecondValue = 97f;
                        setPointsDisplayHeightSecondValue = true;
                        PointsDisplayHeightSecondValue = -37f;
                        if (interpolationValue >= 1)
                        {
                            this.PointsDisplay.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PointsDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setPointsDisplayWidthSecondValue = true;
                        PointsDisplayWidthSecondValue = -583f;
                        if (interpolationValue >= 1)
                        {
                            this.PointsDisplay.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setPointsDisplayXSecondValue = true;
                        PointsDisplayXSecondValue = 16f;
                        setPointsDisplayYSecondValue = true;
                        PointsDisplayYSecondValue = 193f;
                        setPointsEditHeightSecondValue = true;
                        PointsEditHeightSecondValue = -41f;
                        if (interpolationValue >= 1)
                        {
                            this.PointsEdit.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PointsEdit.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setPointsEditWidthSecondValue = true;
                        PointsEditWidthSecondValue = -572f;
                        if (interpolationValue >= 1)
                        {
                            this.PointsEdit.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setPointsEditXSecondValue = true;
                        PointsEditXSecondValue = 82f;
                        setPointsEditYSecondValue = true;
                        PointsEditYSecondValue = 195f;
                        setSpriteInstanceHeightSecondValue = true;
                        SpriteInstanceHeightSecondValue = 243f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown.png");
                        }
                        setSpriteInstanceWidthSecondValue = true;
                        SpriteInstanceWidthSecondValue = 303f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setSpriteInstanceXSecondValue = true;
                        SpriteInstanceXSecondValue = -5f;
                        setSpriteInstanceYSecondValue = true;
                        SpriteInstanceYSecondValue = 1f;
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance.Font = "Impact";
                        }
                        setTextInstanceHeightSecondValue = true;
                        TextInstanceHeightSecondValue = 23f;
                        setTextInstanceOutlineThicknessSecondValue = true;
                        TextInstanceOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance.Text = "Pontos:\n";
                        }
                        setTextInstanceWidthSecondValue = true;
                        TextInstanceWidthSecondValue = 72f;
                        setTextInstanceXSecondValue = true;
                        TextInstanceXSecondValue = 15f;
                        setTextInstanceYSecondValue = true;
                        TextInstanceYSecondValue = 204f;
                        setTimePointsHeightSecondValue = true;
                        TimePointsHeightSecondValue = 20f;
                        setTimePointsOutlineThicknessSecondValue = true;
                        TimePointsOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.TimePoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TimePoints.Text = "0";
                        }
                        setTimePointsXSecondValue = true;
                        TimePointsXSecondValue = 0f;
                        setTimePointsYSecondValue = true;
                        TimePointsYSecondValue = 0f;
                        setTimePointsLabelHeightSecondValue = true;
                        TimePointsLabelHeightSecondValue = 20f;
                        setTimePointsLabelOutlineThicknessSecondValue = true;
                        TimePointsLabelOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.TimePointsLabel.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TimePointsLabel.Text = "Tempo";
                        }
                        setTimePointsLabelXSecondValue = true;
                        TimePointsLabelXSecondValue = 0f;
                        setTimePointsLabelYSecondValue = true;
                        TimePointsLabelYSecondValue = 0f;
                        setTotalPointsHeightSecondValue = true;
                        TotalPointsHeightSecondValue = 0f;
                        setTotalPointsOutlineThicknessSecondValue = true;
                        TotalPointsOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.TotalPoints.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsEdit");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TotalPoints.Text = "00\n\n";
                        }
                        setTotalPointsWidthSecondValue = true;
                        TotalPointsWidthSecondValue = 100f;
                        setTotalPointsXSecondValue = true;
                        TotalPointsXSecondValue = 0f;
                        setTotalPointsYSecondValue = true;
                        TotalPointsYSecondValue = 80f;
                        setTotalPointsDisplayHeightSecondValue = true;
                        TotalPointsDisplayHeightSecondValue = 20f;
                        setTotalPointsDisplayOutlineThicknessSecondValue = true;
                        TotalPointsDisplayOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.TotalPointsDisplay.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "PointsDisplay");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TotalPointsDisplay.Text = "Total";
                        }
                        setTotalPointsDisplayXSecondValue = true;
                        TotalPointsDisplayXSecondValue = 0f;
                        setTotalPointsDisplayYSecondValue = true;
                        TotalPointsDisplayYSecondValue = 80f;
                        break;
                }
                if (setAvatarHeightFirstValue && setAvatarHeightSecondValue)
                {
                    Avatar.Height = AvatarHeightFirstValue * (1 - interpolationValue) + AvatarHeightSecondValue * interpolationValue;
                }
                if (setAvatarWidthFirstValue && setAvatarWidthSecondValue)
                {
                    Avatar.Width = AvatarWidthFirstValue * (1 - interpolationValue) + AvatarWidthSecondValue * interpolationValue;
                }
                if (setAvatarXFirstValue && setAvatarXSecondValue)
                {
                    Avatar.X = AvatarXFirstValue * (1 - interpolationValue) + AvatarXSecondValue * interpolationValue;
                }
                if (setAvatarYFirstValue && setAvatarYSecondValue)
                {
                    Avatar.Y = AvatarYFirstValue * (1 - interpolationValue) + AvatarYSecondValue * interpolationValue;
                }
                if (setContainerInstanceHeightFirstValue && setContainerInstanceHeightSecondValue)
                {
                    ContainerInstance.Height = ContainerInstanceHeightFirstValue * (1 - interpolationValue) + ContainerInstanceHeightSecondValue * interpolationValue;
                }
                if (setContainerInstanceWidthFirstValue && setContainerInstanceWidthSecondValue)
                {
                    ContainerInstance.Width = ContainerInstanceWidthFirstValue * (1 - interpolationValue) + ContainerInstanceWidthSecondValue * interpolationValue;
                }
                if (setContainerInstanceXFirstValue && setContainerInstanceXSecondValue)
                {
                    ContainerInstance.X = ContainerInstanceXFirstValue * (1 - interpolationValue) + ContainerInstanceXSecondValue * interpolationValue;
                }
                if (setContainerInstanceYFirstValue && setContainerInstanceYSecondValue)
                {
                    ContainerInstance.Y = ContainerInstanceYFirstValue * (1 - interpolationValue) + ContainerInstanceYSecondValue * interpolationValue;
                }
                if (setExtrasPointsHeightFirstValue && setExtrasPointsHeightSecondValue)
                {
                    ExtrasPoints.Height = ExtrasPointsHeightFirstValue * (1 - interpolationValue) + ExtrasPointsHeightSecondValue * interpolationValue;
                }
                if (setExtrasPointsOutlineThicknessFirstValue && setExtrasPointsOutlineThicknessSecondValue)
                {
                    ExtrasPoints.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(ExtrasPointsOutlineThicknessFirstValue* (1 - interpolationValue) + ExtrasPointsOutlineThicknessSecondValue * interpolationValue);
                }
                if (setExtrasPointsWidthFirstValue && setExtrasPointsWidthSecondValue)
                {
                    ExtrasPoints.Width = ExtrasPointsWidthFirstValue * (1 - interpolationValue) + ExtrasPointsWidthSecondValue * interpolationValue;
                }
                if (setExtrasPointsXFirstValue && setExtrasPointsXSecondValue)
                {
                    ExtrasPoints.X = ExtrasPointsXFirstValue * (1 - interpolationValue) + ExtrasPointsXSecondValue * interpolationValue;
                }
                if (setExtrasPointsYFirstValue && setExtrasPointsYSecondValue)
                {
                    ExtrasPoints.Y = ExtrasPointsYFirstValue * (1 - interpolationValue) + ExtrasPointsYSecondValue * interpolationValue;
                }
                if (setExtrasPointsLabelHeightFirstValue && setExtrasPointsLabelHeightSecondValue)
                {
                    ExtrasPointsLabel.Height = ExtrasPointsLabelHeightFirstValue * (1 - interpolationValue) + ExtrasPointsLabelHeightSecondValue * interpolationValue;
                }
                if (setExtrasPointsLabelOutlineThicknessFirstValue && setExtrasPointsLabelOutlineThicknessSecondValue)
                {
                    ExtrasPointsLabel.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(ExtrasPointsLabelOutlineThicknessFirstValue* (1 - interpolationValue) + ExtrasPointsLabelOutlineThicknessSecondValue * interpolationValue);
                }
                if (setExtrasPointsLabelXFirstValue && setExtrasPointsLabelXSecondValue)
                {
                    ExtrasPointsLabel.X = ExtrasPointsLabelXFirstValue * (1 - interpolationValue) + ExtrasPointsLabelXSecondValue * interpolationValue;
                }
                if (setExtrasPointsLabelYFirstValue && setExtrasPointsLabelYSecondValue)
                {
                    ExtrasPointsLabel.Y = ExtrasPointsLabelYFirstValue * (1 - interpolationValue) + ExtrasPointsLabelYSecondValue * interpolationValue;
                }
                if (setLevelPointsHeightFirstValue && setLevelPointsHeightSecondValue)
                {
                    LevelPoints.Height = LevelPointsHeightFirstValue * (1 - interpolationValue) + LevelPointsHeightSecondValue * interpolationValue;
                }
                if (setLevelPointsOutlineThicknessFirstValue && setLevelPointsOutlineThicknessSecondValue)
                {
                    LevelPoints.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(LevelPointsOutlineThicknessFirstValue* (1 - interpolationValue) + LevelPointsOutlineThicknessSecondValue * interpolationValue);
                }
                if (setLevelPointsWidthFirstValue && setLevelPointsWidthSecondValue)
                {
                    LevelPoints.Width = LevelPointsWidthFirstValue * (1 - interpolationValue) + LevelPointsWidthSecondValue * interpolationValue;
                }
                if (setLevelPointsXFirstValue && setLevelPointsXSecondValue)
                {
                    LevelPoints.X = LevelPointsXFirstValue * (1 - interpolationValue) + LevelPointsXSecondValue * interpolationValue;
                }
                if (setLevelPointsYFirstValue && setLevelPointsYSecondValue)
                {
                    LevelPoints.Y = LevelPointsYFirstValue * (1 - interpolationValue) + LevelPointsYSecondValue * interpolationValue;
                }
                if (setLevelPointsLabelOutlineThicknessFirstValue && setLevelPointsLabelOutlineThicknessSecondValue)
                {
                    LevelPointsLabel.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(LevelPointsLabelOutlineThicknessFirstValue* (1 - interpolationValue) + LevelPointsLabelOutlineThicknessSecondValue * interpolationValue);
                }
                if (setLevelPointsLabelXFirstValue && setLevelPointsLabelXSecondValue)
                {
                    LevelPointsLabel.X = LevelPointsLabelXFirstValue * (1 - interpolationValue) + LevelPointsLabelXSecondValue * interpolationValue;
                }
                if (setLevelPointsLabelYFirstValue && setLevelPointsLabelYSecondValue)
                {
                    LevelPointsLabel.Y = LevelPointsLabelYFirstValue * (1 - interpolationValue) + LevelPointsLabelYSecondValue * interpolationValue;
                }
                if (setLifePointsHeightFirstValue && setLifePointsHeightSecondValue)
                {
                    LifePoints.Height = LifePointsHeightFirstValue * (1 - interpolationValue) + LifePointsHeightSecondValue * interpolationValue;
                }
                if (setLifePointsOutlineThicknessFirstValue && setLifePointsOutlineThicknessSecondValue)
                {
                    LifePoints.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(LifePointsOutlineThicknessFirstValue* (1 - interpolationValue) + LifePointsOutlineThicknessSecondValue * interpolationValue);
                }
                if (setLifePointsXFirstValue && setLifePointsXSecondValue)
                {
                    LifePoints.X = LifePointsXFirstValue * (1 - interpolationValue) + LifePointsXSecondValue * interpolationValue;
                }
                if (setLifePointsYFirstValue && setLifePointsYSecondValue)
                {
                    LifePoints.Y = LifePointsYFirstValue * (1 - interpolationValue) + LifePointsYSecondValue * interpolationValue;
                }
                if (setLifePointsLabelHeightFirstValue && setLifePointsLabelHeightSecondValue)
                {
                    LifePointsLabel.Height = LifePointsLabelHeightFirstValue * (1 - interpolationValue) + LifePointsLabelHeightSecondValue * interpolationValue;
                }
                if (setLifePointsLabelOutlineThicknessFirstValue && setLifePointsLabelOutlineThicknessSecondValue)
                {
                    LifePointsLabel.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(LifePointsLabelOutlineThicknessFirstValue* (1 - interpolationValue) + LifePointsLabelOutlineThicknessSecondValue * interpolationValue);
                }
                if (setLifePointsLabelXFirstValue && setLifePointsLabelXSecondValue)
                {
                    LifePointsLabel.X = LifePointsLabelXFirstValue * (1 - interpolationValue) + LifePointsLabelXSecondValue * interpolationValue;
                }
                if (setLifePointsLabelYFirstValue && setLifePointsLabelYSecondValue)
                {
                    LifePointsLabel.Y = LifePointsLabelYFirstValue * (1 - interpolationValue) + LifePointsLabelYSecondValue * interpolationValue;
                }
                if (setPlayNameFontSizeFirstValue && setPlayNameFontSizeSecondValue)
                {
                    PlayName.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(PlayNameFontSizeFirstValue* (1 - interpolationValue) + PlayNameFontSizeSecondValue * interpolationValue);
                }
                if (setPlayNameHeightFirstValue && setPlayNameHeightSecondValue)
                {
                    PlayName.Height = PlayNameHeightFirstValue * (1 - interpolationValue) + PlayNameHeightSecondValue * interpolationValue;
                }
                if (setPlayNameOutlineThicknessFirstValue && setPlayNameOutlineThicknessSecondValue)
                {
                    PlayName.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(PlayNameOutlineThicknessFirstValue* (1 - interpolationValue) + PlayNameOutlineThicknessSecondValue * interpolationValue);
                }
                if (setPlayNameWidthFirstValue && setPlayNameWidthSecondValue)
                {
                    PlayName.Width = PlayNameWidthFirstValue * (1 - interpolationValue) + PlayNameWidthSecondValue * interpolationValue;
                }
                if (setPlayNameXFirstValue && setPlayNameXSecondValue)
                {
                    PlayName.X = PlayNameXFirstValue * (1 - interpolationValue) + PlayNameXSecondValue * interpolationValue;
                }
                if (setPlayNameYFirstValue && setPlayNameYSecondValue)
                {
                    PlayName.Y = PlayNameYFirstValue * (1 - interpolationValue) + PlayNameYSecondValue * interpolationValue;
                }
                if (setplayTimeOutlineThicknessFirstValue && setplayTimeOutlineThicknessSecondValue)
                {
                    playTime.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(playTimeOutlineThicknessFirstValue* (1 - interpolationValue) + playTimeOutlineThicknessSecondValue * interpolationValue);
                }
                if (setplayTimeXFirstValue && setplayTimeXSecondValue)
                {
                    playTime.X = playTimeXFirstValue * (1 - interpolationValue) + playTimeXSecondValue * interpolationValue;
                }
                if (setplayTimeYFirstValue && setplayTimeYSecondValue)
                {
                    playTime.Y = playTimeYFirstValue * (1 - interpolationValue) + playTimeYSecondValue * interpolationValue;
                }
                if (setPointsDisplayHeightFirstValue && setPointsDisplayHeightSecondValue)
                {
                    PointsDisplay.Height = PointsDisplayHeightFirstValue * (1 - interpolationValue) + PointsDisplayHeightSecondValue * interpolationValue;
                }
                if (setPointsDisplayWidthFirstValue && setPointsDisplayWidthSecondValue)
                {
                    PointsDisplay.Width = PointsDisplayWidthFirstValue * (1 - interpolationValue) + PointsDisplayWidthSecondValue * interpolationValue;
                }
                if (setPointsDisplayXFirstValue && setPointsDisplayXSecondValue)
                {
                    PointsDisplay.X = PointsDisplayXFirstValue * (1 - interpolationValue) + PointsDisplayXSecondValue * interpolationValue;
                }
                if (setPointsDisplayYFirstValue && setPointsDisplayYSecondValue)
                {
                    PointsDisplay.Y = PointsDisplayYFirstValue * (1 - interpolationValue) + PointsDisplayYSecondValue * interpolationValue;
                }
                if (setPointsEditHeightFirstValue && setPointsEditHeightSecondValue)
                {
                    PointsEdit.Height = PointsEditHeightFirstValue * (1 - interpolationValue) + PointsEditHeightSecondValue * interpolationValue;
                }
                if (setPointsEditWidthFirstValue && setPointsEditWidthSecondValue)
                {
                    PointsEdit.Width = PointsEditWidthFirstValue * (1 - interpolationValue) + PointsEditWidthSecondValue * interpolationValue;
                }
                if (setPointsEditXFirstValue && setPointsEditXSecondValue)
                {
                    PointsEdit.X = PointsEditXFirstValue * (1 - interpolationValue) + PointsEditXSecondValue * interpolationValue;
                }
                if (setPointsEditYFirstValue && setPointsEditYSecondValue)
                {
                    PointsEdit.Y = PointsEditYFirstValue * (1 - interpolationValue) + PointsEditYSecondValue * interpolationValue;
                }
                if (setSpriteInstanceHeightFirstValue && setSpriteInstanceHeightSecondValue)
                {
                    SpriteInstance.Height = SpriteInstanceHeightFirstValue * (1 - interpolationValue) + SpriteInstanceHeightSecondValue * interpolationValue;
                }
                if (setSpriteInstanceWidthFirstValue && setSpriteInstanceWidthSecondValue)
                {
                    SpriteInstance.Width = SpriteInstanceWidthFirstValue * (1 - interpolationValue) + SpriteInstanceWidthSecondValue * interpolationValue;
                }
                if (setSpriteInstanceXFirstValue && setSpriteInstanceXSecondValue)
                {
                    SpriteInstance.X = SpriteInstanceXFirstValue * (1 - interpolationValue) + SpriteInstanceXSecondValue * interpolationValue;
                }
                if (setSpriteInstanceYFirstValue && setSpriteInstanceYSecondValue)
                {
                    SpriteInstance.Y = SpriteInstanceYFirstValue * (1 - interpolationValue) + SpriteInstanceYSecondValue * interpolationValue;
                }
                if (setTextInstanceHeightFirstValue && setTextInstanceHeightSecondValue)
                {
                    TextInstance.Height = TextInstanceHeightFirstValue * (1 - interpolationValue) + TextInstanceHeightSecondValue * interpolationValue;
                }
                if (setTextInstanceOutlineThicknessFirstValue && setTextInstanceOutlineThicknessSecondValue)
                {
                    TextInstance.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstanceOutlineThicknessFirstValue* (1 - interpolationValue) + TextInstanceOutlineThicknessSecondValue * interpolationValue);
                }
                if (setTextInstanceWidthFirstValue && setTextInstanceWidthSecondValue)
                {
                    TextInstance.Width = TextInstanceWidthFirstValue * (1 - interpolationValue) + TextInstanceWidthSecondValue * interpolationValue;
                }
                if (setTextInstanceXFirstValue && setTextInstanceXSecondValue)
                {
                    TextInstance.X = TextInstanceXFirstValue * (1 - interpolationValue) + TextInstanceXSecondValue * interpolationValue;
                }
                if (setTextInstanceYFirstValue && setTextInstanceYSecondValue)
                {
                    TextInstance.Y = TextInstanceYFirstValue * (1 - interpolationValue) + TextInstanceYSecondValue * interpolationValue;
                }
                if (setTimePointsHeightFirstValue && setTimePointsHeightSecondValue)
                {
                    TimePoints.Height = TimePointsHeightFirstValue * (1 - interpolationValue) + TimePointsHeightSecondValue * interpolationValue;
                }
                if (setTimePointsOutlineThicknessFirstValue && setTimePointsOutlineThicknessSecondValue)
                {
                    TimePoints.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TimePointsOutlineThicknessFirstValue* (1 - interpolationValue) + TimePointsOutlineThicknessSecondValue * interpolationValue);
                }
                if (setTimePointsXFirstValue && setTimePointsXSecondValue)
                {
                    TimePoints.X = TimePointsXFirstValue * (1 - interpolationValue) + TimePointsXSecondValue * interpolationValue;
                }
                if (setTimePointsYFirstValue && setTimePointsYSecondValue)
                {
                    TimePoints.Y = TimePointsYFirstValue * (1 - interpolationValue) + TimePointsYSecondValue * interpolationValue;
                }
                if (setTimePointsLabelHeightFirstValue && setTimePointsLabelHeightSecondValue)
                {
                    TimePointsLabel.Height = TimePointsLabelHeightFirstValue * (1 - interpolationValue) + TimePointsLabelHeightSecondValue * interpolationValue;
                }
                if (setTimePointsLabelOutlineThicknessFirstValue && setTimePointsLabelOutlineThicknessSecondValue)
                {
                    TimePointsLabel.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TimePointsLabelOutlineThicknessFirstValue* (1 - interpolationValue) + TimePointsLabelOutlineThicknessSecondValue * interpolationValue);
                }
                if (setTimePointsLabelXFirstValue && setTimePointsLabelXSecondValue)
                {
                    TimePointsLabel.X = TimePointsLabelXFirstValue * (1 - interpolationValue) + TimePointsLabelXSecondValue * interpolationValue;
                }
                if (setTimePointsLabelYFirstValue && setTimePointsLabelYSecondValue)
                {
                    TimePointsLabel.Y = TimePointsLabelYFirstValue * (1 - interpolationValue) + TimePointsLabelYSecondValue * interpolationValue;
                }
                if (setTotalPointsHeightFirstValue && setTotalPointsHeightSecondValue)
                {
                    TotalPoints.Height = TotalPointsHeightFirstValue * (1 - interpolationValue) + TotalPointsHeightSecondValue * interpolationValue;
                }
                if (setTotalPointsOutlineThicknessFirstValue && setTotalPointsOutlineThicknessSecondValue)
                {
                    TotalPoints.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TotalPointsOutlineThicknessFirstValue* (1 - interpolationValue) + TotalPointsOutlineThicknessSecondValue * interpolationValue);
                }
                if (setTotalPointsWidthFirstValue && setTotalPointsWidthSecondValue)
                {
                    TotalPoints.Width = TotalPointsWidthFirstValue * (1 - interpolationValue) + TotalPointsWidthSecondValue * interpolationValue;
                }
                if (setTotalPointsXFirstValue && setTotalPointsXSecondValue)
                {
                    TotalPoints.X = TotalPointsXFirstValue * (1 - interpolationValue) + TotalPointsXSecondValue * interpolationValue;
                }
                if (setTotalPointsYFirstValue && setTotalPointsYSecondValue)
                {
                    TotalPoints.Y = TotalPointsYFirstValue * (1 - interpolationValue) + TotalPointsYSecondValue * interpolationValue;
                }
                if (setTotalPointsDisplayHeightFirstValue && setTotalPointsDisplayHeightSecondValue)
                {
                    TotalPointsDisplay.Height = TotalPointsDisplayHeightFirstValue * (1 - interpolationValue) + TotalPointsDisplayHeightSecondValue * interpolationValue;
                }
                if (setTotalPointsDisplayOutlineThicknessFirstValue && setTotalPointsDisplayOutlineThicknessSecondValue)
                {
                    TotalPointsDisplay.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TotalPointsDisplayOutlineThicknessFirstValue* (1 - interpolationValue) + TotalPointsDisplayOutlineThicknessSecondValue * interpolationValue);
                }
                if (setTotalPointsDisplayXFirstValue && setTotalPointsDisplayXSecondValue)
                {
                    TotalPointsDisplay.X = TotalPointsDisplayXFirstValue * (1 - interpolationValue) + TotalPointsDisplayXSecondValue * interpolationValue;
                }
                if (setTotalPointsDisplayYFirstValue && setTotalPointsDisplayYSecondValue)
                {
                    TotalPointsDisplay.Y = TotalPointsDisplayYFirstValue * (1 - interpolationValue) + TotalPointsDisplayYSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.PlayerPoints_displayRuntime.VariableState fromState,DungeonRun.GumRuntimes.PlayerPoints_displayRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.SourceFile",
                            Type = "string",
                            Value = SpriteInstance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Y",
                            Type = "float",
                            Value = SpriteInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Height",
                            Type = "float",
                            Value = Avatar.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Height Units",
                            Type = "DimensionUnitType",
                            Value = Avatar.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Parent",
                            Type = "string",
                            Value = Avatar.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.SourceFile",
                            Type = "string",
                            Value = Avatar.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Width",
                            Type = "float",
                            Value = Avatar.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Width Units",
                            Type = "DimensionUnitType",
                            Value = Avatar.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.X",
                            Type = "float",
                            Value = Avatar.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Y",
                            Type = "float",
                            Value = Avatar.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Font",
                            Type = "string",
                            Value = PlayName.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.FontSize",
                            Type = "int",
                            Value = PlayName.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Height",
                            Type = "float",
                            Value = PlayName.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = PlayName.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.OutlineThickness",
                            Type = "int",
                            Value = PlayName.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Parent",
                            Type = "string",
                            Value = PlayName.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Text",
                            Type = "string",
                            Value = PlayName.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Width",
                            Type = "float",
                            Value = PlayName.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.X",
                            Type = "float",
                            Value = PlayName.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Y",
                            Type = "float",
                            Value = PlayName.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "playTime.OutlineThickness",
                            Type = "int",
                            Value = playTime.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "playTime.Parent",
                            Type = "string",
                            Value = playTime.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "playTime.Text",
                            Type = "string",
                            Value = playTime.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "playTime.X",
                            Type = "float",
                            Value = playTime.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "playTime.Y",
                            Type = "float",
                            Value = playTime.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height",
                            Type = "float",
                            Value = ContainerInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = ContainerInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width",
                            Type = "float",
                            Value = ContainerInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = ContainerInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.X",
                            Type = "float",
                            Value = ContainerInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Y",
                            Type = "float",
                            Value = ContainerInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Height",
                            Type = "float",
                            Value = PointsDisplay.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Height Units",
                            Type = "DimensionUnitType",
                            Value = PointsDisplay.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Parent",
                            Type = "string",
                            Value = PointsDisplay.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Width",
                            Type = "float",
                            Value = PointsDisplay.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Width Units",
                            Type = "DimensionUnitType",
                            Value = PointsDisplay.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.X",
                            Type = "float",
                            Value = PointsDisplay.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Y",
                            Type = "float",
                            Value = PointsDisplay.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.Height",
                            Type = "float",
                            Value = TimePointsLabel.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.OutlineThickness",
                            Type = "int",
                            Value = TimePointsLabel.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.Parent",
                            Type = "string",
                            Value = TimePointsLabel.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.Text",
                            Type = "string",
                            Value = TimePointsLabel.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.X",
                            Type = "float",
                            Value = TimePointsLabel.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.Y",
                            Type = "float",
                            Value = TimePointsLabel.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.Height",
                            Type = "float",
                            Value = LifePointsLabel.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.OutlineThickness",
                            Type = "int",
                            Value = LifePointsLabel.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.Parent",
                            Type = "string",
                            Value = LifePointsLabel.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.Text",
                            Type = "string",
                            Value = LifePointsLabel.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.X",
                            Type = "float",
                            Value = LifePointsLabel.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.Y",
                            Type = "float",
                            Value = LifePointsLabel.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.Height",
                            Type = "float",
                            Value = ExtrasPointsLabel.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.OutlineThickness",
                            Type = "int",
                            Value = ExtrasPointsLabel.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.Parent",
                            Type = "string",
                            Value = ExtrasPointsLabel.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.Text",
                            Type = "string",
                            Value = ExtrasPointsLabel.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.X",
                            Type = "float",
                            Value = ExtrasPointsLabel.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.Y",
                            Type = "float",
                            Value = ExtrasPointsLabel.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.Height",
                            Type = "float",
                            Value = LevelPoints.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.OutlineThickness",
                            Type = "int",
                            Value = LevelPoints.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.Parent",
                            Type = "string",
                            Value = LevelPoints.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.Text",
                            Type = "string",
                            Value = LevelPoints.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.Width",
                            Type = "float",
                            Value = LevelPoints.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.X",
                            Type = "float",
                            Value = LevelPoints.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.Y",
                            Type = "float",
                            Value = LevelPoints.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.Height",
                            Type = "float",
                            Value = TimePoints.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.OutlineThickness",
                            Type = "int",
                            Value = TimePoints.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.Parent",
                            Type = "string",
                            Value = TimePoints.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.Text",
                            Type = "string",
                            Value = TimePoints.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.X",
                            Type = "float",
                            Value = TimePoints.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.Y",
                            Type = "float",
                            Value = TimePoints.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.Height",
                            Type = "float",
                            Value = LifePoints.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.OutlineThickness",
                            Type = "int",
                            Value = LifePoints.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.Parent",
                            Type = "string",
                            Value = LifePoints.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.Text",
                            Type = "string",
                            Value = LifePoints.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.X",
                            Type = "float",
                            Value = LifePoints.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.Y",
                            Type = "float",
                            Value = LifePoints.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.Height",
                            Type = "float",
                            Value = ExtrasPoints.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.OutlineThickness",
                            Type = "int",
                            Value = ExtrasPoints.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.Parent",
                            Type = "string",
                            Value = ExtrasPoints.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.Text",
                            Type = "string",
                            Value = ExtrasPoints.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.Width",
                            Type = "float",
                            Value = ExtrasPoints.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.X",
                            Type = "float",
                            Value = ExtrasPoints.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.Y",
                            Type = "float",
                            Value = ExtrasPoints.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPointsLabel.OutlineThickness",
                            Type = "int",
                            Value = LevelPointsLabel.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPointsLabel.Parent",
                            Type = "string",
                            Value = LevelPointsLabel.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPointsLabel.Text",
                            Type = "string",
                            Value = LevelPointsLabel.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPointsLabel.X",
                            Type = "float",
                            Value = LevelPointsLabel.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPointsLabel.Y",
                            Type = "float",
                            Value = LevelPointsLabel.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.Height",
                            Type = "float",
                            Value = TotalPoints.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.OutlineThickness",
                            Type = "int",
                            Value = TotalPoints.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.Parent",
                            Type = "string",
                            Value = TotalPoints.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.Text",
                            Type = "string",
                            Value = TotalPoints.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.Width",
                            Type = "float",
                            Value = TotalPoints.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.X",
                            Type = "float",
                            Value = TotalPoints.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.Y",
                            Type = "float",
                            Value = TotalPoints.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.Height",
                            Type = "float",
                            Value = TotalPointsDisplay.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.OutlineThickness",
                            Type = "int",
                            Value = TotalPointsDisplay.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.Parent",
                            Type = "string",
                            Value = TotalPointsDisplay.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.Text",
                            Type = "string",
                            Value = TotalPointsDisplay.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.X",
                            Type = "float",
                            Value = TotalPointsDisplay.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.Y",
                            Type = "float",
                            Value = TotalPointsDisplay.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Height",
                            Type = "float",
                            Value = PointsEdit.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Height Units",
                            Type = "DimensionUnitType",
                            Value = PointsEdit.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Parent",
                            Type = "string",
                            Value = PointsEdit.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Width",
                            Type = "float",
                            Value = PointsEdit.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Width Units",
                            Type = "DimensionUnitType",
                            Value = PointsEdit.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.X",
                            Type = "float",
                            Value = PointsEdit.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Y",
                            Type = "float",
                            Value = PointsEdit.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Font",
                            Type = "string",
                            Value = TextInstance.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Height",
                            Type = "float",
                            Value = TextInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.OutlineThickness",
                            Type = "int",
                            Value = TextInstance.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Text",
                            Type = "string",
                            Value = TextInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Width",
                            Type = "float",
                            Value = TextInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.X",
                            Type = "float",
                            Value = TextInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Y",
                            Type = "float",
                            Value = TextInstance.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height + 243f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.SourceFile",
                            Type = "string",
                            Value = SpriteInstance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width + 303f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X + -5f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Y",
                            Type = "float",
                            Value = SpriteInstance.Y + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Height",
                            Type = "float",
                            Value = Avatar.Height + -59f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Height Units",
                            Type = "DimensionUnitType",
                            Value = Avatar.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Parent",
                            Type = "string",
                            Value = Avatar.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.SourceFile",
                            Type = "string",
                            Value = Avatar.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Width",
                            Type = "float",
                            Value = Avatar.Width + -600f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Width Units",
                            Type = "DimensionUnitType",
                            Value = Avatar.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.X",
                            Type = "float",
                            Value = Avatar.X + 22f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Y",
                            Type = "float",
                            Value = Avatar.Y + 30f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Font",
                            Type = "string",
                            Value = PlayName.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.FontSize",
                            Type = "int",
                            Value = PlayName.FontSize + 22
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Height",
                            Type = "float",
                            Value = PlayName.Height + 44f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = PlayName.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.OutlineThickness",
                            Type = "int",
                            Value = PlayName.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Parent",
                            Type = "string",
                            Value = PlayName.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Text",
                            Type = "string",
                            Value = PlayName.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Width",
                            Type = "float",
                            Value = PlayName.Width + 178f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.X",
                            Type = "float",
                            Value = PlayName.X + 98f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PlayName.Y",
                            Type = "float",
                            Value = PlayName.Y + 63f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "playTime.OutlineThickness",
                            Type = "int",
                            Value = playTime.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "playTime.Parent",
                            Type = "string",
                            Value = playTime.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "playTime.Text",
                            Type = "string",
                            Value = playTime.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "playTime.X",
                            Type = "float",
                            Value = playTime.X + 99f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "playTime.Y",
                            Type = "float",
                            Value = playTime.Y + 97f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height",
                            Type = "float",
                            Value = ContainerInstance.Height + 14f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = ContainerInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width",
                            Type = "float",
                            Value = ContainerInstance.Width + 501f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = ContainerInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.X",
                            Type = "float",
                            Value = ContainerInstance.X + 21f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Y",
                            Type = "float",
                            Value = ContainerInstance.Y + 37f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Height",
                            Type = "float",
                            Value = PointsDisplay.Height + -37f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Height Units",
                            Type = "DimensionUnitType",
                            Value = PointsDisplay.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Parent",
                            Type = "string",
                            Value = PointsDisplay.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Width",
                            Type = "float",
                            Value = PointsDisplay.Width + -583f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Width Units",
                            Type = "DimensionUnitType",
                            Value = PointsDisplay.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.X",
                            Type = "float",
                            Value = PointsDisplay.X + 16f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsDisplay.Y",
                            Type = "float",
                            Value = PointsDisplay.Y + 193f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.Height",
                            Type = "float",
                            Value = TimePointsLabel.Height + 20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.OutlineThickness",
                            Type = "int",
                            Value = TimePointsLabel.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.Parent",
                            Type = "string",
                            Value = TimePointsLabel.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.Text",
                            Type = "string",
                            Value = TimePointsLabel.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.X",
                            Type = "float",
                            Value = TimePointsLabel.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePointsLabel.Y",
                            Type = "float",
                            Value = TimePointsLabel.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.Height",
                            Type = "float",
                            Value = LifePointsLabel.Height + 20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.OutlineThickness",
                            Type = "int",
                            Value = LifePointsLabel.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.Parent",
                            Type = "string",
                            Value = LifePointsLabel.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.Text",
                            Type = "string",
                            Value = LifePointsLabel.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.X",
                            Type = "float",
                            Value = LifePointsLabel.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePointsLabel.Y",
                            Type = "float",
                            Value = LifePointsLabel.Y + 20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.Height",
                            Type = "float",
                            Value = ExtrasPointsLabel.Height + 20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.OutlineThickness",
                            Type = "int",
                            Value = ExtrasPointsLabel.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.Parent",
                            Type = "string",
                            Value = ExtrasPointsLabel.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.Text",
                            Type = "string",
                            Value = ExtrasPointsLabel.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.X",
                            Type = "float",
                            Value = ExtrasPointsLabel.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPointsLabel.Y",
                            Type = "float",
                            Value = ExtrasPointsLabel.Y + 60f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.Height",
                            Type = "float",
                            Value = LevelPoints.Height + 20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.OutlineThickness",
                            Type = "int",
                            Value = LevelPoints.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.Parent",
                            Type = "string",
                            Value = LevelPoints.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.Text",
                            Type = "string",
                            Value = LevelPoints.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.Width",
                            Type = "float",
                            Value = LevelPoints.Width + 100f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.X",
                            Type = "float",
                            Value = LevelPoints.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPoints.Y",
                            Type = "float",
                            Value = LevelPoints.Y + 40f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.Height",
                            Type = "float",
                            Value = TimePoints.Height + 20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.OutlineThickness",
                            Type = "int",
                            Value = TimePoints.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.Parent",
                            Type = "string",
                            Value = TimePoints.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.Text",
                            Type = "string",
                            Value = TimePoints.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.X",
                            Type = "float",
                            Value = TimePoints.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TimePoints.Y",
                            Type = "float",
                            Value = TimePoints.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.Height",
                            Type = "float",
                            Value = LifePoints.Height + 20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.OutlineThickness",
                            Type = "int",
                            Value = LifePoints.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.Parent",
                            Type = "string",
                            Value = LifePoints.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.Text",
                            Type = "string",
                            Value = LifePoints.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.X",
                            Type = "float",
                            Value = LifePoints.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LifePoints.Y",
                            Type = "float",
                            Value = LifePoints.Y + 20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.Height",
                            Type = "float",
                            Value = ExtrasPoints.Height + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.OutlineThickness",
                            Type = "int",
                            Value = ExtrasPoints.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.Parent",
                            Type = "string",
                            Value = ExtrasPoints.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.Text",
                            Type = "string",
                            Value = ExtrasPoints.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.Width",
                            Type = "float",
                            Value = ExtrasPoints.Width + 100f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.X",
                            Type = "float",
                            Value = ExtrasPoints.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ExtrasPoints.Y",
                            Type = "float",
                            Value = ExtrasPoints.Y + 60f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPointsLabel.OutlineThickness",
                            Type = "int",
                            Value = LevelPointsLabel.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPointsLabel.Parent",
                            Type = "string",
                            Value = LevelPointsLabel.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPointsLabel.Text",
                            Type = "string",
                            Value = LevelPointsLabel.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPointsLabel.X",
                            Type = "float",
                            Value = LevelPointsLabel.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "LevelPointsLabel.Y",
                            Type = "float",
                            Value = LevelPointsLabel.Y + 40f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.Height",
                            Type = "float",
                            Value = TotalPoints.Height + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.OutlineThickness",
                            Type = "int",
                            Value = TotalPoints.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.Parent",
                            Type = "string",
                            Value = TotalPoints.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.Text",
                            Type = "string",
                            Value = TotalPoints.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.Width",
                            Type = "float",
                            Value = TotalPoints.Width + 100f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.X",
                            Type = "float",
                            Value = TotalPoints.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPoints.Y",
                            Type = "float",
                            Value = TotalPoints.Y + 80f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.Height",
                            Type = "float",
                            Value = TotalPointsDisplay.Height + 20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.OutlineThickness",
                            Type = "int",
                            Value = TotalPointsDisplay.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.Parent",
                            Type = "string",
                            Value = TotalPointsDisplay.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.Text",
                            Type = "string",
                            Value = TotalPointsDisplay.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.X",
                            Type = "float",
                            Value = TotalPointsDisplay.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TotalPointsDisplay.Y",
                            Type = "float",
                            Value = TotalPointsDisplay.Y + 80f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Height",
                            Type = "float",
                            Value = PointsEdit.Height + -41f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Height Units",
                            Type = "DimensionUnitType",
                            Value = PointsEdit.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Parent",
                            Type = "string",
                            Value = PointsEdit.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Width",
                            Type = "float",
                            Value = PointsEdit.Width + -572f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Width Units",
                            Type = "DimensionUnitType",
                            Value = PointsEdit.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.X",
                            Type = "float",
                            Value = PointsEdit.X + 82f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PointsEdit.Y",
                            Type = "float",
                            Value = PointsEdit.Y + 195f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Font",
                            Type = "string",
                            Value = TextInstance.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Height",
                            Type = "float",
                            Value = TextInstance.Height + 23f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.OutlineThickness",
                            Type = "int",
                            Value = TextInstance.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Text",
                            Type = "string",
                            Value = TextInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Width",
                            Type = "float",
                            Value = TextInstance.Width + 72f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.X",
                            Type = "float",
                            Value = TextInstance.X + 15f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Y",
                            Type = "float",
                            Value = TextInstance.Y + 204f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.SpriteRuntime SpriteInstance { get; set; }
            private DungeonRun.GumRuntimes.SpriteRuntime Avatar { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime PlayName { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime playTime { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime ContainerInstance { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime PointsDisplay { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TimePointsLabel { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime LifePointsLabel { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime ExtrasPointsLabel { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime LevelPoints { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TimePoints { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime LifePoints { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime ExtrasPoints { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime LevelPointsLabel { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TotalPoints { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TotalPointsDisplay { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime PointsEdit { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TextInstance { get; set; }
            public string ExtrasPointsText
            {
                get
                {
                    return ExtrasPoints.Text;
                }
                set
                {
                    if (ExtrasPoints.Text != value)
                    {
                        ExtrasPoints.Text = value;
                        ExtrasPointsTextChanged?.Invoke(this, null);
                    }
                }
            }
            public string LevelPointsText
            {
                get
                {
                    return LevelPoints.Text;
                }
                set
                {
                    if (LevelPoints.Text != value)
                    {
                        LevelPoints.Text = value;
                        LevelPointsTextChanged?.Invoke(this, null);
                    }
                }
            }
            public string LifePointsText
            {
                get
                {
                    return LifePoints.Text;
                }
                set
                {
                    if (LifePoints.Text != value)
                    {
                        LifePoints.Text = value;
                        LifePointsTextChanged?.Invoke(this, null);
                    }
                }
            }
            public string PlayNameDisplay
            {
                get
                {
                    return PlayName.Text;
                }
                set
                {
                    if (PlayName.Text != value)
                    {
                        PlayName.Text = value;
                        PlayNameDisplayChanged?.Invoke(this, null);
                    }
                }
            }
            public string PlayTime
            {
                get
                {
                    return playTime.Text;
                }
                set
                {
                    if (playTime.Text != value)
                    {
                        playTime.Text = value;
                        PlayTimeChanged?.Invoke(this, null);
                    }
                }
            }
            public string TimePointsText
            {
                get
                {
                    return TimePoints.Text;
                }
                set
                {
                    if (TimePoints.Text != value)
                    {
                        TimePoints.Text = value;
                        TimePointsTextChanged?.Invoke(this, null);
                    }
                }
            }
            public string TotalPointsText
            {
                get
                {
                    return TotalPoints.Text;
                }
                set
                {
                    if (TotalPoints.Text != value)
                    {
                        TotalPoints.Text = value;
                        TotalPointsTextChanged?.Invoke(this, null);
                    }
                }
            }
            public event System.EventHandler ExtrasPointsTextChanged;
            public event System.EventHandler LevelPointsTextChanged;
            public event System.EventHandler LifePointsTextChanged;
            public event System.EventHandler PlayNameDisplayChanged;
            public event System.EventHandler PlayTimeChanged;
            public event System.EventHandler TimePointsTextChanged;
            public event System.EventHandler TotalPointsTextChanged;
            public PlayerPoints_displayRuntime (bool fullInstantiation = true) 
            	: base(false)
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "PlayerPoints_display");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                SpriteInstance = this.GetGraphicalUiElementByName("SpriteInstance") as DungeonRun.GumRuntimes.SpriteRuntime;
                Avatar = this.GetGraphicalUiElementByName("Avatar") as DungeonRun.GumRuntimes.SpriteRuntime;
                PlayName = this.GetGraphicalUiElementByName("PlayName") as DungeonRun.GumRuntimes.TextRuntime;
                playTime = this.GetGraphicalUiElementByName("playTime") as DungeonRun.GumRuntimes.TextRuntime;
                ContainerInstance = this.GetGraphicalUiElementByName("ContainerInstance") as DungeonRun.GumRuntimes.ContainerRuntime;
                PointsDisplay = this.GetGraphicalUiElementByName("PointsDisplay") as DungeonRun.GumRuntimes.ContainerRuntime;
                TimePointsLabel = this.GetGraphicalUiElementByName("TimePointsLabel") as DungeonRun.GumRuntimes.TextRuntime;
                LifePointsLabel = this.GetGraphicalUiElementByName("LifePointsLabel") as DungeonRun.GumRuntimes.TextRuntime;
                ExtrasPointsLabel = this.GetGraphicalUiElementByName("ExtrasPointsLabel") as DungeonRun.GumRuntimes.TextRuntime;
                LevelPoints = this.GetGraphicalUiElementByName("LevelPoints") as DungeonRun.GumRuntimes.TextRuntime;
                TimePoints = this.GetGraphicalUiElementByName("TimePoints") as DungeonRun.GumRuntimes.TextRuntime;
                LifePoints = this.GetGraphicalUiElementByName("LifePoints") as DungeonRun.GumRuntimes.TextRuntime;
                ExtrasPoints = this.GetGraphicalUiElementByName("ExtrasPoints") as DungeonRun.GumRuntimes.TextRuntime;
                LevelPointsLabel = this.GetGraphicalUiElementByName("LevelPointsLabel") as DungeonRun.GumRuntimes.TextRuntime;
                TotalPoints = this.GetGraphicalUiElementByName("TotalPoints") as DungeonRun.GumRuntimes.TextRuntime;
                TotalPointsDisplay = this.GetGraphicalUiElementByName("TotalPointsDisplay") as DungeonRun.GumRuntimes.TextRuntime;
                PointsEdit = this.GetGraphicalUiElementByName("PointsEdit") as DungeonRun.GumRuntimes.ContainerRuntime;
                TextInstance = this.GetGraphicalUiElementByName("TextInstance") as DungeonRun.GumRuntimes.TextRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
