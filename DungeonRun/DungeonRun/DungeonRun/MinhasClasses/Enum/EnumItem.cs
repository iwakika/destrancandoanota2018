﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonRun.Entities
{

   

    /// <summary>
    /// TIpos de itens no jogo
    /// </summary>
    public enum EnumItem
    {

        /// <summary>
        /// Item Não definido
        /// </summary>
        DEFAULT,

        /// <summary>
        /// Item Chave
        /// </summary>
        KEY_SILVER,
        FIREBALL
            
    }
}
