﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonRun.Entities
{
    /// <summary>
    ///Status do jogador INTANGIBLE

    /// </summary>
    public enum EnumPlay
    {
        DEFAULT,
        INTANGIBLE,
        ZERO_HP

    }
}
