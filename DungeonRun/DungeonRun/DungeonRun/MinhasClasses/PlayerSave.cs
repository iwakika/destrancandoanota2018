﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DungeonRun.MinhasClasses
{
    [Serializable]
    class PlayerSave : IComparable<PlayerSave> 
    {
        public String playerName { get; set; }
        public int playerPoints { get; set; }
        public double playerTime { get; set; }
        public int Id { get; set; }

        public PlayerSave(String playerName , int point, double playerTime)
        {
            this.playerName = playerName;            
            this.playerPoints = point;
            this.playerTime = playerTime;


        }

        public int CompareTo(PlayerSave obj)
        {
            return playerPoints.CompareTo(obj.playerPoints);
        }



    }


}
