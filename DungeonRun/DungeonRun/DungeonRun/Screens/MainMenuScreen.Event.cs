using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using DungeonRun.Entities;
using DungeonRun.Screens;
using DungeonRun.MinhasClasses.Data;
namespace DungeonRun.Screens
{
    public partial class MainMenuScreen        
    {
        private bool menuControle = false;

        void OnButtonGum1Click(FlatRedBall.Gui.IWindow window)
        {
            if (!menuControle)
            {

                if (txt_NameInputText == "Nome do Jogador" || txt_NameInputText.Trim() == "")
                {
                    DialogBoxInstance.Visible = true;
                    DialogBoxInstance.Text = "Hey Amigo! Porque n�o coloca um nome? Se n�o tiver Ideia pode botar Xalakom";
                }
                else
                {

                    MoveToScreen(typeof(Screen).FullName);
                    GlobalData.PlayerData.name = txt_NameInputText;
                }

            }

        }
        void Ontxt_NameInputClick(FlatRedBall.Gui.IWindow window)
        {
            txt_NameInputText = "";
        }

        void key_enter()
        {
            if (!menuControle) {
                //MoveToScreen(typeof(Screen).FullName);
                // GlobalData.PlayerData.name = txt_NameInputText;
                txt_NameInputText = "";
            }
        }

        void key_C()
        {           
            menuControle = HudInstance.loadControle();
        }
        

        void OnButtonGum2Click (FlatRedBall.Gui.IWindow window) 
        {
            if (!menuControle)
            {               
                menuControle = HudInstance.loadControle(); 
            }
            
        }
        void OnButtonGum3Click (FlatRedBall.Gui.IWindow window) 
        {
            if (!menuControle)
            {
                HudInstance.loadRanking(true);
            }
        }

    }
}
