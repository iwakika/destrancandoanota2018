#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
namespace DungeonRun.Screens
{
    public partial class MainMenuScreen : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        protected static FlatRedBall.Gum.GumIdb MainMenuGum;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D panelInset_brown;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D panel_brown;
        
        private DungeonRun.GumRuntimes.ButtonRuntime ButtonGum1;
        private DungeonRun.GumRuntimes.EditTextBoxRuntime txt_NameInput;
        private DungeonRun.GumRuntimes.ButtonRuntime ButtonGum2;
        private DungeonRun.GumRuntimes.ButtonRuntime ButtonGum3;
        private DungeonRun.Entities.Hud HudInstance;
        private DungeonRun.GumRuntimes.DialogBoxRuntime DialogBoxInstance;
        public string txt_NameInputText
        {
            get
            {
                return txt_NameInput.Text;
            }
            set
            {
                txt_NameInput.Text = value;
            }
        }
        public event FlatRedBall.Gui.WindowEvent ButtonGum1Click;
        public event FlatRedBall.Gui.WindowEvent txt_NameInputClick;
        public event FlatRedBall.Gui.WindowEvent ButtonGum2Click;
        public event FlatRedBall.Gui.WindowEvent ButtonGum3Click;
        public MainMenuScreen () 
        	: base ("MainMenuScreen")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            ButtonGum1 = MainMenuGum.GetGraphicalUiElementByName("ButtonGum1") as DungeonRun.GumRuntimes.ButtonRuntime;
            txt_NameInput = MainMenuGum.GetGraphicalUiElementByName("EditTextBoxInstance") as DungeonRun.GumRuntimes.EditTextBoxRuntime;
            ButtonGum2 = MainMenuGum.GetGraphicalUiElementByName("ButtonGum2") as DungeonRun.GumRuntimes.ButtonRuntime;
            ButtonGum3 = MainMenuGum.GetGraphicalUiElementByName("ButtonGum3") as DungeonRun.GumRuntimes.ButtonRuntime;
            HudInstance = new DungeonRun.Entities.Hud(ContentManagerName, false);
            HudInstance.Name = "HudInstance";
            DialogBoxInstance = MainMenuGum.GetGraphicalUiElementByName("DialogBoxInstance") as DungeonRun.GumRuntimes.DialogBoxRuntime;
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            MainMenuGum.InstanceInitialize(); FlatRedBall.FlatRedBallServices.GraphicsOptions.SizeOrOrientationChanged += MainMenuGum.HandleResolutionChanged;
            HudInstance.AddToManagers(mLayer);
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
                HudInstance.Activity();
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            base.Destroy();
            FlatRedBall.SpriteManager.RemoveDrawableBatch(MainMenuGum); FlatRedBall.FlatRedBallServices.GraphicsOptions.SizeOrOrientationChanged -= MainMenuGum.HandleResolutionChanged;
            MainMenuGum = null;
            panelInset_brown = null;
            panel_brown = null;
            
            if (ButtonGum1 != null)
            {
                ButtonGum1.RemoveFromManagers();
            }
            if (txt_NameInput != null)
            {
                txt_NameInput.RemoveFromManagers();
            }
            if (ButtonGum2 != null)
            {
                ButtonGum2.RemoveFromManagers();
            }
            if (ButtonGum3 != null)
            {
                ButtonGum3.RemoveFromManagers();
            }
            if (HudInstance != null)
            {
                HudInstance.Destroy();
                HudInstance.Detach();
            }
            if (DialogBoxInstance != null)
            {
                DialogBoxInstance.RemoveFromManagers();
            }
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            ButtonGum1.Click += OnButtonGum1Click;
            ButtonGum1.Click += OnButtonGum1ClickTunnel;
            txt_NameInput.Click += Ontxt_NameInputClick;
            txt_NameInput.Click += Ontxt_NameInputClickTunnel;
            ButtonGum2.Click += OnButtonGum2Click;
            ButtonGum2.Click += OnButtonGum2ClickTunnel;
            ButtonGum3.Click += OnButtonGum3Click;
            ButtonGum3.Click += OnButtonGum3ClickTunnel;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            if (ButtonGum1 != null)
            {
                ButtonGum1.RemoveFromManagers();
            }
            if (txt_NameInput != null)
            {
                txt_NameInput.RemoveFromManagers();
            }
            if (ButtonGum2 != null)
            {
                ButtonGum2.RemoveFromManagers();
            }
            if (ButtonGum3 != null)
            {
                ButtonGum3.RemoveFromManagers();
            }
            HudInstance.RemoveFromManagers();
            if (DialogBoxInstance != null)
            {
                DialogBoxInstance.RemoveFromManagers();
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
                HudInstance.AssignCustomVariables(true);
            }
            txt_NameInputText = "Nome do Jogador";
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            HudInstance.ConvertToManuallyUpdated();
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            Gum.Wireframe.GraphicalUiElement.IsAllLayoutSuspended = true;  MainMenuGum = new FlatRedBall.Gum.GumIdb();  MainMenuGum.LoadFromFile("content/gumproject/screens/mainmenugum.gusx");  MainMenuGum.AssignReferences();Gum.Wireframe.GraphicalUiElement.IsAllLayoutSuspended = false; MainMenuGum.Element.UpdateLayout(); MainMenuGum.Element.UpdateLayout();
            panelInset_brown = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/mainmenuscreen/panelinset_brown.png", contentManagerName);
            panel_brown = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/mainmenuscreen/panel_brown.png", contentManagerName);
            DungeonRun.Entities.Hud.LoadStaticContent(contentManagerName);
            CustomLoadStaticContent(contentManagerName);
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "MainMenuGum":
                    return MainMenuGum;
                case  "panelInset_brown":
                    return panelInset_brown;
                case  "panel_brown":
                    return panel_brown;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "MainMenuGum":
                    return MainMenuGum;
                case  "panelInset_brown":
                    return panelInset_brown;
                case  "panel_brown":
                    return panel_brown;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "MainMenuGum":
                    return MainMenuGum;
                case  "panelInset_brown":
                    return panelInset_brown;
                case  "panel_brown":
                    return panel_brown;
            }
            return null;
        }
    }
}
