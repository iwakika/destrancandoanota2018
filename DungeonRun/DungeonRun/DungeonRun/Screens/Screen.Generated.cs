#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math;
namespace DungeonRun.Screens
{
    public partial class Screen : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static FlatRedBall.TileGraphics.LayeredTileMap mLevel1;
        static string mLastContentManagerForLevel1;
        public static FlatRedBall.TileGraphics.LayeredTileMap Level1
        {
            get
            {
                if (mLevel1 == null || mLastContentManagerForLevel1 != "Screen")
                {
                    mLastContentManagerForLevel1 = "Screen";
                    mLevel1 = FlatRedBall.TileGraphics.LayeredTileMap.FromTiledMapSave("content/screens/screen/level1.tmx", "Screen");
                }
                return mLevel1;
            }
        }
        static FlatRedBall.TileGraphics.LayeredTileMap mLevel2;
        static string mLastContentManagerForLevel2;
        public static FlatRedBall.TileGraphics.LayeredTileMap Level2
        {
            get
            {
                if (mLevel2 == null || mLastContentManagerForLevel2 != "Screen")
                {
                    mLastContentManagerForLevel2 = "Screen";
                    mLevel2 = FlatRedBall.TileGraphics.LayeredTileMap.FromTiledMapSave("content/screens/screen/level2.tmx", "Screen");
                }
                return mLevel2;
            }
        }
        static FlatRedBall.TileGraphics.LayeredTileMap mLevel0;
        static string mLastContentManagerForLevel0;
        public static FlatRedBall.TileGraphics.LayeredTileMap Level0
        {
            get
            {
                if (mLevel0 == null || mLastContentManagerForLevel0 != "Screen")
                {
                    mLastContentManagerForLevel0 = "Screen";
                    mLevel0 = FlatRedBall.TileGraphics.LayeredTileMap.FromTiledMapSave("content/screens/screen/level0.tmx", "Screen");
                }
                return mLevel0;
            }
        }
        static FlatRedBall.TileGraphics.LayeredTileMap mLevel3;
        static string mLastContentManagerForLevel3;
        public static FlatRedBall.TileGraphics.LayeredTileMap Level3
        {
            get
            {
                if (mLevel3 == null || mLastContentManagerForLevel3 != "Screen")
                {
                    mLastContentManagerForLevel3 = "Screen";
                    mLevel3 = FlatRedBall.TileGraphics.LayeredTileMap.FromTiledMapSave("content/screens/screen/level3.tmx", "Screen");
                }
                return mLevel3;
            }
        }
        static FlatRedBall.TileGraphics.LayeredTileMap mLevel4;
        static string mLastContentManagerForLevel4;
        public static FlatRedBall.TileGraphics.LayeredTileMap Level4
        {
            get
            {
                if (mLevel4 == null || mLastContentManagerForLevel4 != "Screen")
                {
                    mLastContentManagerForLevel4 = "Screen";
                    mLevel4 = FlatRedBall.TileGraphics.LayeredTileMap.FromTiledMapSave("content/screens/screen/level4.tmx", "Screen");
                }
                return mLevel4;
            }
        }
        
        private FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Player> PlayerList;
        private DungeonRun.Entities.Player PlayerInstance;
        private DungeonRun.Entities.Hud HudInstance;
        private FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Door> DoorList;
        private DungeonRun.Entities.MonsterSpawer MonsterSpawerInstance;
        private FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Enemy> EnemyList;
        private FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Item> ItemList;
        private FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Projectiles> ProjectilesList;
        private DungeonRun.Entities.ItemSpawer mItemSpawerInstance;
        public DungeonRun.Entities.ItemSpawer ItemSpawerInstance
        {
            get
            {
                return mItemSpawerInstance;
            }
            private set
            {
                mItemSpawerInstance = value;
            }
        }
        private FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.HitMelee> HitMeleeList;
        private FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Npc> NpcList;
        public bool ShowShapes = true;
        FlatRedBall.Gum.GumIdb gumIdb;
        public Screen () 
        	: base ("Screen")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            PlayerList = new FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Player>();
            PlayerList.Name = "PlayerList";
            PlayerInstance = new DungeonRun.Entities.Player(ContentManagerName, false);
            PlayerInstance.Name = "PlayerInstance";
            HudInstance = new DungeonRun.Entities.Hud(ContentManagerName, false);
            HudInstance.Name = "HudInstance";
            DoorList = new FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Door>();
            DoorList.Name = "DoorList";
            MonsterSpawerInstance = new DungeonRun.Entities.MonsterSpawer(ContentManagerName, false);
            MonsterSpawerInstance.Name = "MonsterSpawerInstance";
            EnemyList = new FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Enemy>();
            EnemyList.Name = "EnemyList";
            ItemList = new FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Item>();
            ItemList.Name = "ItemList";
            ProjectilesList = new FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Projectiles>();
            ProjectilesList.Name = "ProjectilesList";
            mItemSpawerInstance = new DungeonRun.Entities.ItemSpawer(ContentManagerName, false);
            mItemSpawerInstance.Name = "mItemSpawerInstance";
            HitMeleeList = new FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.HitMelee>();
            HitMeleeList.Name = "HitMeleeList";
            NpcList = new FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Npc>();
            NpcList.Name = "NpcList";
            gumIdb = new FlatRedBall.Gum.GumIdb();
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            FlatRedBall.SpriteManager.AddDrawableBatch(gumIdb);
            Factories.DoorFactory.Initialize(ContentManagerName);
            Factories.EnemyFactory.Initialize(ContentManagerName);
            Factories.ItemFactory.Initialize(ContentManagerName);
            Factories.ProjectilesFactory.Initialize(ContentManagerName);
            Factories.HitMeleeFactory.Initialize(ContentManagerName);
            Factories.NpcFactory.Initialize(ContentManagerName);
            Factories.DoorFactory.AddList(DoorList);
            Factories.EnemyFactory.AddList(EnemyList);
            Factories.ItemFactory.AddList(ItemList);
            Factories.ProjectilesFactory.AddList(ProjectilesList);
            Factories.HitMeleeFactory.AddList(HitMeleeList);
            Factories.NpcFactory.AddList(NpcList);
            PlayerInstance.AddToManagers(mLayer);
            HudInstance.AddToManagers(mLayer);
            MonsterSpawerInstance.AddToManagers(mLayer);
            mItemSpawerInstance.AddToManagers(mLayer);
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
                for (int i = PlayerList.Count - 1; i > -1; i--)
                {
                    if (i < PlayerList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        PlayerList[i].Activity();
                    }
                }
                HudInstance.Activity();
                for (int i = DoorList.Count - 1; i > -1; i--)
                {
                    if (i < DoorList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        DoorList[i].Activity();
                    }
                }
                MonsterSpawerInstance.Activity();
                for (int i = EnemyList.Count - 1; i > -1; i--)
                {
                    if (i < EnemyList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        EnemyList[i].Activity();
                    }
                }
                for (int i = ItemList.Count - 1; i > -1; i--)
                {
                    if (i < ItemList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        ItemList[i].Activity();
                    }
                }
                for (int i = ProjectilesList.Count - 1; i > -1; i--)
                {
                    if (i < ProjectilesList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        ProjectilesList[i].Activity();
                    }
                }
                ItemSpawerInstance.Activity();
                for (int i = HitMeleeList.Count - 1; i > -1; i--)
                {
                    if (i < HitMeleeList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        HitMeleeList[i].Activity();
                    }
                }
                for (int i = NpcList.Count - 1; i > -1; i--)
                {
                    if (i < NpcList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        NpcList[i].Activity();
                    }
                }
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            FlatRedBall.SpriteManager.RemoveDrawableBatch(gumIdb);
            base.Destroy();
            Factories.DoorFactory.Destroy();
            Factories.EnemyFactory.Destroy();
            Factories.ItemFactory.Destroy();
            Factories.ProjectilesFactory.Destroy();
            Factories.HitMeleeFactory.Destroy();
            Factories.NpcFactory.Destroy();
            if (mLevel1 != null)
            {
                mLevel1.Destroy();
                mLevel1 = null;
            }
            if (mLevel2 != null)
            {
                mLevel2.Destroy();
                mLevel2 = null;
            }
            if (mLevel0 != null)
            {
                mLevel0.Destroy();
                mLevel0 = null;
            }
            if (mLevel3 != null)
            {
                mLevel3.Destroy();
                mLevel3 = null;
            }
            if (mLevel4 != null)
            {
                mLevel4.Destroy();
                mLevel4 = null;
            }
            
            PlayerList.MakeOneWay();
            DoorList.MakeOneWay();
            EnemyList.MakeOneWay();
            ItemList.MakeOneWay();
            ProjectilesList.MakeOneWay();
            HitMeleeList.MakeOneWay();
            NpcList.MakeOneWay();
            for (int i = PlayerList.Count - 1; i > -1; i--)
            {
                PlayerList[i].Destroy();
            }
            if (HudInstance != null)
            {
                HudInstance.Destroy();
                HudInstance.Detach();
            }
            for (int i = DoorList.Count - 1; i > -1; i--)
            {
                DoorList[i].Destroy();
            }
            if (MonsterSpawerInstance != null)
            {
                MonsterSpawerInstance.Destroy();
                MonsterSpawerInstance.Detach();
            }
            for (int i = EnemyList.Count - 1; i > -1; i--)
            {
                EnemyList[i].Destroy();
            }
            for (int i = ItemList.Count - 1; i > -1; i--)
            {
                ItemList[i].Destroy();
            }
            for (int i = ProjectilesList.Count - 1; i > -1; i--)
            {
                ProjectilesList[i].Destroy();
            }
            if (ItemSpawerInstance != null)
            {
                ItemSpawerInstance.Destroy();
                ItemSpawerInstance.Detach();
            }
            for (int i = HitMeleeList.Count - 1; i > -1; i--)
            {
                HitMeleeList[i].Destroy();
            }
            for (int i = NpcList.Count - 1; i > -1; i--)
            {
                NpcList[i].Destroy();
            }
            PlayerList.MakeTwoWay();
            DoorList.MakeTwoWay();
            EnemyList.MakeTwoWay();
            ItemList.MakeTwoWay();
            ProjectilesList.MakeTwoWay();
            HitMeleeList.MakeTwoWay();
            NpcList.MakeTwoWay();
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            PlayerList.Add(PlayerInstance);
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            for (int i = PlayerList.Count - 1; i > -1; i--)
            {
                PlayerList[i].Destroy();
            }
            HudInstance.RemoveFromManagers();
            for (int i = DoorList.Count - 1; i > -1; i--)
            {
                DoorList[i].Destroy();
            }
            MonsterSpawerInstance.RemoveFromManagers();
            for (int i = EnemyList.Count - 1; i > -1; i--)
            {
                EnemyList[i].Destroy();
            }
            for (int i = ItemList.Count - 1; i > -1; i--)
            {
                ItemList[i].Destroy();
            }
            for (int i = ProjectilesList.Count - 1; i > -1; i--)
            {
                ProjectilesList[i].Destroy();
            }
            ItemSpawerInstance.RemoveFromManagers();
            for (int i = HitMeleeList.Count - 1; i > -1; i--)
            {
                HitMeleeList[i].Destroy();
            }
            for (int i = NpcList.Count - 1; i > -1; i--)
            {
                NpcList[i].Destroy();
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
                PlayerInstance.AssignCustomVariables(true);
                HudInstance.AssignCustomVariables(true);
                MonsterSpawerInstance.AssignCustomVariables(true);
                ItemSpawerInstance.AssignCustomVariables(true);
            }
            ShowShapes = true;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            for (int i = 0; i < PlayerList.Count; i++)
            {
                PlayerList[i].ConvertToManuallyUpdated();
            }
            HudInstance.ConvertToManuallyUpdated();
            for (int i = 0; i < DoorList.Count; i++)
            {
                DoorList[i].ConvertToManuallyUpdated();
            }
            MonsterSpawerInstance.ConvertToManuallyUpdated();
            for (int i = 0; i < EnemyList.Count; i++)
            {
                EnemyList[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < ItemList.Count; i++)
            {
                ItemList[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < ProjectilesList.Count; i++)
            {
                ProjectilesList[i].ConvertToManuallyUpdated();
            }
            ItemSpawerInstance.ConvertToManuallyUpdated();
            for (int i = 0; i < HitMeleeList.Count; i++)
            {
                HitMeleeList[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < NpcList.Count; i++)
            {
                NpcList[i].ConvertToManuallyUpdated();
            }
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            DungeonRun.Entities.Hud.LoadStaticContent(contentManagerName);
            DungeonRun.Entities.MonsterSpawer.LoadStaticContent(contentManagerName);
            DungeonRun.Entities.ItemSpawer.LoadStaticContent(contentManagerName);
            CustomLoadStaticContent(contentManagerName);
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Level1":
                    return Level1;
                case  "Level2":
                    return Level2;
                case  "Level0":
                    return Level0;
                case  "Level3":
                    return Level3;
                case  "Level4":
                    return Level4;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "Level1":
                    return Level1;
                case  "Level2":
                    return Level2;
                case  "Level0":
                    return Level0;
                case  "Level3":
                    return Level3;
                case  "Level4":
                    return Level4;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Level1":
                    return Level1;
                case  "Level2":
                    return Level2;
                case  "Level0":
                    return Level0;
                case  "Level3":
                    return Level3;
                case  "Level4":
                    return Level4;
            }
            return null;
        }
    }
}
