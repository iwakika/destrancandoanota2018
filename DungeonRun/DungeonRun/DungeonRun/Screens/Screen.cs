using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Localization;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using FlatRedBall.TileCollisions;
using FlatRedBall.TileGraphics;
using DungeonRun.Entities;
using DungeonRun.MinhasClasses.Data;
using FlatRedBall.Math;
using Microsoft.Xna.Framework;
using DungeonRun.Factories;

namespace DungeonRun.Screens

{
	public partial class Screen
	{

        TileShapeCollection solidCollision;
       // TileNodeNetwork tileNodeNetwork;
       // LayeredTileMap currentLevelTile;
        public static string LevelToLoad { get; set; } = GlobalData.currentLevel.name;
        public IPressableInput pause = InputManager.Keyboard.GetKey(Keys.P);
        public IPressableInput ranking = InputManager.Keyboard.GetKey(Keys.R);
        public IPressableInput controle = InputManager.Keyboard.GetKey(Keys.C);
        public IPressableInput blood = InputManager.Keyboard.GetKey(Keys.M);
        public IPressableInput escape = InputManager.Keyboard.GetKey(Keys.Escape);
        public IPressableInput space = InputManager.Keyboard.GetKey(Keys.Space);

        //public static FlatRedBall.Gum.GumIdb GumInstance;

        void CustomInitialize()
		{
            Assing();
            CollisionInitialize();

         
        }    

        
        void Assing()
           
        {

            // Console.WriteLine(PlayerInstance.statusPlay);
            //Console.WriteLine(GlobalData.currentLevel.name);
            if (GlobalData.gameOver)
            {
                GlobalData.resetarVariaveis();
                LevelToLoad = GlobalData.currentLevel.name;
               // Console.WriteLine("Reseta vari�vei global");
                //Console.WriteLine("teste4");
            }
            

            GlobalData.hudInstance = HudInstance;
            GlobalData.PlayerData.playerInstance = PlayerInstance;           
         

            CameraAssing();
            CurrentLevelAssing();
            FlatRedBall.TileEntities.TileEntityInstantiator.CreateEntitiesFrom(GlobalData.currentLevel.tileMap);
            HudInstance.PLayerList = PlayerList;
            HudInstance.EnemyList = EnemyList;


            MovimentAssing();

            MonsterSpawerInstance.mEnemylist = EnemyList;
            ItemSpawerInstance.mItemList = ItemList;

           

        }
        
        public void CreateNodeNetWork()
        {
            float xSeed = 32;
            float ySeed = -576;
            float distanceBetweenNodes = 32;
            int numberOfNodesX = 23;
            int numberofNodesY = 18;

            TileNodeNetwork tileNodeNetwork = new TileNodeNetwork(
                xSeed,
                ySeed,
                distanceBetweenNodes,
                numberOfNodesX,
                numberofNodesY,
                DirectionalType.Four);

            tileNodeNetwork.FillCompletely();

            tileNodeNetwork.Visible = true;

            PositionedNode node = new PositionedNode();
            node.Position = new Vector3(300, -300, 0);
            PositionedNode node2 = new PositionedNode();
            node2.Position = new Vector3(300, -500, 0);
            List<PositionedNode> list = tileNodeNetwork.GetPath(node, node2);

        }


        private void CurrentLevelAssing()
        {

            
           // Console.WriteLine("LevelToLoad" + LevelToLoad);
                   
            GlobalData.currentLevel.tileMap = GetFile(LevelToLoad) as FlatRedBall.TileGraphics.LayeredTileMap;
            
            GlobalData.currentLevel.tileMap.AddToManagers();
            GlobalData.hudInstance = HudInstance;            
            

        }

        private void CameraAssing()
        {
            SpriteManager.Camera.UsePixelCoordinates();
            Camera.Main.X = GlobalData.camX;
            Camera.Main.Y = GlobalData.camY;
        }

        private void MovimentAssing()
        {
            GlobalData.PlayerData.playerInstance.MovementInput = InputManager.Keyboard.Get2DInput(Keys.A, Keys.D, Keys.W, Keys.S);

            GlobalData.PlayerData.playerInstance.ShotInput = InputManager.Keyboard.Get2DInput(Keys.Left, Keys.Right, Keys.Up, Keys.Down);
            GlobalData.PlayerData.playerInstance.ShotInput_UP = InputManager.Keyboard.GetKey(Keys.Up);
            GlobalData.PlayerData.playerInstance.ShotInput_Dow = InputManager.Keyboard.GetKey(Keys.Down);
            GlobalData.PlayerData.playerInstance.ShotInput_Left = InputManager.Keyboard.GetKey(Keys.Left);
            GlobalData.PlayerData.playerInstance.ShotInput_Rigth= InputManager.Keyboard.GetKey(Keys.Right);

            GlobalData.PlayerData.playerInstance.DashInput = InputManager.Keyboard.GetKey(Keys.Space);

            
           
        }

        /// <summary>
        /// Inicializa as colis�es solidas dos Tiles
        /// </summary>
        private void CollisionInitialize()
        {           

            solidCollision = new TileShapeCollection();
            solidCollision.Visible = false;
            //  wallCollision.AddCollisionFrom(currentLevel, "Wall1");
            //solidCollision.AddCollisionFromTilesWithProperty(GlobalData.currentLevel.tileMap, "SolidCollision");
            
            solidCollision.SortAxis = FlatRedBall.Math.Axis.Y;
            solidCollision.AddMergedCollisionFromTilesWithProperty(GlobalData.currentLevel.tileMap, "SolidCollision");

        }


        void CustomActivity(bool firstTimeCalled)

        {         
            if(GlobalData.gameOver && !GlobalData.isGamePause)
            {
                GlobalData.isGamePause = true;     
            
                PauseThisScreen();
                //Console.WriteLine("Pause Game Screen");           
            }


            if (!GlobalData.gameOver)
            {
                btnPress();
            }
            Collision();
            RemovalActivity();
           

        }

        public void btnPress()
        {
            if (pause.WasJustPressed && !GlobalData.hudInstance.RakingVisible )
            {
                if (!GlobalData.isGamePause)
                {
                   
                    GlobalData.isGamePause = true;
                    PauseThisScreen();
                    Console.WriteLine("Pause");
                    GlobalData.hudInstance.loadPlayPoints();
                    
                }
                else
                {
                    
                    GlobalData.isGamePause = false;
                    UnpauseThisScreen();

                    GlobalData.hudInstance.UnloadPlayPoints();
                    //InstructionManager.UnpauseEngine();

                    // MoveToScreen(typeof(MainMenuScreen).FullName);
                    //  Console.WriteLine("UnPause");
                }

            }

            if (escape.WasJustPressed)
            {
                GlobalData.resetarVariaveis();
                LevelToLoad = GlobalData.currentLevel.name;
                MoveToScreen(typeof(MainMenuScreen).FullName);

            }

            if (ranking.WasJustPressed && !GlobalData.isGamePause)
            {
                if (GlobalData.hudInstance != null)
                {
                    bool pausar = GlobalData.hudInstance.loadRanking(false);
                    //GlobalData.hudInstance.Panel_DisplayInstance.Visible = true;
                   if (pausar)
                    {
                        PauseThisScreen();
                    }else
                    {
                        UnpauseThisScreen();
                    }
                }
                
            }
            if (blood.WasJustPressed)
            {
                GlobalData.hudInstance.LoadBlood();
            }

            if (controle.WasJustPressed)
            {
                if (GlobalData.hudInstance != null)
                {
                    bool pausar = GlobalData.hudInstance.loadControle();
                    //GlobalData.hudInstance.Panel_DisplayInstance.Visible = true;
                    if (pausar)
                    {
                        PauseThisScreen();
                    }
                    else
                    {
                        UnpauseThisScreen();
                    }
                }

            }


        }
  

        public void Collision()
        {
            //Console.WriteLine("WPL");
            CollisionWallVsPlayer();
            //Console.WriteLine("DPL");
            CollisionDoorVsPlayer();
            //Console.WriteLine("ENPL");
            CollisionEnemyVsPlayer();
           // Console.WriteLine("WEN");
            CollisionEnemyVsWall();
            //Console.WriteLine("ENEM");
            CollisionEnemyVsEnemy();
            //Console.WriteLine("DOORENY");
            CollisionDoorVsEnemy();
            ///Console.WriteLine("PLITEM");
            CollisionItemVsPlayer();
           /// Console.WriteLine("PJENEYM");
            CollisionProjectilesVsEnemy();
           // Console.WriteLine("PLAYATACK");
            CollisionPlayerVsAtack();
            //Console.WriteLine("PRJWALL");
            CollisionProjectilesVsWall();
            //Console.WriteLine("PROJECTSPLAY");
            CollisionProjectilesVsPlayer();
           // Console.WriteLine("ENEMYPATHWALL");
            collisionEnemyPathVSWall();
            // Console.WriteLine("FIM COLLISION");
            callsionPlayerVcNPC();

        }

        private void callsionPlayerVcNPC()
        {
            if (NpcList.Count > 0) {
                foreach (Player player in PlayerList)
                {
                    foreach(Npc npc in NpcList)
                    {
                        if(npc.collisionNpc(player)){
                            if (space.WasJustPressed)
                            {
                                GlobalData.hudInstance.LoadNpcTex(npc);
                                player.interacaoNPC = true;
                                }
                            }
                        }
                    }
                
            }
        }
        
        private void CollisionPlayerVsAtack()
        {
            foreach(Player player in PlayerList)
            {
                foreach(Enemy enemy in EnemyList)
                {
                    if (enemy.isAtackMelee)
                    {
                       // Console.WriteLine("Atack");
                        if (enemy.AtackHit(player))
                        {
                            player.ActionTakeHitMelee(enemy); 
                        }
                       
                    }
                }
            }
        }


        private void CollisionProjectilesVsWall()
        {
            foreach (Projectiles project in ProjectilesList)
            {
                //solidCollision.CollideAgainstSolid(project.CollisionInstance);
               // Console.WriteLine(project.CollisionWallInstance.Y + "," + project.CollisionWallInstance.X);

                if (solidCollision.CollideAgainst(project.CollisionWallInstance))
                {
                 //   Console.WriteLine("colide///\\");
                    project.Destroy();
                }

            }
        }

        private void CollisionProjectilesVsEnemy()
        {
            foreach (Projectiles project in ProjectilesList)
            {
                foreach (Enemy enemy in EnemyList) {
                    if (project.isPlayAtack)
                    {
                        if (project.CollideAgainst(enemy))
                        {
                            //Console.WriteLine("Acerta");                           
                            enemy.ActionTakeHit(project);
                            
                            
                            //takeHit()
                            //project.Destroy();
                        }
                    }
                }

            }

        }

        private void CollisionProjectilesVsPlayer()
        {
            foreach (Projectiles project in ProjectilesList)
            {
                foreach (Player play in PlayerList)
                {
                    //se � o ataque � do inimigo
                    if (!project.isPlayAtack)
                    {
                        if (project.CollideAgainst(play.CollisionHitBox))
                        {
                           // Console.WriteLine("Flecha atacque");
                            play.ActionTakeHit(project);
                            project.ishit = true;
                        }
                    }
                }

            }

        }

        public void CollisionItemVsPlayer()
        {
            foreach (Item item in ItemList)
            {
                if (GlobalData.PlayerData.playerInstance.CollideAgainst(item))
                {
                    GlobalData.PlayerData.playerInstance.takeItem(item);
                       
                }
            }
        }

        public void CollisionDoorVsEnemy()
        {
           /* foreach (Door door in DoorList)
            {

                GlobalData.PlayerData.playerInstance.CollideAgainstMove(door, 0, 1);
            }
            */


        }

        public void CollisionEnemyVsEnemy()
        {

            foreach (Enemy enemy in EnemyList)

            {
                foreach (Enemy enemyOther in EnemyList)

                {
                    if (enemy != enemyOther)
                    {
                        // float enemyMass = enemy.Mass;
                        //enemy.Collision.CollideAgainstMove(enemyOther.Collision, 0, enemyMass);
                        enemy.colission(enemyOther.CollisionInstance, 0, "ENEMY");



                    }
                }
            }
            }

        public void CollisionDoorVsPlayer()
        {
            foreach (Door door in DoorList)
            {
                if (GlobalData.PlayerData.playerInstance.CollisionInstance.CollideAgainst(door.CollisionInstance))
                {
                    
                    if (GlobalData.currentLevel.currentDoor.Index == door.Index)
                    {
                        if (door.isDoorOpen)
                        {
                            if (GlobalData.currentGameMode == GlobalData.GameMode.NORMAL)
                            {
                                // LevelToLoad = GlobalData.NextLevel();
                              //  Console.WriteLine("Screen: Next Room " );
                                LevelToLoad = GlobalData.NextRoom();
                                this.RestartScreen(reloadContent: false);
                            }
                            else
                            {
                                LevelToLoad = GlobalData.RandomLevel();
                            }
                            //Console.WriteLine("Level to load:" + GlobalData.currentLevel.currentDoor.Index + ":" + door.Index);

                            this.RestartScreen(reloadContent: false);
                        }else
                        {
                            GlobalData.PlayerData.playerInstance.CollisionInstance.CollideAgainstMove(door.CollisionInstance, 0, 1);
                        }

                    }else
                    {
                        // solidCollision.A

                        GlobalData.PlayerData.playerInstance.CollisionInstance.CollideAgainstMove(door.CollisionInstance, 0, 1);
                    }
                    
                    

                }
                
            }
        }

        public void CollisionWallVsPlayer()
        {

            solidCollision.CollideAgainstSolid(GlobalData.PlayerData.playerInstance.CollisionInstance);

        }
        /// <summary>
        /// colis�o do Play com o inimico, se o playstatus = DEFAULT a colis�o � againsdtoMove, se for INTANGIBLE, N�O � COLIS�O
        /// </summary>
        public void CollisionEnemyVsPlayer()
        {
            if (GlobalData.PlayerData.playerInstance.statusPlay == EnumPlay.INTANGIBLE)
            {
                //N�O COLIDE
              //  Console.WriteLine("N�o COlide");
                
            }
            else
            {
                foreach (Enemy enemy in EnemyList)

                {
                    enemy.colission(GlobalData.PlayerData.playerInstance.CollisionInstance,0, "PLAYER");
                    
                }
               // Console.WriteLine(" COlide");
            }
        }
        public void CollisionEnemyVsWall()
        {
            
            foreach(Enemy enemy in EnemyList)
            {
               solidCollision.CollideAgainstSolid(enemy.CollisionInstance);

            }
        }

        public void collisionEnemyPathVSWall()
        {
            foreach (Enemy enemy in EnemyList)
            {
                if (enemy.isAtackProjectiles && enemy.gotoLine != null)
                {
                    if (solidCollision.CollideAgainstSolid(enemy.gotoLine))
                    {
                        //  Console.WriteLine("Colide");
                        enemy.hasColisionGotoLine = true;
                    }
                    else
                    {
                        enemy.hasColisionGotoLine = false;
                        //Console.WriteLine("...");
                    }
                }

            }

        }

       

        public void RemovalActivity()
        {
            removalProjectiles();
        }
        public void removalProjectiles()
        {
            for (int i = ProjectilesList.Count - 1; i > -1; i--)
            {
                float absoluteX = Math.Abs(ProjectilesList[i].X);
                float absoluteY = Math.Abs(ProjectilesList[i].Y);

                const float removeBeyond = 900;
                if (absoluteX > removeBeyond || absoluteY > removeBeyond)
                {
                    ProjectilesList[i].Destroy();
                }


            }

        }

        void CustomDestroy()
		{
            solidCollision.RemoveFromManagers();
            solidCollision.Visible = false;
            

            
           // Console.WriteLine("Destruidor");
            if (GlobalData.currentLevel != null)
            {
                if (GlobalData.currentLevel.tileNodeNetwork != null)
                {
                   // Console.WriteLine("Destruindo TileNodeNetWork");
                    GlobalData.currentLevel.tileNodeNetwork.Visible = false;
                    GlobalData.currentLevel.tileMap.Destroy();
                }
            }

            //GumInstance.Destroy();
           
        }

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

	}
}
